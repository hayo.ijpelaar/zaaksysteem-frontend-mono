// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createBrowserHistory } from 'history';
import { createStore } from 'redux-dynamic-modules-core';
import { getThunkExtension } from 'redux-dynamic-modules-thunk';
import getErrorModule from '@zaaksysteem/common/src/store/error/error.module';
import getRouterModule from './store/route/route.module';
import getUIModule from './store/ui/ui.module';
import { DIALOG_ERROR } from './constants/dialog.constants';

export const history = createBrowserHistory();

export const configureStore = () => {
  const store = createStore(
    { initialState: {}, extensions: [getThunkExtension()] },
    getRouterModule(history) as any,
    getUIModule(),
    getErrorModule({
      errorDialogType: DIALOG_ERROR,
      handleAuthenticationError() {},
    })
  );

  return store;
};
