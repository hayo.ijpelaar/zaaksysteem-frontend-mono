// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { number } from './number';

const DECIMAL = 1.75;
const INT = 42;

/**
 * @test {number}
 */
describe('The `number` function', () => {
  test('accepts an empty string', () => {
    expect(number('')).toBe(true);
  });

  test('does not accept a string value', () => {
    expect(number('string')).toBe(false);
  });

  test('accepts a decimal value when no config is provided ', () => {
    expect(number(DECIMAL)).toBe(true);
  });

  test('accepts an integer when no config is provided ', () => {
    expect(number(INT)).toBe(true);
  });

  test('accepts an integer when the config specifies the integer type ', () => {
    expect(
      number(INT, {
        type: 'integer',
      })
    ).toBe(true);
  });

  test('does not accept a decimal when the config specifies the integer type ', () => {
    expect(
      number(DECIMAL, {
        type: 'integer',
      })
    ).toBe(false);
  });
});
