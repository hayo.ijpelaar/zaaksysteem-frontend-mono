// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const AJAX_STATE_INIT = 'init';
export const AJAX_STATE_PENDING = 'pending';
export const AJAX_STATE_VALID = 'valid';
export const AJAX_STATE_ERROR = 'error';

/**
 * Create Ajax constants object
 * @param {string} name
 * @returns {{PENDING: string, SUCCESS: string, ERROR: string}}
 */
export const createAjaxConstants = name => ({
  PENDING: `${name}:PENDING`,
  SUCCESS: `${name}:SUCCESS`,
  ERROR: `${name}:ERROR`,
});
