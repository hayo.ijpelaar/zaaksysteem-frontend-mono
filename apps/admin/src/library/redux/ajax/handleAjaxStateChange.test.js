// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { handleAjaxStateChange } from './handleAjaxStateChange';
import {
  createAjaxConstants,
  AJAX_STATE_ERROR,
  AJAX_STATE_PENDING,
  AJAX_STATE_VALID,
} from './createAjaxConstants';

describe('The `handleAjaxStateChange` reducer', () => {
  const constants = createAjaxConstants('TEST');
  const tests = [
    [constants.SUCCESS, AJAX_STATE_VALID],
    [constants.PENDING, AJAX_STATE_PENDING],
    [constants.ERROR, AJAX_STATE_ERROR],
  ];
  const reducer = handleAjaxStateChange(constants);

  tests.map(([actionType, expectedState]) => {
    test(`should set state to ${expectedState} on action with type ${actionType}`, () => {
      const result = reducer(
        {},
        {
          type: actionType,
        }
      );
      expect(result).toEqual({
        state: expectedState,
      });
    });
  });
});
