// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray } from '@mintlab/kitchen-sink/source';

/**
 * @param {Array} queue
 *   The callbacks to be run.
 * @param {*} parameters
 *   The parameters to run each callback with.
 */
export function runQueue(queue, parameters) {
  for (const callback of queue) {
    callback(...asArray(parameters));
  }
}
