// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export {
  showDialog,
  hideDialog,
} from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';

export {
  showSnackbar,
  hideSnackbar,
} from '@zaaksysteem/common/src/store/ui/snackbar/snackbar.actions';

import {
  UI_DRAWER_OPEN,
  UI_DRAWER_CLOSE,
  UI_BANNER_SHOW,
  UI_BANNER_HIDE,
  UI_OVERLAY_OPEN,
  UI_OVERLAY_CLOSE,
  UI_WINDOW_LOAD,
  UI_WINDOW_UNLOAD,
} from './ui.constants';

const createAction = type => payload => ({
  type,
  payload,
});

export const openDrawer = createAction(UI_DRAWER_OPEN);
export const closeDrawer = createAction(UI_DRAWER_CLOSE);
export const showBanner = createAction(UI_BANNER_SHOW);
export const hideBanner = createAction(UI_BANNER_HIDE);
export const openOverlay = createAction(UI_OVERLAY_OPEN);
export const closeOverlay = createAction(UI_OVERLAY_CLOSE);
export const loadWindow = createAction(UI_WINDOW_LOAD);
export const unloadWindow = createAction(UI_WINDOW_UNLOAD);
