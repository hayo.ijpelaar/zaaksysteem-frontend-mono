// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  APP_BOOTSTRAP_INIT,
  APP_BOOTSTRAP_VALID,
  APP_BOOTSTRAP_ERROR,
  APP_SECTION_CHANGE,
} from './app.constants';

export const init = payload => ({
  type: APP_BOOTSTRAP_INIT,
  payload,
});

export const valid = payload => ({
  type: APP_BOOTSTRAP_VALID,
  payload,
});

export const error = payload => ({
  type: APP_BOOTSTRAP_ERROR,
  payload,
});

export const sectionChange = section => ({
  type: APP_SECTION_CHANGE,
  payload: {
    section,
  },
});
