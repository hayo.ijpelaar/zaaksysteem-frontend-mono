// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { logout } from '@zaaksysteem/common/src/store/session/session.actions';
import { openDrawer, closeDrawer } from '../../store/ui/ui.actions';
import { getUserNavigation } from '../../library/auth';
import { navigation } from '../../modules/catalog/fixtures/navigation';
import { invoke } from '../../store/route/route.actions';
import Layout from './Layout';

const mapStateToProps = ({
  ui: { drawer, banners },
  route,
  session: {
    data: {
      logged_in_user: { capabilities, initials, surname },
      account: {
        instance: { company },
      },
    },
  },
}) => ({
  isDrawerOpen: drawer,
  requestUrl: route,
  userName: `${initials} ${surname}`,
  userNavigation: getUserNavigation(navigation, capabilities),
  customer: company,
  banners,
});

const mapDispatchToProps = dispatch => ({
  logout: payload => dispatch(logout(payload)),
  closeDrawer: payload => dispatch(closeDrawer(payload)),
  openDrawer: payload => dispatch(openDrawer(payload)),
  route: path => dispatch(invoke(path)),
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    toggleDrawer() {
      if (stateProps.isDrawerOpen) {
        dispatchProps.closeDrawer();
      } else {
        dispatchProps.openDrawer();
      }
    },
  };
}

const connectWithTranslation = withTranslation();
const connectWithStore = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
);

/**
 * The Layout Container Component is connected
 * with the store and the translations.
 *
 * @type {Function}
 */
const LayoutContainer = connectWithTranslation(connectWithStore(Layout));

export default LayoutContainer;
