// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { login } from '@zaaksysteem/common/src/store/session/session.actions';
import { invoke } from '../../store/route/route.actions';
import { navigation } from '../../modules/catalog/fixtures/navigation';
import Login from './Login';

const mapStateToProps = ({
  app: { bootstrap },
  session: { data: { logged_in_user } = {} },
}) => {
  return {
    capabilities: logged_in_user ? logged_in_user.capabilities : null,
    bootstrap,
  };
};

const mapDispatchToProps = dispatch => ({
  route(path) {
    dispatch(invoke(path));
  },
  login: payload => dispatch(login(payload)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  navigation,
});

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Login);

export default LoginContainer;
