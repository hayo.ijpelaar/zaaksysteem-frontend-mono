// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const SECTION_CATALOG = 'catalog';
export const SECTION_USERS = 'users';
export const SECTION_LOG = 'log';
export const SECTION_TRANSACTIONS = 'transactions';
export const SECTION_CONNECTORS = 'connectors';
export const SECTION_DATA_WAREHOUSE = 'data_warehouse';
export const SECTION_SYSTEM_CONFIG = 'system_config';
