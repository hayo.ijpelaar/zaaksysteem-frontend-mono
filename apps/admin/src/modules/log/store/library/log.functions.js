// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getSegments, objectifyParams } from '@mintlab/kitchen-sink/source';

/**
 * @param {String} path
 * @return {Boolean}
 */
export const pathIsInLog = path => path.indexOf('/admin/logboek') > -1;

/**
 * @param {String} path
 * @return {Boolean}
 */
export const pathContainsPage = path => getSegments(path).length > 2;

/**
 * @param {String} path
 * @return {Boolean}
 */
export const isCompleteLogPath = path =>
  pathIsInLog(path) && pathContainsPage(path);

/**
 * @param {String} path
 * @return {Boolean}
 */
export const shouldRedirect = path =>
  pathIsInLog(path) && !pathContainsPage(path);

/**
 * @param {String} path
 * @return {Boolean}
 */
export const shouldFetchEvents = path => isCompleteLogPath(path);

/**
 * @param {Object} state
 * @return {Boolean}
 */
export const shouldFetchUserLabel = state => {
  const { uuid, label } = state.log.filters.user;

  return uuid !== '' && label === '';
};

/**
 * @param {String} path
 * @returns {Object}
 */
export const getFiltersFromPath = path => {
  const [, queryString] = path.split('?');
  const {
    keyword = '',
    caseNumber = '',
    user = '',
  } = objectifyParams(queryString);

  return {
    keyword,
    caseNumber,
    user: {
      uuid: user,
      label: '',
    },
  };
};

/**
 * @param {Object} state
 * @param {String} path
 * @returns {Boolean}
 */
export const urlFiltersMatchState = (state, path) => {
  const { keyword, caseNumber, user } = getFiltersFromPath(path);
  const { filters } = state.log;

  return (
    keyword === filters.keyword &&
    caseNumber === filters.caseNumber &&
    user.uuid === filters.user.uuid
  );
};

/**
 * @param {String} path
 * @returns {Object}
 */
export const getPageAndRowsFromPath = path => {
  const [, , page, rowsPerPage] = getSegments(path);

  return {
    page: parseInt(page, 10),
    rowsPerPage: parseInt(rowsPerPage, 10),
  };
};

/**
 * @param {Object} options
 * @param {Number} options.page
 * @param {Number} options.rowsPerPage
 * @param {Number} options.caseNumber
 * @param {String} options.keyword
 * @param {Object} options.user
 * @return {Object}
 */
export const getEventsQueryParams = ({
  page,
  rowsPerPage,
  caseNumber,
  keyword,
  user,
}) => ({
  page,
  rows_per_page: rowsPerPage,
  'query:match:case_id': caseNumber,
  'query:match:keyword': keyword,
  'query:match:subject': user.uuid,
});
