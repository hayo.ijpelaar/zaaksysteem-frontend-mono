// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const EVENTS_FETCH = createAjaxConstants('LOG:EVENTS_FETCH');
export const EVENTS_SET_ROWS_PER_PAGE = 'LOG:EVENTS_SET_ROWS_PER_PAGE';
export const EVENTS_SET_PAGE = 'LOG:EVENTS_SET_PAGE';
export const EVENTS_EXPORT = createAjaxConstants('LOG:EVENTS_EXPORT');
