// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl, get } from '@mintlab/kitchen-sink/source';
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_FETCH_DETAILS,
  CATALOG_TOGGLE_DETAIL_VIEW,
} from './details.constants';

/* eslint complexity: [2, 17] */
export const fetchCatalogItemSuccess = (state, action) => {
  const getUsedInCaseTypeRelationships = () => {
    const caseTypes = get(relationships, '.used_in_case_types', []).filter(
      thisCaseType => get(thisCaseType, '.attributes.is_current_version')
    );

    if (!isPopulatedArray(caseTypes)) return;
    return caseTypes.map(thisCaseType => thisCaseType.attributes.name);
  };

  const { response } = action.payload;
  const { id, type, links, attributes, relationships } = response.data;
  const usedInCaseTypeRelationships = getUsedInCaseTypeRelationships();
  const internalUrlValue = buildUrl(`/intern/aanvragen/${id}/`, {
    contactkanaal: 'behandelaar',
  });

  const hasAttribute = attribute =>
    Object.prototype.hasOwnProperty.call(attributes, attribute);

  const folderName =
    type === 'folder'
      ? get(attributes, '.parent_name')
      : get(relationships, 'folder.attributes.name');

  const folderId =
    type === 'folder'
      ? get(attributes, '.parent_id')
      : get(relationships, 'folder.id');

  const detailsObj = {
    ...(hasAttribute('last_modified') && {
      lastModified: {
        value: attributes.last_modified,
      },
    }),
    locationFolder: {
      title: folderName,
      value: folderId,
    },
    ...(hasAttribute('magic_string') && {
      magicString: {
        value: attributes.magic_string,
      },
    }),
    ...(hasAttribute('value_type') && {
      valueType: {
        value: attributes.value_type,
      },
    }),
    ...(hasAttribute('identification') && {
      identification: {
        value: attributes.identification,
      },
    }),
    ...(usedInCaseTypeRelationships && {
      used_in_case_types: {
        value: usedInCaseTypeRelationships,
      },
    }),
    ...(type === 'document_template' && {
      enclosedDocument: {
        value: attributes.filename,
        ...(attributes.has_default_integration && {
          link: links.download,
        }),
      },
    }),
    ...((type === 'case_type' || type === 'object_type') && {
      uuid: {
        value: id,
      },
    }),
    ...(type === 'custom_object_type' && {
      versionUuid: {
        value: id,
      },
      uuid: {
        value: attributes.version_independent_uuid,
      },
      status: {
        value: attributes.status,
      },
    }),
    ...(type === 'case_type' && {
      caseTypeApi: {
        value: `/api/v1/casetype/${id}`,
      },
      internalUrl: {
        value: internalUrlValue,
        context: attributes.context || '',
      },
    }),
  };

  const data = {
    id,
    name: attributes.name,
    type,
    url: attributes.url,
    version: attributes.current_version || attributes.version,
    details: detailsObj,
  };

  return {
    ...state,
    data,
  };
};

export const initialState = {
  state: AJAX_STATE_INIT,
  data: {},
  showDetailView: false,
};

export function details(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_FETCH_DETAILS);

  switch (action.type) {
    case CATALOG_FETCH_DETAILS.PENDING:
    case CATALOG_FETCH_DETAILS.ERROR:
      return handleAjaxState(state, action);

    case CATALOG_FETCH_DETAILS.SUCCESS:
      return fetchCatalogItemSuccess(handleAjaxState(state, action), action);

    case CATALOG_TOGGLE_DETAIL_VIEW:
      return {
        ...state,
        showDetailView: !state.showDetailView,
      };

    default:
      return state;
  }
}

export default details;
