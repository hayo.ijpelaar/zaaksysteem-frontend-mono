// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_CASE_TYPE_VERSIONS_INIT =
  'CATALOG:CASE_TYPE_VERSIONS:INIT';
export const CATALOG_CASE_TYPE_VERSIONS_FETCH = createAjaxConstants(
  'CATALOG:CASE_TYPE_VERSIONS:FETCH'
);
export const CATALOG_CASE_TYPE_VERSIONS_ACTIVATE_INIT =
  'CATALOG:CASE_TYPE_VERSIONS_ACTIVATE:INIT';
export const CATALOG_CASE_TYPE_VERSIONS_ACTIVATE = createAjaxConstants(
  'CATALOG:CASE_TYPE_VERSIONS_ACTIVATE'
);
