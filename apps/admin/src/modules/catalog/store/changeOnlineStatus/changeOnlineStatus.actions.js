// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  CATALOG_INIT_CHANGE_ONLINE_STATUS,
  CATALOG_CHANGE_ONLINE_STATUS,
} from './changeOnlineStatus.constants';

const changeOnlineStatusAjaxAction = createAjaxAction(
  CATALOG_CHANGE_ONLINE_STATUS
);

export const initChangeOnlineStatus = payload => ({
  type: CATALOG_INIT_CHANGE_ONLINE_STATUS,
  payload,
});

export const changeOnlineStatus = payload => {
  const {
    id,
    active,
    values: { reason },
  } = payload;

  return changeOnlineStatusAjaxAction({
    url: '/api/v2/admin/catalog/change_case_type_online_status',
    method: 'POST',
    data: {
      case_type_uuid: id,
      active,
      reason,
    },
    payload,
  });
};
