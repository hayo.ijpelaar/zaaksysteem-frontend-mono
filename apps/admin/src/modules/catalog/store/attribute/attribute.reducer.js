// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { get } from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_ATTRIBUTE_INIT,
  CATALOG_ATTRIBUTE_FETCH,
  CATALOG_ATTRIBUTE_SAVE,
} from './attribute.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
  savingState: AJAX_STATE_INIT,
  id: null,
};

const handleInitAttribute = (state, action) => {
  const id = get(action, 'payload.options.id', null);
  return {
    ...initialState,
    id,
  };
};

/**
 * Process incoming data from backend
 *
 * @param {Object} state
 * @param {Object} action
 * @return {*}
 */
const handleFetchSuccess = (state, action) => ({
  ...state,
  values: get(action, 'payload.response.data.attributes'),
});

/* eslint complexity: [2, 12] */
export function attribute(state = initialState, action) {
  const handleFetchAjaxState = handleAjaxStateChange(CATALOG_ATTRIBUTE_FETCH);
  const handleSaveAjaxState = handleAjaxStateChange(
    CATALOG_ATTRIBUTE_SAVE,
    'savingState'
  );

  switch (action.type) {
    case CATALOG_ATTRIBUTE_INIT:
      return handleInitAttribute(state, action);
    case CATALOG_ATTRIBUTE_FETCH.SUCCESS:
      return handleFetchSuccess(handleFetchAjaxState(state, action), action);
    case CATALOG_ATTRIBUTE_FETCH.PENDING:
    case CATALOG_ATTRIBUTE_FETCH.ERROR:
      return handleFetchAjaxState(state, action);
    case CATALOG_ATTRIBUTE_SAVE.PENDING:
    case CATALOG_ATTRIBUTE_SAVE.ERROR:
    case CATALOG_ATTRIBUTE_SAVE.SUCCESS:
      return handleSaveAjaxState(state, action);
    default:
      return state;
  }
}

export default attribute;
