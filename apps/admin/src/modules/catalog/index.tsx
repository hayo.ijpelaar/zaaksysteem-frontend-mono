// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getCatalogModule } from './store/catalog.module';
import CatalogContainer from './components/CatalogContainer';

type CatalogRouteType = {
  segments: [string];
};

const iframed = () => {
  try {
    return window.self !== window.top;
  } catch (err: any) {
    return true;
  }
};

const CatalogModule: React.ComponentType<CatalogRouteType> = ({ segments }) => {
  const [id] = segments;
  iframed() && window.top?.postMessage('IFRAME:RELOAD', '*');

  return (
    <DynamicModuleLoader modules={[getCatalogModule(id)]}>
      {/* @ts-ignore */}
      <CatalogContainer />
    </DynamicModuleLoader>
  );
};

export default CatalogModule;
