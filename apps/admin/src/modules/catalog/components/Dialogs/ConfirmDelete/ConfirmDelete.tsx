// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
import { formDefinition } from '../../../fixtures/confirmDelete';

const ConfirmDelete: React.FunctionComponent<any> = ({
  onConfirm,
  t,
  hide,
}) => {
  const handleOnSubmit = (values: FormikValues) => onConfirm(values.reason);

  return (
    <FormDialog
      formDefinition={translateFormDefinition(formDefinition, t)}
      onSubmit={handleOnSubmit}
      title={t('catalog:delete:confirm:title')}
      scope="catalog-delete-dialog"
      icon="error_outline"
      onClose={hide}
      saveLabel={t('catalog:delete:confirm:ok')}
    />
  );
};

export default ConfirmDelete;
