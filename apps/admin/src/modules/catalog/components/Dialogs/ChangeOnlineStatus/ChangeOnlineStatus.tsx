// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';

const ChangeOnlineStatus: React.FunctionComponent<any> = ({
  changeOnlineStatusAction,
  formDefinition,
  t,
  hide,
  active,
  saving,
}) => {
  const handleOnSubmit = (values: FormikValues) =>
    changeOnlineStatusAction({ values });

  const type = active
    ? t('catalog:changeOnlineStatus.offline')
    : t('catalog:changeOnlineStatus.online');
  const title = t('catalog:changeOnlineStatus.title', { type });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="catalog:changeOnlineStatus"
      icon="extension"
      onClose={hide}
      saving={saving}
    />
  );
};

export default ChangeOnlineStatus;
