// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { default as ConfirmDelete } from './ConfirmDelete/ConfirmDeleteContainer';
export { default as ChangeOnlineStatus } from './ChangeOnlineStatus/ChangeOnlineStatusContainer';
export { default as Attribute } from './Attribute/Attribute';
export { default as EmailTemplate } from './EmailTemplate/EmailTemplateContainer';
export { default as Folder } from './Folder/FolderContainer';
export { default as AddElement } from './AddElement/AddElementContainer';
export { default as CaseTypeVersions } from './CaseTypeVersions/CaseTypeVersionsContainer';
export { default as CaseTypeVersionsActivate } from './CaseTypeVersionsActivate/CaseTypeVersionsActivateContainer';
export { default as DocumentTemplate } from './DocumentTemplate/DocumentTemplateContainer';
