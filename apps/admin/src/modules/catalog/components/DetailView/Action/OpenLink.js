// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Button } from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { withStyles } from '@material-ui/styles';
import { actionStylesheet } from './Action.style';

/**
 * @reactProps {Object} classes
 * @reactProps {string} link
 * @reactProps {Object} t
 * @reactProps {string} icon=['open_in_new']
 * @reactProps {string} title
 * @return {ReactElement}
 */
const OpenLink = ({ classes, icon = 'open_in_new', link, title }) => (
  <div>
    <Tooltip title={title}>
      <Button
        component="a"
        classes={{
          root: classes.valueActionButton,
        }}
        presets={['icon', 'extraSmall']}
        href={link}
        target="_blank"
      >
        {icon}
      </Button>
    </Tooltip>
  </div>
);

export default withStyles(actionStylesheet)(OpenLink);
