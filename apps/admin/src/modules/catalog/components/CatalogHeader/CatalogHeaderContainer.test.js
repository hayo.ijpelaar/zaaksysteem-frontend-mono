// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getBreadcrumbs } from './CatalogHeaderContainer';

describe('The `CatalogHeaderContainer` module', () => {
  describe('The `getBreadcrumbs` function', () => {
    test('filters `null` values', () => {
      const breadcrumbs = {
        parent: null,
        current: {
          name: 'Test',
          id: '1',
        },
      };

      expect(getBreadcrumbs(breadcrumbs)).toHaveLength(1);
    });
  });
});
