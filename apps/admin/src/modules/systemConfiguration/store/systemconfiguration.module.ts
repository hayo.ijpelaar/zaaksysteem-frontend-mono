// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { systemConfiguration } from './systemconfiguration.reducer';
import { systemConfigurationMiddleware } from './systemconfiguration.middleware';
import { fetchSystemConfiguration } from './systemconfiguration.actions';

export const getSystemConfigurationModule = () => ({
  id: 'systemConfiguration',
  middlewares: [systemConfigurationMiddleware as any],
  initialActions: [fetchSystemConfiguration() as any],
  reducerMap: {
    systemConfiguration,
  },
});

export default getSystemConfigurationModule;
