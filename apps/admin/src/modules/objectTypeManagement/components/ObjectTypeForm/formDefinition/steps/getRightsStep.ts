// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getRightsStep(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.rights.title'),
    fields: [
      {
        format: 'object',
        name: 'authorizations',
        type: 'AuthorizationList',
        value: [],
        required: true,
      },
    ],
  };
}
