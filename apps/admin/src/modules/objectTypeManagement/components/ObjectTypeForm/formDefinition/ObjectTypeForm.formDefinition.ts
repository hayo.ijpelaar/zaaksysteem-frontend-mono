// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../ObjectTypeForm.types';
import { getGeneralStep } from './steps/getGeneralStep';
import { getAttributesStep } from './steps/getAttributesStep';
import { getRelationsStep } from './steps/getRelationsStep';
import { getRightsStep } from './steps/getRightsStep';
import { getAuditStep } from './steps/getAuditStep';

export const getObjectTypeFormDefinition = ({
  t,
}: {
  t: i18next.TFunction;
}): FormDefinition<ObjectTypeFormShapeType> => [
  getGeneralStep(t),
  getAttributesStep(t),
  getRelationsStep(t),
  getRightsStep(t),
  getAuditStep(t),
];
