// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormStepType } from '@zaaksysteem/common/src/components/form/hooks/useFormSteps';
import { H3, Subtitle1 } from '@mintlab/ui/App/Material/Typography';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { useObjectTypeFormStepStyles } from './ObjectTypeFormStep.style';

export type ObjectTypeFormStepPropsType = {
  step: FormStepType;
  fields: FormRendererFormField[];
};

export const renderFormField = (field: FormRendererFormField) => {
  const { FieldComponent, label, applyBackgroundColor, ...rest } = field;
  const { error, touched, required, help, hint } = rest;

  return (
    <FormControlWrapper
      label={label}
      help={help}
      hint={hint}
      error={error}
      touched={touched}
      required={required}
      applyBackgroundColor={applyBackgroundColor}
    >
      <FieldComponent {...rest} />
    </FormControlWrapper>
  );
};

export const ObjectTypeFormStep: React.ComponentType<
  ObjectTypeFormStepPropsType
> = ({ step, fields }) => {
  const { title, description } = step;
  const classes = useObjectTypeFormStepStyles();

  return (
    <React.Fragment>
      <div className={classes.stepIntro}>
        <H3>{title}</H3>
        {description && (
          <Subtitle1 classes={{ root: classes.stepIntroSubTitle }}>
            {description}
          </Subtitle1>
        )}
      </div>
      <div>
        {fields.map(field => (
          <div key={field.name} className={classes.fieldWrapper}>
            {renderFormField(field)}
          </div>
        ))}
      </div>
    </React.Fragment>
  );
};
