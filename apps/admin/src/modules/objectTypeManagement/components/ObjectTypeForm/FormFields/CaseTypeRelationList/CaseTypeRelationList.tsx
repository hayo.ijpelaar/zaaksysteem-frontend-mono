// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Select, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import Button from '@mintlab/ui/App/Material/Button';
import Icon from '@mintlab/ui/App/Material/Icon';
import {
  SortableList,
  useListStyle,
  listSelectStyleSheet,
} from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICatalog } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

type CaseType = {
  id: string;
  name: string;
};

const extractSelectedValue = (event: any) => event.target.value.value;

const fetchCaseTypeRelationChoices: (
  keyword: string
) => Promise<ValueType<CaseType>[]> = async keyword => {
  const response = await request<APICatalog.SearchCatalogResponseBody>(
    'GET',
    buildUrl<APICatalog.SearchCatalogRequestParams>(
      '/api/v2/admin/catalog/search',
      { keyword, 'filter[type]': 'case_type' }
    )
  );

  return response.data.map(({ attributes: { name }, id }) => {
    return {
      value: {
        id: id || '',
        name,
      },
      label: name,
    };
  });
};

export const CaseTypeRelationList: React.ComponentType<
  FormRendererFormField<any, any, CaseType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const [input, setInput] = React.useState('');
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, CaseType>(props);
  const value = fields.map(field => field.value);

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <div className={classes.itemContainerSimple}>
            {field?.value?.name}
            <Button
              className={classes.removeButton}
              presets={['icon', 'small']}
              action={field.remove}
            >
              close
            </Button>
          </div>
        )}
      />
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchCaseTypeRelationChoices}
        providerArguments={[input]}
      >
        {({ data, busy }) => {
          return (
            <Select
              name={name}
              generic={true}
              loading={busy}
              onChange={event => add(extractSelectedValue(event))}
              startAdornment={<Icon size="small">{'search' as const}</Icon>}
              isMulti={false}
              value={null}
              isClearable={false}
              choices={
                input.length > 2 && data
                  ? data.filter(option =>
                      value.every(item => item.id !== option.value.id)
                    )
                  : []
              }
              getChoices={setInput}
              createStyleSheet={listSelectStyleSheet}
              placeholder={placeholder}
            />
          );
        }}
      </DataProvider>
    </div>
  );
};
