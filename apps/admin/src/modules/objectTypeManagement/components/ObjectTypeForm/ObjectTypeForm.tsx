// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import {
  ATTRIBUTE_LIST,
  OBJECTTYPE_RELATION_LIST,
  CASETYPE_RELATION_LIST,
} from '../../../../components/Form/Constants/fieldTypes';
import { AttributeList } from './FormFields/AttributeList/AttributeList';
import { AuthorizationList } from './FormFields/AuthorizationList/AuthorizationList';
import { CaseTypeRelationList } from './FormFields/CaseTypeRelationList/CaseTypeRelationList';
import { ObjectTypeRelationList } from './FormFields/ObjectTypeRelationList/ObjectTypeRelationList';
import { getObjectTypeFormDefinition } from './formDefinition/ObjectTypeForm.formDefinition';
import { useObjectTypeFormStyles } from './ObjectTypeForm.style';
import ObjectTypeStepControls from './ObjectTypeStepControls/ObjectTypeStepControls';
import { ObjectTypeFormShapeType } from './ObjectTypeForm.types';
import { ObjectTypeProgressBar } from './ObjectTypeProgressBar/ObjectTypeProgressBar';
import { ObjectTypeFormStep } from './ObjectTypeFormStep/ObjectTypeFormStep';

export type ObjectTypeFormPropsType = {
  initialValues?: Partial<ObjectTypeFormShapeType>;
  isInitialValid?: boolean;
  busy?: boolean;
  onChange?: (values: ObjectTypeFormShapeType) => {};
  onSubmit: (values: ObjectTypeFormShapeType) => void;
};

export const ObjectTypeForm: React.ComponentType<ObjectTypeFormPropsType> = ({
  onSubmit,
  onChange,
  busy = false,
  isInitialValid = false,
  initialValues = {},
}) => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useObjectTypeFormStyles();
  const formDefinition = getObjectTypeFormDefinition({ t });
  const formDefinitionWithValues = mapValuesToFormDefinition(
    initialValues,
    formDefinition
  );
  const {
    fields,
    activeStep,
    activeStepValid,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    formik: { submitForm },
  } = useForm<ObjectTypeFormShapeType>({
    formDefinition: formDefinitionWithValues,
    isInitialValid,
    onSubmit,
    onChange,
    fieldComponents: {
      [ATTRIBUTE_LIST]: AttributeList,
      [CASETYPE_RELATION_LIST]: CaseTypeRelationList,
      [OBJECTTYPE_RELATION_LIST]: ObjectTypeRelationList,
      AuthorizationList,
    },
  });

  return (
    <div className={classes.wrapper}>
      <div className={classes.formWrapper}>
        <div className={classes.innerWrapper}>
          <div>
            <ObjectTypeProgressBar steps={steps} />
          </div>

          <ObjectTypeFormStep step={activeStep} fields={fields} />
        </div>
      </div>

      <div className={classes.buttonWrapper}>
        <div className={classes.innerWrapper}>
          <ObjectTypeStepControls
            activeStepValid={activeStepValid}
            handleNextStep={handleNextStep}
            handlePreviousStep={handlePreviousStep}
            hasNextStep={hasNextStep}
            hasPreviousStep={hasPreviousStep}
            submitForm={submitForm}
            busy={busy}
          />
        </div>
      </div>
    </div>
  );
};

export default ObjectTypeForm;
