// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ObjectTypeForm from '../ObjectTypeForm/ObjectTypeForm';
import { objectTypeSelector } from '../../store/selectors/objectTypeSelector';
import { ObjectTypeFormShapeType } from '../ObjectTypeForm/ObjectTypeForm.types';
import { updateObjectTypeAction } from '../../store/update/update.actions';
import { busyUpdatingSelector } from '../../store/selectors/busyUpdatingSelector';
import { unsavedChangesSelector } from '../../store/selectors/unsavedChangesSelector';
import { setObjectTypeUnsavedChangesAction } from '../../store/unsavedChanges/unsavedChanges.actions';
import { createIntitialValuesFromObjectType } from '../../library';
import { fetchObjectTypeAction } from './../../store/objectType/objectType.actions';
import { useObjectTypeEditStyle } from './ObjectTypeEdit.style';

export type ObjectTypeEditPropsType = {
  uuid: string;
};

const ObjectTypeEdit: React.ComponentType<ObjectTypeEditPropsType> = ({
  uuid,
}) => {
  const classes = useObjectTypeEditStyle();
  const { state, objectType } = useSelector(objectTypeSelector);
  const unsavedChanges = useSelector(unsavedChangesSelector);
  const busyUpdating = useSelector(busyUpdatingSelector);
  const dispatch = useDispatch();

  const handleSubmit = (values: ObjectTypeFormShapeType) =>
    dispatch(
      updateObjectTypeAction({
        ...values,
        uuid: objectType?.uuid || '',
      })
    );

  const handleChange = () =>
    !unsavedChanges && dispatch(setObjectTypeUnsavedChangesAction(true));

  useEffect(() => {
    dispatch(fetchObjectTypeAction(uuid));
  }, []);

  if (state === 'pending' || !objectType) {
    return <Loader delay={200} />;
  }

  const initialValues = createIntitialValuesFromObjectType(objectType);

  return (
    <div className={classes.wrapper}>
      <ObjectTypeForm
        busy={busyUpdating}
        onChange={handleChange}
        onSubmit={handleSubmit}
        initialValues={initialValues}
      />
    </div>
  );
};

export default ObjectTypeEdit;
