// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export interface ObjectTypeManagementRouteType {
  segments: ['create' | 'edit', string];
  params: { folder_uuid?: string };
}
