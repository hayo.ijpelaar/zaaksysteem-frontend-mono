// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SET_OBJECT_TYPE_UNSAVED_CHANGES } from './unsavedChanges.constants';
import { SetObjectTypeUnsavedChangesActionPayloadType } from './unsavedChanges.actions';

export type UnsavedChangesStateType = boolean;

const initialState: UnsavedChangesStateType = false;

export const unsavedChanges: Reducer<UnsavedChangesStateType> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case SET_OBJECT_TYPE_UNSAVED_CHANGES:
      return (
        action as ActionWithPayload<SetObjectTypeUnsavedChangesActionPayloadType>
      ).payload;

    default:
      return state;
  }
};

export default unsavedChanges;
