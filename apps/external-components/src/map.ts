// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity */

import * as Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw';
import 'leaflet-draw/dist/leaflet.draw-src.css';
import 'proj4leaflet';
import 'leaflet-fullscreen';
import 'leaflet-fullscreen/dist/leaflet.fullscreen.css';
import {
  MapInitType,
  ExternalMapEventType,
  Geojson,
  ExternalMapMessageType,
  ZaaksysteemFeatureProp,
} from '@mintlab/ui/types/MapIntegration';

let map: Leaflet.Map;
let initData: MapInitType;
let name: string;
let layers: Leaflet.TileLayer[];
let layerControl: Leaflet.Control.Layers;
let drawControl: any;
let mainFeatureGroup = Leaflet.featureGroup();
let marker = Leaflet.marker([0, 0]);
const popups: any = { '': null };

const version = 5;

//@ts-ignore
const RD = new Leaflet.Proj.CRS(
  'EPSG:28992',
  '+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +units=m +towgs84=565.2369,50.0087,465.658,-0.406857330322398,0.350732676542563,-1.8703473836068,4.0812 +no_defs',
  {
    transformation: new Leaflet.Transformation(-1, -1, 0, 0),
    resolutions: [
      3440.64, 1720.32, 860.16, 430.08, 215.04, 107.52, 53.76, 26.88, 13.44,
      6.72, 3.36, 1.68, 0.84, 0.42,
    ],
    origin: [-285401.92, 903401.92],
    bounds: Leaflet.bounds([-285401.92, 903401.92], [595401.92, 22598.08]),
  }
);

const resetLayerControl = () => {
  layerControl && layerControl.remove();
  layerControl = Leaflet.control
    .layers(
      {},
      layers.reduce(
        (acc, layer, ix) => ({
          [initData.wmsLayers[ix].label]: layer,
          ...acc,
        }),
        { markers: mainFeatureGroup }
      )
    )
    .addTo(map);
};

/* eslint complexity: [2, 8] */
const renderPopup = (zs: ZaaksysteemFeatureProp) => {
  const container = document.createElement('div');
  const titleEl = document.createElement('a');
  titleEl.target = '_parent';
  titleEl.style.textDecoration = 'none';
  titleEl.style.fontSize = '14px';

  let rowsData: (
    | { title: string; text: string | number; linkto?: any }
    | 'break'
  )[];

  if (zs.origin.family === 'case' && zs.self.family === 'case') {
    titleEl.href = '/intern/zaak/' + zs.origin.caseNumber;
    titleEl.innerText = zs.self.title;
    rowsData = [
      {
        title: 'Nummer',
        text: zs.self.caseNumber,
      },
      {
        title: 'Zaaktype',
        text: zs.self.type,
      },
      {
        title: 'Adres',
        text: zs.self.address,
      },
      {
        title: 'Status',
        text: zs.self.status,
      },
      {
        title: 'Aanvrager',
        text: zs.self.requestor,
      },
      {
        title: 'Behandelaar',
        text: zs.self.assignee,
      },
    ];
  } else if (zs.origin.family === 'object' && zs.self.family === 'case') {
    titleEl.href = '/main/object/' + zs.origin.identifier;
    titleEl.innerText = zs.self.title;
    rowsData = [
      {
        title: 'Uuid',
        text: zs.origin.identifier,
      },
      {
        title: 'Objecttype',
        text: zs.origin.type,
      },
      'break',
      {
        title: 'Zaak',
        linkto: zs.self.parentLink,
        text: zs.self.caseNumber,
      },
      {
        title: 'Zaaktype',
        text: zs.self.type,
      },
      {
        title: 'Aanvrager',
        text: zs.self.requestor,
      },
      {
        title: 'Behandelaar',
        text: zs.self.assignee,
      },
      {
        title: 'Status',
        text: zs.self.status,
      },
    ];
  } else if (zs.origin.family === 'object' && zs.self.family === 'object') {
    titleEl.href = '/main/object/' + zs.origin.identifier;
    titleEl.innerText = zs.self.title;
    rowsData = [
      {
        title: 'Uuid',
        text: zs.origin.identifier,
      },
      {
        title: 'Objecttype',
        text: zs.origin.type,
      },
      {
        title: 'Status',
        text: zs.self.status,
      },
    ];
  } else if (
    zs.origin.family === 'organization' ||
    zs.origin.family === 'person'
  ) {
    titleEl.innerText = zs.origin.title;
    rowsData = [
      {
        title: 'Adres',
        text: zs.origin.location_description,
      },
      {
        title: 'Status',
        text: zs.origin.status,
      },
    ];
  } else {
    throw new Error('Unrecognized object family, cannot render marker popup');
  }

  /* eslint complexity: [2, 8] */
  const rows = rowsData.map((rowData, ix) => {
    const row = document.createElement('div');
    const rowTitle = document.createElement('span');
    const rowText = document.createElement('span');

    if (rowData === 'break') {
      row.style.paddingBottom = '20px';
    } else if (rowData?.linkto) {
      rowText.style.cursor = 'default';
      rowText.style.color = '#0078A8';
      rowTitle.innerText = rowData.title + ': ';
      rowTitle.style.fontWeight = 'bold';
      rowText.innerText = rowData.text.toString();

      row.appendChild(rowTitle);
      row.appendChild(rowText);

      rowText.addEventListener('click', () => {
        const pps = popups[rowData.linkto];
        rowData.linkto && map.openPopup(pps[0], [pps[1][1], pps[1][0]]);
      });
    } else if (rowData.text) {
      rowTitle.innerText = rowData.title + ': ';
      rowTitle.style.fontWeight = 'bold';
      rowText.innerText = rowData.text.toString();

      row.appendChild(rowTitle);
      row.appendChild(rowText);
    }

    if (ix === 0) {
      row.style.paddingTop = '15px';
    }

    return row;
  });

  container.appendChild(titleEl);
  rows.forEach(row => container.appendChild(row));

  return container;
};

const sendMessage = (message: ExternalMapMessageType) => {
  window.top?.postMessage(message, window.location.origin);
};

const addFeatureSetterListener = () => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'setFeature') {
      const feature = event.data.value;
      drawGeoJson(feature, { append: false, focus: false });
      resetLayerControl();
    }
  });
};

const addMarkerSetterListener = () => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'setMarker' && event.data.value) {
      const latlng: [number, number] = [
        event.data.value.coordinates[1],
        event.data.value.coordinates[0],
      ];
      marker.setLatLng(latlng);
      marker.addTo(map);
      map.flyTo(latlng, map.getZoom() > 8 ? undefined : 8);
    } else if (event.data.type === 'setMarker') {
      marker.removeFrom(map);
    }
  });
};

const addDrawControlSetterListener = () => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'setDrawToolbar') {
      if (event.data.value && drawControl === undefined) {
        addDrawPlugins();
      } else if (event.data.value && drawControl) {
        map.addControl(drawControl);
      } else if (!event.data.value && drawControl) {
        map.removeControl(drawControl);
      }
    }
  });
};

const loadZsConfig = (done: (value?: unknown) => void) => {
  window.addEventListener('message', (event: ExternalMapEventType) => {
    if (event.data.type === 'init') {
      initData = event.data.value;
      name = event.data.name;
      done();

      if (window.top?.location.host.includes('development')) {
        console.log(event.data);
      }
    }
  });
};

const postOnFeatureChange = (geojson: Geojson) => {
  sendMessage({
    name,
    version,
    value: geojson,
    type: 'featureChange',
  });
};

const postOnClick = ({ lat, lng }: { lat: number; lng: number }) => {
  sendMessage({
    name,
    version,
    value: { type: 'Point', coordinates: [lng, lat] },
    type: 'click',
  });
};

const createMap = () => {
  const mainLayer = new Leaflet.TileLayer(
    'https://geodata.nationaalgeoregister.nl/tiles/service/wmts/opentopoachtergrondkaart/EPSG:28992/{z}/{x}/{y}.png',
    {
      maxZoom: 13,
      minZoom: 3,
      tileSize: 256,
      zoomOffset: 0,
    }
  );

  map = Leaflet.map('zs-app', {
    crs: RD,
    layers: [mainLayer],
    center: [52.268, 4.998],
    zoom: 5,
  });

  layers = initData.wmsLayers.map(layerData => {
    return Leaflet.tileLayer.wms(layerData.url, {
      minZoom: 3,
      maxZoom: 13,
      layers: layerData.layers,
      format: 'image/png',
      transparent: true,
    });
  });

  layers.forEach(layer => layer.addTo(map).bringToFront());
  mainFeatureGroup.addTo(map);
};

const addFullscreenPlugin = () => {
  //@ts-ignore
  map.addControl(new Leaflet.Control.Fullscreen());
};

const addDrawPlugins = () => {
  //@ts-ignore
  drawControl = new Leaflet.Control.Draw({
    edit: {
      featureGroup: mainFeatureGroup,
      poly: {
        allowIntersection: false,
      },
    },
    draw: {
      polygon: {
        allowIntersection: false,
        showArea: true,
      },
      circle: false,
      circlemarker: false,
    },
  });

  map.addControl(drawControl);

  const currentGeojson = () => {
    return mainFeatureGroup.toGeoJSON();
  };

  map.on('draw:created', event => {
    const layer = event.layer;
    mainFeatureGroup.addLayer(layer);
    postOnFeatureChange(currentGeojson());
  });

  map.on('draw:edited', event => {
    postOnFeatureChange(currentGeojson());
  });

  map.on('draw:deleted', event => {
    postOnFeatureChange(currentGeojson());
  });
};

const drawGeoJson = (
  data: GeoJSON.GeoJsonObject | null,
  options = {
    append: true,
    focus: false,
  }
) => {
  const style = {
    color: '#3388FF',
    weight: 5,
    opacity: 0.65,
  };

  let geoFeatures: GeoJSON.GeoJsonObject[];

  if (!data) {
    geoFeatures = [];
  } else if (data.type === 'FeatureCollection') {
    geoFeatures = (data as GeoJSON.FeatureCollection).features.map(feature => ({
      ...feature,
      type: 'Feature',
    }));
  } else {
    geoFeatures = [data];
  }

  !options.append && mainFeatureGroup.clearLayers();
  const features = geoFeatures.map(geoFeature => {
    return Leaflet.geoJSON(geoFeature, {
      style,
      pointToLayer: (feature, latlng) => {
        const marker = Leaflet.marker(latlng);

        return marker;
      },
      onEachFeature: (feature, layer) => {
        if (feature.properties.zaaksysteem) {
          if (feature.properties.zaaksysteem.self === undefined) {
            feature.properties.zaaksysteem.self =
              feature.properties.zaaksysteem.origin;
          }
          if (feature.properties.zaaksysteem.self.objectType) {
            const cn = feature.properties.zaaksysteem.origin.caseNumber;
            const parentFeature = geoFeatures.find(featureLookup => {
              return (
                //@ts-ignore
                cn === featureLookup.properties.zaaksysteem.self?.caseNumber
              );
            });
            parentFeature &&
              (feature.properties.zaaksysteem.self.parentLink = cn);
          }
          const pp = renderPopup(feature.properties.zaaksysteem);
          if (feature.properties.zaaksysteem.self.caseNumber) {
            popups[
              feature.properties.zaaksysteem.self.caseNumber.toString()
              //@ts-ignore
            ] = [pp, feature?.geometry?.coordinates];
          }
          layer.bindPopup(pp);
        }
      },
    })
      .addTo(mainFeatureGroup)
      .bringToFront();
  });

  if (options.focus && features.length) {
    const lastFeature = features[features.length - 1];
    map.flyToBounds(lastFeature.getBounds(), { maxZoom: 6 });
  }
};

new Promise(loadZsConfig).then(() => {
  createMap();
  initData.canDrawFeatures && addDrawPlugins();
  map.on('click', event => postOnClick((event as any).latlng));
  initData.initialFeature &&
    drawGeoJson(initData.initialFeature, { append: false, focus: true });
  initData.canSelectLayers && resetLayerControl();
  addFeatureSetterListener();
  addMarkerSetterListener();
  addFullscreenPlugin();
  addDrawControlSetterListener();
});
