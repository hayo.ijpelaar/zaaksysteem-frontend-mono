// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import createUIReducer from '@zaaksysteem/common/src/store/ui/ui.reducer';

export const getUIModule = () => ({
  id: 'ui',
  reducerMap: {
    ui: createUIReducer(),
  },
});

export default getUIModule;
