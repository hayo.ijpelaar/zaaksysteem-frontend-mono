// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getDashboardModule } from './store/dashboard.module';
import Dashboard from './components/Dashboard';
import { DashboardPropsType } from './components/Dashboard';

const DashboardModule: React.ComponentType<DashboardPropsType> = ({
  match,
}) => {
  return (
    <DynamicModuleLoader modules={[getDashboardModule()]}>
      <Dashboard match={match} />
    </DynamicModuleLoader>
  );
};

export default DashboardModule;
