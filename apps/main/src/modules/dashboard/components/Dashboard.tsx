// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Tasks from './widgets/Tasks/Tasks';
import { useDashboardStyles } from './Dashboard.style';

export type DashboardPropsType = {
  match: { path: string };
};

const Dashboard: React.FunctionComponent<DashboardPropsType> = ({ match }) => {
  const classes = useDashboardStyles();

  return (
    <div className={classes.wrapper}>
      <Switch>
        <Route
          path={`${match.path}/widgets/tasks/:widgetUuid`}
          component={Tasks}
        />
      </Switch>
    </div>
  );
};

export default Dashboard;
