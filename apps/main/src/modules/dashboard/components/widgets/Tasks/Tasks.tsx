// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { useTranslation } from 'react-i18next';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import useInfiniteScroll from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { InfiniteLoader } from 'react-virtualized';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Widget from '../Widget';
import { updateWidgetPostMessage } from '../Widget.library';
import { useDashboardSortableTableStyles } from '../../Dashboard.style';
import TasksHeader from './TasksHeader';
import { useTasksStyles } from './Tasks.style';
import locale from './Tasks.locale';

import {
  getAllColumns,
  getWidgetData,
  getFilteredColumns,
  getData,
} from './Tasks.library';
import {
  TasksPropsType,
  FilterType,
  CasesRowType,
  TasksRowType,
  GetDataParamsType,
} from './types/Tasks.types';

const PAGE_LENGTH = 20;
const THRESHOLD = 5;
const REMOTE_ROW_COUNT = 99999;

const Tasks: React.FunctionComponent<TasksPropsType> = ({ match }) => {
  const [searchTerm, setSearchTerm] = useState<any>(null);
  const [sortBy, setSortBy] = useState<any>(null);
  const [sortDirection, setSortDirection] = useState<any>(null);
  const [filters, setFilters] = useState<FilterType[] | null>(null);
  const [columns, setColumns] = useState<ItemValueType[]>([]);
  const [initialLoading, setInitialLoading] = useState<boolean>(true);

  const infiniteLoaderRef = useRef<InfiniteLoader>(null);
  const didMountRef = useRef(false);

  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [t] = useTranslation();
  const classes = useTasksStyles();
  const tableStyles = useSortableTableStyles();
  const dashboardStyles = useDashboardSortableTableStyles();

  const widgetUuid = match.params.widgetUuid;
  const allColumns = getAllColumns({ classes, t });

  const { list, isRowLoaded, loadMoreRows, resetList } = useInfiniteScroll<
    TasksRowType,
    GetDataParamsType
  >({
    ref: infiniteLoaderRef,
    pageLength: PAGE_LENGTH,
    getData,
    getDataParams: {
      pageLength: PAGE_LENGTH,
      searchTerm,
      filters,
      sortBy,
      sortDirection,
      openServerErrorDialog,
    },
  });

  const userUuid = useSelector(
    (state: SessionRootStateType) => state.session.data?.logged_in_user.uuid
  );

  const userDisplayName = useSelector(
    (state: SessionRootStateType) =>
      state.session.data?.logged_in_user.display_name
  );

  const locationHref = (id: number) => {
    if (window.top) {
      window.top.location.href = `/intern/zaak/${id}`;
    }
  };

  const handleSearch = (ev: string) => {
    return setSearchTerm(ev);
  };

  useEffect(() => {
    (async function () {
      const { filters, columns } = await getWidgetData(widgetUuid);
      setFilters(
        filters && filters.length
          ? filters
          : [
              {
                label: userDisplayName,
                value: userUuid || '',
                data: { type: 'assignee' },
              },
            ]
      );

      setColumns(
        columns && columns.length
          ? columns
          : allColumns.map(col => ({
              id: col.name,
              value: String(col.label),
              active: true,
              isNew: false,
            }))
      );

      setInitialLoading(false);
    })();
  }, []);

  useEffect(() => {
    if (!filters || !columns) return;

    if (didMountRef.current) {
      updateWidgetPostMessage({ widgetUuid, filters, columns });
    } else {
      didMountRef.current = true;
    }
  }, [filters, columns]);

  useEffect(() => {
    if (!initialLoading) loadMoreRows({ startIndex: 1 });
  }, [initialLoading]);

  useEffect(() => {
    if (filters === null && searchTerm === null) return;
    resetList();
  }, [filters, searchTerm, sortBy, sortDirection]);

  const filteredColumns = getFilteredColumns({ allColumns, columns });

  if (initialLoading) return <Loader />;

  return (
    <Widget
      header={
        <TasksHeader
          title={t('tasks:title', {
            counter: list.length,
          })}
          widgetUuid={widgetUuid}
          onChange={handleSearch}
          filters={filters}
          setFilters={setFilters}
          columns={columns}
          setColumns={setColumns}
        />
      }
    >
      <React.Fragment>
        {ServerErrorDialog}
        <div className={classes.tableWrapper}>
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={REMOTE_ROW_COUNT}
            threshold={THRESHOLD}
            ref={infiniteLoaderRef}
          >
            {({ onRowsRendered, registerChild }) => (
              <SortableTable
                rows={list}
                //@ts-ignore
                columns={filteredColumns}
                noRowsMessage={
                  filters && filters.length
                    ? t('tasks:noRowsFilters')
                    : t('tasks:no_rows')
                }
                loading={false}
                rowHeight={53}
                onRowClick={(
                  { rowData }: { rowData: CasesRowType },
                  event: React.MouseEvent
                ) => {
                  locationHref(rowData.display_number);
                  event.preventDefault();
                }}
                onRowDoubleClick={() => {}}
                styles={{ ...tableStyles, ...dashboardStyles }}
                onRowsRendered={onRowsRendered}
                externalRef={registerChild}
                sorting="column"
                sortInternal={false}
                onSort={(sortBy: any, sortDirection: any) => {
                  setSortBy(sortBy);
                  setSortDirection(sortDirection);
                }}
              />
            )}
          </InfiniteLoader>
        </div>
      </React.Fragment>
    </Widget>
  );
};

const TasksModule: React.FunctionComponent<
  RouteComponentProps<{
    widgetUuid: string;
  }>
> = ({ match }) => (
  <I18nResourceBundle resource={locale} namespace="tasks">
    <Tasks match={match} />
  </I18nResourceBundle>
);

export default TasksModule;
