// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import locale from './Case.locale';
import Case from './Case';
import { provideForCase } from './Case.library';

const CaseModule: React.FunctionComponent<{
  rootPath: string;
  caseUuid: string;
}> = ({ rootPath, caseUuid }) => (
  <I18nResourceBundle resource={locale} namespace="case">
    <DataProvider
      provider={provideForCase}
      providerArguments={[caseUuid]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy) {
          return <Loader />;
        }

        return (
          data && (
            <Case
              caseObj={data.caseObj}
              caseType={data.caseType}
              rootPath={rootPath}
            />
          )
        );
      }}
    </DataProvider>
  </I18nResourceBundle>
);

export default CaseModule;
