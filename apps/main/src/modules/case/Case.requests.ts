// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import {
  CaseV2Type,
  CaseV1Type,
  CaseTypeV2Type,
  CaseTypeV1Type,
  SubjectV2Type,
} from './Case.types';

export const fetchCase = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCaseRequestParams>(
    '/api/v2/cm/case/get_case',
    { case_uuid: uuid }
  );

  const response = await request<CaseV2Type>('GET', url);

  return response;
};

export const fetchCaseV1 = async (uuid: string) => {
  const url = `/api/v1/case/${uuid}`;

  const response = await request<CaseV1Type>('GET', url);

  return response.result.instance;
};

export const fetchCaseType = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCaseTypeVersionRequestParams>(
    '/api/v2/cm/case_type/get_case_type_version',
    { version_uuid: uuid }
  );

  const response = await request<CaseTypeV2Type>('GET', url);

  return response;
};

export const fetchCaseTypeV1 = async (uuid: string, version: number) => {
  const url = buildUrl<{ version: number }>(`/api/v1/casetype/${uuid}`, {
    version,
  });

  const response = await request<CaseTypeV1Type>('GET', url);

  return response.result.instance;
};

export const fetchSubject = async (type: SubjectTypeType, uuid: string) => {
  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact',
    { type, uuid }
  );

  const response = await request<SubjectV2Type>('GET', url);

  return response;
};
