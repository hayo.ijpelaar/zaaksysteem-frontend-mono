// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { CaseObjType } from '../../Case.types';
import { useNavigationStyles } from './Navigation.styles';

const getSelectedItem = (rootPath: string): string => {
  const url = window.location.toString();
  const path = url.replace(rootPath, '');
  const view = path.split('/')[3];

  return view;
};

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);

const Navigation: React.ComponentType<{
  rootPath: string;
  caseObj: CaseObjType;
}> = ({ rootPath, caseObj }) => {
  const classes = useNavigationStyles();
  const [t] = useTranslation('case');
  const selectedItem = getSelectedItem(rootPath);

  const menuItems: SideMenuItemType[] = [
    {
      id: 'phases',
      label: t('views.phases'),
      href: `${rootPath}/phases`,
      icon: iconNames.done_all,
      component: MenuItemLink,
    },
    {
      id: 'documents',
      label: t('views.documents'),
      href: `${rootPath}/documents`,
      icon: iconNames.insert_drive_file,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('views.timeline'),
      href: `${rootPath}/timeline`,
      icon: iconNames.schedule,
      component: MenuItemLink,
    },
    {
      id: 'communication',
      label: t('views.communication'),
      href: `${rootPath}/communication`,
      icon: iconNames.chat_bubble,
      component: MenuItemLink,
    },
    ...(caseObj.location
      ? [
          {
            id: 'location',
            label: t('views.location'),
            href: `${rootPath}/location`,
            icon: iconNames.map,
            component: MenuItemLink,
          },
        ]
      : []),
    {
      id: 'relations',
      label: t('views.relations'),
      href: `${rootPath}/relations`,
      icon: iconNames.link,
      component: MenuItemLink,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: selectedItem === menuItem.id,
    icon: <Icon size="small">{menuItem.icon}</Icon>,
  }));

  return (
    <div className={classes.wrapper}>
      <SideMenu items={menuItems} />
    </div>
  );
};

export default Navigation;
