// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useAboutDialogStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    minWidth: '500px',
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
    overflowY: 'scroll',
    fontFamily: typography.fontFamily,
  },
  itemWrapper: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
  },
  label: {
    fontWeight: typography.fontWeightBold,
  },
  value: {},
}));
