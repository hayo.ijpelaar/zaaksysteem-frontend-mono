// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { capitalize } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, DialogChangeType } from '../../Case.types';
import { useInfoStyles } from './Info.styles';
import {
  Result,
  Status,
  Requestor,
  Recipient,
  Assignee,
  Department,
  RegistrationDate,
  TargetDate,
  CompletionDate,
  Location,
  Confidentiality,
  PaymentStatus,
} from './Icons';
import { getSubjectUrl, formatDate } from './Info.library';

export interface InfoPropsType {
  caseObj: CaseObjType;
  dialogChange: DialogChangeType;
}

type ItemType = {
  id: string;
  tooltip?: string;
  icon: React.ReactElement;
  item?: string | React.ReactElement;
};

type LinkComponentPropsType = {
  url: string;
  children: string;
};

const LinkComponent = React.forwardRef<
  HTMLAnchorElement,
  LinkComponentPropsType
>((props, ref) => <Link innerRef={ref} to={props.url} {...props} />);
LinkComponent.displayName = 'InfoLink';

/* eslint complexity: [2, 18] */
const Info: React.ComponentType<InfoPropsType> = ({
  caseObj,
  dialogChange,
}) => {
  const classes = useInfoStyles();
  const [t] = useTranslation('case');
  const {
    result,
    status,
    requestor,
    presetClient,
    recipient,
    assignee,
    department,
    registrationDate,
    targetDate,
    completionDate,
    location,
    confidentiality,
    payment,
  } = caseObj;

  const items: ItemType[] = [
    ...(result
      ? [
          {
            id: 'result',
            icon: <Result />,
            item: capitalize(result),
          },
        ]
      : []),
    {
      id: 'status',
      icon: <Status status={status} />,
      item: t(`status.${status}`),
    },
    // a case always has a requestor, but not according to the API docs
    ...(requestor
      ? [
          {
            id: 'requestor',
            icon: (
              <Requestor type={requestor.type} presetClient={presetClient} />
            ),
            tooltip: presetClient
              ? t('info.presetClient')
              : t(`info.requestor`),
            item: (
              <div className={classes.urlItem}>
                <LinkComponent url={getSubjectUrl(requestor)}>
                  {requestor.name || '-'}
                </LinkComponent>
                <Icon size="tiny">{iconNames.open_in_new}</Icon>
              </div>
            ),
          },
        ]
      : []),
    ...(recipient
      ? [
          {
            id: 'recipient',
            icon: <Recipient type={recipient.type} />,
            item: (
              <div className={classes.urlItem}>
                <LinkComponent url={getSubjectUrl(recipient)}>
                  {recipient.name || '-'}
                </LinkComponent>
                <Icon size="tiny">{iconNames.open_in_new}</Icon>
              </div>
            ),
          },
        ]
      : []),
    ...(assignee
      ? [
          {
            id: 'assignee',
            icon: <Assignee />,
            item: (
              <div className={classes.urlItem}>
                <LinkComponent url={getSubjectUrl(assignee)}>
                  {assignee.name || '-'}
                </LinkComponent>
                <Icon size="tiny">{iconNames.open_in_new}</Icon>
              </div>
            ),
          },
        ]
      : []),
    ...(!assignee
      ? [
          {
            id: 'assignToSelf',
            icon: <Assignee />,
            item: (
              <Button
                className={classes.assignToSelfButton}
                action={() => {}}
                presets={['outlined']}
              >
                {t('info.assignToSelf')}
              </Button>
            ),
          },
        ]
      : []),
    {
      id: 'department',
      icon: <Department />,
      item: department,
    },
    {
      id: 'registrationDate',
      icon: <RegistrationDate />,
      item: formatDate(registrationDate),
    },
    {
      id: 'targetDate',
      icon: <TargetDate />,
      item: (
        <div
          className={
            new Date() > new Date(targetDate || '') ? classes.warning : ''
          }
        >
          {formatDate(targetDate)}
        </div>
      ),
    },
    ...(completionDate
      ? [
          {
            id: 'completionDate',
            icon: <CompletionDate />,
            item: formatDate(completionDate),
          },
        ]
      : []),
    ...(location
      ? [
          {
            id: 'location',
            icon: <Location />,
            item: location,
          },
        ]
      : []),
    {
      id: 'confidentiality',
      icon: <Confidentiality confidentiality={confidentiality} />,
      item: t(`confidentiality.${confidentiality || ''}`),
    },
    ...(payment.status
      ? [
          {
            id: 'paymentStatus',
            tooltip:
              t('info.paymentStatus') + payment.status
                ? t(`paymentStatus.${payment.status}`)
                : '',
            icon: <PaymentStatus t={t} />,
            item: payment.amount || '-',
          },
        ]
      : []),
  ];

  return (
    <div className={classes.wrapper}>
      {items.map(item => {
        const tooltip = item.tooltip || t(`info.${item.id}`) || '';

        return (
          <Tooltip key={item.id} title={tooltip} placement="right">
            <div className={classes.item}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              {item.item || '-'}
            </div>
          </Tooltip>
        );
      })}
      <Button
        className={classes.about}
        action={() => dialogChange('about', true)}
        presets={['contained', 'primary']}
      >
        {t('info.about')}
      </Button>
    </div>
  );
};

export default Info;
