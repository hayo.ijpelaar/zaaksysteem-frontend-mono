// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useInfoStyles = makeStyles(
  ({ palette: { error, thundercloud } }: Theme) => ({
    wrapper: {
      padding: '8px 0',
    },
    item: {
      padding: '8px 16px',
      display: 'flex',
      alignItems: 'center',
    },
    assignToSelfButton: {
      height: 30,
    },
    urlItem: {
      color: thundercloud.main,
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    warning: {
      color: error.main,
      fontWeight: 'bold',
    },
    about: {
      width: 'calc(100% - 16px - 16px)',
      margin: '8px 16px',
    },
  })
);
