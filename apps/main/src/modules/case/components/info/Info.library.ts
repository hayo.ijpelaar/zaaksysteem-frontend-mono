// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { LeadTimeType, SubjectType } from './../../Case.types';

export const getSubjectUrl = (subject: SubjectType) =>
  `/main/contact-view/${subject.type}/${subject.uuid}`;

export const formatDate = (date: string | null): string | undefined => {
  if (!date) return;

  return fecha.format(new Date(date), 'DD-MM-YYYY');
};

export const formatLeadTime = ({ value, type }: LeadTimeType): string =>
  `${value} ${type}`;
