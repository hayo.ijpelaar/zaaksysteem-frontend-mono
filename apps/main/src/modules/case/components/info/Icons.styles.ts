// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useIconsStyles = makeStyles(({ palette: { sahara } }: Theme) => ({
  result: {},
  status: {},
  requestor: {},
  recipient: {},
  assignee: {
    color: sahara.main,
  },
}));
