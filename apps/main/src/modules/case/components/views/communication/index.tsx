// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import { CaseObjType, CaseTypeType } from '../../../Case.types';

export type CommunicationPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  rootPath: string;
};

const Communication: React.ComponentType<CommunicationPropsType> = ({
  caseObj,
  caseType,
  rootPath,
}) => {
  const caseOpen = caseObj.caseOpen;
  const canCreatePipMessage =
    caseObj.requestor?.type !== 'employee' &&
    caseType.settings.disable_pip_for_requestor !== true;

  return (
    <CommunicationModule
      capabilities={{
        allowSplitScreen: true,
        canAddAttachmentToCase: caseOpen,
        canAddSourceFileToCase: caseOpen,
        canAddThreadToCase: false,
        canCreateContactMoment: true,
        canCreatePipMessage: caseOpen && canCreatePipMessage,
        canCreateEmail:
          process.env.WEBPACK_BUILD_TARGET !== 'production' && caseOpen,
        canCreateNote: true,
        canCreateMijnOverheid: false,
        canDeleteMessage: caseOpen,
        canImportMessage: caseOpen,
        canSelectCase: false,
        canSelectContact: true,
        canFilter: true,
        canOpenPDFPreview: true,
      }}
      context="case"
      caseUuid={caseObj.uuid}
      contactUuid={caseObj.requestor?.uuid}
      contactName={caseObj.requestor?.name}
      htmlEmailTemplateName={caseObj.htmlEmailTemplateName}
      rootPath={rootPath}
    />
  );
};

export default Communication;
