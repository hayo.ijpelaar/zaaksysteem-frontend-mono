// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RelationsPropsType } from './Relations.types';
import { useRelationsStyles } from './Relations.style';
import CaseTables from './Tables/Cases/CaseTables';
import PlannedCasesTable from './Tables/PlannedCases/PlannedCasesTable';
import SubjectsTable from './Tables/Subjects/SubjectsTable';
import ObjectsTable from './Tables/Objects/ObjectsTable';
import CustomObjectsTable from './Tables/CustomObjects/CustomObjectsTable';
import PlannedEmailsTable from './Tables/PlannedEmails/PlannedEmailsTable';

const Relations: React.ComponentType<RelationsPropsType> = ({
  session,
  caseObj,
  caseType,
}) => {
  const classes = useRelationsStyles();
  const {
    configurable: { show_object_v1, show_object_v2 },
    active_interfaces,
  } = session;
  const showPlannedEmails = active_interfaces.includes('email');
  const { uuid, number, caseOpen } = caseObj;

  return (
    <div className={classes.wrapper}>
      <CaseTables caseObj={caseObj} caseType={caseType} />
      <PlannedCasesTable
        caseUuid={uuid}
        caseNumber={number}
        caseOpen={caseOpen}
      />
      <SubjectsTable caseUuid={caseObj.uuid} caseOpen={caseOpen} />
      {show_object_v1 && (
        <ObjectsTable
          caseUuid={caseObj.uuid}
          caseNumber={caseObj.number}
          caseOpen={caseOpen}
          caseType={caseType}
        />
      )}
      {show_object_v2 && (
        <CustomObjectsTable
          caseUuid={caseObj.uuid}
          caseOpen={caseObj.caseOpen}
          caseType={caseType}
        />
      )}
      {showPlannedEmails && (
        <PlannedEmailsTable
          caseNumber={caseObj.number}
          caseOpen={caseObj.caseOpen}
        />
      )}
    </div>
  );
};

export default Relations;
