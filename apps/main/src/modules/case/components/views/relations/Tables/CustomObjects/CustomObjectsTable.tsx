// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { H2 } from '@mintlab/ui/App/Material/Typography';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import {
  CustomObjectType,
  CustomObjectsTablePropsType,
} from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import { relateCustomObject, unrelateCustomObject } from '../requests';
import { getCustomObjects } from './library';

const CustomObjectsTable: React.ComponentType<CustomObjectsTablePropsType> = ({
  caseUuid,
  caseOpen,
  caseType,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [saving, setSaving] = useState(false);
  const [customObjects, setCustomObjects] = useState<CustomObjectType[]>([]);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  useEffect(() => {
    getCustomObjects(caseUuid, setCustomObjects, caseType);
  }, []);

  const rows = customObjects;
  const columns = [
    {
      name: 'type',
      width: 1,
      flexGrow: 1,
    },
    {
      name: 'name',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CustomObjectType }) => (
        <a target="_parent" href={`/main/object/${rowData.uuid}`}>
          {rowData.name}
        </a>
      ),
    },
    {
      name: 'customFieldName',
      width: 1,
      flexGrow: 1,
    },
    {
      name: 'delete',
      width: 70,
      minWidth: 70,
      // eslint-disable-next-line
      cellRenderer: ({ rowData: { uuid } }: { rowData: CustomObjectType }) => (
        <Button
          presets={['icon', 'extraSmall']}
          disabled={saving}
          action={async () => {
            setSaving(true);

            // currently the result is always undefined
            // when fixed we should only refresh when successful

            // const result = await unrelateCustomObject([caseUuid], uuid);
            await unrelateCustomObject([caseUuid], uuid);

            setSaving(false);

            // if (result) {
            getCustomObjects(caseUuid, setCustomObjects, caseType);
            // }
          }}
        >
          close
        </Button>
      ),
    },
  ].map(row => ({
    ...row,
    label: t(`customObjects.columns.${row.name}`),
  }));

  const formDefinition: FormDefinition<{ object_uuid: string }> = [
    {
      name: 'object_uuid',
      type: fieldTypes.OBJECT_FINDER,
      value: null,
      required: true,
      placeholder: t('relatedCases.new.placeholder'),
      label: t('relatedCases.new.label'),
    },
  ];

  const { fields } = useForm({
    formDefinition: formDefinition,
  });

  return (
    <div className={classes.section}>
      {ServerErrorDialog}
      <H2 classes={{ root: classes.header }}>{t('customObjects.title')}</H2>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      {caseOpen && (
        <div className={classes.actionFooter}>
          {fields.map(({ FieldComponent, name, ...rest }) => {
            const props = cloneWithout(rest, 'definition', 'mode');

            return (
              <FormControlWrapper {...props} key={name}>
                <FieldComponent
                  {...props}
                  name={name}
                  key={name}
                  onBlur={() => {}}
                  onChange={async ({ target: { value } }) => {
                    if (!value) {
                      return;
                    }

                    setSaving(true);

                    const result = await relateCustomObject(
                      [caseUuid],
                      value.value
                    ).catch(openServerErrorDialog);

                    setSaving(false);

                    if (result) {
                      getCustomObjects(caseUuid, setCustomObjects, caseType);
                    }
                  }}
                />
              </FormControlWrapper>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default CustomObjectsTable;
