// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { H2 } from '@mintlab/ui/App/Material/Typography';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { CaseRelationType } from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import { getColumns } from './library';

type FamilyTablePropsType = {
  cases: CaseRelationType[];
};

const FamilyTable: React.ComponentType<FamilyTablePropsType> = ({ cases }) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();

  const rows = cases;
  const columns = getColumns(t, [
    {
      name: 'level',
      width: 50,
      minWidth: 50,
    },
  ]);

  return (
    <div className={classes.section}>
      <H2 classes={{ root: classes.header }}>{t('familyCases.title')}</H2>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
    </div>
  );
};

export default FamilyTable;
