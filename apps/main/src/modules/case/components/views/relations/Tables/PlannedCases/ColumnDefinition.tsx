// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { PlannedCaseType } from '../../Relations.types';
import { isOnce } from './library';

type GetColumnsType = (
  t: i18next.TFunction,
  caseOpen: boolean,
  saving: boolean,
  startEdit: (plannedCase: PlannedCaseType) => void,
  doDelete: (relationUuid: string) => void
) => any;

type RowDataType = {
  rowData: PlannedCaseType;
};

export const getColumns: GetColumnsType = (
  t,
  caseOpen,
  saving,
  startEdit,
  remove
) =>
  [
    {
      name: 'casetype_title',
      width: 1,
      flexGrow: 1,
    },
    {
      name: 'pattern',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => {
        if (isOnce(rowData.interval_period)) {
          return t('plannedCases.values.pattern.once');
        }

        const each = t('plannedCases.values.pattern.each');
        const period = t(
          `plannedCases.values.interval_period.${rowData.interval_period}`
        ).toLocaleLowerCase();

        return `${each} ${rowData.interval_value} ${period}`;
      },
    },
    {
      name: 'reach',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => {
        const startAt = t('plannedCases.values.reach.startAt');
        const repeats = t('plannedCases.values.reach.repeats');
        const date = fecha.format(new Date(rowData.next_run), 'DD-MM-YYYY');

        return isOnce(rowData.interval_period)
          ? date
          : `${startAt} ${date} (${rowData.runs_left} ${repeats})`;
      },
    },
    {
      name: 'edit',
      width: 70,
      minWidth: 70,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => (
        <Button
          presets={['icon', 'extraSmall']}
          disabled={saving}
          action={() => startEdit(rowData)}
        >
          edit
        </Button>
      ),
    },
    {
      name: 'delete',
      width: 70,
      minWidth: 70,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => (
        <Button
          presets={['icon', 'extraSmall']}
          disabled={saving}
          action={() => remove(rowData.uuid)}
        >
          close
        </Button>
      ),
    },
  ].map(row => ({
    ...row,
    label: t(`plannedCases.columns.${row.name}`),
  }));
