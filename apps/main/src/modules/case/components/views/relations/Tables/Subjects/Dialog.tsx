// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SubjectType } from '../../Relations.types';
import { getRoles } from './library';
import { getFormDefinition, getRules } from './formDefintion';

type DialogPropsType = {
  isOpen: boolean;
  close: () => void;
  add: (formValues: any) => void;
  edit: (formValues: any, subject?: SubjectType) => void;
  subject?: SubjectType;
  subjects: SubjectType[];
};

const Dialog: React.ComponentType<DialogPropsType> = ({
  isOpen,
  close,
  add,
  edit,
  subject,
  subjects,
}) => {
  const [t] = useTranslation('caseRelations');
  const [roles, setRoles] = useState<string[]>([]);
  const editing = Boolean(subject);
  const mode = editing ? 'edit' : 'add';

  useEffect(() => {
    getRoles(setRoles);
  }, []);

  return (
    <FormDialog
      formDefinition={getFormDefinition(t, roles, subject)}
      title={t(`subjects.dialog.${mode}.title`)}
      icon={iconNames.person}
      onClose={close}
      scope="add-subject"
      open={isOpen}
      onSubmit={editing ? formValues => edit(formValues, subject) : add}
      saveLabel={t(`subjects.dialog.${mode}.submit`)}
      rules={getRules(subjects, subject)}
    />
  );
};

export default Dialog;
