// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import { CaseTask } from '../../../types/List.types';
import { useTaskItemStyles } from './TaskItem.style';
import TaskItemAddons from './TaskItemAddons/TaskItemAddons';

export interface TaskItemPropsType extends CaseTask {
  onToggle: () => void;
  rootPath: string;
  style: any;
}

/* eslint complexity: [2, 8] */
export const TaskItem: React.ComponentType<TaskItemPropsType> = ({
  assignee,
  title,
  completed,
  due_date,
  description,
  onToggle,
  rootPath,
  style,
  task_uuid,
}) => {
  const classes = useTaskItemStyles();
  const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
    <Link innerRef={ref} to={`${rootPath}/edit/${task_uuid}`} {...props} />
  ));
  LinkComponent.displayName = 'DetailsButtonLink';
  const hasAddonsRow = Boolean(assignee || description || due_date);

  let titleResizeClass:
    | ''
    | typeof classes.titleSmaller
    | typeof classes.titleExtraSmall;

  if (hasAddonsRow && title.length > 199) {
    titleResizeClass = classes.titleExtraSmall;
  } else if (hasAddonsRow && title.length > 59) {
    titleResizeClass = classes.titleSmaller;
  } else if (title.length > 99) {
    titleResizeClass = classes.titleSmaller;
  } else {
    titleResizeClass = '';
  }

  return (
    <li style={style} className={classes.container}>
      <Button
        className={classNames(classes.doneIndicator, {
          [classes.doneIndicatorCompleted]: completed,
        })}
        disabled={completed}
        presets={['icon', 'extraLarge']}
        action={onToggle}
      >
        done
      </Button>
      <div
        className={classNames(
          { [classes.bodyCompleted]: completed },
          classes.body
        )}
      >
        <div className={classes.details}>
          <span
            className={classNames(
              {
                [classes.titleCompleted]: completed,
                [titleResizeClass]: Boolean(titleResizeClass),
              },
              classes.title
            )}
          >
            {title}
          </span>
          <div>
            <TaskItemAddons
              assignee={assignee}
              description={description}
              due_date={due_date}
            />
          </div>
        </div>
        <Button
          className={classes.detailsLink}
          component={LinkComponent}
          presets={['icon']}
        >
          navigate_next
        </Button>
      </div>
    </li>
  );
};
