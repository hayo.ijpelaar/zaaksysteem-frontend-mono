// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { tasks } from './tasks.reducer';
import { tasksMiddleware } from './tasks.middleware';
import {
  setTasksContext,
  TasksSetContextActionPayload,
} from './context/tasks.context.actions';

export function getTasksModule(context: TasksSetContextActionPayload) {
  return {
    id: 'tasks',
    reducerMap: {
      tasks,
    },
    initialActions: [setTasksContext(context)],
    middlewares: [tasksMiddleware],
  };
}
