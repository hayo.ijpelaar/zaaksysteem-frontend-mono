// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useDetailStyles = makeStyles(() => ({
  formControlWrapper: {
    marginBottom: 20,
  },
  scrollWrapper: {
    flex: '1 1 auto',
    overflowY: 'scroll',
    paddingRight: 13,
  },
}));
