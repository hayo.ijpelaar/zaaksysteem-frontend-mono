// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import {
  makeInitialAjaxState,
  AjaxResponseHandler,
  AjaxStateShape,
  ActionWithPayload,
} from '@zaaksysteem/common/src/library/redux/ajax/ajaxTools';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { CaseTask } from '../../types/List.types';
import {
  TASKS_FETCH,
  TASK_CREATE,
  TASK_EDIT,
  TASK_DELETE,
  TASK_SET_COMPLETE_STATUS,
} from './tasks.constants';

type MainShape = { data: CaseTask[] };

const handleFetchSuccess: AjaxResponseHandler<
  MainShape,
  APICaseManagement.GetTaskListResponseBody
> = (state, action) => {
  if (!action.payload.response || !action.payload.response.data) {
    return state;
  }

  const data = action.payload.response.data.map(
    ({
      id,
      attributes: { completed, description, due_date, title },
      meta: { is_editable, can_set_completion },
      relationships: { assignee },
    }: any) => {
      return {
        task_uuid: id,
        title,
        completed,
        description,
        due_date,
        assignee:
          assignee && assignee.data
            ? {
                label: assignee.data.meta.display_name,
                value: assignee.data.id,
              }
            : null,
        is_editable,
        can_set_completion,
      };
    }
  );

  return { ...state, data };
};

const handleCreateTask = (
  state: MainShape & AjaxStateShape,
  action: ActionWithPayload<Pick<CaseTask, 'title' | 'task_uuid'>>
) => {
  const { title, task_uuid } = action.payload;

  const newTask: CaseTask = {
    task_uuid,
    description: '',
    completed: false,
    due_date: null,
    assignee: null,
    is_editable: true,
    can_set_completion: true,
    title,
  };

  return {
    ...state,
    data: [...state.data, newTask],
  };
};

const handleEditTask = (
  state: MainShape & AjaxStateShape,
  action: ActionWithPayload<Partial<CaseTask>>
) => {
  const { data } = state;
  const { payload } = action;

  const newTasks = data.map((thisTask: CaseTask) => {
    return thisTask.task_uuid === payload.task_uuid
      ? {
          ...thisTask,
          ...payload,
        }
      : thisTask;
  });

  return {
    ...state,
    data: newTasks,
  };
};

const handleDeleteTask = (
  state: MainShape & AjaxStateShape,
  action: ActionWithPayload<Pick<CaseTask, 'task_uuid'>>
) => {
  const { payload } = action;
  const { data } = state;

  const newTasks = data.filter(
    (thisTask: CaseTask) => thisTask.task_uuid !== payload.task_uuid
  );

  return {
    ...state,
    data: newTasks,
  };
};

/* eslint complexity: [2, 16] */
export const tasks = (
  state = makeInitialAjaxState<MainShape>({ data: [] }),
  action: any
): MainShape & AjaxStateShape => {
  const { type } = action;
  state;
  switch (type) {
    case TASKS_FETCH.PENDING:
    case TASKS_FETCH.ERROR:
      return handleAjaxStateChange(TASKS_FETCH)(state, action);
    case TASKS_FETCH.SUCCESS:
      return handleAjaxStateChange(TASKS_FETCH)(
        handleFetchSuccess(state, action as any),
        action
      );
    case TASK_CREATE.PENDING:
      return handleCreateTask(state, action);
    case TASK_CREATE.ERROR:
    case TASK_CREATE.SUCCESS:
    case TASK_EDIT.SUCCESS:
    case TASK_EDIT.ERROR:
    case TASK_EDIT.PENDING:
      return handleEditTask(state, action);
    case TASK_DELETE.PENDING:
      return handleDeleteTask(state, action);
    case TASK_DELETE.ERROR:
    case TASK_DELETE.SUCCESS:
    case TASK_SET_COMPLETE_STATUS.PENDING:
      return handleEditTask(state, action);
    case TASK_SET_COMPLETE_STATUS.ERROR:
    case TASK_SET_COMPLETE_STATUS.SUCCESS:
    default:
      return state;
  }
};

export default tasks;
