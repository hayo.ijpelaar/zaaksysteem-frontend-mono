// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { getTasksModule } from './store/tasks.module';
import TasksContainer from './TasksContainer';
import locale from './Tasks.locale';
import { TasksContextType } from './types/Context.types';

export interface TasksModulePropsType extends TasksContextType {}

const TaskModule: React.ComponentType<TasksModulePropsType> = options => {
  const [t] = useTranslation('tasks');
  const [, addMessages, removeMessages] = useMessages();

  useEffect(() => {
    const messages: { [key: string]: any } = t('serverErrors', {
      returnObjects: true,
    });

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  return (
    <DynamicModuleLoader modules={[getTasksModule(options)]}>
      <I18nResourceBundle resource={locale} namespace="tasks">
        <TasksContainer />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default TaskModule;
