// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

type Attributes = Pick<
  APICaseManagement.TaskEntity['attributes'],
  'completed' | 'description' | 'title'
>;

type Meta = Pick<
  APICaseManagement.TaskEntity['meta'],
  'is_editable' | 'can_set_completion'
>;

export type CaseTask = {
  task_uuid: string;
  assignee: null | { label: string; value: string };
  due_date: string | null;
} & Attributes &
  Meta;
