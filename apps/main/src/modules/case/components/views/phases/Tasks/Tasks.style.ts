// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

const wrapper = {
  padding: '13px 0 13px 13px',
  height: '100vh',
};

export const useTasksStyles = makeStyles(() => ({
  wrapper,
  wrapperDetails: {
    ...wrapper,
    display: 'flex',
    flexDirection: 'column',
  },
}));
