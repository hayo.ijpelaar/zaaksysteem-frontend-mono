// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { TasksRootStateType } from './store/tasks.reducer';
import { TasksPropsType, Tasks } from './Tasks';

type PropsFromStateType = Pick<TasksPropsType, 'rootPath'>;

const mapStateToProps = ({
  tasks: {
    context: { rootPath },
  },
}: TasksRootStateType): PropsFromStateType => {
  return { rootPath };
};

const TasksContainer = connect<PropsFromStateType, {}, {}, TasksRootStateType>(
  mapStateToProps
)(Tasks);

export default TasksContainer;
