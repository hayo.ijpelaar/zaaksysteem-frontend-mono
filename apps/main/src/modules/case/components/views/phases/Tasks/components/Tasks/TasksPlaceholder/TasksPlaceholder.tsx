// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTasksPlaceholderStyle } from './TasksPlaceholder.style';

export const TasksPlaceholder: React.ComponentType<{}> = () => {
  const classes = useTasksPlaceholderStyle();
  // no need to translate, temporary text
  return (
    <div className={classes.container}>Er zijn geen taken voor deze fase</div>
  );
};
