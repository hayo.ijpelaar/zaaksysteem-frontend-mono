// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useDocumentTabStyles = makeStyles(() => ({
  iframe: {
    border: 0,
    width: '100%',
    height: '99%',
  },
}));
