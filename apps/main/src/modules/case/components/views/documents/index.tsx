// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useDocumentTabStyles } from './Documents.style';

const Documents: React.ComponentType<{ caseId: number }> = ({ caseId }) => {
  const classes = useDocumentTabStyles();
  return (
    <iframe
      title="document-tab"
      className={classes.iframe}
      src={`${window.location.origin}/intern/zaak/${caseId}/documenten/?compact=1`}
    />
  );
};

export default Documents;
