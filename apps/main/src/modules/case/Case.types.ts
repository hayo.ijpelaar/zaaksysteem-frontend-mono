// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';

export type SubjectV2Type = APICaseManagement.GetContactResponseBody;
export type SubjectType = {
  type: SubjectTypeType;
  uuid: string;
  name?: string;
  cocNumber?: string;
  cocLocationNumber?: number;
};
export type SystemRolesType = {
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
};

export type CaseV2Type = APICaseManagement.GetCaseResponseBody;
export type CaseV1Type = any;
export type CaseObjType = {
  uuid: string;
  name: string;
  number: number;
  progress_status: number;
  presetClient: boolean;
  result: string | null;
  status: string;
  caseOpen: boolean;
  summary: string;
  htmlEmailTemplateName?: string;
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
  department: string;
  role: string;
  registrationDate: string | null;
  targetDate: string | null;
  completionDate: string | null;
  destructionDate: string | null;
  contactChannel:
    | 'behandelaar'
    | 'balie'
    | 'telefoon'
    | 'post'
    | 'email'
    | 'webformulier'
    | 'sociale media';
  confidentiality?: string;
  location?: string;
  payment: {
    amount: string | null;
    status: 'success' | 'failed' | 'pending' | 'offline' | null;
  };
  customFields?: any;
  caseV1?: CaseV1Type;
};

export type LeadTimeType = {
  type?: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
  value?: string | number;
};

export type CaseTypeV2Type =
  APICaseManagement.GetCaseTypeActiveVersionResponseBody;
export type CaseTypeV1Type = any;
export type CaseTypeType = {
  phases: any;
  settings: any;
  name?: string;
  metaData: {
    legalBasis?: string;
    localBasis?: string;
    designationOfConfidentiality?:
      | 'Openbaar'
      | 'Beperkt openbaar'
      | 'Intern'
      | 'Zaakvertrouwelijk'
      | 'Vertrouwelijk'
      | 'Confidentieel'
      | 'Geheim'
      | 'Zeer geheim';
    responsibleRelationship?: string;
    processDescription?: string;
  };
  leadTimeLegal: LeadTimeType;
  leadTimeService: LeadTimeType;
  objectFields?: any;
  fields?: any[];
};

export type DialogsType = 'about';
export type DialogChangeType = (type: DialogsType, setting: boolean) => void;
