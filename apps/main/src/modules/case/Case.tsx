// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../components/BreadcrumbBar/BreadcrumbBar';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import Relations from './components/views/relations';
import Timeline from './components/views/timeline';
import Communication from './components/views/communication';
import Navigation from './components/navigation/Navigation';
import Info from './components/info/Info';
import {
  CaseObjType,
  CaseTypeType,
  DialogsType,
  DialogChangeType,
} from './Case.types';
import { sessionSelector } from './Case.selectors';
import { useCaseStyles } from './Case.style';
import { formatBreadcrumbLabel } from './Case.library';
import AboutDialog from './components/info/AboutDialog';
import Documents from './components/views/documents';

const INITIAL_DIALOGS: Record<DialogsType, boolean> = {
  about: false,
};

export interface CasePropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  rootPath: string;
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  caseObj,
  caseType,
  rootPath,
}) => {
  const classes = useCaseStyles();
  const [t] = useTranslation('case');
  const session = useSelector(sessionSelector);
  const [dialogs, setDialogs] = useState(INITIAL_DIALOGS);

  const dialogChange: DialogChangeType = (type, setting) =>
    setDialogs({
      ...dialogs,
      [type]: setting,
    });

  if (!session) {
    return <Loader />;
  }

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('main:modules.dashboard'),
      path: '/intern',
    },
    { label: formatBreadcrumbLabel(t, caseObj, caseType), path: rootPath },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <NotificationBar notifications={[]} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <Navigation rootPath={rootPath} caseObj={caseObj} />
            <Info caseObj={caseObj} dialogChange={dialogChange} />
          </Panel>
          <Switch>
            <Route
              exact
              path={`${rootPath}/`}
              render={() => <Redirect to={`${rootPath}/phases`} />}
            />
            <Panel>
              <Route
                path={`${rootPath}/documents`}
                render={() => <Documents caseId={caseObj.number} />}
              />
              <Route
                path={`${rootPath}/communication`}
                render={({ match }) => (
                  <Communication
                    caseObj={caseObj}
                    caseType={caseType}
                    rootPath={match.url}
                  />
                )}
              />
              <Route
                path={`${rootPath}/timeline`}
                render={() => (
                  <Route
                    path={`${rootPath}/timeline`}
                    render={() => {
                      return <Timeline caseObj={caseObj} />;
                    }}
                  />
                )}
              />
              <Route
                path={`${rootPath}/relations`}
                render={() => (
                  <Relations
                    session={session}
                    caseObj={caseObj}
                    caseType={caseType}
                  />
                )}
              />
            </Panel>
          </Switch>
        </PanelLayout>
      </div>
      <AboutDialog
        caseObj={caseObj}
        caseType={caseType}
        onClose={() => dialogChange('about', false)}
        open={dialogs.about}
      />
    </div>
  );
};

export default Case;
