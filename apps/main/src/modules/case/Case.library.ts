// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  fetchCase,
  fetchCaseV1,
  fetchCaseType,
  fetchCaseTypeV1,
  fetchSubject,
} from './Case.requests';
import {
  CaseV2Type,
  CaseV1Type,
  CaseTypeType,
  CaseTypeV2Type,
  CaseTypeV1Type,
  CaseObjType,
  SystemRolesType,
  SubjectV2Type,
  SubjectType,
} from './Case.types';

export const formatBreadcrumbLabel = (
  t: i18next.TFunction,
  caseObj: CaseObjType,
  caseType: CaseTypeType
): string =>
  `${t('common:entityType.case')} ${caseObj.number}: ${caseType.name}`;

export const formatSubject = (subjectV2: SubjectV2Type): SubjectType => {
  const type = subjectV2.data.type;

  return {
    type:
      // typescript
      type === 'person' || type === 'organization' || type === 'employee'
        ? type
        : 'employee',
    uuid: subjectV2.data.id,
    name:
      // persons summary is `name (dateOfBirth)`, and we only want the name
      type === 'person'
        ? subjectV2.data.attributes.name
        : subjectV2.data.meta?.summary,
    cocNumber: subjectV2.data.attributes.coc_number,
    cocLocationNumber: subjectV2.data.attributes.coc_location_number,
  };
};

// recipient is a caseRole, but is not included in the get_case call as one
// it's instead included as a regular related contact (legacy mistake)
const getSystemRoles = async (caseV2: CaseV2Type) => {
  const relationships = caseV2.data?.relationships;
  const caseRoles = ['assignee', 'coordinator', 'requestor' /*, 'recipient' */];
  const subjectPromises = caseRoles.map(async (caseRole: string) => {
    const existsInCase = Object.prototype.hasOwnProperty.call(
      relationships,
      caseRole
    );

    if (!existsInCase) return;

    const subjectRelationship = relationships[caseRole];
    const type = subjectRelationship.data.type;
    const correctedType = type === 'subject' ? 'employee' : type;

    return fetchSubject(correctedType, subjectRelationship.data.id);
  });

  const relatedContacts = relationships.related_contacts;
  const recipientData = (relatedContacts || []).find(
    (contact: any) => contact.meta?.role === 'Ontvanger'
  );
  const recipientPromises = recipientData
    ? [fetchSubject(recipientData.data.type, recipientData.data.id)]
    : [];

  const systemRoles = await Promise.all([
    ...subjectPromises,
    ...recipientPromises,
  ]);

  const [assignee, coordinator, requestor, recipient] = systemRoles.map(
    subjectV2 => {
      if (!subjectV2) return;

      return formatSubject(subjectV2);
    }
  );

  return {
    assignee,
    requestor,
    recipient,
    coordinator,
  };
};

export const formatCaseV1 = (caseV1: CaseV1Type) => ({
  // v2 doesn't return the correct phase (for relations tab)
  phase: caseV1.phase,
  // v2 doesn't return the html_email_template (for communication tab)
  htmlEmailTemplateName: caseV1.html_email_template,
  // map fields require the v1 response to be passed into the context (for Wetterskip)
  caseV1,
});

export const formatCaseV2 = (caseV2: CaseV2Type) => ({
  uuid: caseV2.data?.id,
  name: caseV2.data?.id,
  number: caseV2.data?.attributes.number,
  progress_status: caseV2.data?.attributes.progress_status / 100,
  result: caseV2.data?.attributes.result.result,
  status: caseV2.data?.attributes.status,
  presetClient: caseV2.data?.attributes.preset_client === 'True' ? true : false,
  caseOpen: caseV2.data?.attributes.status !== 'resolved',
  summary: caseV2.data?.attributes.subject,
  registrationDate: caseV2.data?.attributes.registration_date,
  targetDate: caseV2.data?.attributes.target_completion_date,
  completionDate: caseV2.data?.attributes.completion_date,
  destructionDate: caseV2.data?.attributes.destruction_date,
  contactChannel: caseV2.data?.attributes.contact_channel,
  // not supported by v2, or even v1
  location: undefined,
  customFields: caseV2.data?.attributes.custom_fields,
  department: caseV2.data?.attributes.department.name,
  role: caseV2.data?.attributes.role.name,
  confidentiality: caseV2.data?.attributes.confidentiality,
  payment: caseV2.data?.attributes.payment,
});

const buildCaseObj = (
  caseV2: CaseV2Type,
  caseV1?: CaseV1Type,
  systemRoles?: SystemRolesType
): CaseObjType => ({
  ...formatCaseV1(caseV1),
  ...formatCaseV2(caseV2),
  ...systemRoles,
});

const getFields = (caseTypeV1: CaseTypeV1Type) => {
  const phases = caseTypeV1.phases;
  const fields = phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.fields],
    []
  );

  return fields;
};

const getObjectFields = (caseTypeV1: CaseTypeV1Type) => {
  const fields = getFields(caseTypeV1);
  const objectFields = fields.filter((field: any) => field.type === 'object');

  return objectFields;
};

export const formatCaseTypeV1 = (caseTypeV1: CaseTypeV1Type) => ({
  // v2 lacks object(v1) attributes in its phase definition (for relations tab)
  objectFields: getObjectFields(caseTypeV1),
  // v2 lacks the 'id' of an attribute in its field definition (for object form)
  fields: getFields(caseTypeV1),
});

export const formatCaseTypeV2 = (caseTypeV2: CaseTypeV2Type) => ({
  phases: caseTypeV2.data?.attributes.phases,
  settings: caseTypeV2.data?.attributes.settings,
  name: caseTypeV2.data?.meta.summary,
  metaData: {
    legalBasis: caseTypeV2.data?.attributes.metadata.legal_basis,
    localBasis: caseTypeV2.data?.attributes.metadata.local_basis,
    designationOfConfidentiality:
      caseTypeV2.data?.attributes.metadata.designation_of_confidentiality,
    responsibleRelationship:
      caseTypeV2.data?.attributes.metadata.responsible_relationship,
    processDescription:
      caseTypeV2.data?.attributes.metadata.process_description,
  },
  leadTimeLegal: {
    type: caseTypeV2.data?.attributes.terms.lead_time_legal.type,
    value: caseTypeV2.data?.attributes.terms.lead_time_legal.value,
  },
  leadTimeService: {
    type: caseTypeV2.data?.attributes.terms.lead_time_service.type,
    value: caseTypeV2.data?.attributes.terms.lead_time_service.value,
  },
});

const buildCaseType = (
  caseTypeV2: CaseTypeV2Type,
  caseTypeV1: CaseTypeV1Type
): CaseTypeType => ({
  ...formatCaseTypeV1(caseTypeV1),
  ...formatCaseTypeV2(caseTypeV2),
});

export const provideForCase = async (caseUuid: string) => {
  const [caseV2, caseV1] = await Promise.all([
    fetchCase(caseUuid),
    fetchCaseV1(caseUuid),
  ]);

  const systemRoles = await getSystemRoles(caseV2);

  const caseTypeVersionUuid = caseV2.data?.attributes.case_type_version_uuid;
  const caseTypeUuid = caseV1.casetype.reference;
  const caseTypeVersion = caseV1.casetype.instance.version;

  const [caseTypeV2, caseTypeV1] = await Promise.all([
    fetchCaseType(caseTypeVersionUuid),
    fetchCaseTypeV1(caseTypeUuid, caseTypeVersion),
  ]);

  const caseType = buildCaseType(caseTypeV2, caseTypeV1);

  const caseObj = buildCaseObj(caseV2, caseV1, systemRoles);

  return {
    caseObj,
    caseType,
  };
};
