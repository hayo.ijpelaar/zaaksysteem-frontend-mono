// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './locale/objectView.locale';
import { getObjectViewModule } from './store/objectView.module';
import ObjectView, { ObjectViewPropsType } from './components/ObjectView';

export type ObjectViewModulePropsType = ObjectViewPropsType &
  RouteComponentProps<{
    uuid: string;
  }>;

const ObjectViewModule: React.ComponentType<ObjectViewModulePropsType> = ({
  prefix,
  match: {
    params: { uuid },
  },
}) => {
  return (
    <DynamicModuleLoader modules={[getObjectViewModule(uuid)]}>
      <I18nResourceBundle resource={locale} namespace="objectView">
        <ObjectView prefix={prefix} />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};
export default ObjectViewModule;
