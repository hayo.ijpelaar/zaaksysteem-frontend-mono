// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Middleware } from 'redux';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { ObjectViewRootStateType } from './objectView.reducer';
import { fetchObjectType } from './objectType/objectType.actions';
import { fetchObject } from './object/object.actions';
import { OBJECT_FETCH, OBJECT_UPDATE } from './object/object.constants';

const loadObjectType: MiddlewareHelper<
  ObjectViewRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const {
    objectView: {
      object: { object },
    },
  } = store.getState();

  if (object && object.objectTypeVersionUuid) {
    fetchObjectType(object.objectTypeVersionUuid)(store.dispatch);
  }
};

const refreshObject: MiddlewareHelper<
  ObjectViewRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const {
    objectView: {
      object: { object },
    },
  } = store.getState();

  if (object && object.versionIndependentUuid) {
    fetchObject(object.versionIndependentUuid)(store.dispatch);
  }
};

export const objectViewMiddleware: Middleware<{}, ObjectViewRootStateType> =
  store => next => action => {
    switch (action.type) {
      case OBJECT_FETCH.SUCCESS:
        return (loadObjectType as any)(store, next, action);
      case OBJECT_UPDATE.SUCCESS:
        return (refreshObject as any)(store, next, action);
      default:
        next(action);
    }
  };

export default objectViewMiddleware;
