// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IModule } from 'redux-dynamic-modules';
import objectViewReducer, {
  ObjectViewRootStateType,
} from './objectView.reducer';
import { fetchObject } from './object/object.actions';
import { objectViewMiddleware } from './objectView.middleware';

export function getObjectViewModule(
  uuid: string
): IModule<ObjectViewRootStateType> {
  return {
    id: 'objectView',
    reducerMap: {
      objectView: objectViewReducer as any,
    },
    middlewares: [objectViewMiddleware],
    initialActions: [fetchObject(uuid) as any],
  };
}
