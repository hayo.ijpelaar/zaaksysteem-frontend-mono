// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import object, { ObjectStateType } from './object/object.reducer';
import objectType, {
  ObjectTypeStateType,
} from './objectType/objectType.reducer';
import relationships, {
  RelationshipsRootStateType,
} from './relationships/relationships.reducer';

export interface ObjectViewStateType {
  object: ObjectStateType;
  objectType: ObjectTypeStateType;
  relationships: RelationshipsRootStateType;
}

export interface ObjectViewRootStateType {
  objectView: ObjectViewStateType;
}

const objectViewReducer = combineReducers<ObjectViewStateType>({
  object,
  objectType,
  relationships,
});

export default objectViewReducer;
