// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useProfileStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      marginBottom: 20,
      padding: 0,
      '&>:first-child': {
        marginLeft: 10,
      },
    },
  })
);
