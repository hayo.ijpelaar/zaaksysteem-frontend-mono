// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { fetchContact } from './Location.actions';
import { useLocationStyles } from './Location.style';
import locale from './location.locale';

export const Location: React.FunctionComponent<{
  uuid: string;
  type: string;
}> = ({ uuid, type }) => {
  const [t] = useTranslation();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useLocationStyles();

  return (
    <I18nResourceBundle resource={locale} namespace="location">
      <DataProvider
        provider={fetchContact(openServerErrorDialog)}
        providerArguments={[uuid, type]}
        autoProvide={true}
      >
        {({ data, busy }) => {
          const contact = data?.contact;
          const contactType = type;
          const contactLocation = data?.contact.location_address;

          if (busy) {
            return <Loader />;
          } else if (contact && contactLocation) {
            return (
              <GeoMap
                geoFeature={contact?.geojson || null}
                name="ContactMap"
                canDrawFeatures={false}
                minHeight="100%"
                context={{
                  type: 'ContactMap',
                  data: { contact, contactType },
                }}
              />
            );
          } else {
            return (
              <div className={classes.wrapper}>{t('location:no_address')}</div>
            );
          }
        }}
      </DataProvider>
      {ServerErrorDialog}
    </I18nResourceBundle>
  );
};

export default Location;
