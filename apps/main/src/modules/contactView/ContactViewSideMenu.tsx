// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { ContactViewPropsType } from './ContactView';

export type ContactViewSideMenuPropsType = {
  selectedItem: ContactViewPropsType['match']['params']['selectedItem'];
  basePath: string;
  uuid: string;
};

export const ContactViewSideMenu: React.ComponentType<
  ContactViewSideMenuPropsType
> = ({ selectedItem, basePath, uuid }) => {
  const [t] = useTranslation('contactView');

  const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
    ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
  );
  const MenuItemLinkOld = React.forwardRef<any, SideMenuItemType>(
    // eslint-disable-next-line jsx-a11y/anchor-has-content
    ({ href, ...restProps }, ref) => <a href={href} {...restProps} />
  );
  MenuItemLink.displayName = 'MenuItemLink';
  MenuItemLinkOld.displayName = 'MenuItemLinkOld';

  const menuItems: SideMenuItemType[] = [
    {
      id: 'data',
      label: t('contactView:sideMenu.data'),
      href: `${basePath}/data`,
      icon: <Icon>{iconNames.person}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'communication',
      label: t('contactView:sideMenu.communication'),
      href: `${basePath}/communication`,
      icon: <Icon>{iconNames.alternate_email}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'cases',
      label: t('contactView:sideMenu.cases'),
      href: `${basePath}/cases`,
      icon: <Icon>{iconNames.list}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'map',
      label: t('contactView:sideMenu.map'),
      href: `${basePath}/map`,
      icon: <Icon>{iconNames.map}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'relationships',
      label: t('contactView:sideMenu.relationships'),
      href: `${basePath}/relationships`,
      icon: <Icon>{iconNames.link}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('contactView:sideMenu.timeline'),
      href: `${basePath}/timeline`,
      icon: <Icon>{iconNames.schedule}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'oldView',
      label: t('contactView:sideMenu.oldView'),
      href: `/redirect/contact_page?uuid=${uuid}&version=v1`,
      icon: <Icon>{iconNames.person}</Icon>,
      component: MenuItemLinkOld,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: selectedItem === menuItem.id,
  }));

  return (
    <div>
      <SideMenu items={menuItems} />
    </div>
  );
};
