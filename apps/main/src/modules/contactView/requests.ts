// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import { SubjectType, SessionType } from './ContactView.types';

type FetchSubjectType = (
  uuid: string,
  type: SubjectTypeType
) => Promise<SubjectType>;

export const fetchSubject: FetchSubjectType = async (uuid, type) => {
  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact',
    {
      uuid,
      type,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

type FetchSessionType = () => Promise<SessionType>;

export const fetchSession: FetchSessionType = async () => {
  const result = await request('GET', '/api/v1/session/current');
  const {
    logged_in_user: { capabilities, system_roles, uuid },
    configurable,
    active_interfaces,
  } = result.result.instance;

  return {
    activeInterfaces: active_interfaces,
    capabilities,
    signatureUploadRole: configurable?.signature_upload_role,
    loggedInUserRoles: system_roles,
    loggedInUserUuid: uuid,
  };
};
