// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const ensureEmptyValue = (value: any) => {
  return typeof value === 'undefined' || value === null ? '' : value;
};
