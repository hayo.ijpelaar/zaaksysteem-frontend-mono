// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useInformationStyles = makeStyles(
  ({
    palette: {
      cloud,
      danger: { main },
    },
  }: Theme) => ({
    wrapper: {
      padding: 20,
      boxSizing: 'border-box',
    },
    titleSuffix: {
      color: main,
      marginLeft: '5px',
    },
    formWrapper: {
      padding: 20,
    },
    buttonWrapper: {
      display: 'flex',
    },
    button: {
      marginTop: 20,
      marginRight: 20,
    },
    formLabel: {
      fontWeight: 600,
      margin: '16px 0px 16px 0px',
      backgroundColor: cloud.light,
      borderRadius: 6,
      padding: 8,
    },
  })
);
