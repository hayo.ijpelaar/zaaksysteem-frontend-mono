// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  AltAuthDataType,
  ObjectType,
  ObjectTypeType,
} from '../Information.types';
import Common from '../tables/common/Common';
import Additional from '../tables/additional/Additional';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import AltAuth from '../tables/altAuth/AltAuth';
import Special from '../tables/special/Special';
import Sensitive from '../tables/sensitive/Sensitive';
import {
  commonRules,
  getCommonFormDefinition,
} from '../tables/common/common.formDefinition.person';
import { getAdditionalFormDefinition } from '../tables/additional/additional.formDefinition.person';
import { getSpecialFormDefinition } from '../tables/special/special.formDefinition.person';
import { SubjectType, SessionType } from './../../ContactView.types';

export interface PersonViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    altAuthData?: AltAuthDataType;
  };
  refreshSubject: () => {};
  session: SessionType;
  setSnackOpen: any;
}

const PersonView: React.FunctionComponent<PersonViewPropsType> = ({
  data: { subject, object, objectType, altAuthData },
  refreshSubject,
  session,
  setSnackOpen,
}) => (
  <>
    <Common
      subject={subject}
      rules={commonRules}
      getFormDefinition={getCommonFormDefinition}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
    />
    <Additional
      subject={subject}
      getFormDefinition={getAdditionalFormDefinition}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
    />
    {altAuthData && <AltAuth altAuthData={altAuthData} />}
    {object && <RelatedObject object={object} objectType={objectType} />}
    <Special
      subject={subject}
      getFormDefinition={getSpecialFormDefinition}
      session={session}
    />
    <Sensitive />
  </>
);

export default PersonView;
