// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import * as i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Rule } from '@zaaksysteem/common/src/components/form/rules';
import { saveCommonValues } from '../../requests';
import { SeperatorLabel } from '../../Information.library';
import { GetFormDefinitionType } from '../../Information.types';
import { useInformationStyles } from '../../Information.style';
import { hasEditRightsFromState } from '../../../library';
import { SubjectType } from './../../../ContactView.types';

const getTitleSuffix = (subject: SubjectType, t: i18next.TFunction) => {
  if (subject.isActive) return;

  const type = subject.dateOfDeath ? 'passed' : 'inactive';
  const text = t(`common.titleSuffix.${type}`);

  return `(${text})`;
};

type CommonPropsType = {
  subject: SubjectType;
  rules?: Rule[];
  getFormDefinition: GetFormDefinitionType;
  refreshSubject: () => void;
  setSnackOpen: any;
};

const Common: React.FunctionComponent<CommonPropsType> = ({
  subject,
  rules,
  getFormDefinition,
  refreshSubject,
  setSnackOpen,
}) => {
  const hasEditRights = useSelector(hasEditRightsFromState);
  const isAuthenticated = subject.authenticated;
  const [t] = useTranslation('contactView');
  const [busy, setBusy] = useState<boolean>(false);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition({
    t,
    subject,
    hasEditRights,
    isAuthenticated,
  });
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { values, isValid },
  } = useForm({
    rules,
    formDefinition,
    validationMap,
    enableReinitialize: true,
  });

  return (
    <>
      {ServerErrorDialog}

      <SubHeader
        title={t('common.title')}
        description={t('common.subTitle')}
        titleSuffix={getTitleSuffix(subject, t)}
        titleSuffixClass={classes.titleSuffix}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            if (rest.isLabel) return <SeperatorLabel str={rest.label} />;

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}

        {hasEditRights && !isAuthenticated && (
          <Button
            action={() => {
              setBusy(true);
              saveCommonValues(t, subject.uuid, values, subject.type)
                .then(() => {
                  refreshSubject();
                  setSnackOpen(true);
                })
                .catch(openServerErrorDialog)
                .finally(() => {
                  setBusy(false);
                });
            }}
            className={classes.button}
            presets={['contained', 'primary']}
            disabled={busy || !isValid}
          >
            {t('save')}
          </Button>
        )}
      </div>
    </>
  );
};

export default Common;
