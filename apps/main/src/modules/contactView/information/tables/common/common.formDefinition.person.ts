// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  setRequired,
  setOptional,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

const transformDate = (date: string) =>
  date ? fecha.format(new Date(date), 'DD-MM-YYYY') : null;

const getNormalizedValue = (field: any) =>
  field.value?.label ? field.value.label : field.value;

/* eslint complexity: [2, 50] */
export const getCommonFormDefinition = ({
  t,
  subject,
  hasEditRights,
  isAuthenticated,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  hasEditRights?: Boolean;
  isAuthenticated?: Boolean;
}): AnyFormDefinitionField[] => {
  return [
    {
      name: 'generalLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'firstNames',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
      required: true,
    },
    {
      name: 'insertions',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'familyName',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
      required: true,
    },

    {
      name: 'nobleTitle',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'gender',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t(`common.person.genderValue.Male`),
          value: 'M',
        },
        {
          label: t(`common.person.genderValue.Female`),
          value: 'F',
        },
        {
          label: t(`common.person.genderValue.Other`),
          value: 'X',
        },
      ],
      value: subject.gender ? subject.gender : null,
      applyBackgroundColor: false,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'dateOfBirth',
      type: fieldTypes.TEXT,
      value: transformDate(subject['dateOfBirth']),
      readOnly: true,
    },
    {
      name: 'dateOfDeath',
      type: fieldTypes.TEXT,
      value: transformDate(subject['dateOfDeath']),
      readOnly: true,
    },
    {
      name: 'insideMunicipality',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t('common.yesNo.yes'),
          value: t('common.yesNo.yes'),
        },
        {
          label: t('common.yesNo.no'),
          value: t('common.yesNo.no'),
        },
      ],
      value: t(
        `special.insideMunicipalityValue.${subject['insideMunicipality']}`
      ),
      applyBackgroundColor: false,
      readOnly: true,
    },
    {
      name: 'addressLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'country',
      type: fieldTypes.COUNTRY_FINDER,
      value: subject['country'] || t('common.person.countryValue.netherlands'),
      readOnly: Boolean(!hasEditRights || isAuthenticated),
      required: true,
    },
    {
      name: 'residenceStreet',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'residenceHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'residenceHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'residenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'residenceZipcode',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'residenceCity',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'foreignAddress1',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'foreignAddress2',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'foreignAddress3',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
    },
    {
      name: 'hasCorrespondenceAddress',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t('common.yesNo.yes'),
          value: t('common.yesNo.yes'),
        },
        {
          label: t('common.yesNo.no'),
          value: t('common.yesNo.no'),
        },
      ],
      value: t(`common.yesNo.${subject['hasCorrespondenceAddress']}`),
      applyBackgroundColor: false,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceCountry',
      label: t(`common.person.country`),
      type: fieldTypes.COUNTRY_FINDER,
      value:
        subject['correspondenceCountry'] ||
        t('common.person.countryValue.netherlands'),
      readOnly: Boolean(!hasEditRights || isAuthenticated),
      required: true,
    },
    {
      name: 'correspondenceStreet',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceZipcode',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceCity',
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceForeignAddress1',
      label: t(`common.person.foreignAddress1`),
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceForeignAddress2',
      label: t(`common.person.foreignAddress2`),
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
    {
      name: 'correspondenceForeignAddress3',
      label: t(`common.person.foreignAddress3`),
      type: fieldTypes.TEXT,
      readOnly: Boolean(!hasEditRights || isAuthenticated),
    },
  ].map(field => ({
    label: t(`common.person.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};

const residenceAddressFields = [
  'residenceStreet',
  'residenceHouseNumber',
  'residenceHouseNumberLetter',
  'residenceHouseNumberSuffix',
  'residenceZipcode',
  'residenceCity',
];

const correspondenceAddressFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceHouseNumberLetter',
  'correspondenceHouseNumberSuffix',
  'correspondenceZipcode',
  'correspondenceCity',
];

const foreignAddressFields = [
  'foreignAddress1',
  'foreignAddress2',
  'foreignAddress3',
];

const correspondenceForeignAddressFields = [
  'correspondenceForeignAddress1',
  'correspondenceForeignAddress2',
  'correspondenceForeignAddress3',
];

const residenceRequiredFields = [
  'residenceStreet',
  'residenceHouseNumber',
  'residenceZipcode',
  'residenceCity',
];

const correspondenceRequiredFields = [
  'correspondenceCountry',
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceZipcode',
  'correspondenceCity',
];

export const commonRules = [
  //Dutch address
  new Rule()
    .when(
      'country',
      field => getNormalizedValue(field)?.toLowerCase() === 'nederland'
    )
    .then(showFields(residenceAddressFields))
    .then(hideFields(foreignAddressFields))
    .then(setRequired(residenceRequiredFields))
    .then(setOptional(['foreignAddress1']))
    .else(showFields(foreignAddressFields))
    .else(hideFields(residenceAddressFields))
    .else(setRequired(['foreignAddress1']))
    .else(setOptional(residenceRequiredFields)),
  new Rule()
    // No correspondence address
    .when(fields => fields.hasCorrespondenceAddress.value === 'Nee')
    .then(
      hideFields([
        ...correspondenceAddressFields,
        ...correspondenceForeignAddressFields,
        'correspondenceCountry',
      ])
    )
    .then(
      setOptional([
        ...correspondenceRequiredFields,
        ...['correspondenceForeignAddress1'],
      ])
    ),
  new Rule()
    // Dutch correspondence address
    .when(
      fields =>
        fields.hasCorrespondenceAddress.value === 'Ja' &&
        getNormalizedValue(fields.correspondenceCountry)?.toLowerCase() ===
          'nederland'
    )
    .then(showFields(['correspondenceCountry', ...correspondenceAddressFields]))
    .then(hideFields(correspondenceForeignAddressFields))
    .then(setRequired(correspondenceRequiredFields)),
  new Rule()
    // Foreign correspondence address
    .when(
      fields =>
        fields.hasCorrespondenceAddress.value === 'Ja' &&
        getNormalizedValue(fields.correspondenceCountry)?.toLowerCase() !==
          'nederland'
    )
    .then(
      showFields([
        'correspondenceCountry',
        ...correspondenceForeignAddressFields,
      ])
    )
    .then(hideFields(correspondenceAddressFields))
    .then(setRequired(['correspondenceForeignAddress1'])),
];
