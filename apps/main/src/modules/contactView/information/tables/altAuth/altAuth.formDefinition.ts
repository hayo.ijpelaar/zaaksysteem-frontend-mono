// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { AltAuthDataType } from '../../Information.types';

export const getFormDefinition = (
  t: i18next.TFunction,
  altAuthData: AltAuthDataType
): AnyFormDefinitionField[] => {
  return [
    {
      name: 'username',
      type: fieldTypes.EMAIL,
      required: true,
    },
    {
      name: 'phone',
      type: fieldTypes.PHONE_NUMBER,
      required: true,
    },
    {
      name: 'active',
      type: fieldTypes.RADIO_GROUP,
      applyBackgroundColor: false,
      required: true,
      choices: [
        {
          label: t('altAuth.activeValue.active'),
          value: '1',
        },
        {
          label: t('altAuth.activeValue.inactive'),
          value: '0',
        },
      ],
    },
    {
      name: 'subject_type',
      type: fieldTypes.TEXT,
      hidden: true,
    },
  ].map(field => ({
    label: t(`altAuth.${field.name}`),
    value: altAuthData[field.name],
    ...field,
  }));
};
