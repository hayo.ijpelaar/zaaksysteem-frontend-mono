// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { GetFormDefinitionType } from '../../Information.types';
import { useInformationStyles } from '../../Information.style';
import { SessionType, SubjectType } from './../../../ContactView.types';

type SpecialPropsType = {
  subject: SubjectType;
  getFormDefinition: GetFormDefinitionType;
  session?: SessionType;
};

const Special: React.FunctionComponent<SpecialPropsType> = ({
  subject,
  getFormDefinition,
  session,
}) => {
  const [t] = useTranslation('contactView');
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition({ t, subject, session });

  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <SubHeader
        title={t('special.title')}
        description={t('special.subTitle')}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
      </div>
    </>
  );
};

export default Special;
