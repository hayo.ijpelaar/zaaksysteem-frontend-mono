// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType, SessionType } from './../../../ContactView.types';

export type FormValuesType = {
  insideMunicipality: string;
  authenticated: string;
  source: string;
  uuid: string;
};

export const getSpecialFormDefinition = ({
  t,
  subject,
  session,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  session?: SessionType;
}): AnyFormDefinitionField[] => {
  const allowedToViewSensitiveContactData = session?.capabilities.includes(
    'view_sensitive_contact_data'
  );

  return [
    {
      name: 'personalNumber',
      type: fieldTypes.PERSONAL_NUMBER,
      config: {
        uuid: subject.uuid,
        allowedToView: allowedToViewSensitiveContactData,
      },
    },
    {
      name: 'authenticated',
      type: fieldTypes.TEXT,
    },
    {
      name: 'source',
      type: fieldTypes.TEXT,
    },
    {
      name: 'uuid',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`special.${field.name}`),
    value: subject[field.name],
    readOnly: true,
    ...field,
  }));
};
