// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Timeline from '../../../../../../packages/common/src/components/Timeline/Timeline';
import { getData } from './library';
import { useStyles } from './styles';
import { getExportFunction } from './library';

const ContactTimeline: React.FunctionComponent<any> = ({ uuid, type }) => {
  const classes = useStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const getDataFunction = getData({ openServerErrorDialog, uuid, type });

  return (
    <div className={classes.wrapper}>
      {ServerErrorDialog}
      <Timeline
        getData={getDataFunction}
        exportFunction={getExportFunction(uuid, type)}
      />
    </div>
  );
};

export default ContactTimeline;
