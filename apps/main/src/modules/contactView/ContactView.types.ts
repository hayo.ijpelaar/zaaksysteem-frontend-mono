// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type SubjectType = APICaseManagement.GetContactResponseBody['data'];

type CapabilitiesType =
  | 'admin'
  | 'beheer'
  | 'beheer_gegevens_admin'
  | 'beheer_plugin_admin'
  | 'beheer_zaaktype_admin'
  | 'case_allocation'
  | 'case_registration_allow_partial'
  | 'contact_edit_subset'
  | 'contact_nieuw'
  | 'contact_search'
  | 'contact_search_extern'
  | 'dashboard'
  | 'documenten_intake_all'
  | 'documenten_intake_subject'
  | 'gebruiker'
  | 'message_intake'
  | 'owner_signatures'
  | 'plugin_mgmt'
  | 'search'
  | 'view_sensitive_contact_data'
  | 'view_sensitive_data'
  | 'useradmin'
  | 'vernietigingslijst'
  | 'zaak_afdeling'
  | 'zaak_create_skip_required'
  | 'zaak_beheer'
  | 'zaak_eigen';

export type SessionType = {
  activeInterfaces: string[];
  capabilities: CapabilitiesType[];
  signatureUploadRole: 'Behandelaar' | 'Zaaksysteembeheerder';
  loggedInUserRoles: string[];
  loggedInUserUuid: string;
};
