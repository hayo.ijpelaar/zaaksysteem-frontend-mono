// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  DocumentItemType,
  DirectoryItemType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { APIDocument } from '@zaaksysteem/generated';
import { GetDataReturnType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { DataParams, DialogsType } from './Intake.types';

const sortMapping: any = {
  name: 'attributes.name',
  mimeType: 'attributes.extension',
  description: 'attributes.description',
  modified: 'attributes.last_modified_date_time',
};

export const getSelectedPreview = (items: DocumentItemType[]) => {
  if (!items.length || items.length > 1) return null;
  return items[0].preview as DocumentItemType['preview'];
};

export const isDialogOpen = (dialogs: Record<DialogsType, boolean>) =>
  Object.values(dialogs).some(value => value === true);

type GetDataType = (
  params: DataParams
) => Promise<GetDataReturnType<DocumentItemType>>;

export const getData: GetDataType = async ({
  pageNum,
  pageLength,
  openServerErrorDialog,
  sortDirection,
  sortBy,
  assignment,
  search,
}: Pick<
  DataParams,
  | 'pageNum'
  | 'pageLength'
  | 'openServerErrorDialog'
  | 'assignment'
  | 'sortBy'
  | 'sortDirection'
  | 'search'
>) => {
  let queryParams: any = {
    page: pageNum,
    page_size: pageLength,
  };

  if (sortBy && sortDirection) {
    let sorting = sortMapping[sortBy];
    if (sortDirection === 'DESC') sorting = '-' + sorting;
    queryParams.sort = sorting;
  }

  if (search) {
    queryParams['filter[fulltext]'] = search;
  }

  if (assignment === 'assigned') {
    queryParams['filter[attributes.assignment.is_set]'] = true;
  } else if (assignment === 'unassigned') {
    queryParams['filter[attributes.assignment.is_set]'] = 'false';
  } else if (assignment === 'assignedToMe') {
    queryParams['filter[attributes.assignment.is_self]'] = true;
  }

  const url = buildUrl<APIDocument.GetDirectoryEntriesForCaseRequestParams>(
    '/api/v2/document/get_directory_entries_for_intake',
    queryParams
  );

  const results =
    await request<APIDocument.GetDirectoryEntriesForIntakeResponseBody>(
      'GET',
      url
    ).catch(openServerErrorDialog);

  const items = results?.data
    ? results.data.map(
        /* eslint complexity: [2, 12] */
        ({
          attributes: {
            name,
            last_modified_date_time,
            description,
            mimetype,
            accepted,
            document_number,
            rejection_reason,
            extension,
            rejected_by_display_name,
            assignment,
          },
          links: { download, preview, thumbnail },
          id,
        }): DocumentItemType => {
          return {
            uuid: id,
            name,
            type: 'document',
            document_number,
            modified: last_modified_date_time
              ? new Date(last_modified_date_time)
              : undefined,
            description: description ? description : undefined,
            mimeType: mimetype || '',
            extension: extension || '',
            beingVirusScanned: false,
            selected: false,
            accepted: accepted ? accepted : false,
            rejectionReason: rejection_reason || '',
            rejectionName: rejected_by_display_name || '',
            download: download?.href ? { url: download.href } : undefined,
            thumbnail: thumbnail?.href ? { url: thumbnail.href } : undefined,
            preview: preview?.href
              ? {
                  url: preview.href,
                  contentType: preview.meta['content-type'],
                }
              : undefined,
            assignment,
          };
        }
      )
    : [];

  return { rows: items || [] };
};

export const mergeListSelected = (
  list: DocumentItemType[],
  selected: string[]
) => {
  return list.map(item => ({
    ...item,
    selected: selected.includes(item.uuid),
  }));
};

export const toggleSelected = (
  selected: string[],
  setSelected: React.Dispatch<React.SetStateAction<string[]>>,
  rowData: DirectoryItemType
) => {
  if (!selected.includes(rowData.uuid)) {
    setSelected([...selected, rowData.uuid]);
  } else {
    setSelected(selected.filter((uuid: string) => uuid !== rowData.uuid));
  }
};
