// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  Rule,
  showFields,
  hideFields,
  setRequired,
  setOptional,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  FormValuesType,
  AssignType,
  AssignDialogPropsType,
} from './AssignDialog.types';
import { getFormDefinition } from './AssignDialog.formDefinition';
import {
  assignDocumentContact,
  assignDocumentRole,
} from './AssignDialog.library';

const rules = [
  new Rule<FormValuesType>()
    .when(fields => (fields.type.value as AssignType) === 'user')
    .then(showFields(['contact']))
    .and(hideFields(['group', 'role']))
    .and(setRequired(['contact']))
    .and(setOptional(['group', 'role']))
    .else(hideFields(['contact']))
    .and(showFields(['group', 'role']))
    .and(transferDataAsConfig('group', 'role', 'parentRoleUuid'))
    .and(setRequired(['group', 'role']))
    .and(setOptional(['contact'])),
];

const AssignDialog: React.ComponentType<AssignDialogPropsType> = ({
  selectedDocuments,
  open,
  onClose,
  onConfirm,
  container,
}) => {
  const [saving, setSaving] = React.useState(false);
  const [t] = useTranslation('Intake');
  const formDefinition = getFormDefinition(t);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <FormDialog
        formDefinition={formDefinition}
        title={t('assignDocument.title')}
        icon="insert_drive_file"
        onClose={onClose}
        saving={saving}
        scope="assign-document"
        open={open}
        rules={rules}
        onSubmit={formValues => {
          const { role, contact, group } = formValues;

          setSaving(true);

          return Promise.all(
            selectedDocuments.map(({ uuid }) =>
              contact
                ? assignDocumentContact({
                    uuid,
                    contact: contact?.value,
                  })
                : assignDocumentRole({
                    uuid,
                    role,
                    group: group?.value,
                  })
            )
          )
            .then(onConfirm)
            .catch(openServerErrorDialog)
            .then(() => setSaving(false));
        }}
        container={container}
      />
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default AssignDialog;
