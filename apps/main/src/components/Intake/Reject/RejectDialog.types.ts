// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type RejectDialogPropsType = {
  selectedDocuments: { uuid: string; name: string }[];
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
};

export type FormValuesType = {
  reason: string;
};
