// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    new: 'Nieuw',
    savedSearches: {
      mine: 'Mijn openstaande zaken',
      myDepartment: 'Mijn afdeling',
      all: 'Alle zaken',
    },
    filters: {
      all: 'Zoek in alles',
      savedSearches: 'Zoekopdrachten',
      contacts: 'Contacten',
      documents: 'Documenten',
      case: 'Zaak',
    },
  },
};

export default locale;
