// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useBreadcrumbBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: '16px 32px',
      minHeight: 75,
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    breadCrumbs: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
    },
    search: {
      paddingLeft: 20,
      paddingRight: 20,
      width: 550,
      display: 'flex',
    },
  })
);
