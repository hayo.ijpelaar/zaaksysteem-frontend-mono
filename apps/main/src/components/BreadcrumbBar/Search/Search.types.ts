// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

export type OptionType = {
  value: string | number;
  label: string;
  subLabel?: string;
  url?: string;
  icon?: React.ReactElement;
};

export type FilterType = ValueType<string, string>;
