// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/// <reference types="react-scripts" />

declare namespace NodeJS {
  interface ProcessEnv {
    readonly WEBPACK_BUILD_TARGET: 'development' | 'production';
  }
}
