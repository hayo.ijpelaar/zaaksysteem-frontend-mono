// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const { callOrNothingAtAll } = require('./function');

test('callOrNothingAtAll()', assert => {
  {
    const actual = callOrNothingAtAll();
    const expected = undefined;
    const message = 'returns `undefined` if no function is provided';

    assert.equal(actual, expected, message);
  }
  {
    const getAnswer = () => '42';

    const actual = callOrNothingAtAll(getAnswer);
    const expected = '42';
    const message = 'returns the return value of the provided function';

    assert.equal(actual, expected, message);
  }
  {
    const getAnswer = answer => answer;
    const argumentList = ['42'];

    const actual = callOrNothingAtAll(getAnswer, argumentList);
    const expected = '42';
    const message =
      'calls the provided function with the provided arguments array';

    assert.equal(actual, expected, message);
  }
  {
    const getAnswer = answer => answer;
    const getArgumentList = () => ['42'];

    const actual = callOrNothingAtAll(getAnswer, getArgumentList);
    const expected = '42';
    const message =
      'calls the provided function with the return value of the provided arguments getter';

    assert.equal(actual, expected, message);
  }
  assert.end();
});
