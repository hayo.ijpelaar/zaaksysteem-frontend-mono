// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SHOW_SNACKBAR, HIDE_SNACKBAR } from './snackbar.constants';
import { ShowSnackbarAction, HideSnackbarAction } from './snackbar.actions';

export type SnackbarStateType = { messageId: string | null };

const initialState: SnackbarStateType = { messageId: null };

export default function ui(
  state: SnackbarStateType = initialState,
  action: ShowSnackbarAction | HideSnackbarAction
): SnackbarStateType {
  switch (action.type) {
    case SHOW_SNACKBAR:
      return { messageId: (action as ShowSnackbarAction).payload.messageId };

    case HIDE_SNACKBAR:
      return { messageId: null };

    default:
      return state;
  }
}
