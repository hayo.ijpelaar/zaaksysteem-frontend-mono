// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SessionRootStateType } from './session.reducer';

export const hasUserDocumentIntakerRole = (store: SessionRootStateType) => {
  return Boolean(
    store.session.data?.logged_in_user.system_roles.includes('Documentintaker')
  );
};

export const getBagPreferences = (store: SessionRootStateType) => {
  return [
    store.session.data?.configurable.bag_priority_gemeentes,
    store.session.data?.configurable.bag_local_only,
  ];
};

export const isAdminSelector = (store: SessionRootStateType) => {
  return (store.session.data?.logged_in_user.capabilities || []).includes(
    'admin'
  );
};
