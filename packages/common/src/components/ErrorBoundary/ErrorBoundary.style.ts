// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const errorBoundaryStylesheet: StyleRulesCallback<any, {}> = () => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
  },
  titleWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontWeight: 600,
  },
  icon: {
    marginRight: 20,
  },
  stacktrace: {
    whiteSpace: 'pre-line',
  },
});
