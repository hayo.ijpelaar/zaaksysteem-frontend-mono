// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useOptionsListStylesheet } from './OptionsList.style';
import { OptionsListType } from './Options.types';

const OptionsList: React.FunctionComponent<OptionsListType> = ({
  provided,
  snapshot,
  children,
}) => {
  const classes = useOptionsListStylesheet();
  const [items] = children;

  return (
    <div
      {...provided.droppableProps}
      ref={provided.innerRef}
      className={classNames(classes.list, {
        [classes.hidden]: !items || !React.Children.count(items),
        [classes.draggingOver]: snapshot.isDraggingOver,
      })}
    >
      {children}
    </div>
  );
};

export default OptionsList;
