// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import TextField from '@mintlab/ui/App/Material/TextField/index';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const CurrencyField: FormFieldComponentType<string> = props => {
  const { value, readOnly } = props;

  return readOnly && !value ? (
    <ReadonlyValuesContainer value={value} />
  ) : (
    <TextField
      {...props}
      formatType="eurCurrency"
      value={value as string}
      displayType={readOnly ? 'text' : 'input'}
    />
  );
};
