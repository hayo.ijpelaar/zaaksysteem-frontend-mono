// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type CaseRoleFinderConfigType = {
  valueResolver?: (role: RoleAndSubjectType) => string;
  subLabelResolver?: (role: RoleAndSubjectType) => string;
  itemFilter?: (role: RoleAndSubjectType) => boolean;
  caseUuid: string;
};

type SubjectType =
  | APICaseManagement.PersonEntity
  | APICaseManagement.EmployeeEntity
  | APICaseManagement.OrganizationEntity;

export type RoleAndSubjectType = {
  role: APICaseManagement.SubjectRelationEntity;
  subject?: SubjectType;
};
