// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { FormType, AnyTableRowType, AnyRecordType } from '../../types';

const emptyFilter = (part: any) => part;
const getFormattedAddress = (address: any) => {
  return address
    ? address.foreign_address_line1
      ? [
          address.foreign_address_line1,
          address.foreign_address_line2,
          address.foreign_address_line3,
          address.country.instance.label,
        ]
          .filter(emptyFilter)
          .join(', ')
      : [
          [address.street, address.street_number, address.street_letter]
            .filter(emptyFilter)
            .join(' '),
          address.zipcode,
          address.city,
        ]
          .filter(emptyFilter)
          .join(', ')
    : '';
};

export const getFormattedRows = ({
  rows,
  formType,
}: {
  rows: AnyRecordType[];
  formType: FormType;
}): AnyTableRowType[] => {
  if (formType === 'person') {
    return rows.map(row => {
      const subject = row.instance.subject.instance;
      const {
        gender,
        address_residence,
        address_correspondence,
        date_of_death,
        is_secret,
      } = subject;

      const name = [subject.first_names, subject.prefix, subject.family_name]
        .filter(emptyFilter)
        .join(' ');
      const dateOfBirth = fecha.format(
        new Date(subject.date_of_birth),
        'DD-MM-YYYY'
      );
      const address =
        address_residence?.instance || address_correspondence?.instance || null;

      return {
        uuid: row.uuid,
        type: 'person',
        name,
        dateOfBirth,
        address: getFormattedAddress(address),
        gender: gender || '',
        hasCorrespondenceAddress: Boolean(address_correspondence),
        isDeceased: Boolean(date_of_death),
        isSecret: Boolean(is_secret),
        isInResearch: !address,
      };
    });
  } else {
    return rows.map(row => {
      const subject = row.instance.subject.instance;
      const {
        company,
        coc_number,
        coc_location_number,
        address_residence,
        address_correspondence,
      } = subject;

      return {
        type: 'organization',
        uuid: row.uuid,
        name: company,
        cocNumber: coc_number,
        cocLocationNumber: coc_location_number,
        address: getFormattedAddress(
          address_residence?.instance ||
            address_correspondence?.instance ||
            null
        ),
      };
    });
  }
};
