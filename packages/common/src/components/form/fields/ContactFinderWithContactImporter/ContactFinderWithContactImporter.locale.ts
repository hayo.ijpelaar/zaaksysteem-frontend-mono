// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const locale = {
  nl: {
    import: 'Importeer een contact',
  },
};

export default locale;
