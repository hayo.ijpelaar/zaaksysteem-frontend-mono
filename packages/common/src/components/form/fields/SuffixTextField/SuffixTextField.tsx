// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { useStyles } from './SuffieldTextField.style';

type SuffixTextFieldConfigType = {
  suffix: string;
};

const SuffixTextField: React.ComponentType<
  FormFieldPropsType<any, SuffixTextFieldConfigType>
> = props => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <TextField {...props} />
      <div className={classes.suffix}>{props.config?.suffix}</div>
    </div>
  );
};

export default SuffixTextField;
