// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { FormFieldComponentType } from '../../types/Form2.types';
import { TextField } from '../TextField';
import { usePersonNumberStylesheet } from './PersonalNumber.style';
import { getPersonalNumber } from './getPersonalNumber';

const bulletPoint = '\u25CF';
const personalNumberMask = bulletPoint.repeat(9);

const PersonalNumber: FormFieldComponentType<string> = props => {
  const [value, setValue] = useState(personalNumberMask);
  const [t] = useTranslation('common');
  const classes = usePersonNumberStylesheet();
  const {
    readOnly,
    definition: {
      config: { uuid, allowedToView },
    },
  } = props;

  return readOnly ? (
    <div className={classes.wrapper}>
      <TextField {...props} value={value} />
      {allowedToView && value === personalNumberMask && (
        <Button
          className={classes.button}
          presets={['contained', 'primary']}
          action={() => getPersonalNumber(setValue, uuid)}
        >
          {t('forms.view')}
        </Button>
      )}
    </div>
  ) : (
    <TextField {...props} readOnly={false} />
  );
};

export default PersonalNumber;
