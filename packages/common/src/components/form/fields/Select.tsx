// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import SelectCmp from '@mintlab/ui/App/Zaaksysteem/Select';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const Select: FormFieldComponentType<any> = props => {
  const { value, readOnly } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={props.value} />
  ) : (
    <SelectCmp {...props} value={value || null} />
  );
};
