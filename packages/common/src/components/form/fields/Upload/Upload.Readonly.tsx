// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { FileSelect } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
//@ts-ignore
import { FileList, File } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { DocumentPreviewModal } from '../../../DocumentPreview';
import { FileObject } from './Upload.types';

type UploadReadonlyProps = {
  value: any[];
  multiValue: boolean;
  withPreview: boolean;
};

type PreviewPropsType = {
  title: string;
  downloadUrl: string;
  contentType: string;
  url: string;
};

/* eslint complexity: [2, 7] */
const UploadReadonly: React.ComponentType<UploadReadonlyProps> = ({
  value,
  multiValue,
  withPreview,
}) => {
  const [previewItem, setPreviewItem] = React.useState(
    null as PreviewPropsType | null
  );
  const openPreviewModal = (uuid: string) => {
    request('GET', '/api/v2/document/get_document?document_uuid=' + uuid).then(
      response => {
        setPreviewItem({
          title: response.data.attributes.name,
          contentType: response.data.attributes.mimetype,
          url: '/api/v2/document/preview_document?id=' + uuid,
          downloadUrl: '/api/v2/document/download_document?id=' + uuid,
        });
      }
    );
  };

  const mapFiles = (file: FileObject) => {
    const status = () =>
      file.status
        ? {
            status: file.status,
          }
        : {};

    return (
      <File
        key={file.key}
        name={file.label}
        {...status()}
        onLinkClick={
          withPreview
            ? () => openPreviewModal(file.value || file.uuid || '')
            : false
        }
      />
    );
  };

  return (
    <React.Fragment>
      <FileSelect
        hasFiles={value && value.length}
        multiValue={multiValue}
        fileList={<FileList>{(value || []).map(mapFiles)}</FileList>}
        applyBackground={false}
      />
      {withPreview && (
        <DocumentPreviewModal
          open={previewItem !== null}
          onClose={() => setPreviewItem(null)}
          {...(previewItem || {})}
        />
      )}
    </React.Fragment>
  );
};

export default UploadReadonly;
