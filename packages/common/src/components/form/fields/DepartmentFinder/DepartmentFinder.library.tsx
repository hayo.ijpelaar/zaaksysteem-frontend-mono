// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { DepartmentFinderOptionType } from './DepartmentFinder.types';

export const fetchDepartmentChoices =
  (onServerError: OpenServerErrorDialogType) => async () => {
    const body = await request<APICaseManagement.GetDepartmentsResponseBody>(
      'GET',
      '/api/v2/cm/authorization/get_departments'
    ).catch(onServerError);

    const data = body
      ? body.data.map(department => ({
          id: department.id,
          name: department.attributes?.name || '',
          parent: department?.relationships?.parent?.data?.id || null,
        }))
      : [];

    const optionsReducer =
      (depth: number) =>
      (
        acc: DepartmentFinderOptionType[],
        current: { name: string; id: string }
      ) => {
        acc = [
          ...acc,
          {
            label: (
              <Fragment>
                <span style={{ paddingLeft: depth * 12 }}>&nbsp;</span>
                {current.name}
              </Fragment>
            ),
            value: current.id,
            data: {
              textLabel: current.name,
            },
          },
        ];

        const children = data.filter(
          thisItem => thisItem.parent === current.id
        );
        if (children.length) {
          acc = children.reduce(optionsReducer(depth + 1), acc);
        }
        return acc;
      };

    return data
      .filter(thisItem => thisItem.parent === null)
      .reduce(optionsReducer(0), []);
  };
