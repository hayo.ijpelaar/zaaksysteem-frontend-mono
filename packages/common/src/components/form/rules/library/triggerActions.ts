// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormDefinition,
  AnyFormDefinitionField,
  MultiStepFormDefinition,
} from '../../types';
import { isSteppedForm } from '../../library/formHelpers';
import { RuleEngineActionExecution } from './actions';

function triggerActionsOnFields<Values>(
  actions: RuleEngineActionExecution[],
  fields: AnyFormDefinitionField<Values>[]
): AnyFormDefinitionField<Values>[] {
  return actions.reduce((thisFields, action) => action(thisFields), fields);
}

export function triggerActions<Values>(
  actions: RuleEngineActionExecution[],
  formDefinition: FormDefinition<Values>
): FormDefinition<Values> {
  return isSteppedForm<Values>(formDefinition)
    ? formDefinition.reduce<MultiStepFormDefinition<Values>>(
        (acc, step) => [
          ...acc,
          {
            ...step,
            fields: triggerActionsOnFields(actions, step.fields),
          },
        ],
        []
      )
    : triggerActionsOnFields(actions, formDefinition);
}

export default triggerActions;
