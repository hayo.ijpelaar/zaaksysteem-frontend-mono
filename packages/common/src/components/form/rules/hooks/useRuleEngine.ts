// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useMemo, useEffect } from 'react';
import { FormDefinition } from '../../types/formDefinition.types';
import { UseRuleEngineType } from '../../types/formRenderer.types';
import getValuesFromDefinition from '../../library/getValuesFromFormDefinition';
import {
  getFormDefinitionAfterRules,
  getValuesDiff,
  isFormDefinitionUpdated,
} from './helpers';

export function useRuleEngine<FormShape = any>({
  formDefinition,
  setValues,
  setFieldValue,
  setFormDefinition,
  values,
  namespace,
  rules = [],
}: UseRuleEngineType<FormShape>): [FormDefinition<FormShape>, any] {
  const updatedFormDefinition = useMemo(() => {
    return getFormDefinitionAfterRules(rules, formDefinition, values);
  }, [values]);

  const updatedValues = useMemo(() => {
    return getValuesFromDefinition<FormShape>(updatedFormDefinition);
  }, [updatedFormDefinition]);

  const mergedValues = {
    ...values,
    ...updatedValues,
  };

  const valueDiff = getValuesDiff<FormShape>(
    values as Partial<FormShape>,
    mergedValues as Partial<FormShape>
  );

  const hasUpdatedValues = Boolean(Object.keys(valueDiff).length);

  const hasUpdatedFormDefinition = isFormDefinitionUpdated<FormShape>(
    formDefinition,
    updatedFormDefinition
  );

  const setFormikValuesOnChangeEffect = () => {
    if (hasUpdatedValues) {
      if (namespace) {
        setFieldValue(namespace, mergedValues);
      } else {
        setValues(mergedValues as FormShape);
      }
    }
  };

  const setFormDefinitionEffect = () => {
    if (hasUpdatedFormDefinition) {
      setFormDefinition(updatedFormDefinition);
    }
  };

  useEffect(setFormikValuesOnChangeEffect, [updatedValues]);
  useEffect(setFormDefinitionEffect, [updatedFormDefinition]);

  return [
    hasUpdatedFormDefinition ? updatedFormDefinition : formDefinition,
    hasUpdatedValues ? mergedValues : values,
  ];
}

export default useRuleEngine;
