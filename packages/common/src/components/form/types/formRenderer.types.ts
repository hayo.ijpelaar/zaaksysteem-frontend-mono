// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ComponentType } from 'react';
import { FormikProps, FormikTouched, FormikErrors } from 'formik';
import { Rule } from '../rules';
import { ValidationRule } from '../validation/createValidationRule';
import {
  FormValuesType,
  RegisterValidationForNamespaceType,
} from './generic.types';
import { FormDefinition } from './formDefinition.types';

type UseFormBaseType<Values> = {
  formDefinition: FormDefinition<Values>;
  rules?: Rule[];
  fieldComponents?: {
    [key: string]: ComponentType<any>;
  };
  validationMap?: {
    [key: string]: ValidationRule;
  };
};

export type UseFormType<Values> = UseFormBaseType<Values> & {
  isInitialValid?: boolean;
  enableReinitialize?: boolean;
  onChange?: (values: FormValuesType<Values>) => void;
  onSubmit?: (values: Values, formikHelpers: any) => void | Promise<any>;
  onTouched?: (
    touched: FormikTouched<Values>,
    values: FormValuesType<Values>
  ) => void;
};

export type UseSubFormType<Values> = UseFormBaseType<Values> & {
  namespace: string;
  validateForm: () => void;
  formik: FormikProps<FormValuesType<Values>>;
  registerValidation: RegisterValidationForNamespaceType;
};

export type UseRuleEngineType<FormShape> = {
  formDefinition: FormDefinition<FormShape>;
  values: FormShape | Partial<FormShape>;
  rules: Rule[];
  setValues: (values: FormShape) => void;
  setFieldValue: (fieldName: string, value: any) => void;
  setFormDefinition: (formDefinition: FormDefinition<FormShape>) => void;
  namespace?: string;
};

export type UseFormStepsType<FormShape> = {
  formDefinition: FormDefinition<FormShape>;
  errors: FormikErrors<FormShape>;
  touched: FormikTouched<FormShape>;
  namespace?: string;
};
