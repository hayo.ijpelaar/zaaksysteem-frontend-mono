// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useMemo } from 'react';
import {
  FormDefinition,
  AnyFormDefinitionField,
  UseFormStepsType,
} from '../types/formDefinition.types';
import {
  isSteppedForm,
  getActiveSteps,
  isStepValid,
  isStepTouched,
} from '../library/formHelpers';
import { useNamespace } from './useNamespace';

export type FormStepType = {
  title: string;
  description?: string;
  isValid: boolean;
  isActive: boolean;
  isTouched: boolean;
};

export type UseFormStepsReturnType<FormShape> = {
  activeFields: AnyFormDefinitionField<FormShape>[];
  activeStepNumber: number;
  activeStepValid: boolean;
  activeStep: FormStepType;
  steps: FormStepType[];
  hasNextStep: boolean;
  hasPreviousStep: boolean;
  handleNextStep: () => void;
  handlePreviousStep: () => void;
};

function getActiveFields<FormShape>(
  formDefinition: FormDefinition<FormShape>,
  activeStepIndex: number
): AnyFormDefinitionField<FormShape>[] {
  return isSteppedForm(formDefinition)
    ? formDefinition[activeStepIndex].fields
    : formDefinition;
}

export function useFormSteps<FormShape>({
  formDefinition,
  errors,
  touched,
  namespace,
}: UseFormStepsType<FormShape>): UseFormStepsReturnType<FormShape> {
  const { withNameSpace } = useNamespace<FormShape>(namespace);
  const [activeStepNumber, setActiveStepNumber] = useState(1);
  const activeStepIndex = activeStepNumber - 1;

  const activeFields = useMemo(() => {
    return getActiveFields<FormShape>(formDefinition, activeStepIndex).map<
      AnyFormDefinitionField<FormShape>
    >(field => ({
      ...field,
      name: withNameSpace(field.name),
    }));
  }, [formDefinition, activeStepNumber]);

  const activeSteps = isSteppedForm(formDefinition)
    ? getActiveSteps(formDefinition)
    : [];

  const numSteps = activeSteps.length;

  const hasNextStep = activeStepNumber + 1 <= numSteps;
  const hasPreviousStep = activeStepNumber - 1 > 0;

  const handleNextStep = () => {
    if (hasNextStep) {
      setActiveStepNumber(activeStepNumber + 1);
    }
  };

  const handlePreviousStep = () => {
    if (hasPreviousStep) {
      setActiveStepNumber(activeStepNumber - 1);
    }
  };

  const steps = useMemo(() => {
    return isSteppedForm(formDefinition)
      ? activeSteps.map<FormStepType>(
          ({ title, description, fields }, index) => ({
            title,
            description,
            isValid: isStepValid(fields, errors, withNameSpace),
            isActive: index === activeStepIndex,
            isTouched: isStepTouched(fields, touched),
          })
        )
      : [];
  }, [formDefinition, errors, touched, activeStepIndex]);

  const activeStep = steps[activeStepIndex];

  return {
    activeFields,
    activeStepNumber,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    activeStep,
    activeStepValid: isStepValid(activeFields, errors, withNameSpace),
  };
}

export default useFormSteps;
