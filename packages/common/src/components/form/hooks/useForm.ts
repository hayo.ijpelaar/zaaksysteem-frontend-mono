// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useRef, useEffect, useState, useMemo } from 'react';
import { useFormik, FormikProps } from 'formik';
import { useDebouncedCallback } from 'use-debounce';
import { FormValuesType, UseFormType } from '../types/formDefinition.types';
import useValidation from '../validation/hooks/useValidation';
import getValuesFromDefinition from '../library/getValuesFromFormDefinition';
import useRuleEngine from '../rules/hooks/useRuleEngine';
import { useFormFields, UseFormFieldsReturnType } from './useFormFields';
import { useFormSteps, UseFormStepsReturnType } from './useFormSteps';

export type UseFormReturnType<FormShape> = {
  fields: UseFormFieldsReturnType<FormShape>;
  formik: FormikProps<FormValuesType<FormShape>>;
} & Omit<UseFormStepsReturnType<FormShape>, 'activeFields'>;

export function useForm<FormShape>({
  formDefinition,
  onSubmit,
  onChange,
  enableReinitialize = false,
  isInitialValid = false,
  validationMap = {},
  fieldComponents = {},
  rules = [],
}: UseFormType<FormShape>): UseFormReturnType<FormShape> {
  const [formDefinitionFromState, setFormDefinition] = useState(formDefinition);
  const initialValues = useMemo(
    () => getValuesFromDefinition<FormShape>(formDefinitionFromState),
    []
  );

  const { schema, validate, registerValidation } = useValidation<FormShape>({
    formDefinition: formDefinitionFromState,
    validationMap: validationMap || {},
  });

  const formik = useFormik<FormShape>({
    validate,
    onSubmit: onSubmit || (() => {}),
    enableReinitialize,
    initialValues,
    validateOnMount: !isInitialValid,
  });

  const [debouncedValidate] = useDebouncedCallback(
    () => formik.validateForm(),
    10
  );

  const [updatedFormDefinition, updatedValues] = useRuleEngine<FormShape>({
    rules,
    setFormDefinition,
    formDefinition: formDefinitionFromState,
    values: formik.values,
    setValues: formik.setValues,
    setFieldValue: formik.setFieldValue,
  });

  const { activeFields, ...restFormSteps } = useFormSteps<FormShape>({
    formDefinition: updatedFormDefinition,
    errors: formik.errors,
    touched: formik.touched,
  });

  const fields = useFormFields<FormShape>({
    formik,
    registerValidation,
    validateForm: debouncedValidate,
    formDefinitionFields: activeFields,
    values: updatedValues,
    customFields: fieldComponents,
  });

  const valueRef = useRef(updatedValues);

  const triggerOnChange = () => {
    if (valueRef.current !== updatedValues && onChange) {
      onChange(updatedValues);
    }

    valueRef.current = updatedValues;

    formik.validateForm();
  };

  const validateFormAfterUpdate = () => {
    debouncedValidate();
  };

  useEffect(triggerOnChange, [updatedValues]);
  useEffect(validateFormAfterUpdate, [schema]);

  return { fields, formik, ...restFormSteps };
}
