// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { H4, Body1 } from '@mintlab/ui/App/Material/Typography';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import { useSubHeaderStyles } from './Subheader.style';

export type SubHeaderPropsType = {
  title: string;
  titleSuffix?: string;
  titleSuffixClass?: string;
  description: string;
  styles?: ReturnType<typeof useSubHeaderStyles>;
};

export const SubHeader: React.FunctionComponent<SubHeaderPropsType> = ({
  title,
  description,
  titleSuffix,
  titleSuffixClass,
  styles,
}) => {
  const SubHeaderStyles = useSubHeaderStyles();
  const classes = styles || SubHeaderStyles;
  const descriptionId = unique('table-description');

  return (
    <div className={classes.wrapper}>
      <div className={classes.titleWrapper}>
        <H4 classes={{ root: classes.title }}>{title}</H4>
        <H4 classes={{ root: classNames(classes.title, titleSuffixClass) }}>
          {titleSuffix}
        </H4>
      </div>
      <Body1 classes={{ root: classes.description }} id={descriptionId}>
        {description}
      </Body1>
    </div>
  );
};
