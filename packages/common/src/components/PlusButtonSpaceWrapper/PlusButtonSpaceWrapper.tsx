// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { usePlusButtonSpaceWrapperStyle } from './PlusButtonSpaceWrapper.style';

const PlusButtonSpaceWrapper: React.FunctionComponent<{}> = ({ children }) => {
  const classes = usePlusButtonSpaceWrapperStyle();
  return <div className={classes.root}>{children}</div>;
};

export default PlusButtonSpaceWrapper;
