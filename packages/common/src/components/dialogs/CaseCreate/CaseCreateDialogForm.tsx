// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, Fragment } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source/unique';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import {
  Rule,
  hasValue,
  showFields,
  updateFields,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { getRequestorFilter, RequestorTitle } from './CaseCreateDialog.library';
import { CaseCreateDialogInnerPropsType } from './CaseCreateDialog.types';
import {
  getCaseCreateFormDefinition,
  CaseCreateFormValuesType,
} from './CaseCreateDialog.formDefinition';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

/**
 * This dialog can be opened in two configurations:
 *
 * 1) Without the 'contact' prop. In this case the user must search and find, or import
 * the requestor. The form will have rules that interact with the requestor and casetype fields.
 *
 * 2) With the 'contact' prop. The contactdetails are fetched from the backend and the contact option
 * is prefilled as the value. The field is locked and the user cannot change the requestor value.
 * No rules are added that interact with the requestor or casetype fields. The contact option's
 * label is shown at the top of the form.
 *
 */
export const CaseCreateDialogForm: React.ComponentType<
  CaseCreateDialogInnerPropsType
> = ({
  legacyDocumentId,
  documentName,
  contactOption,
  contactChannel,
  open,
  onClose,
  savedCaseType,
}) => {
  const [t] = useTranslation('');
  const prevRef = useRef();

  const caseTypeRequestorRule = new Rule<CaseCreateFormValuesType>()
    .when('caseType', hasValue)
    .then(fields => {
      const caseType = (
        fields.find((field: any) => field.name === 'caseType') as any
      ).value;

      const caseTypeChanged = prevRef.current !== caseType.value;
      prevRef.current = caseType.value;

      return fields.map(
        updateFields(['requestor'], {
          ...getRequestorFilter(caseType),
          disabled: false,
          ...(caseTypeChanged && { value: null, choices: [] }),
        })
      );
    })
    .else(fields =>
      fields.map(
        updateFields(['requestor'], {
          value: null,
          disabled: true,
          readOnly: true,
        })
      )
    );

  const rules = [
    new Rule<CaseCreateFormValuesType>()
      .when(fields => (fields.requestor.value as any)?.type === 'employee')
      .then(showFields(['recipient']))
      .else(fields =>
        fields.map(
          updateFields(['recipient'], {
            hidden: true,
            value: null,
          })
        )
      ),
    new Rule()
      .when(() => true)
      .then(
        transferDataAsConfig('caseType', 'requestor', 'caseMeta', 'value.data')
      ),
    ...(contactOption ? [] : [caseTypeRequestorRule]),
  ];

  const dialogEl = useRef(null);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition: getCaseCreateFormDefinition({
      t,
      prefillContactOption: contactOption,
      prefillCaseTypeOption: savedCaseType,
      contactChannel,
      documentName,
      legacyDocumentId,
    }),
    rules: rules,
  });

  const handleSubmit = () => {
    const {
      caseType: {
        data: { id },
      },
      contactChannel,
      requestor: { value: requestorId },
      recipient,
    } = values;

    const base = `/intern/aanvragen/${id}/`;
    if (window.top) {
      window.top.location.href = buildUrl(base, {
        ontvanger: recipient ? recipient.value : '',
        aanvrager: requestorId,
        contactkanaal: normalizeValue(contactChannel),
        document: legacyDocumentId,
      });
    }
  };

  return (
    <Fragment>
      <Dialog
        aria-label={t('ContactImporter:importContact')}
        open={open}
        onClose={onClose}
        ref={dialogEl}
        disableBackdropClick={true}
        //@ts-ignore
        fullWidth={true}
      >
        <DialogTitle
          elevated={true}
          id={unique()}
          title={t('caseCreate:title')}
          onCloseClick={onClose}
          scope={'todo'}
        />

        <DialogContent padded={true}>
          {contactOption && (
            <RequestorTitle contactOption={contactOption} t={t} />
          )}

          {fields.map(
            ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
              const props = cloneWithout(rest, 'definition', 'mode');
              if (contactOption && props.name === 'requestor') return null;

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? false : props.label}
                  compact={true}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    t={t}
                    containerRef={dialogEl.current}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </DialogContent>
        <Fragment>
          <DialogDivider />
          <DialogActions>
            {getDialogActions(
              {
                disabled: !isValid,
                text: t('common:forms.proceed'),
                action() {
                  handleSubmit();
                  onClose();
                },
              },
              {
                text: t('common:forms.cancel'),
                action: onClose,
              },
              'case-create-dialogd'
            )}
          </DialogActions>
        </Fragment>
      </Dialog>
    </Fragment>
  );
};
