// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  StoreShapeType,
  PresetLocations,
} from '../components/DocumentExplorer/types/types';
import {
  SetLoadingActionType,
  NavigateActionType,
  DoSearchActionType,
  SetItemsActionType,
  ToggleSelectActionType,
  SetSearchItemsAction,
} from './actions';

export const initialState: StoreShapeType = {
  location: PresetLocations.Home,
  path: [],
  items: [],
  loading: false,
  search: '',
  createDocumentsOpen: false,
};

/* eslint complexity: [2, 8] */
export const reducer = (
  state: StoreShapeType,
  action:
    | NavigateActionType
    | SetLoadingActionType
    | DoSearchActionType
    | SetItemsActionType
    | SetSearchItemsAction
    | ToggleSelectActionType
): StoreShapeType => {
  switch (action.type) {
    case 'navigate':
      return {
        ...state,
        location: action.payload.id,
      };

    case 'setLoading':
      return {
        ...state,
        loading: action.payload.loading,
      };
    case 'doSearch':
      return {
        ...state,
        search: action.payload.search,
        location: PresetLocations.Search,
      };
    case 'setItems': {
      const { items, path } = action.payload;
      return {
        ...state,
        items,
        path,
      };
    }
    case 'toggleSelected': {
      const { uuid } = action.payload;
      return {
        ...state,
        items: state.items.map(item => {
          return item.uuid === uuid
            ? { ...item, selected: !item.selected }
            : item;
        }),
      };
    }
    default:
      return state;
  }
};
