// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  DocumentItemType,
  ItemType,
} from '../../FileExplorer/types/FileExplorerTypes';

export const isDocument = (
  item: Omit<ItemType, 'modified'>
): item is DocumentItemType => {
  return item.type === 'document';
};
