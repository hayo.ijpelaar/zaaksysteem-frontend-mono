// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

export interface ActionWithPayload<P, T = string> extends Action<T> {
  payload: P;
}

export type ActionPayloadWithResponse<P = {}, R = {}> = P & {
  response: R;
};

export type ThunkActionWithPayload<
  State,
  Payload,
  ExtraArgument = {}
> = ThunkAction<void, State, ExtraArgument, ActionWithPayload<Payload>>;
