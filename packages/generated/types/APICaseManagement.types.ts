// Generated on: Wed Mar 02 2022 09:48:32 GMT+0100 (Central European Standard Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of CaseManagement domain.

/* eslint-disable */
export namespace APICaseManagement {
  export type GetDepartmentsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: 'department';
      meta?: {
        summary: string;
        [k: string]: any;
      };
      id: string;
      attributes?: {
        name: string;
        description?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          type: 'department';
          id: string;
          meta?: {
            summary: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface GetRolesRequestParams {
    filter?: {
      'relationships.parent.id'?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type GetRolesResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: 'role';
      id: string;
      attributes?: {
        name: string;
        description?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          type: 'department';
          id: string;
          meta?: {
            summary: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface GetCaseRequestParams {
    case_uuid: string;
    [k: string]: any;
  }

  export type GetCaseResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    data?: {
      data?: {
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * The public case number of this case
           */
          number?: number;
          /**
           * Short slug (description) of this case
           */
          summary?: string;
          /**
           * Short slug (description) of this case, for public use
           */
          public_summary?: string;
          /**
           * Defines whether this case is closed, open or suspended
           */
          status?: {
            name?: 'new' | 'open' | 'resolved' | 'stalled';
            /**
             * When prematurely closed or stalled, this is the reason
             */
            reason?: string;
            /**
             * When stalled: the since date
             */
            since?: string;
            /**
             * When stalled: the until date
             */
            until?: string;
            [k: string]: any;
          };
          /**
           * The date this case was registered
           */
          registration_date?: string;
          /**
           * Date when this case should be resolved
           */
          target_completion_date?: string;
          /**
           * Date when this case was resolved
           */
          completion_date?: string;
          /**
           * Date when this case is due for destruction
           */
          destruction_date?: string;
          /**
           * The phase and milestone this case is currently in
           */
          phase?: {
            label?: string;
            milestone_label?: string;
            sequence?: number;
            next_sequence?: number;
            [k: string]: any;
          };
          /**
           * Information about the result of this case
           */
          result?: {
            result?: string;
            result_id?: number;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * In which way this case ended in creation
           */
          contactchannel?:
            | 'behandelaar'
            | 'balie'
            | 'telefoon'
            | 'post'
            | 'email'
            | 'webformulier'
            | 'sociale media';
          /**
           * Specifies the payment information of a case
           */
          payment?: {
            amount?: number;
            status?: 'success' | 'failed' | 'pending' | 'offline';
            [k: string]: any;
          };
          /**
           * All user defined properties for this case
           */
          custom_fields?: {
            [k: string]: any;
          };
          /**
           * The confidentiality of this case, which defines the authorization path to use
           */
          confidentiality?: {
            mapped?: string;
            original?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        relationships?: {
          /**
           * Contains the subject (employee) who handles this case
           */
          assignee?: {
            data?: {
              type?: 'employee';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Contains the coordinator (employee) who handles this case
           */
          coordinator?: {
            data?: {
              type?: 'employee';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Contains the requestor (employee/company/person) who handles this case
           */
          requestor?: {
            data?: {
              type?: 'employee' | 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Department this case is currently assigned to
           */
          department?: {
            data?: {
              type?: 'department';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Location this case references to
           */
          location?: {
            data?: {
              type?: 'location';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Subject this case references to (most of the time the requestor of the case
           */
          subjects?: {
            data?: {
              type?: 'employee' | 'person' | 'organization';
              id?: string;
              meta?: {
                role?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Case type this case descended from
           */
          casetype?: {
            data?: {
              type?: 'casetype';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * List of related cases
           */
          related_cases?: {
            data?: {
              type?: 'case';
              id?: string;
              [k: string]: any;
            }[];
            [k: string]: any;
          };
          /**
           * List of related contacts
           */
          related_contacts?: {
            data?: {
              type?: string;
              id?: string;
              [k: string]: any;
            };
            meta?: {
              magic_string_prefix?: string;
              role?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Case relates to, e.g. a subject
         */
        relates_to?: {
          data?: {
            type?: 'subject';
            id?: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface GetCaseBasicRequestParams {
    case_uuid: string;
    [k: string]: any;
  }

  export type GetCaseBasicResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    data?: {
      data?: {
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * The public case number of this case
           */
          number?: number;
          case_type_uuid?: string;
          case_type_version_uuid?: string;
          /**
           * All user defined properties for this case
           */
          custom_fields?: {
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface SearchCaseRequestParams {
    page: number;
    page_size: number;
    'filter[attribute.status]'?: ('new' | 'open' | 'stalled' | 'resolved')[];
    'filter[relationship.case_type.id]'?: string[];
    'filter[relationship.assignee.id]'?: string[];
    'filter[relationship.coordinator.id]'?: string[];
    'filter[relationship.requestor.id]'?: string[];
    'filter[attributes.registration_date]'?: string[];
    'filter[attributes.completion_date]'?: string[];
    'filter[attributes.payment_status]'?: ('success' | 'pending' | 'failed')[];
    'filter[attributes.channel_of_contact]'?: (
      | 'behandelaar'
      | 'balie'
      | 'telefoon'
      | 'post'
      | 'email'
      | 'webformulier'
      | 'sociale media'
      | 'externe applicatie'
    )[];
    'filter[attributes.confidentiality]'?: (
      | 'public'
      | 'internal'
      | 'confidential'
    )[];
    'filter[attributes.archival_state]'?: ('vernietigen' | 'overdragen')[];
    'filter[attributes.retention_period_source_date]'?: (
      | 'vervallen'
      | 'onherroepelijk'
      | 'verwerking'
      | 'verleend'
      | 'einde dienstverband'
      | 'afhandeling'
      | 'geboorte'
      | 'geweigerd'
    )[];
    [k: string]: any;
  }

  export interface SearchCaseResponseBody {
    data: {
      attributes: {
        /**
         * Archival state of the case
         */
        archival_state?: string;
        /**
         * Assignee of the case
         */
        assignee: {
          /**
           * Name of the assignee of the case
           */
          name?: string;
          /**
           * UUID of the assignee of the case
           */
          uuid?: string;
          [k: string]: any;
        };
        /**
         * Title of the case_type
         */
        case_type_title?: string;
        /**
         * Destruction date of the case
         */
        destruction_date?: string;
        /**
         * Id of case
         */
        number: number;
        /**
         * Indicates with a percentage how much time is left to complete the case. The percentage will be above 100 if the remaining days are exeeded. When a case has the status stalled, then the value is null
         */
        percentage_days_left?: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Requestor of the case
         */
        requestor: {
          /**
           * Requestor Entity
           */
          data: {
            attributes: {
              /**
               * Another identifier in the dutch systems, called a_nummer
               */
              a_number?: string;
              /**
               * BSN number for the reqestor
               */
              burgerservicenummer?: string;
              /**
               * Chamber of Commerce number of the organization
               */
              coc?: string;
              /**
               * correspondence house number
               */
              correspondence_house_number?: string;
              /**
               * correspondence place of residence
               */
              correspondence_place_of_residence?: string;
              /**
               * correspondence address street value
               */
              correspondence_street?: string;
              /**
               * correspondence zipcode value
               */
              correspondence_zipcode?: string;
              /**
               * Requestor's country of birth
               */
              country_of_birth?: string;
              /**
               * country of residence for requestor
               */
              country_of_residence?: string;
              /**
               * Requestor's date of birth
               */
              date_of_birth?: string;
              /**
               * Date of divorce for requestor
               */
              date_of_divorce?: string;
              /**
               * Marriage date for requestor
               */
              date_of_marriage?: string;
              /**
               * Name of the department for requestor
               */
              department?: string;
              /**
               * Email address of requestor
               */
              email?: string;
              /**
               * Establishment number of requestor organization
               */
              establishment_number?: string;
              /**
               * Family name of the requestor
               */
              family_name?: string;
              /**
               * First names for the requestor
               */
              first_names?: string;
              /**
               * foreign residence address first line
               */
              foreign_residence_address_line1?: string;
              /**
               * foreign residence address second line
               */
              foreign_residence_address_line2?: string;
              /**
               * foreign residence address third line
               */
              foreign_residence_address_line3?: string;
              /**
               * Full name of the requestor
               */
              full_name?: string;
              /**
               * Gender of the requestor
               */
              gender?: string;
              /**
               * Indicate if requestor has correspondence address
               */
              has_correspondence_address?: boolean;
              /**
               * House number for the requestor
               */
              house_number?: string;
              /**
               * Id of the requestor
               */
              id?: string;
              /**
               * Initials for the requestor
               */
              initials?: string;
              /**
               * investigation value for reqestor
               */
              investigation?: string;
              /**
               * If its secret value
               */
              is_secret?: boolean;
              /**
               * login details for requestor
               */
              login?: string;
              /**
               * Contact mobile number of requestor
               */
              mobile_number?: string;
              /**
               * Name of the requestor
               */
              name?: string;
              /**
               * Title of the requestor
               */
              noble_title?: string;
              /**
               * Password details for requestor
               */
              password?: string;
              /**
               * contact phone number of requestor
               */
              phone_number?: string;
              /**
               * Requestor's place of birth
               */
              place_of_birth?: string;
              /**
               * place of residence for requestor
               */
              place_of_residence?: string;
              /**
               * Type of the requestor
               */
              requestor_type?: string;
              /**
               * Residence house number for requestor
               */
              residence_house_number?: string;
              /**
               * Residence address street name
               */
              residence_street?: string;
              /**
               * Residence address zipcode
               */
              residence_zipcode?: string;
              /**
               * Salutation used for requestor
               */
              salutation?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation1?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation2?: string;
              /**
               * Status of requestor
               */
              status?: string;
              /**
               * Address street name
               */
              street?: string;
              /**
               * type of the subject
               */
              subject_type?: string;
              /**
               * Surname of the requestor
               */
              surname?: string;
              /**
               * Surname prefix of the requestor
               */
              surname_prefix?: string;
              /**
               * Title for the requestor
               */
              title?: string;
              /**
               * Trade name for requestor organization
               */
              trade_name?: string;
              /**
               * Type of the business
               */
              type_of_business_entity?: string;
              /**
               * Unique name for the requestor
               */
              unique_name?: string;
              /**
               * Identifier of case type
               */
              used_name?: string;
              /**
               * Zipcode value
               */
              zipcode?: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Status of case
         */
        status: string;
        /**
         * Subject of te case
         */
        subject?: string;
        /**
         * Count of unaccepted updates
         */
        unaccepted_attribute_update_count: number;
        /**
         * Count of unaccepted files
         */
        unaccepted_files_count: number;
        /**
         * Number of unread messages
         */
        unread_message_count: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetCompletionDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetCompletionDateRequestBody {
    target_date?: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface SetTargetCompletionDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetTargetCompletionDateRequestBody {
    target_date: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface SetRegistrationDateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetRegistrationDateRequestBody {
    target_date: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface AssignCaseToSelfResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToSelfRequestBody {
    case_uuid: string;
    [k: string]: any;
  }

  export interface ChangeCaseCoordinatorResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ChangeCaseCoordinatorRequestBody {
    case_uuid: string;
    coordinator_uuid: string;
    [k: string]: any;
  }

  export interface AssignCaseToUserResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToUserRequestBody {
    /**
     * UUID of the case to be updated
     */
    case_uuid: string;
    /**
     * UUID of the user to be assigned to case.
     */
    user_uuid?: string;
    [k: string]: any;
  }

  export interface AssignCaseToDepartmentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AssignCaseToDepartmentRequestBody {
    /**
     * UUID of the case to be updated
     */
    case_uuid: string;
    /**
     * UUID of the department to be assigned to case.
     */
    department_uuid?: string;
    /**
     * UUID of the role to be assigned to case.
     */
    role_uuid?: string;
    [k: string]: any;
  }

  export interface PauseCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface PauseCaseRequestBody {
    case_uuid: string;
    suspension_reason: string;
    suspension_term_value?: number | string;
    suspension_term_type:
      | 'weeks'
      | 'work_days'
      | 'calendar_days'
      | 'fixed_date'
      | 'indefinite';
    [k: string]: any;
  }

  export interface ResumeCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ResumeCaseRequestBody {
    case_uuid: string;
    resume_reason: string;
    stalled_since_date: string;
    stalled_until_date: string;
    [k: string]: any;
  }

  export interface CreateCaseResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCaseRequestBody {
    /**
     * UUID of the case to be created. If case_uuid field is missing, then back-end will generate one
     */
    case_uuid: string;
    /**
     * UUID of the Casetype the case descended from.
     */
    case_type_version_uuid: string;
    /**
     * In which way the case ended in creation
     */
    contact_channel:
      | 'behandelaar'
      | 'balie'
      | 'telefoon'
      | 'post'
      | 'email'
      | 'webformulier'
      | 'sociale media';
    /**
     * Requestor (employee/organization/person) of case
     */
    requestor: {
      /**
       * Type of the requestor(employee/organization/person)
       */
      type?: 'employee' | 'person' | 'organization';
      /**
       * UUID of the requestor
       */
      id?: string;
      [k: string]: any;
    };
    /**
     * Confidentiality for the case.
     */
    confidentiality?: 'public' | 'internal' | 'confidential';
    /**
     * Custom fields for the case.
     */
    custom_fields?: {
      [k: string]: (
        | number
        | string
        | string[]
        | {
            [k: string]: any;
          }
      )[];
    };
    assignment?:
      | {
          role?: {
            id: string;
            type: 'role';
          };
          department?: {
            id: string;
            type: 'department';
          };
        }
      | {
          employee?: {
            id: string;
            type: 'employee';
            use_employee_department?: boolean;
            send_email_notification?: boolean;
          };
        };
    /**
     * Contact information for case
     */
    contact_information?: {
      /**
       * Mobile number of requestor
       */
      mobile_number?: string;
      /**
       * Phone number of requestor
       */
      phone_number?: string;
      /**
       * Email address for the requestor
       */
      email?: string;
    };
    options?: {
      allow_missing_required_fields?: boolean;
    };
  }

  export interface CreateCaseRelationRequestParams {
    uuid1: string;
    uuid2: string;
    [k: string]: any;
  }

  export interface CreateCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCaseTypeVersionRequestParams {
    version_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseTypeVersionResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified: string;
        created: string;
        summary: string;
        is_eligible_for_case_creation: boolean;
      };
      attributes: {
        /**
         * Indicates that this casetype is a 'mother' of child casetypes
         */
        is_parent: boolean;
        /**
         * Fullname of the casetype
         */
        name: string;
        /**
         * Userdefined identification of this casetype
         */
        identification: string;
        /**
         * Describes this casetype in detail
         */
        description: string;
        /**
         * List of tags defining this casetype, used for faster searching
         */
        tags: string;
        /**
         * A templating line of text which defines the summary of a generated case
         */
        case_summary: string;
        /**
         * A templating line of text which defines the summary of a generated case which we use on public communication
         */
        case_public_summary: string;
        settings: {
          custom_webform: string;
          reuse_casedata: boolean;
          enable_webform: boolean;
          enable_online_payment: boolean;
          enable_manual_payment: boolean;
          require_email_on_webform: boolean;
          require_phonenumber_on_webform: boolean;
          require_mobilenumber_on_webform: boolean;
          disable_captcha_for_predefined_requestor: boolean;
          show_confidentiality: boolean;
          show_contact_info: boolean;
          enable_subject_relations_on_form: boolean;
          open_case_on_create: boolean;
          enable_allocation_on_form: boolean;
          disable_pip_for_requestor: boolean;
          lock_registration_phase: boolean;
          enable_queue_for_changes_from_other_subjects: boolean;
          check_acl_on_allocation: boolean;
          api_can_transition: boolean;
          is_public: boolean;
          list_of_default_folders: {
            parent?: string;
            name?: string;
            [k: string]: any;
          }[];
          text_confirmation_message_title: string;
          text_public_confirmation_message: string;
          payment: {
            assignee: {
              amount: number;
            };
            frontdesk: {
              amount: number;
            };
            phone: {
              amount: number;
            };
            mail: {
              amount: number;
            };
            email: {
              amount: number;
            };
            webform: {
              amount: number;
            };
          };
        };
        /**
         * Extra information about this casetype for archiving and other purposes
         */
        metadata: {
          /**
           * Whether postponing a case is a possibility
           */
          may_postpone: boolean;
          /**
           * Whether extending the duration of a case is a possibility
           */
          may_extend: boolean;
          /**
           * Amount of days a case can be extended
           */
          extension_period: number;
          /**
           * Amount of days a case can be adjourned
           */
          adjourn_period: number;
          /**
           * Link to the e-form of this casetype
           */
          'e-webform': string;
          process_description: string;
          /**
           * Motivation of this casetype (NL: Aanleiding)
           */
          motivation: string;
          /**
           * Purpose of this casetype (NL: Doel)
           */
          purpose: string;
          /**
           * Classification for archiving (NL: Archiefclassificatiecode)
           */
          archive_classification_code: string;
          /**
           * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
           */
          designation_of_confidentiality:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          /**
           * The person, team, department or other subject responsible (NL: Verantwoordelijke)
           */
          responsible_subject: string;
          /**
           * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
           */
          responsible_relationship: string;
          /**
           * Whether its possible to object and appeal to this casetype
           */
          possibility_for_objection_and_appeal: boolean;
          /**
           * Whether a decision needs to be published
           */
          publication: boolean;
          /**
           * The text used for publishing decisions of this casetype
           */
          publication_text: string;
          bag: boolean;
          /**
           * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
           */
          lex_silencio_positivo: boolean;
          /**
           * Whether a penalty is required (NL: Wet dwangsom)
           */
          penalty_law: boolean;
          /**
           * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
           */
          wkpb_applies: boolean;
          /**
           * The legal basis (NL: Wettelijke grondslag) of this casetype
           */
          legal_basis: string;
          /**
           * The legal basis (NL: Lokale grondslag) of this casetype
           */
          local_basis: string;
        };
        initiator_type:
          | 'aangaan'
          | 'aangeven'
          | 'aanmelden'
          | 'aanschrijven'
          | 'aanvragen'
          | 'afkopen'
          | 'afmelden'
          | 'indienen'
          | 'inschrijven'
          | 'melden'
          | 'ontvangen'
          | 'opstellen'
          | 'opzeggen'
          | 'registreren'
          | 'reserveren'
          | 'starten'
          | 'stellen'
          | 'uitvoeren'
          | 'vaststellen'
          | 'versturen'
          | 'voordragen'
          | 'vragen';
        initiator_source: 'internal' | 'external' | 'all';
        terms: {
          /**
           * The maximum duration of a case according to the law
           */
          lead_time_legal: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_legal' a case may take
             */
            value: string | number;
          };
          /**
           * The maximum duration of a case we would like
           */
          lead_time_service: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_service' a case may take
             */
            value: string | number;
          };
        };
        requestor: {
          type_of_requestors: (
            | 'niet_natuurlijk_persoon'
            | 'preset_client'
            | 'natuurlijk_persoon_na'
            | 'natuurlijk_persoon'
            | 'medewerker'
          )[];
          predefined_requestor?: string;
          use_for_correspondence: boolean;
        };
        relates_to?: {
          subject?: {
            type_of_requestors?: string[];
            predefined_requestor?: string;
            [k: string]: any;
          };
          location?: {
            location_from?: 'requestor' | 'attribute';
            [k: string]: any;
          };
          [k: string]: any;
        };
        phases: {
          name?: string;
          milestone?: number;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          custom_fields?: {
            name: string;
            public_name: string;
            title: string;
            title_multiple: string;
            description: string;
            is_required: boolean;
            external_description: string;
            publish_on: {
              name: string;
            }[];
            requestor_can_change_from_pip: boolean;
            is_hidden_field: boolean;
            edit_authorizations: {
              [k: string]: any;
            }[];
            enable_skip_of_queue: boolean;
            sensitive_data: boolean;
            field_magic_string: string;
            field_type: 'text' | 'option' | 'checkbox';
            field_options: string[];
            default_value: string;
            referential: boolean;
            date_field_limit: {
              start?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
              end?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
            };
          }[];
          rules?: {
            label?: string;
            conditional_type?: 'AND' | 'OR';
            when?: {
              condition?: string;
              values?: string[];
              immediate?: boolean;
              [k: string]: any;
            };
            then?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            else?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          document_templates?: {
            related_document_template_element?: string;
            description?: string;
            generate_on_transition?: boolean;
            /**
             * Add to this custom field of type 'document'
             */
            add_to_case_document?: string;
            storage_format?: 'odt' | 'pdf' | 'docx';
            [k: string]: any;
          }[];
          cases?: {
            type_of_relation?:
              | 'deelzaak'
              | 'gerelateerd'
              | 'vervolgzaak'
              | 'vervolgzaak_datum';
            related_casetype_element?: string;
            allocation?: {
              department?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              role?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            requestor?: {
              requestor_type?:
                | 'aanvrager'
                | 'betrokkene'
                | 'anders'
                | 'behandelaar'
                | 'ontvanger';
              related_role?: string;
              /**
               * Not implemented yet
               */
              custom_subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Phase number before which this case must be resolved
             */
            resolve_before_phase?: number;
            copy_custom_fields_from_parent?: boolean;
            start_on_transition?: boolean;
            /**
             * Automatically open the case (in behandeling)
             */
            open_case_on_create?: boolean;
            /**
             * Not implemented yet
             */
            related_subject?: {
              subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              role?: string;
              magic_string_prefix?: string;
              /**
               * Authorized to view this case on the PIP (NL: gemachtigde)
               */
              authorized_subject?: boolean;
              send_confirmation_message?: boolean;
              [k: string]: any;
            };
            show_in_pip?: boolean;
            label_in_pip?: string;
            [k: string]: any;
          }[];
          emails?: {
            related_email_element?: string;
            recipient?: {
              recipient_type?:
                | 'requestor'
                | 'assignee'
                | 'coordinator'
                | 'employee'
                | 'authorized_subject'
                | 'related_subject'
                | 'custom';
              id?: string;
              role?: string;
              emailaddress?: string;
              [k: string]: any;
            };
            cc?: string;
            bcc?: string;
            send_on_transition?: boolean;
            [k: string]: any;
          }[];
          subjects?: {
            subject_type?: 'person' | 'employee';
            id?: string;
            role?: string;
            magic_string_prefix?: string;
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          }[];
          results?: {
            title?: string;
            is_default_value?: boolean;
            result?: string;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              selection_list_source_date?: string;
              selection_list_end_date?: string;
              activate_period_of_preservation?: boolean;
              type_of_archiving?: 'vernietigen';
              period_of_preservation?: string;
              archiving_procedure?: 'Afhandeling';
              description?: string;
              selection_list_number?: string;
              process_type_number?: string;
              process_type_name?: string;
              process_type_description?: string;
              process_type_object?: string;
              process_type_generic?: 'generic' | 'specific';
              origin?: string;
              process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          checklist_items?: string[];
          [k: string]: any;
        }[];
        authorizations?: {
          [k: string]: any;
        }[];
        changelog?: {
          summary?: string;
          changes?: {
            [k: string]: any;
          }[];
          [k: string]: any;
        };
      };
      relationships?: {
        /**
         * In which folder this casetype resides
         */
        catalogue_folder?: {
          data?: {
            type?: 'folder';
            id?: string;
            meta?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Related parent casetype
         */
        parent?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of child casetypes of this mother casetype
         */
        children?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    [k: string]: any;
  }

  export interface GetCaseTypeActiveVersionRequestParams {
    case_type_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseTypeActiveVersionResponseBody {
    data?: {
      type: string;
      id: string;
      meta: {
        last_modified: string;
        created: string;
        summary: string;
        is_eligible_for_case_creation: boolean;
      };
      attributes: {
        /**
         * Indicates that this casetype is a 'mother' of child casetypes
         */
        is_parent: boolean;
        /**
         * Fullname of the casetype
         */
        name: string;
        /**
         * Userdefined identification of this casetype
         */
        identification: string;
        /**
         * Describes this casetype in detail
         */
        description: string;
        /**
         * List of tags defining this casetype, used for faster searching
         */
        tags: string;
        /**
         * A templating line of text which defines the summary of a generated case
         */
        case_summary: string;
        /**
         * A templating line of text which defines the summary of a generated case which we use on public communication
         */
        case_public_summary: string;
        settings: {
          custom_webform: string;
          reuse_casedata: boolean;
          enable_webform: boolean;
          enable_online_payment: boolean;
          enable_manual_payment: boolean;
          require_email_on_webform: boolean;
          require_phonenumber_on_webform: boolean;
          require_mobilenumber_on_webform: boolean;
          disable_captcha_for_predefined_requestor: boolean;
          show_confidentiality: boolean;
          show_contact_info: boolean;
          enable_subject_relations_on_form: boolean;
          open_case_on_create: boolean;
          enable_allocation_on_form: boolean;
          disable_pip_for_requestor: boolean;
          lock_registration_phase: boolean;
          enable_queue_for_changes_from_other_subjects: boolean;
          check_acl_on_allocation: boolean;
          api_can_transition: boolean;
          is_public: boolean;
          list_of_default_folders: {
            parent?: string;
            name?: string;
            [k: string]: any;
          }[];
          text_confirmation_message_title: string;
          text_public_confirmation_message: string;
          payment: {
            assignee: {
              amount: number;
            };
            frontdesk: {
              amount: number;
            };
            phone: {
              amount: number;
            };
            mail: {
              amount: number;
            };
            email: {
              amount: number;
            };
            webform: {
              amount: number;
            };
          };
        };
        /**
         * Extra information about this casetype for archiving and other purposes
         */
        metadata: {
          /**
           * Whether postponing a case is a possibility
           */
          may_postpone: boolean;
          /**
           * Whether extending the duration of a case is a possibility
           */
          may_extend: boolean;
          /**
           * Amount of days a case can be extended
           */
          extension_period: number;
          /**
           * Amount of days a case can be adjourned
           */
          adjourn_period: number;
          /**
           * Link to the e-form of this casetype
           */
          'e-webform': string;
          process_description: string;
          /**
           * Motivation of this casetype (NL: Aanleiding)
           */
          motivation: string;
          /**
           * Purpose of this casetype (NL: Doel)
           */
          purpose: string;
          /**
           * Classification for archiving (NL: Archiefclassificatiecode)
           */
          archive_classification_code: string;
          /**
           * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
           */
          designation_of_confidentiality:
            | 'Openbaar'
            | 'Beperkt openbaar'
            | 'Intern'
            | 'Zaakvertrouwelijk'
            | 'Vertrouwelijk'
            | 'Confidentieel'
            | 'Geheim'
            | 'Zeer geheim';
          /**
           * The person, team, department or other subject responsible (NL: Verantwoordelijke)
           */
          responsible_subject: string;
          /**
           * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
           */
          responsible_relationship: string;
          /**
           * Whether its possible to object and appeal to this casetype
           */
          possibility_for_objection_and_appeal: boolean;
          /**
           * Whether a decision needs to be published
           */
          publication: boolean;
          /**
           * The text used for publishing decisions of this casetype
           */
          publication_text: string;
          bag: boolean;
          /**
           * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
           */
          lex_silencio_positivo: boolean;
          /**
           * Whether a penalty is required (NL: Wet dwangsom)
           */
          penalty_law: boolean;
          /**
           * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
           */
          wkpb_applies: boolean;
          /**
           * The legal basis (NL: Wettelijke grondslag) of this casetype
           */
          legal_basis: string;
          /**
           * The legal basis (NL: Lokale grondslag) of this casetype
           */
          local_basis: string;
        };
        initiator_type:
          | 'aangaan'
          | 'aangeven'
          | 'aanmelden'
          | 'aanschrijven'
          | 'aanvragen'
          | 'afkopen'
          | 'afmelden'
          | 'indienen'
          | 'inschrijven'
          | 'melden'
          | 'ontvangen'
          | 'opstellen'
          | 'opzeggen'
          | 'registreren'
          | 'reserveren'
          | 'starten'
          | 'stellen'
          | 'uitvoeren'
          | 'vaststellen'
          | 'versturen'
          | 'voordragen'
          | 'vragen';
        initiator_source: 'internal' | 'external' | 'all';
        terms: {
          /**
           * The maximum duration of a case according to the law
           */
          lead_time_legal: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_legal' a case may take
             */
            value: string | number;
          };
          /**
           * The maximum duration of a case we would like
           */
          lead_time_service: {
            type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
            /**
             * The value of 'lead_time_service' a case may take
             */
            value: string | number;
          };
        };
        requestor: {
          type_of_requestors: (
            | 'niet_natuurlijk_persoon'
            | 'preset_client'
            | 'natuurlijk_persoon_na'
            | 'natuurlijk_persoon'
            | 'medewerker'
          )[];
          predefined_requestor?: string;
          use_for_correspondence: boolean;
        };
        relates_to?: {
          subject?: {
            type_of_requestors?: string[];
            predefined_requestor?: string;
            [k: string]: any;
          };
          location?: {
            location_from?: 'requestor' | 'attribute';
            [k: string]: any;
          };
          [k: string]: any;
        };
        phases: {
          name?: string;
          milestone?: number;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          custom_fields?: {
            name: string;
            public_name: string;
            title: string;
            title_multiple: string;
            description: string;
            is_required: boolean;
            external_description: string;
            publish_on: {
              name: string;
            }[];
            requestor_can_change_from_pip: boolean;
            is_hidden_field: boolean;
            edit_authorizations: {
              [k: string]: any;
            }[];
            enable_skip_of_queue: boolean;
            sensitive_data: boolean;
            field_magic_string: string;
            field_type: 'text' | 'option' | 'checkbox';
            field_options: string[];
            default_value: string;
            referential: boolean;
            date_field_limit: {
              start?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
              end?: {
                active: boolean;
                interval_type: 'days' | 'weeks' | 'months' | 'years';
                interval: number;
                during: 'pre' | 'post';
                refernce: 'current';
              };
            };
          }[];
          rules?: {
            label?: string;
            conditional_type?: 'AND' | 'OR';
            when?: {
              condition?: string;
              values?: string[];
              immediate?: boolean;
              [k: string]: any;
            };
            then?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            else?: {
              action?: string;
              attribute?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          document_templates?: {
            related_document_template_element?: string;
            description?: string;
            generate_on_transition?: boolean;
            /**
             * Add to this custom field of type 'document'
             */
            add_to_case_document?: string;
            storage_format?: 'odt' | 'pdf' | 'docx';
            [k: string]: any;
          }[];
          cases?: {
            type_of_relation?:
              | 'deelzaak'
              | 'gerelateerd'
              | 'vervolgzaak'
              | 'vervolgzaak_datum';
            related_casetype_element?: string;
            allocation?: {
              department?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              role?: {
                id?: number;
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            requestor?: {
              requestor_type?:
                | 'aanvrager'
                | 'betrokkene'
                | 'anders'
                | 'behandelaar'
                | 'ontvanger';
              related_role?: string;
              /**
               * Not implemented yet
               */
              custom_subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Phase number before which this case must be resolved
             */
            resolve_before_phase?: number;
            copy_custom_fields_from_parent?: boolean;
            start_on_transition?: boolean;
            /**
             * Automatically open the case (in behandeling)
             */
            open_case_on_create?: boolean;
            /**
             * Not implemented yet
             */
            related_subject?: {
              subject?: {
                subject_type?: 'person' | 'organization';
                id?: string;
                [k: string]: any;
              };
              role?: string;
              magic_string_prefix?: string;
              /**
               * Authorized to view this case on the PIP (NL: gemachtigde)
               */
              authorized_subject?: boolean;
              send_confirmation_message?: boolean;
              [k: string]: any;
            };
            show_in_pip?: boolean;
            label_in_pip?: string;
            [k: string]: any;
          }[];
          emails?: {
            related_email_element?: string;
            recipient?: {
              recipient_type?:
                | 'requestor'
                | 'assignee'
                | 'coordinator'
                | 'employee'
                | 'authorized_subject'
                | 'related_subject'
                | 'custom';
              id?: string;
              role?: string;
              emailaddress?: string;
              [k: string]: any;
            };
            cc?: string;
            bcc?: string;
            send_on_transition?: boolean;
            [k: string]: any;
          }[];
          subjects?: {
            subject_type?: 'person' | 'employee';
            id?: string;
            role?: string;
            magic_string_prefix?: string;
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          }[];
          results?: {
            title?: string;
            is_default_value?: boolean;
            result?: string;
            archival_attributes?: {
              state?: 'vernietigen' | 'overdragen';
              selection_list?: string;
              selection_list_source_date?: string;
              selection_list_end_date?: string;
              activate_period_of_preservation?: boolean;
              type_of_archiving?: 'vernietigen';
              period_of_preservation?: string;
              archiving_procedure?: 'Afhandeling';
              description?: string;
              selection_list_number?: string;
              process_type_number?: string;
              process_type_name?: string;
              process_type_description?: string;
              process_type_object?: string;
              process_type_generic?: 'generic' | 'specific';
              origin?: string;
              process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          checklist_items?: string[];
          [k: string]: any;
        }[];
        authorizations?: {
          [k: string]: any;
        }[];
        changelog?: {
          summary?: string;
          changes?: {
            [k: string]: any;
          }[];
          [k: string]: any;
        };
      };
      relationships?: {
        /**
         * In which folder this casetype resides
         */
        catalogue_folder?: {
          data?: {
            type?: 'folder';
            id?: string;
            meta?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Related parent casetype
         */
        parent?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of child casetypes of this mother casetype
         */
        children?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
    };
    [k: string]: any;
  }

  export interface SetCaseParentRequestParams {
    case_uuid: string;
    parent_uuid: string;
    [k: string]: any;
  }

  export interface SetCaseParentResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetSubjectRelatedCustomObjectResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type SetSubjectRelatedCustomObjectRequestBody = {
    /**
     * UUID of the custom object to link. If this is set to `null`, the subject/custom object link will be broken.
     */
    custom_object_uuid: string;
    [k: string]: any;
  } & (
    | {
        /**
         * UUID of an employee to link a custom_object to.
         */
        employee_uuid: string;
        [k: string]: any;
      }
    | {
        /**
         * UUID of an organization to link a custom_object to.
         */
        organization_uuid: string;
        [k: string]: any;
      }
    | {
        /**
         * UUID of a person to link a custom_object to.
         */
        person_uuid: string;
        [k: string]: any;
      }
  );

  export interface GetSubjectRequestParams {
    employee_uuid?: string;
    organization_uuid?: string;
    person_uuid?: string;
    [k: string]: any;
  }

  export type GetSubjectResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    data?:
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_name?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
            department?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
              };
            };
            roles?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              [k: string]: any;
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_names?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            /**
             * List of statuses for this person, e.g.: [secret deceased]
             */
            status?: string[];
            date_of_birth?: string;
            date_of_death?: string;
            /**
             * In dutch, the geslachtsnaam
             */
            family_name?: string;
            gender?: 'Male' | 'Female';
            noble_title?: string;
            /**
             * Another 'hidden' identifier in the dutch systems, called a_nummer
             */
            personal_a_number?: string;
            /**
             * Flag to check if person lives in the municipality
             */
            inside_municipality?: boolean;
            /**
             * Flag to check if person has correspondence_address
             */
            has_correspondence_address?: boolean;
            residence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * Name of the organization
             */
            name?: string;
            /**
             * The KVKNumber of this organization, chamber of commerce identifier
             */
            coc_number?: string;
            /**
             * The location number at the chamber of commerce (vestigingsnummer)
             */
            coc_location_number?: string;
            organization_type?: (
              | 'Eenmanszaak'
              | 'Eenmanszaak met meer dan één eigenaar'
              | 'N.V./B.V. in oprichting op A-formulier'
              | 'Rederij'
              | 'Maatschap'
              | 'Vennootschap onder firma'
              | 'N.V/B.V. in oprichting op B-formulier'
              | 'Commanditaire vennootschap met een beherend vennoot'
              | 'Commanditaire vennootschap met meer dan één beherende vennoot'
              | 'N.V./B.V. in oprichting op D-formulier'
              | 'Rechtspersoon in oprichting'
              | 'Besloten vennootschap met gewone structuur'
              | 'Besloten vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap met gewone structuur'
              | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
              | 'Europese naamloze vennootschap (SE) met gewone structuur'
              | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
              | 'Coöperatie U.A. met gewone structuur'
              | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie W.A. met gewone structuur'
              | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie B.A. met gewone structuur'
              | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
              | 'Vereniging van eigenaars'
              | 'Vereniging met volledige rechtsbevoegdheid'
              | 'Vereniging met beperkte rechtsbevoegdheid'
              | 'Kerkgenootschap'
              | 'Stichting'
              | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
              | 'Publiekrechtelijke rechtspersoon'
              | 'Privaatrechtelijke rechtspersoon'
              | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
              | 'Nevenvest. met hoofdvest. in buitenl.'
              | 'Europees economisch samenwerkingsverband'
              | 'Buitenl. EG-venn. met onderneming in Nederland'
              | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
              | 'Coöperatie'
              | 'Vereniging'
            )[];
            location_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            /**
             * Flag to check if organization has correspondence address
             */
            has_correspondence_address?: boolean;
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        });
    [k: string]: any;
  };

  export interface GetRelatedObjectsForSubjectRequestParams {
    subject_uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedObjectsForSubjectResponseBody {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedSubjectsForSubjectRequestParams {
    subject_uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsForSubjectResponseBody {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * Type of the subject (person/employee/organization)
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateLogForBsnRetrievedRequestParams {
    subject_uuid: string;
    [k: string]: any;
  }

  export interface CreateLogForBsnRetrievedResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetSubjectRelationsRequestParams {
    case_uuid: string;
    include?: 'subject'[];
    [k: string]: any;
  }

  export type GetSubjectRelationsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data: {
      type: string;
      id: string;
      attributes: {
        role: string;
        magic_string_prefix: string;
        /**
         * flag to check if the subject_relation is pip_authorized. Only if the subject_relation is of type person/organization
         */
        authorized: boolean;
        /**
         * permission of subject_relations. Only if subject_relation is of type employee
         */
        permission: 'none' | 'search' | 'read' | 'write';
        /**
         * flag that indicates whether this is a 'preset' subject relation
         */
        is_preset_client: boolean;
        [k: string]: any;
      };
      relationships: {
        subject: {
          data: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          meta?: {
            name?: string;
            [k: string]: any;
          };
          links?: {
            links?: {
              self: string;
              [k: string]: any;
            };
            [k: string]: any;
          } & {
            related?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case: {
          data: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          meta?: {
            name?: string;
            [k: string]: any;
          };
          links?: {
            links?: {
              self: string;
              [k: string]: any;
            };
            [k: string]: any;
          } & {
            related?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      links: {
        links?: {
          self: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    }[];
    included?: (
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_names?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            /**
             * List of statuses for this person, e.g.: [secret deceased]
             */
            status?: string[];
            date_of_birth?: string;
            date_of_death?: string;
            /**
             * In dutch, the geslachtsnaam
             */
            family_name?: string;
            gender?: 'Male' | 'Female';
            noble_title?: string;
            /**
             * Another 'hidden' identifier in the dutch systems, called a_nummer
             */
            personal_a_number?: string;
            /**
             * Flag to check if person lives in the municipality
             */
            inside_municipality?: boolean;
            /**
             * Flag to check if person has correspondence_address
             */
            has_correspondence_address?: boolean;
            residence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * The name, for example changed after marriage
             */
            surname?: string;
            first_name?: string;
            /**
             * The prefix of the surname, the 'DE' in 'A. DE boer'
             */
            surname_prefix?: string;
            initials?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
            department?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
              };
            };
            roles?: {
              data?: {
                id?: string;
                type?: string;
                meta?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              [k: string]: any;
            };
          };
        })
      | ({
          type?: string;
          id?: string;
          meta?: {
            last_modified_datetime?: string;
            created_datetime?: string;
            summary?: string;
            [k: string]: any;
          };
          attributes?: {
            /**
             * Tells us if this is a cached subject
             */
            is_cache?: boolean;
            /**
             * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
             */
            summary?: string;
            contact_information: {
              email: string | null;
              phone_number: string | null;
              mobile_number: string | null;
              internal_note?: string;
            };
          };
          [k: string]: any;
        } & {
          type: string;
          id: string;
          attributes: {
            /**
             * Name of the organization
             */
            name?: string;
            /**
             * The KVKNumber of this organization, chamber of commerce identifier
             */
            coc_number?: string;
            /**
             * The location number at the chamber of commerce (vestigingsnummer)
             */
            coc_location_number?: string;
            organization_type?: (
              | 'Eenmanszaak'
              | 'Eenmanszaak met meer dan één eigenaar'
              | 'N.V./B.V. in oprichting op A-formulier'
              | 'Rederij'
              | 'Maatschap'
              | 'Vennootschap onder firma'
              | 'N.V/B.V. in oprichting op B-formulier'
              | 'Commanditaire vennootschap met een beherend vennoot'
              | 'Commanditaire vennootschap met meer dan één beherende vennoot'
              | 'N.V./B.V. in oprichting op D-formulier'
              | 'Rechtspersoon in oprichting'
              | 'Besloten vennootschap met gewone structuur'
              | 'Besloten vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap met gewone structuur'
              | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
              | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
              | 'Europese naamloze vennootschap (SE) met gewone structuur'
              | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
              | 'Coöperatie U.A. met gewone structuur'
              | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie W.A. met gewone structuur'
              | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
              | 'Coöperatie B.A. met gewone structuur'
              | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
              | 'Vereniging van eigenaars'
              | 'Vereniging met volledige rechtsbevoegdheid'
              | 'Vereniging met beperkte rechtsbevoegdheid'
              | 'Kerkgenootschap'
              | 'Stichting'
              | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
              | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
              | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
              | 'Publiekrechtelijke rechtspersoon'
              | 'Privaatrechtelijke rechtspersoon'
              | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
              | 'Nevenvest. met hoofdvest. in buitenl.'
              | 'Europees economisch samenwerkingsverband'
              | 'Buitenl. EG-venn. met onderneming in Nederland'
              | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
              | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
              | 'Coöperatie'
              | 'Vereniging'
            )[];
            location_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            /**
             * Flag to check if organization has correspondence address
             */
            has_correspondence_address?: boolean;
            correspondence_address?: {
              street?: string;
              street_number?: number;
              street_number_suffix?: string;
              street_number_letter?: string;
              zipcode?: string;
              city?: string;
              foreign_address_line1?: string;
              foreign_address_line2?: string;
              foreign_address_line3?: string;
              country?: string;
              municipality?: string;
            };
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              links: {
                self: string;
              };
              data: {
                type: 'custom_object';
                id: string;
              };
            };
          };
        })
    )[];
    [k: string]: any;
  };

  export interface CreateSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateSubjectRelationRequestBody {
    /**
     * UUID of the case.
     */
    case_uuid: string;
    subject: {
      type: 'employee' | 'organization' | 'person';
      id: string;
    };
    /**
     * Magic string prefix for new related_subject
     */
    magic_string_prefix: string;
    /**
     * Role for new related_subject.
     */
    role: string;
    /**
     * Flag to set pip_authorization for subject_relation. Can set only if subject_relation is of type person/organization.
     */
    authorized?: boolean;
    /**
     * Flag to check if confirmation email needed to be send to the subject.
     */
    send_confirmation_email?: boolean;
    /**
     * Permission of subject_relation. Only if subject_relation is of type employee.
     */
    permission?: 'none' | 'search' | 'read' | 'write';
  }

  export interface GetCaseRelationsRequestParams {
    case_uuid: string;
    include?: 'this_case' | 'other_case' | 'this_case,other_case';
    [k: string]: any;
  }

  export type GetCaseRelationsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      type?: string;
      id?: string;
      attributes?: {
        blocks_deletion?: boolean;
        owner_uuid?: string;
        sequence_number?: number;
        relation_type?: 'related_case' | 'child_case' | 'parent_case';
        [k: string]: any;
      };
      relationships?: {
        this_case?: {
          data?: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        other_case?: {
          data?: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    included?: {
      type?: string;
      id?: string;
      meta?: {
        summary?: string;
        display_id?: number;
        [k: string]: any;
      };
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface UpdateSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateSubjectRelationRequestBody {
    /**
     * UUID of the subject_relation.
     */
    relation_uuid: string;
    /**
     * New magic string for subject_relation
     */
    magic_string_prefix: string;
    /**
     * New role for subject_relation
     */
    role: string;
    /**
     * Flag to set the pip authorization of subject_relation.Can set only if subject_relation is of type person/organization.
     */
    authorized?: boolean;
    /**
     * New permission for subject_relation. Can set only if the subject_relation is of type employee.
     */
    permission?: 'none' | 'search' | 'read' | 'write';
  }

  export interface GetCaseEventLogsRequestParams {
    page: number;
    page_size: number;
    period_start?: string;
    period_end?: string;
    case_uuid: string;
    'filter[attributes.category]'?: ('document' | 'case_update')[];
    [k: string]: any;
  }

  export type GetCaseEventLogsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) &
    (({
      meta: {
        api_version: number;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      links?: {
        self?: string;
        prev?: string;
        next?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }) & {
      data: {
        id?: string;
        type?: string;
        meta?: {
          /**
           * Describing summary of this object
           */
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          id: number;
          type: string;
          date: string;
          user: string;
          description: string;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    });

  export interface GetTaskListRequestParams {
    'filter[relationships.case.id]'?: string[];
    'filter[relationships.case.number]'?: number[];
    'filter[relationships.case_type.id]'?: string[];
    'filter[attributes.phase]'?: number;
    'filter[attributes.completed]'?: boolean;
    'filter[relationships.assignee.id]'?: string[];
    'filter[relationships.department.id]'?: string[];
    'filter[attributes.title.contains]'?: string[];
    keyword?: string;
    sort?:
      | 'attributes.title'
      | 'attributes.due_date'
      | 'relationships.case.number'
      | 'relationships.assignee.name'
      | '-attributes.title'
      | '-attributes.due_date'
      | '-relationships.case.number'
      | '-relationships.assignee.name';
    page?: number;
    page_size?: number;
    [k: string]: any;
  }

  export type GetTaskListResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      type: string;
      id: string;
      meta: {
        is_editable: boolean;
        can_set_completion: boolean;
      };
      attributes: {
        completed: boolean;
        description: string;
        title: string;
        due_date: string;
        phase: number;
      };
      relationships?: {
        case: {
          data: {
            id: string;
            type: 'case';
          };
          meta?: {
            phase?: number;
            display_number?: number;
            [k: string]: any;
          };
        };
        case_type?: {
          data: {
            id: string;
            type: 'case_type';
          };
          meta: {
            display_name: string;
          };
        };
        department?: {
          data: {
            id: string;
            type: 'department';
          };
          meta: {
            display_name: string;
          };
        };
        assignee?: {
          data: {
            id: string;
            type: 'employee';
            meta: {
              display_name: string;
            };
          };
        };
      };
      links?: {
        links?: {
          self: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
    }[];
    [k: string]: any;
  };

  export interface CreateTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateTaskRequestBody {
    case_uuid: string;
    title: string;
    task_uuid: string;
    phase: number;
    [k: string]: any;
  }

  export interface DeleteTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteTaskRequestBody {
    task_uuid: string;
    [k: string]: any;
  }

  export interface UpdateTaskResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateTaskRequestBody {
    task_uuid: string;
    description?: string;
    title: string;
    due_date: string | null;
    assignee: string | null;
    [k: string]: any;
  }

  export interface SetTaskCompletionResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetTaskCompletionRequestBody {
    task_uuid: string;
    completed: boolean;
    [k: string]: any;
  }

  export interface DeleteSubjectRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteSubjectRelationRequestBody {
    /**
     * UUID of the subject_relation.
     */
    relation_uuid: string;
  }

  export interface ReorderCaseRelationRequestParams {
    relation_uuid: string;
    case_uuid: string;
    new_index: string;
    [k: string]: any;
  }

  export interface ReorderCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseRelationResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseRelationRequestBody {
    /**
     * UUID of the case relation to be deleted.
     */
    relation_uuid: string;
    /**
     * UUID of the case to be affected.
     */
    case_uuid: string;
  }

  export interface CreateCustomObjectTypeResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCustomObjectTypeRequestBody {
    /**
     * Name of this custom_object_type
     */
    name: string;
    /**
     * Internal identifier of this specific custom_object_type version
     */
    uuid: string;
    /**
     * Title for the created custom_object_types
     */
    title?: string;
    /**
     * Subtitle for the created custom_object_types
     */
    subtitle?: string;
    /**
     * The current status of this custom_object_type, either 'active' or 'offline'
     */
    status?: 'active' | 'offline';
    /**
     * The catalog folder UUID to store the new custom object in. None will store in root.
     */
    catalog_folder_uuid?: string;
    /**
     * Custom field definition pointing to configured custom fields
     */
    custom_field_definition?: {
      /**
       * List of custom fields
       */
      custom_fields?: {
        /**
         * Label of this field
         */
        label: string;
        /**
         * Name of this field in the catalog
         */
        name: string;
        /**
         * Indicates whether this field is a required field
         */
        is_required?: boolean;
        /**
         * Indicates whether this field is a hidden field, or 'system field'
         */
        is_hidden_field?: boolean;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Type of this custom field
         */
        custom_field_type:
          | 'bankaccount'
          | 'date'
          | 'email'
          | 'geojson'
          | 'option'
          | 'relationship'
          | 'richtext'
          | 'select'
          | 'text'
          | 'textarea'
          | 'url'
          | 'valuta'
          | 'address_v2'
          | 'numeric'
          | 'checkbox';
        /**
         * The magic string of this field
         */
        magic_string?: string;
        /**
         * UUID referencing the field in our attribute catalog
         */
        attribute_uuid?: string;
        /**
         * The CustomField specific attributes for this field
         */
        custom_field_specification?: {
          /**
           * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Authorization settings for this object type
     */
    authorization_definition?: {
      /**
       * Optional list of authorizations for this object
       */
      authorizations?: {
        /**
         * Set of permissions
         */
        authorization?: 'read' | 'readwrite' | 'admin';
        /**
         * Role name
         */
        role: {
          /**
           * Unique identifier for this role
           */
          uuid: string;
          /**
           * Name of the role
           */
          name?: string;
          [k: string]: any;
        };
        /**
         * Group name
         */
        department?: {
          /**
           * Unique identifier for this department
           */
          uuid: string;
          /**
           * Name of the department
           */
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Relationship settings for this object type
     */
    relationship_definition?: {
      /**
       * Optional list of relationships for this object
       */
      relationships?: {
        /**
         * Name of this relationship in the catalog
         */
        name?: string;
        /**
         * UUID of related Custom Object Type
         */
        custom_object_type_uuid?: string;
        /**
         * UUID of related Case Type
         */
        case_type_uuid?: string;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Indicate that this relationship is required on creation of this object
         */
        is_required?: boolean;
        [k: string]: any;
      }[];
      [k: string]: any;
    };
  }

  export interface UpdateCustomObjectTypeRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UpdateCustomObjectTypeResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCustomObjectTypeRequestBody {
    /**
     * Name of this custom_object_type
     */
    name?: string;
    /**
     * Internal identifier of this specific custom_object_type version
     */
    uuid: string;
    /**
     * Title for the created custom_object_types
     */
    title?: string;
    /**
     * Subtitle for the created custom_object_types
     */
    subtitle?: string;
    /**
     * The current status of this custom_object_type, either 'active' or 'offline'
     */
    status?: 'active' | 'offline';
    /**
     * Custom field definition pointing to configured custom fields
     */
    custom_field_definition?: {
      /**
       * List of custom fields
       */
      custom_fields?: {
        /**
         * Label of this field
         */
        label: string;
        /**
         * Name of this field in the catalog
         */
        name: string;
        /**
         * Indicates whether this field is a required field
         */
        is_required?: boolean;
        /**
         * Indicates whether this field is a hidden field, or 'system field'
         */
        is_hidden_field?: boolean;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Type of this custom field
         */
        custom_field_type:
          | 'bankaccount'
          | 'date'
          | 'email'
          | 'geojson'
          | 'option'
          | 'relationship'
          | 'richtext'
          | 'select'
          | 'text'
          | 'textarea'
          | 'url'
          | 'valuta'
          | 'address_v2'
          | 'numeric'
          | 'checkbox';
        /**
         * The magic string of this field
         */
        magic_string?: string;
        /**
         * UUID referencing the field in our attribute catalog
         */
        attribute_uuid?: string;
        /**
         * The CustomField specific attributes for this field
         */
        custom_field_specification?: {
          /**
           * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Authorization settings for this object type
     */
    authorization_definition?: {
      /**
       * Optional list of authorizations for this object
       */
      authorizations?: {
        /**
         * Set of permissions
         */
        authorization?: 'read' | 'readwrite' | 'admin';
        /**
         * Role name
         */
        role: {
          /**
           * Unique identifier for this role
           */
          uuid: string;
          /**
           * Name of the role
           */
          name?: string;
          [k: string]: any;
        };
        /**
         * Group name
         */
        department?: {
          /**
           * Unique identifier for this department
           */
          uuid: string;
          /**
           * Name of the department
           */
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    };
    /**
     * Relationship settings for this object type
     */
    relationship_definition?: {
      /**
       * Optional list of relationships for this object
       */
      relationships?: {
        /**
         * Name of this relationship in the catalog
         */
        name?: string;
        /**
         * UUID of related Custom Object Type
         */
        custom_object_type_uuid?: string;
        /**
         * UUID of related Case Type
         */
        case_type_uuid?: string;
        /**
         * Description of this field for internal purposes
         */
        description?: string;
        /**
         * Description of this field for public purposes
         */
        external_description?: string;
        /**
         * Indicate that this relationship is required on creation of this object
         */
        is_required?: boolean;
        [k: string]: any;
      }[];
      [k: string]: any;
    };
  }

  export interface DeleteCustomObjectTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type DeleteCustomObjectTypeRequestBody = {
    [k: string]: any;
  };

  export interface CreateCustomObjectResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateCustomObjectRequestBody {
    /**
     * Internal identifier of this specific custom object version
     */
    uuid: string;
    /**
     * The current status of this custom object.One of active,inactive,draft
     */
    status?: 'active' | 'inactive' | 'draft';
    /**
     * Key-value pair of custom fields
     */
    custom_fields: {
      [k: string]: any;
    };
    /**
     * Archiving metadata for this object
     */
    archive_metadata?: {
      /**
       * Archival status for this custom object
       */
      status?: 'archived' | 'to destroy' | 'to preserve';
      /**
       * Archival ground for this custom object
       */
      ground?: string;
      /**
       * Archival retention for this custom object
       */
      retention?: number;
      [k: string]: any;
    };
    /**
     * Internal identifier of the custom object type version
     */
    custom_object_type_uuid?: string;
    related_to?: {
      case?: string[];
      [k: string]: any;
    };
  }

  export interface UpdateCustomObjectRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UpdateCustomObjectResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCustomObjectRequestBody {
    /**
     * Internal identifier of this specific custom object version
     */
    uuid: string;
    /**
     * The current status of this custom object
     */
    status?: 'active' | 'inactive' | 'draft';
    /**
     * Key-value pair of custom fields
     */
    custom_fields?: {
      [k: string]: any;
    };
    /**
     * Archiving metadata for this object
     */
    archive_metadata?: {
      /**
       * Archival status for this custom object
       */
      status?: 'archived' | 'to destroy' | 'to preserve';
      /**
       * Archival ground for this custom object
       */
      ground?: string;
      /**
       * Archival retention for this custom object
       */
      retention?: number;
      [k: string]: any;
    };
    /**
     * Internal identifier of the custom object type
     */
    custom_object_type_uuid?: string;
  }

  export interface RelateCustomObjectToRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface RelateCustomObjectToResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelateCustomObjectToRequestBody {
    cases?: string[];
  }

  export interface UnrelateCustomObjectFromRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface UnrelateCustomObjectFromResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UnrelateCustomObjectFromRequestBody {
    cases?: string[];
  }

  export interface GetCustomObjectTypeRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetCustomObjectTypeResponseBody {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * Set of permissions
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * Type of this relationship
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * Type of this custom field
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Indicates if multiple values are allowed.
             */
            multiple_values?: boolean;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * The current status of this object type
         */
        status?: 'active' | 'inactive';
        /**
         * Subtitle for the created object type
         */
        subtitle?: string;
        /**
         * Title for the created object type
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for objects of the custom object type
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetPersistentCustomObjectTypeRequestParams {
    uuid: string;
    use?: 'read' | 'write' | 'manage';
    [k: string]: any;
  }

  export interface GetPersistentCustomObjectTypeResponseBody {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * Set of permissions
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * Type of this relationship
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * Type of this custom field
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Indicates if multiple values are allowed.
             */
            multiple_values?: boolean;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * The current status of this object type
         */
        status?: 'active' | 'inactive';
        /**
         * Subtitle for the created object type
         */
        subtitle?: string;
        /**
         * Title for the created object type
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for objects of the custom object type
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCustomObjectRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export type GetCustomObjectResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * Archival status for this custom object
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * The current status of this custom object
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for the custom object
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface GetCustomObjectsRequestParams {
    'filter[relationships.cases.id]'?: string;
    'filter[relationships.custom_object_type.attributes.version_independent_uuid]'?: string;
    [k: string]: any;
  }

  export interface GetCustomObjectsResponseBody {
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * Archival status for this custom object
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * The current status of this custom object
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for the custom object
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SearchRequestParams {
    keyword: string;
    max_results?: number;
    max_results_per_type?: number;
    type: (
      | 'case'
      | 'custom_object'
      | 'custom_object_type'
      | 'document'
      | 'employee'
      | 'object_v1'
      | 'organization'
      | 'person'
      | 'case_type'
      | 'saved_search'
    )[];
    'filter[relationships.custom_object_type.id]'?: string;
    'filter[relationships.case_type.requestor_type]'?:
      | 'employee'
      | 'organization'
      | 'person';
    'filter[relationships.custom_object.active]'?: 'true' | 'false';
    [k: string]: any;
  }

  export interface SearchResponseBody {
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedCasesRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedCasesResponseBody {
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetContactRelatedCasesRequestParams {
    contact_uuid: string;
    'filter[attributes.status]'?: string;
    'filter[attributes.roles.contains]'?:
      | 'Aanvrager'
      | 'Behandelaar'
      | 'Coordinator';
    'filter[attributes.is_authorized]'?: boolean;
    'filter[attributes.roles.not_contains]'?:
      | 'Aanvrager'
      | 'Behandelaar'
      | 'Coordinator';
    'filter[keyword]'?: string;
    include_cases_on_location?: boolean;
    sort?:
      | 'attributes.number'
      | 'attributes.status'
      | 'attributes.days_left'
      | 'attributes.result'
      | 'attributes.progress'
      | 'relationships.case_type.name'
      | '-attributes.number'
      | '-attributes.status'
      | '-attributes.days_left'
      | '-attributes.result'
      | '-attributes.progress'
      | '-relationships.case_type.name';
    page: number;
    page_size: number;
    [k: string]: any;
  }

  export interface GetContactRelatedCasesResponseBody {
    data: {
      attributes: {
        /**
         * Archival state of case
         */
        archival_state: 'overdragen' | 'vernietigen';
        /**
         * Days left for given case
         */
        days_left?: {
          /**
           * Number of days left
           */
          amount: number;
          /**
           * Unit of measure
           */
          unit: string;
          [k: string]: any;
        };
        /**
         * Destruction date of case
         */
        destruction_date: string;
        /**
         * Authorization status for given case
         */
        is_authorized?: boolean;
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Roles that contact has for the case (can be an empty list)
         */
        roles: string[];
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        /**
         * Summary of the case
         */
        summary?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetRelatedObjectsRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedObjectsResponseBody {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCustomObjectEventLogsRequestParams {
    page: number;
    page_size: number;
    period_start?: string;
    period_end?: string;
    custom_object_uuid: string;
    [k: string]: any;
  }

  export type GetCustomObjectEventLogsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) &
    (({
      meta: {
        api_version: number;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      links?: {
        self?: string;
        prev?: string;
        next?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }) & {
      data: {
        id?: string;
        type?: string;
        meta?: {
          /**
           * Describing summary of this object
           */
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          id: number;
          type: string;
          date: string;
          user: string;
          description: string;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    });

  export interface DeleteCustomObjectResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCustomObjectRequestBody {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetRelatedSubjectsResponseBody {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * Type of the subject (person/employee/organization)
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetContactRequestParams {
    type: 'employee' | 'organization' | 'person';
    uuid: string;
    [k: string]: any;
  }

  export type GetContactResponseBody =
    | {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Branch identification number ('Vestigingsnummer')
             */
            coc_location_number?: number;
            /**
             * Chamber of Commerce (Kamer van Koophandel, KvK) number of the organization
             */
            coc_number: string;
            /**
             * Contact information
             */
            contact_information: {
              /**
               * Email address of the contact
               */
              email?: string;
              /**
               * Internal note
               */
              internal_note?: string;
              /**
               * Mobile phone number
               */
              mobile_number?: string;
              /**
               * Preferred phone number
               */
              phone_number?: string;
              /**
               * Preffered contact channel
               */
              preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
              [k: string]: any;
            };
            /**
             * Contact person for this organization
             */
            contact_person?: {
              /**
               * Family name of the contact person
               */
              family_name?: string;
              /**
               * First name of the contact person
               */
              first_name?: string;
              /**
               * Initials in the name of contact person
               */
              initials?: string;
              /**
               * Insertions in the name of contact person
               */
              insertions?: string;
              [k: string]: any;
            };
            /**
             * Correspondence address
             */
            correspondence_address:
              | {
                  /**
                   * City
                   */
                  city: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: false;
                  /**
                   * Street name
                   */
                  street: string;
                  /**
                   * House number
                   */
                  street_number: number;
                  /**
                   * House letter (part of house number)
                   */
                  street_number_letter?: string;
                  /**
                   * House number extension
                   */
                  street_number_suffix?: string;
                  /**
                   * Postal code (zipcode)
                   */
                  zipcode?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * First line of the address
                   */
                  address_line_1?: string;
                  /**
                   * Second line of the address
                   */
                  address_line_2?: string;
                  /**
                   * Third line of the address
                   */
                  address_line_3?: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: true;
                  [k: string]: any;
                };
            /**
             * Date the organization ceased operations
             */
            date_ceased: string;
            /**
             * Date the organization was founded
             */
            date_founded: string;
            /**
             * Date the organization was first registered
             */
            date_registered: string;
            /**
             * True if the contact has a valid address
             */
            has_valid_address: boolean;
            /**
             * Organization is active/inactive
             */
            is_active?: boolean;
            /**
             * Main address
             */
            location_address:
              | {
                  /**
                   * City
                   */
                  city: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: false;
                  /**
                   * Street name
                   */
                  street: string;
                  /**
                   * House number
                   */
                  street_number: number;
                  /**
                   * House letter (part of house number)
                   */
                  street_number_letter?: string;
                  /**
                   * House number extension
                   */
                  street_number_suffix?: string;
                  /**
                   * Postal code (zipcode)
                   */
                  zipcode?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * First line of the address
                   */
                  address_line_1?: string;
                  /**
                   * Second line of the address
                   */
                  address_line_2?: string;
                  /**
                   * Third line of the address
                   */
                  address_line_3?: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: true;
                  [k: string]: any;
                };
            /**
             * Code and descripton of the organization's main activity
             */
            main_activity: {
              /**
               * Activity code
               */
              code: string;
              /**
               * Description of the activity
               */
              description: string;
              [k: string]: any;
            };
            /**
             * Organization name
             */
            name: string;
            /**
             * OIN (Organization Identification Number)
             */
            oin: number;
            /**
             * Type of organization
             */
            organization_type: string;
            /**
             * Preferred channel of contact for this organization
             */
            preferred_contact_channel?: string;
            /**
             * RSIN of the organization
             */
            rsin: number;
            /**
             * Codes and descriptons of the organization's other activities
             */
            secondary_activities: {
              /**
               * Activity code
               */
              code: string;
              /**
               * Description of the activity
               */
              description: string;
              [k: string]: any;
            }[];
            /**
             *
             *         The source of the data in this entity. Will be 'Zaaksysteem' if it was
             *         created manually, or name of the integration if it was imported from
             *         elsewhere.
             *
             */
            source: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        /**
         * Content of a user defined Custom Object
         */
        data: {
          attributes: {
            /**
             * Boolean indicates if the person is authenticated
             */
            authenticated: boolean;
            /**
             * BSN number for the person
             */
            bsn?: string;
            /**
             * Contact info of the person
             */
            contact_information?: {
              /**
               * Email address of the contact
               */
              email?: string;
              /**
               * Internal note
               */
              internal_note?: string;
              /**
               * Is the contact person is anonymous
               */
              is_an_anonymous_contact_person?: boolean;
              /**
               * Mobile phone number
               */
              mobile_number?: string;
              /**
               * Preferred phone number
               */
              phone_number?: string;
              /**
               * Preffered contact channel
               */
              preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
              [k: string]: any;
            };
            /**
             * Correspondence address of the person
             */
            correspondence_address?:
              | {
                  /**
                   * City
                   */
                  city: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: false;
                  /**
                   * Street name
                   */
                  street: string;
                  /**
                   * House number
                   */
                  street_number: number;
                  /**
                   * House letter (part of house number)
                   */
                  street_number_letter?: string;
                  /**
                   * House number extension
                   */
                  street_number_suffix?: string;
                  /**
                   * Postal code (zipcode)
                   */
                  zipcode?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * First line of the address
                   */
                  address_line_1?: string;
                  /**
                   * Second line of the address
                   */
                  address_line_2?: string;
                  /**
                   * Third line of the address
                   */
                  address_line_3?: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: true;
                  [k: string]: any;
                };
            /**
             * Country Code of the address
             */
            country_code?: string;
            /**
             * Date of birth of person
             */
            date_of_birth?: string;
            /**
             * Date of death of person
             */
            date_of_death?: string;
            /**
             * Family name of person
             */
            family_name?: string;
            /**
             * First names of person
             */
            first_names?: string;
            /**
             * Gender of the person
             */
            gender: string;
            /**
             * True if the contact has a valid address
             */
            has_valid_address: boolean;
            /**
             * Initials of Person
             */
            initials?: string;
            /**
             * Insertions in the name of the person
             */
            insertions?: string;
            /**
             * Boolean that indicates if the person lives within the municipality or not
             */
            inside_municipality?: boolean;
            /**
             * Mark user as active/inactive
             */
            is_active?: boolean;
            /**
             * If its secret value
             */
            is_secret?: boolean;
            /**
             * Name of the person
             */
            name: string;
            /**
             * Title of person
             */
            noble_title?: string;
            /**
             * Residence address of the person
             */
            residence_address?:
              | {
                  /**
                   * City
                   */
                  city: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: false;
                  /**
                   * Street name
                   */
                  street: string;
                  /**
                   * House number
                   */
                  street_number: number;
                  /**
                   * House letter (part of house number)
                   */
                  street_number_letter?: string;
                  /**
                   * House number extension
                   */
                  street_number_suffix?: string;
                  /**
                   * Postal code (zipcode)
                   */
                  zipcode?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * First line of the address
                   */
                  address_line_1?: string;
                  /**
                   * Second line of the address
                   */
                  address_line_2?: string;
                  /**
                   * Third line of the address
                   */
                  address_line_3?: string;
                  /**
                   * Country
                   */
                  country: string;
                  /**
                   * Indicates that this is an address outside the Netherlands
                   */
                  is_foreign?: true;
                  [k: string]: any;
                };
            /**
             * Source of the person
             */
            source?: string;
            /**
             * Surname of person
             */
            surname?: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            related_custom_object?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        /**
         * Content of a user defined Custom Object
         */
        data: {
          attributes: {
            /**
             * Contact info of the person
             */
            contact_information?: {
              /**
               * Email address of the contact
               */
              email?: string;
              /**
               * Internal note
               */
              internal_note?: string;
              /**
               * Mobile phone number
               */
              mobile_number?: string;
              /**
               * Preferred phone number
               */
              phone_number?: string;
              /**
               * Preffered contact channel
               */
              preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
              [k: string]: any;
            };
            /**
             * First names of employee
             */
            first_name?: string;
            /**
             * True if the contact has a valid address. Always false for employees.
             */
            has_valid_address?: boolean;
            /**
             * User is active/inactive
             */
            is_active?: boolean;
            /**
             * Name of the person
             */
            name: string;
            /**
             * Source of the employee
             */
            source?: string;
            /**
             * Status of the employee.
             */
            status: 'active' | 'inactive';
            /**
             * Surname of employee
             */
            surname?: string;
            /**
             * Title of person
             */
            title?: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            department?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            related_custom_object?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            roles?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };

  export interface GetContactSettingsRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export interface GetContactSettingsResponseBody {
    /**
     * Settings for an employee
     */
    data: {
      attributes: {
        /**
         * Notification settings of employee
         */
        notification_settings?: {
          /**
           * Send notification when the suspension period of case has expired
           */
          case_suspension_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when the processing time of case has expired
           */
          case_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when new case is assigned to the employee
           */
          new_assigned_case: boolean;
          /**
           * Send notification outside zaaksytem when attribute has changed
           */
          new_attribute_proposal: boolean;
          /**
           * Notify outside zaaksytem in case of new document events.
           */
          new_document: boolean;
          /**
           * Send notification outside zaaksyteem when employee receives new pip message
           */
          new_ext_pip_message: boolean;
          [k: string]: any;
        };
        /**
         * The phone extension is used by the phone/PBX integration to show a pop-up when there's a call on the specified extension.
         */
        phone_extension?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        employee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        signature?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetNotificationSettingsResponseBody {
    /**
     * Settings for an employee
     */
    data: {
      attributes: {
        /**
         * Notification settings of employee
         */
        notification_settings?: {
          /**
           * Send notification when the suspension period of case has expired
           */
          case_suspension_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when the processing time of case has expired
           */
          case_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when new case is assigned to the employee
           */
          new_assigned_case: boolean;
          /**
           * Send notification outside zaaksytem when attribute has changed
           */
          new_attribute_proposal: boolean;
          /**
           * Notify outside zaaksytem in case of new document events.
           */
          new_document: boolean;
          /**
           * Send notification outside zaaksyteem when employee receives new pip message
           */
          new_ext_pip_message: boolean;
          [k: string]: any;
        };
        /**
         * The phone extension is used by the phone/PBX integration to show a pop-up when there's a call on the specified extension.
         */
        phone_extension?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        employee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        signature?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SetNotificationSettingsRequestBody {
    /**
     * UUID of the employee.
     */
    uuid?: string;
    /**
     * Dictionary representing the notification preference for employee.
     */
    notification_settings: {
      new_document: boolean;
      new_assigned_case: boolean;
      case_term_exceeded: boolean;
      new_ext_pip_message: boolean;
      new_attribute_proposal: boolean;
      case_suspension_term_exceeded: boolean;
    };
  }

  export interface SaveSignatureResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SaveSignatureRequestBody {
    /**
     * UUID of the employee.
     */
    uuid: string;
    /**
     * UUID of the file containing the signature of the employee.
     */
    file_uuid: string;
  }

  export interface DeleteSignatureResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteSignatureRequestBody {
    /**
     * UUID of the employee.
     */
    uuid: string;
  }

  export interface ChangePhoneExtensionResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ChangePhoneExtensionRequestBody {
    /**
     * UUID of the employee.
     */
    uuid: string;
    /**
     * Phone extension of the employee.
     */
    extension: string | null;
  }

  export interface GetContactEventLogsRequestParams {
    page: number;
    page_size: number;
    contact_uuid: string;
    contact_type: string;
    period_start?: string;
    period_end?: string;
    [k: string]: any;
  }

  export type GetContactEventLogsResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) &
    (({
      meta: {
        api_version: number;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      links?: {
        self?: string;
        prev?: string;
        next?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }) & {
      data: {
        id?: string;
        type?: string;
        meta?: {
          /**
           * Describing summary of this object
           */
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          id: number;
          type: string;
          date: string;
          user: string;
          description: string;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    });

  export interface GetPersonSensitiveDataRequestParams {
    uuid: string;
    [k: string]: any;
  }

  export type GetPersonSensitiveDataResponseBody = {
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    /**
     * Contains sensitive information about person
     */
    data: {
      attributes: {
        /**
         * Burger service number of the person
         */
        personal_number?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  };

  export interface SaveContactInformationResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SaveContactInformationRequestBody {
    contact_information?: {
      /**
       * Phone number of the contact.
       */
      phone_number?: string;
      /**
       * Mobile number of the contact.
       */
      mobile_number?: string;
      /**
       * Email id of the contact.
       */
      email?: string;
      /**
       * Internal note of the contact.
       */
      internal_note?: string;
      /**
       * Prefered contact_channel of the contact.
       */
      preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
      /**
       * This value can be changed only by admin.
       */
      is_an_anonymous_contact_person?: boolean;
      [k: string]: any;
    };
    /**
     * UUID of the subject(person or employee).
     */
    uuid?: string;
    /**
     * Type of subject(person or employee).
     */
    type?: 'person' | 'organization';
    [k: string]: any;
  }

  export interface UpdateNonAuthenticContactResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateNonAuthenticContactRequestBody {
    address?: {
      street: string;
      street_number: string;
      street_number_letter?: string;
      street_number_suffix?: string;
      zipcode: string;
      city: string;
      country: string;
      [k: string]: any;
    };
    contact_information?: {
      /**
       * Phone number of the contact.
       */
      phone_number?: string;
      /**
       * Mobile number of the contact.
       */
      mobile_number?: string;
      /**
       * Email id of the contact.
       */
      email?: string;
      [k: string]: any;
    };
    correspondence_address?: {
      address_line_1: string;
      address_line_2?: string;
      address_line_3?: string;
      country: string;
      [k: string]: any;
    };
    data: {
      /**
       * BSN of the contact.
       */
      bsn?: string;
      /**
       * Family name of the contact.
       */
      family_name: string;
      /**
       * First name of the contact.
       */
      first_name: string;
      /**
       * Is contact inside municipality
       */
      inside_municipality?: boolean;
      /**
       * Is address mailing address of contact
       */
      mailing_address?: boolean;
      /**
       * Noble title of person
       */
      noble_title?: string;
      /**
       * Surname prefix of person
       */
      surname_prefix?: string;
      [k: string]: any;
    };
    /**
     * Type of contact
     */
    type: 'person' | 'organization';
    /**
     * UUID of the person
     */
    uuid: string;
    [k: string]: any;
  }

  export interface GetCountriesListResponseBody {
    data?: {
      attributes: {
        /**
         * Dutch code of the country
         */
        code: number;
        /**
         * Name of the country
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface ExportTimelineResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ExportTimelineRequestBody {
    /**
     * UUID of the contact
     */
    uuid?: string;
    /**
     * Type of contact
     */
    type?: 'person' | 'employee' | 'organization';
    /**
     * Start timestamp(utc) for period filter; timezone is required, preferably 'Z' timezone
     */
    period_start?: string;
    /**
     * End timestamp(utc) for the period filter; timezone is required, preferably 'Z' timezone
     */
    period_end?: string;
    [k: string]: any;
  }

  export interface GetExportFileListRequestParams {
    page?: number;
    page_size?: number;
    [k: string]: any;
  }

  export type GetExportFileListResponseBody = ({
    meta: {
      api_version: number;
      [k: string]: any;
    };
    [k: string]: any;
  } & {
    links?: {
      self?: string;
      prev?: string;
      next?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }) & {
    data?: {
      /**
       * Data structure for export file data
       */
      data: {
        attributes: {
          /**
           * Number of downloads
           */
          downloads?: number;
          /**
           * The date that export file expires
           */
          expires_at: string;
          /**
           * Name of the export_file
           */
          name: string;
          [k: string]: any;
        };
        id: string;
        links?: {
          self?: string;
          [k: string]: any;
        };
        meta?: {
          /**
           * Human readable summary of the content of the object
           */
          summary?: string;
          [k: string]: any;
        };
        relationships?: {
          [k: string]: any;
        };
        type: string;
        [k: string]: any;
      };
      links: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  };

  export interface SearchCustomObjectsRequestParams {
    'filter[relationship.custom_object_type.id]': string;
    'filter[attributes.status]'?: ('active' | 'inactive' | 'draft')[];
    'filter[attributes.archive_status]'?: (
      | 'archived'
      | 'to destroy'
      | 'to preserve'
    )[];
    'filter[attributes.last_modified]'?: string[];
    'filter[keyword]'?: string;
    page: number;
    page_size: number;
    [k: string]: any;
  }

  export interface SearchCustomObjectsResponseBody {
    data: {
      attributes: {
        /**
         * Archive status of the custom object
         */
        archive_status: 'archived' | 'to destroy' | 'to preserve';
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * Status of the custom object
         */
        status: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ExportCaseEventsTimelineRequestParams {
    period_start?: string;
    period_end?: string;
    case_uuid: string;
    [k: string]: any;
  }

  export interface ExportCaseEventsTimelineResponseBody {
    data?: {
      type?: string;
      id?: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelatedCustomObject {
    links: {
      self: string;
    };
    data: {
      type: 'custom_object';
      id: string;
    };
  }

  export interface CaseEntity {
    data?: {
      type?: string;
      id?: string;
      meta?: {
        last_modified_datetime?: string;
        created_datetime?: string;
        summary?: string;
        [k: string]: any;
      };
      attributes?: {
        /**
         * The public case number of this case
         */
        number?: number;
        /**
         * Short slug (description) of this case
         */
        summary?: string;
        /**
         * Short slug (description) of this case, for public use
         */
        public_summary?: string;
        /**
         * Defines whether this case is closed, open or suspended
         */
        status?: {
          name?: 'new' | 'open' | 'resolved' | 'stalled';
          /**
           * When prematurely closed or stalled, this is the reason
           */
          reason?: string;
          /**
           * When stalled: the since date
           */
          since?: string;
          /**
           * When stalled: the until date
           */
          until?: string;
          [k: string]: any;
        };
        /**
         * The date this case was registered
         */
        registration_date?: string;
        /**
         * Date when this case should be resolved
         */
        target_completion_date?: string;
        /**
         * Date when this case was resolved
         */
        completion_date?: string;
        /**
         * Date when this case is due for destruction
         */
        destruction_date?: string;
        /**
         * The phase and milestone this case is currently in
         */
        phase?: {
          label?: string;
          milestone_label?: string;
          sequence?: number;
          next_sequence?: number;
          [k: string]: any;
        };
        /**
         * Information about the result of this case
         */
        result?: {
          result?: string;
          result_id?: number;
          archival_attributes?: {
            state?: 'vernietigen' | 'overdragen';
            selection_list?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * In which way this case ended in creation
         */
        contactchannel?:
          | 'behandelaar'
          | 'balie'
          | 'telefoon'
          | 'post'
          | 'email'
          | 'webformulier'
          | 'sociale media';
        /**
         * Specifies the payment information of a case
         */
        payment?: {
          amount?: number;
          status?: 'success' | 'failed' | 'pending' | 'offline';
          [k: string]: any;
        };
        /**
         * All user defined properties for this case
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * The confidentiality of this case, which defines the authorization path to use
         */
        confidentiality?: {
          mapped?: string;
          original?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      relationships?: {
        /**
         * Contains the subject (employee) who handles this case
         */
        assignee?: {
          data?: {
            type?: 'employee';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Contains the coordinator (employee) who handles this case
         */
        coordinator?: {
          data?: {
            type?: 'employee';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Contains the requestor (employee/company/person) who handles this case
         */
        requestor?: {
          data?: {
            type?: 'employee' | 'person' | 'organization';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Department this case is currently assigned to
         */
        department?: {
          data?: {
            type?: 'department';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Location this case references to
         */
        location?: {
          data?: {
            type?: 'location';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Subject this case references to (most of the time the requestor of the case
         */
        subjects?: {
          data?: {
            type?: 'employee' | 'person' | 'organization';
            id?: string;
            meta?: {
              role?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Case type this case descended from
         */
        casetype?: {
          data?: {
            type?: 'casetype';
            id?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * List of related cases
         */
        related_cases?: {
          data?: {
            type?: 'case';
            id?: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * List of related contacts
         */
        related_contacts?: {
          data?: {
            type?: string;
            id?: string;
            [k: string]: any;
          };
          meta?: {
            magic_string_prefix?: string;
            role?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      /**
       * Case relates to, e.g. a subject
       */
      relates_to?: {
        data?: {
          type?: 'subject';
          id?: string;
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CaseBasicEntity {
    data?: {
      type?: string;
      id?: string;
      meta?: {
        last_modified_datetime?: string;
        created_datetime?: string;
        summary?: string;
        [k: string]: any;
      };
      attributes?: {
        /**
         * The public case number of this case
         */
        number?: number;
        case_type_uuid?: string;
        case_type_version_uuid?: string;
        /**
         * All user defined properties for this case
         */
        custom_fields?: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type Subject =
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * The name, for example changed after marriage
           */
          surname?: string;
          first_name?: string;
          /**
           * The prefix of the surname, the 'DE' in 'A. DE boer'
           */
          surname_prefix?: string;
          initials?: string;
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
          department?: {
            data?: {
              id?: string;
              type?: string;
              meta?: {
                name?: string;
                [k: string]: any;
              };
            };
          };
          roles?: {
            data?: {
              id?: string;
              type?: string;
              meta?: {
                name?: string;
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          };
        };
      })
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * The name, for example changed after marriage
           */
          surname?: string;
          first_names?: string;
          /**
           * The prefix of the surname, the 'DE' in 'A. DE boer'
           */
          surname_prefix?: string;
          initials?: string;
          /**
           * List of statuses for this person, e.g.: [secret deceased]
           */
          status?: string[];
          date_of_birth?: string;
          date_of_death?: string;
          /**
           * In dutch, the geslachtsnaam
           */
          family_name?: string;
          gender?: 'Male' | 'Female';
          noble_title?: string;
          /**
           * Another 'hidden' identifier in the dutch systems, called a_nummer
           */
          personal_a_number?: string;
          /**
           * Flag to check if person lives in the municipality
           */
          inside_municipality?: boolean;
          /**
           * Flag to check if person has correspondence_address
           */
          has_correspondence_address?: boolean;
          residence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          correspondence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
        };
      })
    | ({
        type?: string;
        id?: string;
        meta?: {
          last_modified_datetime?: string;
          created_datetime?: string;
          summary?: string;
          [k: string]: any;
        };
        attributes?: {
          /**
           * Tells us if this is a cached subject
           */
          is_cache?: boolean;
          /**
           * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
           */
          summary?: string;
          contact_information: {
            email: string | null;
            phone_number: string | null;
            mobile_number: string | null;
            internal_note?: string;
          };
        };
        [k: string]: any;
      } & {
        type: string;
        id: string;
        attributes: {
          /**
           * Name of the organization
           */
          name?: string;
          /**
           * The KVKNumber of this organization, chamber of commerce identifier
           */
          coc_number?: string;
          /**
           * The location number at the chamber of commerce (vestigingsnummer)
           */
          coc_location_number?: string;
          organization_type?: (
            | 'Eenmanszaak'
            | 'Eenmanszaak met meer dan één eigenaar'
            | 'N.V./B.V. in oprichting op A-formulier'
            | 'Rederij'
            | 'Maatschap'
            | 'Vennootschap onder firma'
            | 'N.V/B.V. in oprichting op B-formulier'
            | 'Commanditaire vennootschap met een beherend vennoot'
            | 'Commanditaire vennootschap met meer dan één beherende vennoot'
            | 'N.V./B.V. in oprichting op D-formulier'
            | 'Rechtspersoon in oprichting'
            | 'Besloten vennootschap met gewone structuur'
            | 'Besloten vennootschap blijkens statuten structuurvennootschap'
            | 'Naamloze vennootschap met gewone structuur'
            | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
            | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
            | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
            | 'Europese naamloze vennootschap (SE) met gewone structuur'
            | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
            | 'Coöperatie U.A. met gewone structuur'
            | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
            | 'Coöperatie W.A. met gewone structuur'
            | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
            | 'Coöperatie B.A. met gewone structuur'
            | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
            | 'Vereniging van eigenaars'
            | 'Vereniging met volledige rechtsbevoegdheid'
            | 'Vereniging met beperkte rechtsbevoegdheid'
            | 'Kerkgenootschap'
            | 'Stichting'
            | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
            | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
            | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
            | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
            | 'Publiekrechtelijke rechtspersoon'
            | 'Privaatrechtelijke rechtspersoon'
            | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
            | 'Nevenvest. met hoofdvest. in buitenl.'
            | 'Europees economisch samenwerkingsverband'
            | 'Buitenl. EG-venn. met onderneming in Nederland'
            | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
            | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
            | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
            | 'Coöperatie'
            | 'Vereniging'
          )[];
          location_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          /**
           * Flag to check if organization has correspondence address
           */
          has_correspondence_address?: boolean;
          correspondence_address?: {
            street?: string;
            street_number?: number;
            street_number_suffix?: string;
            street_number_letter?: string;
            zipcode?: string;
            city?: string;
            foreign_address_line1?: string;
            foreign_address_line2?: string;
            foreign_address_line3?: string;
            country?: string;
            municipality?: string;
          };
          [k: string]: any;
        };
        relationships?: {
          related_custom_object?: {
            links: {
              self: string;
            };
            data: {
              type: 'custom_object';
              id: string;
            };
          };
        };
      });

  export interface SubjectBase {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  }

  export type EmployeeEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * The name, for example changed after marriage
       */
      surname?: string;
      first_name?: string;
      /**
       * The prefix of the surname, the 'DE' in 'A. DE boer'
       */
      surname_prefix?: string;
      initials?: string;
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
      department?: {
        data?: {
          id?: string;
          type?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
        };
      };
      roles?: {
        data?: {
          id?: string;
          type?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
  };

  export type PersonEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * The name, for example changed after marriage
       */
      surname?: string;
      first_names?: string;
      /**
       * The prefix of the surname, the 'DE' in 'A. DE boer'
       */
      surname_prefix?: string;
      initials?: string;
      /**
       * List of statuses for this person, e.g.: [secret deceased]
       */
      status?: string[];
      date_of_birth?: string;
      date_of_death?: string;
      /**
       * In dutch, the geslachtsnaam
       */
      family_name?: string;
      gender?: 'Male' | 'Female';
      noble_title?: string;
      /**
       * Another 'hidden' identifier in the dutch systems, called a_nummer
       */
      personal_a_number?: string;
      /**
       * Flag to check if person lives in the municipality
       */
      inside_municipality?: boolean;
      /**
       * Flag to check if person has correspondence_address
       */
      has_correspondence_address?: boolean;
      residence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      correspondence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
    };
  };

  export type OrganizationEntity = {
    type?: string;
    id?: string;
    meta?: {
      last_modified_datetime?: string;
      created_datetime?: string;
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      /**
       * Tells us if this is a cached subject
       */
      is_cache?: boolean;
      /**
       * A short overview of the name, e.g. 'P. Friet', 'Awesome Inc.', etc
       */
      summary?: string;
      contact_information: {
        email: string | null;
        phone_number: string | null;
        mobile_number: string | null;
        internal_note?: string;
      };
    };
    [k: string]: any;
  } & {
    type: string;
    id: string;
    attributes: {
      /**
       * Name of the organization
       */
      name?: string;
      /**
       * The KVKNumber of this organization, chamber of commerce identifier
       */
      coc_number?: string;
      /**
       * The location number at the chamber of commerce (vestigingsnummer)
       */
      coc_location_number?: string;
      organization_type?: (
        | 'Eenmanszaak'
        | 'Eenmanszaak met meer dan één eigenaar'
        | 'N.V./B.V. in oprichting op A-formulier'
        | 'Rederij'
        | 'Maatschap'
        | 'Vennootschap onder firma'
        | 'N.V/B.V. in oprichting op B-formulier'
        | 'Commanditaire vennootschap met een beherend vennoot'
        | 'Commanditaire vennootschap met meer dan één beherende vennoot'
        | 'N.V./B.V. in oprichting op D-formulier'
        | 'Rechtspersoon in oprichting'
        | 'Besloten vennootschap met gewone structuur'
        | 'Besloten vennootschap blijkens statuten structuurvennootschap'
        | 'Naamloze vennootschap met gewone structuur'
        | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
        | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
        | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
        | 'Europese naamloze vennootschap (SE) met gewone structuur'
        | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
        | 'Coöperatie U.A. met gewone structuur'
        | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
        | 'Coöperatie W.A. met gewone structuur'
        | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
        | 'Coöperatie B.A. met gewone structuur'
        | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
        | 'Vereniging van eigenaars'
        | 'Vereniging met volledige rechtsbevoegdheid'
        | 'Vereniging met beperkte rechtsbevoegdheid'
        | 'Kerkgenootschap'
        | 'Stichting'
        | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
        | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
        | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
        | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
        | 'Publiekrechtelijke rechtspersoon'
        | 'Privaatrechtelijke rechtspersoon'
        | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
        | 'Nevenvest. met hoofdvest. in buitenl.'
        | 'Europees economisch samenwerkingsverband'
        | 'Buitenl. EG-venn. met onderneming in Nederland'
        | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
        | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
        | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
        | 'Coöperatie'
        | 'Vereniging'
      )[];
      location_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      /**
       * Flag to check if organization has correspondence address
       */
      has_correspondence_address?: boolean;
      correspondence_address?: {
        street?: string;
        street_number?: number;
        street_number_suffix?: string;
        street_number_letter?: string;
        zipcode?: string;
        city?: string;
        foreign_address_line1?: string;
        foreign_address_line2?: string;
        foreign_address_line3?: string;
        country?: string;
        municipality?: string;
      };
      [k: string]: any;
    };
    relationships?: {
      related_custom_object?: {
        links: {
          self: string;
        };
        data: {
          type: 'custom_object';
          id: string;
        };
      };
    };
  };

  export interface AddressObject {
    street?: string;
    street_number?: number;
    street_number_suffix?: string;
    street_number_letter?: string;
    zipcode?: string;
    city?: string;
    foreign_address_line1?: string;
    foreign_address_line2?: string;
    foreign_address_line3?: string;
    country?: string;
    municipality?: string;
  }

  export interface AllocationObject {
    department?: {
      id?: number;
      name?: string;
      [k: string]: any;
    };
    role?: {
      id?: number;
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type LegalEntityList = (
    | 'Eenmanszaak'
    | 'Eenmanszaak met meer dan één eigenaar'
    | 'N.V./B.V. in oprichting op A-formulier'
    | 'Rederij'
    | 'Maatschap'
    | 'Vennootschap onder firma'
    | 'N.V/B.V. in oprichting op B-formulier'
    | 'Commanditaire vennootschap met een beherend vennoot'
    | 'Commanditaire vennootschap met meer dan één beherende vennoot'
    | 'N.V./B.V. in oprichting op D-formulier'
    | 'Rechtspersoon in oprichting'
    | 'Besloten vennootschap met gewone structuur'
    | 'Besloten vennootschap blijkens statuten structuurvennootschap'
    | 'Naamloze vennootschap met gewone structuur'
    | 'Naamloze vennootschap blijkens statuten structuurvennootschap'
    | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal'
    | 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap'
    | 'Europese naamloze vennootschap (SE) met gewone structuur'
    | 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap'
    | 'Coöperatie U.A. met gewone structuur'
    | 'Coöperatie U.A. blijkens statuten structuurcoöperatie'
    | 'Coöperatie W.A. met gewone structuur'
    | 'Coöperatie W.A. blijkens statuten structuurcoöperatie'
    | 'Coöperatie B.A. met gewone structuur'
    | 'Coöperatie B.A. blijkens statuten structuurcoöperatie'
    | 'Vereniging van eigenaars'
    | 'Vereniging met volledige rechtsbevoegdheid'
    | 'Vereniging met beperkte rechtsbevoegdheid'
    | 'Kerkgenootschap'
    | 'Stichting'
    | 'Onderlinge waarborgmaatschappij U.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge'
    | 'Onderlinge waarborgmaatschappij W.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge'
    | 'Onderlinge waarborgmaatschappij B.A. met gewone structuur'
    | 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge'
    | 'Publiekrechtelijke rechtspersoon'
    | 'Privaatrechtelijke rechtspersoon'
    | 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland'
    | 'Nevenvest. met hoofdvest. in buitenl.'
    | 'Europees economisch samenwerkingsverband'
    | 'Buitenl. EG-venn. met onderneming in Nederland'
    | 'Buitenl. EG-venn. met hoofdnederzetting in Nederland'
    | 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland'
    | 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland'
    | 'Coöperatie'
    | 'Vereniging'
  )[];

  export interface CasetypeEntity {
    type: string;
    id: string;
    meta: {
      last_modified: string;
      created: string;
      summary: string;
      is_eligible_for_case_creation: boolean;
    };
    attributes: {
      /**
       * Indicates that this casetype is a 'mother' of child casetypes
       */
      is_parent: boolean;
      /**
       * Fullname of the casetype
       */
      name: string;
      /**
       * Userdefined identification of this casetype
       */
      identification: string;
      /**
       * Describes this casetype in detail
       */
      description: string;
      /**
       * List of tags defining this casetype, used for faster searching
       */
      tags: string;
      /**
       * A templating line of text which defines the summary of a generated case
       */
      case_summary: string;
      /**
       * A templating line of text which defines the summary of a generated case which we use on public communication
       */
      case_public_summary: string;
      settings: {
        custom_webform: string;
        reuse_casedata: boolean;
        enable_webform: boolean;
        enable_online_payment: boolean;
        enable_manual_payment: boolean;
        require_email_on_webform: boolean;
        require_phonenumber_on_webform: boolean;
        require_mobilenumber_on_webform: boolean;
        disable_captcha_for_predefined_requestor: boolean;
        show_confidentiality: boolean;
        show_contact_info: boolean;
        enable_subject_relations_on_form: boolean;
        open_case_on_create: boolean;
        enable_allocation_on_form: boolean;
        disable_pip_for_requestor: boolean;
        lock_registration_phase: boolean;
        enable_queue_for_changes_from_other_subjects: boolean;
        check_acl_on_allocation: boolean;
        api_can_transition: boolean;
        is_public: boolean;
        list_of_default_folders: {
          parent?: string;
          name?: string;
          [k: string]: any;
        }[];
        text_confirmation_message_title: string;
        text_public_confirmation_message: string;
        payment: {
          assignee: {
            amount: number;
          };
          frontdesk: {
            amount: number;
          };
          phone: {
            amount: number;
          };
          mail: {
            amount: number;
          };
          email: {
            amount: number;
          };
          webform: {
            amount: number;
          };
        };
      };
      /**
       * Extra information about this casetype for archiving and other purposes
       */
      metadata: {
        /**
         * Whether postponing a case is a possibility
         */
        may_postpone: boolean;
        /**
         * Whether extending the duration of a case is a possibility
         */
        may_extend: boolean;
        /**
         * Amount of days a case can be extended
         */
        extension_period: number;
        /**
         * Amount of days a case can be adjourned
         */
        adjourn_period: number;
        /**
         * Link to the e-form of this casetype
         */
        'e-webform': string;
        process_description: string;
        /**
         * Motivation of this casetype (NL: Aanleiding)
         */
        motivation: string;
        /**
         * Purpose of this casetype (NL: Doel)
         */
        purpose: string;
        /**
         * Classification for archiving (NL: Archiefclassificatiecode)
         */
        archive_classification_code: string;
        /**
         * level of confidentiality (NL: Vertrouwelijkheidsaanduiding)
         */
        designation_of_confidentiality:
          | 'Openbaar'
          | 'Beperkt openbaar'
          | 'Intern'
          | 'Zaakvertrouwelijk'
          | 'Vertrouwelijk'
          | 'Confidentieel'
          | 'Geheim'
          | 'Zeer geheim';
        /**
         * The person, team, department or other subject responsible (NL: Verantwoordelijke)
         */
        responsible_subject: string;
        /**
         * The relationship between this casetype and a political reason (NL: Verantwoordingsrelatie)
         */
        responsible_relationship: string;
        /**
         * Whether its possible to object and appeal to this casetype
         */
        possibility_for_objection_and_appeal: boolean;
        /**
         * Whether a decision needs to be published
         */
        publication: boolean;
        /**
         * The text used for publishing decisions of this casetype
         */
        publication_text: string;
        bag: boolean;
        /**
         * Will cases from this casetype automatically turn into a positive result for the citizen when this case is not finished in time.
         */
        lex_silencio_positivo: boolean;
        /**
         * Whether a penalty is required (NL: Wet dwangsom)
         */
        penalty_law: boolean;
        /**
         * Whether the 'NL: Wet Publiekrechtelijke Beperkingen' applies
         */
        wkpb_applies: boolean;
        /**
         * The legal basis (NL: Wettelijke grondslag) of this casetype
         */
        legal_basis: string;
        /**
         * The legal basis (NL: Lokale grondslag) of this casetype
         */
        local_basis: string;
      };
      initiator_type:
        | 'aangaan'
        | 'aangeven'
        | 'aanmelden'
        | 'aanschrijven'
        | 'aanvragen'
        | 'afkopen'
        | 'afmelden'
        | 'indienen'
        | 'inschrijven'
        | 'melden'
        | 'ontvangen'
        | 'opstellen'
        | 'opzeggen'
        | 'registreren'
        | 'reserveren'
        | 'starten'
        | 'stellen'
        | 'uitvoeren'
        | 'vaststellen'
        | 'versturen'
        | 'voordragen'
        | 'vragen';
      initiator_source: 'internal' | 'external' | 'all';
      terms: {
        /**
         * The maximum duration of a case according to the law
         */
        lead_time_legal: {
          type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
          /**
           * The value of 'lead_time_legal' a case may take
           */
          value: string | number;
        };
        /**
         * The maximum duration of a case we would like
         */
        lead_time_service: {
          type: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
          /**
           * The value of 'lead_time_service' a case may take
           */
          value: string | number;
        };
      };
      requestor: {
        type_of_requestors: (
          | 'niet_natuurlijk_persoon'
          | 'preset_client'
          | 'natuurlijk_persoon_na'
          | 'natuurlijk_persoon'
          | 'medewerker'
        )[];
        predefined_requestor?: string;
        use_for_correspondence: boolean;
      };
      relates_to?: {
        subject?: {
          type_of_requestors?: string[];
          predefined_requestor?: string;
          [k: string]: any;
        };
        location?: {
          location_from?: 'requestor' | 'attribute';
          [k: string]: any;
        };
        [k: string]: any;
      };
      phases: {
        name?: string;
        milestone?: number;
        allocation?: {
          department?: {
            id?: number;
            name?: string;
            [k: string]: any;
          };
          role?: {
            id?: number;
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_fields?: {
          name: string;
          public_name: string;
          title: string;
          title_multiple: string;
          description: string;
          is_required: boolean;
          external_description: string;
          publish_on: {
            name: string;
          }[];
          requestor_can_change_from_pip: boolean;
          is_hidden_field: boolean;
          edit_authorizations: {
            [k: string]: any;
          }[];
          enable_skip_of_queue: boolean;
          sensitive_data: boolean;
          field_magic_string: string;
          field_type: 'text' | 'option' | 'checkbox';
          field_options: string[];
          default_value: string;
          referential: boolean;
          date_field_limit: {
            start?: {
              active: boolean;
              interval_type: 'days' | 'weeks' | 'months' | 'years';
              interval: number;
              during: 'pre' | 'post';
              refernce: 'current';
            };
            end?: {
              active: boolean;
              interval_type: 'days' | 'weeks' | 'months' | 'years';
              interval: number;
              during: 'pre' | 'post';
              refernce: 'current';
            };
          };
        }[];
        rules?: {
          label?: string;
          conditional_type?: 'AND' | 'OR';
          when?: {
            condition?: string;
            values?: string[];
            immediate?: boolean;
            [k: string]: any;
          };
          then?: {
            action?: string;
            attribute?: string;
            [k: string]: any;
          };
          else?: {
            action?: string;
            attribute?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        document_templates?: {
          related_document_template_element?: string;
          description?: string;
          generate_on_transition?: boolean;
          /**
           * Add to this custom field of type 'document'
           */
          add_to_case_document?: string;
          storage_format?: 'odt' | 'pdf' | 'docx';
          [k: string]: any;
        }[];
        cases?: {
          type_of_relation?:
            | 'deelzaak'
            | 'gerelateerd'
            | 'vervolgzaak'
            | 'vervolgzaak_datum';
          related_casetype_element?: string;
          allocation?: {
            department?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            role?: {
              id?: number;
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          requestor?: {
            requestor_type?:
              | 'aanvrager'
              | 'betrokkene'
              | 'anders'
              | 'behandelaar'
              | 'ontvanger';
            related_role?: string;
            /**
             * Not implemented yet
             */
            custom_subject?: {
              subject_type?: 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Phase number before which this case must be resolved
           */
          resolve_before_phase?: number;
          copy_custom_fields_from_parent?: boolean;
          start_on_transition?: boolean;
          /**
           * Automatically open the case (in behandeling)
           */
          open_case_on_create?: boolean;
          /**
           * Not implemented yet
           */
          related_subject?: {
            subject?: {
              subject_type?: 'person' | 'organization';
              id?: string;
              [k: string]: any;
            };
            role?: string;
            magic_string_prefix?: string;
            /**
             * Authorized to view this case on the PIP (NL: gemachtigde)
             */
            authorized_subject?: boolean;
            send_confirmation_message?: boolean;
            [k: string]: any;
          };
          show_in_pip?: boolean;
          label_in_pip?: string;
          [k: string]: any;
        }[];
        emails?: {
          related_email_element?: string;
          recipient?: {
            recipient_type?:
              | 'requestor'
              | 'assignee'
              | 'coordinator'
              | 'employee'
              | 'authorized_subject'
              | 'related_subject'
              | 'custom';
            id?: string;
            role?: string;
            emailaddress?: string;
            [k: string]: any;
          };
          cc?: string;
          bcc?: string;
          send_on_transition?: boolean;
          [k: string]: any;
        }[];
        subjects?: {
          subject_type?: 'person' | 'employee';
          id?: string;
          role?: string;
          magic_string_prefix?: string;
          authorized_subject?: boolean;
          send_confirmation_message?: boolean;
          [k: string]: any;
        }[];
        results?: {
          title?: string;
          is_default_value?: boolean;
          result?: string;
          archival_attributes?: {
            state?: 'vernietigen' | 'overdragen';
            selection_list?: string;
            selection_list_source_date?: string;
            selection_list_end_date?: string;
            activate_period_of_preservation?: boolean;
            type_of_archiving?: 'vernietigen';
            period_of_preservation?: string;
            archiving_procedure?: 'Afhandeling';
            description?: string;
            selection_list_number?: string;
            process_type_number?: string;
            process_type_name?: string;
            process_type_description?: string;
            process_type_object?: string;
            process_type_generic?: 'generic' | 'specific';
            origin?: string;
            process_term?: 'A' | 'B' | 'C' | 'D' | 'E';
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        checklist_items?: string[];
        [k: string]: any;
      }[];
      authorizations?: {
        [k: string]: any;
      }[];
      changelog?: {
        summary?: string;
        changes?: {
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
    relationships?: {
      /**
       * In which folder this casetype resides
       */
      catalogue_folder?: {
        data?: {
          type?: 'folder';
          id?: string;
          meta?: {
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Related parent casetype
       */
      parent?: {
        data?: {
          type?: 'casetype';
          id?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * List of child casetypes of this mother casetype
       */
      children?: {
        data?: {
          type?: 'casetype';
          id?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  export interface JsonAPILinks {
    links?: {
      self: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Department users can be a part of
   */
  export interface DepartmentEntity {
    type: 'department';
    meta?: {
      summary: string;
      [k: string]: any;
    };
    id: string;
    attributes?: {
      name: string;
      description?: string;
      [k: string]: any;
    };
    relationships?: {
      parent?: {
        type: 'department';
        id: string;
        meta?: {
          summary: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Role users of the system can have
   */
  export interface RoleEntity {
    type: 'role';
    id: string;
    attributes?: {
      name: string;
      description?: string;
      [k: string]: any;
    };
    relationships?: {
      parent?: {
        type: 'department';
        id: string;
        meta?: {
          summary: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RelationshipObject {
    data: {
      type?: string;
      id?: string;
      [k: string]: any;
    };
    meta?: {
      name?: string;
      [k: string]: any;
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    } & {
      related?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SubjectRelationEntity {
    type: string;
    id: string;
    attributes: {
      role: string;
      magic_string_prefix: string;
      /**
       * flag to check if the subject_relation is pip_authorized. Only if the subject_relation is of type person/organization
       */
      authorized: boolean;
      /**
       * permission of subject_relations. Only if subject_relation is of type employee
       */
      permission: 'none' | 'search' | 'read' | 'write';
      /**
       * flag that indicates whether this is a 'preset' subject relation
       */
      is_preset_client: boolean;
      [k: string]: any;
    };
    relationships: {
      subject: {
        data: {
          type?: string;
          id?: string;
          [k: string]: any;
        };
        meta?: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        } & {
          related?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      case: {
        data: {
          type?: string;
          id?: string;
          [k: string]: any;
        };
        meta?: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        } & {
          related?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    links: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  export interface TaskEntity {
    type: string;
    id: string;
    meta: {
      is_editable: boolean;
      can_set_completion: boolean;
    };
    attributes: {
      completed: boolean;
      description: string;
      title: string;
      due_date: string;
      phase: number;
    };
    relationships?: {
      case: {
        data: {
          id: string;
          type: 'case';
        };
        meta?: {
          phase?: number;
          display_number?: number;
          [k: string]: any;
        };
      };
      case_type?: {
        data: {
          id: string;
          type: 'case_type';
        };
        meta: {
          display_name: string;
        };
      };
      department?: {
        data: {
          id: string;
          type: 'department';
        };
        meta: {
          display_name: string;
        };
      };
      assignee?: {
        data: {
          id: string;
          type: 'employee';
          meta: {
            display_name: string;
          };
        };
      };
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  /**
   * Definition of an object
   */
  export interface CustomObjectType {
    type: string;
    id: string;
    meta: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes: {
      /**
       * Name of this custom object type
       */
      name?: string;
      /**
       * Internal identifier of this specific custom object type version
       */
      uuid?: string;
      /**
       * Title for the created custom object type
       */
      title: string;
      /**
       * The current status of this custom object type
       */
      status?: 'active' | 'inactive';
      /**
       * The current version of this custom object type'
       */
      version?: number;
      /**
       * Date this object got created
       */
      date_created?: string;
      /**
       * Last modified date for this object
       */
      last_modified?: string;
      /**
       * Deleted date for this object
       */
      date_deleted?: string;
      /**
       * The version independent UUID for this object
       */
      version_independent_uuid?: string;
      /**
       * Indicates whether this entity is the latest version
       */
      is_active_version?: boolean;
      /**
       * Definition of the related custom fields
       */
      custom_field_definition?: {
        /**
         * List of custom fields
         */
        custom_fields?: {
          /**
           * Label of this field
           */
          label: string;
          /**
           * Name of this field in the catalog
           */
          name: string;
          /**
           * Indicates whether this field is a required field
           */
          is_required?: boolean;
          /**
           * Indicates whether this field is a hidden field, or 'system field'
           */
          is_hidden_field?: boolean;
          /**
           * Description of this field for internal purposes
           */
          description?: string;
          /**
           * Description of this field for public purposes
           */
          external_description?: string;
          /**
           * Type of this custom field
           */
          custom_field_type:
            | 'bankaccount'
            | 'date'
            | 'email'
            | 'geojson'
            | 'option'
            | 'relationship'
            | 'richtext'
            | 'select'
            | 'text'
            | 'textarea'
            | 'url'
            | 'valuta'
            | 'address_v2'
            | 'numeric'
            | 'checkbox';
          /**
           * The magic string of this field
           */
          magic_string?: string;
          /**
           * UUID referencing the field in our attribute catalog
           */
          attribute_uuid?: string;
          /**
           * The CustomField specific attributes for this field
           */
          custom_field_specification?: {
            /**
             * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
             */
            use_on_map?: boolean;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      /**
       * Authorization settings for this custom object type
       */
      authorization_definition?: {
        /**
         * Optional list of authorizations for this object
         */
        authorizations?: {
          /**
           * Set of permissions
           */
          authorization?: 'read' | 'readwrite' | 'admin';
          /**
           * Role name
           */
          role: {
            /**
             * Unique identifier for this role
             */
            uuid: string;
            /**
             * Name of the role
             */
            name?: string;
            [k: string]: any;
          };
          /**
           * Group name
           */
          department?: {
            /**
             * Unique identifier for this department
             */
            uuid: string;
            /**
             * Name of the department
             */
            name?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
    };
    relationships?: {
      latest_version?: {
        data: {
          id: string;
          type: 'custom_object_type';
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
  }

  /**
   * Definition of a object type related custom field
   */
  export interface CustomObjectTypeCustomField {
    /**
     * Label of this field
     */
    label: string;
    /**
     * Name of this field in the catalog
     */
    name: string;
    /**
     * Indicates whether this field is a required field
     */
    is_required?: boolean;
    /**
     * Indicates whether this field is a hidden field, or 'system field'
     */
    is_hidden_field?: boolean;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Type of this custom field
     */
    custom_field_type:
      | 'bankaccount'
      | 'date'
      | 'email'
      | 'geojson'
      | 'option'
      | 'relationship'
      | 'richtext'
      | 'select'
      | 'text'
      | 'textarea'
      | 'url'
      | 'valuta'
      | 'address_v2'
      | 'numeric'
      | 'checkbox';
    /**
     * The magic string of this field
     */
    magic_string?: string;
    /**
     * UUID referencing the field in our attribute catalog
     */
    attribute_uuid?: string;
    /**
     * The CustomField specific attributes for this field
     */
    custom_field_specification?: {
      /**
       * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
       */
      use_on_map?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Custom field definition pointing to configured custom fields
   */
  export interface CustomObjectTypeCustomFieldDefinition {
    /**
     * List of custom fields
     */
    custom_fields?: {
      /**
       * Label of this field
       */
      label: string;
      /**
       * Name of this field in the catalog
       */
      name: string;
      /**
       * Indicates whether this field is a required field
       */
      is_required?: boolean;
      /**
       * Indicates whether this field is a hidden field, or 'system field'
       */
      is_hidden_field?: boolean;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Type of this custom field
       */
      custom_field_type:
        | 'bankaccount'
        | 'date'
        | 'email'
        | 'geojson'
        | 'option'
        | 'relationship'
        | 'richtext'
        | 'select'
        | 'text'
        | 'textarea'
        | 'url'
        | 'valuta'
        | 'address_v2'
        | 'numeric'
        | 'checkbox';
      /**
       * The magic string of this field
       */
      magic_string?: string;
      /**
       * UUID referencing the field in our attribute catalog
       */
      attribute_uuid?: string;
      /**
       * The CustomField specific attributes for this field
       */
      custom_field_specification?: {
        /**
         * Flag which indicates wether the Geolocation(s) of this custom_field is used in the map of custom_object
         */
        use_on_map?: boolean;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeAuthorization {
    /**
     * Set of permissions
     */
    authorization?: 'read' | 'readwrite' | 'admin';
    /**
     * Role name
     */
    role: {
      /**
       * Unique identifier for this role
       */
      uuid: string;
      /**
       * Name of the role
       */
      name?: string;
      [k: string]: any;
    };
    /**
     * Group name
     */
    department?: {
      /**
       * Unique identifier for this department
       */
      uuid: string;
      /**
       * Name of the department
       */
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * A role in the organization
   */
  export interface CustomObjectTypeRole {
    /**
     * Unique identifier for this role
     */
    uuid: string;
    /**
     * Name of the role
     */
    name?: string;
    [k: string]: any;
  }

  /**
   * A department within an organization
   */
  export interface CustomObjectTypeDepartment {
    /**
     * Unique identifier for this department
     */
    uuid: string;
    /**
     * Name of the department
     */
    name?: string;
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeAuthorizationDefinition {
    /**
     * Optional list of authorizations for this object
     */
    authorizations?: {
      /**
       * Set of permissions
       */
      authorization?: 'read' | 'readwrite' | 'admin';
      /**
       * Role name
       */
      role: {
        /**
         * Unique identifier for this role
         */
        uuid: string;
        /**
         * Name of the role
         */
        name?: string;
        [k: string]: any;
      };
      /**
       * Group name
       */
      department?: {
        /**
         * Unique identifier for this department
         */
        uuid: string;
        /**
         * Name of the department
         */
        name?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface CustomObjectTypeRelationship {
    /**
     * Name of this relationship in the catalog
     */
    name?: string;
    /**
     * UUID of related Custom Object Type
     */
    custom_object_type_uuid?: string;
    /**
     * UUID of related Case Type
     */
    case_type_uuid?: string;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicate that this relationship is required on creation of this object
     */
    is_required?: boolean;
    [k: string]: any;
  }

  /**
   * Relationship settings for this object type
   */
  export interface CustomObjectTypeRelationshipDefinition {
    /**
     * Optional list of relationships for this object
     */
    relationships?: {
      /**
       * Name of this relationship in the catalog
       */
      name?: string;
      /**
       * UUID of related Custom Object Type
       */
      custom_object_type_uuid?: string;
      /**
       * UUID of related Case Type
       */
      case_type_uuid?: string;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicate that this relationship is required on creation of this object
       */
      is_required?: boolean;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface CustomObjectArchiveMetadata {
    /**
     * Archival status for this custom object
     */
    status?: 'archived' | 'to destroy' | 'to preserve';
    /**
     * Archival ground for this custom object
     */
    ground?: string;
    /**
     * Archival retention for this custom object
     */
    retention?: number;
    [k: string]: any;
  }

  /**
   * A custom object
   */
  export interface CustomObject {
    type: string;
    id: string;
    meta: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes: {
      /**
       * Name of the related object type
       */
      name?: string;
      /**
       * Title of this specific object
       */
      title?: string;
      /**
       * Internal identifier of this specific custom object version
       */
      uuid?: string;
      /**
       * The current status of this custom object
       */
      status?: 'active' | 'inactive' | 'draft';
      /**
       * The current version of this custom object
       */
      version?: number;
      /**
       * Date this custom object got created
       */
      date_created?: string;
      /**
       * Last modified date for this custom object
       */
      last_modified?: string;
      /**
       * Deleted date for this custom object
       */
      date_deleted?: string;
      /**
       * The version independent UUID for this object
       */
      version_independent_uuid?: string;
      /**
       * Indicates whether this entity is the latest version
       */
      is_active_version?: boolean;
      /**
       * Key-value pair of custom fields
       */
      custom_fields?: {
        [k: string]: any;
      };
      /**
       * Archiving metadata for this object
       */
      archive_metadata?: {
        /**
         * Archival status for this custom object
         */
        status?: 'archived' | 'to destroy' | 'to preserve';
        /**
         * Archival ground for this custom object
         */
        ground?: string;
        /**
         * Archival retention for this custom object
         */
        retention?: number;
        [k: string]: any;
      };
      [k: string]: any;
    };
    relationships?: {
      custom_object_type?: {
        data: {
          id: string;
          type: 'custom_object_type';
        };
        links?: {
          links?: {
            self: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
      };
    };
  }

  export interface TimelineEntity {
    id?: string;
    type?: string;
    meta?: {
      /**
       * Describing summary of this object
       */
      summary?: string;
      [k: string]: any;
    };
    attributes?: {
      id: number;
      type: string;
      date: string;
      user: string;
      description: string;
    };
    links?: {
      links?: {
        self: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefArchivalAttributes {
    /**
     * selection list
     */
    selection_list?: string;
    /**
     * archival state
     */
    state?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefAssignee {
    /**
     * Name of the assignee of the case
     */
    name?: string;
    /**
     * UUID of the assignee of the case
     */
    uuid?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefAssigneeInfo {
    /**
     * Name of the assignee
     */
    name: string;
    /**
     * UUID of the assignee
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefAuthorization = 'read' | 'readwrite' | 'admin';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseDocumentAttachment {
    /**
     * Id of case document
     */
    case_document_ids: number;
    /**
     * Name of case_document attachment
     */
    name: string;
    /**
     * Selected
     */
    selected?: number;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseMeta {
    /**
     * case handling info
     */
    afhandeling?: string;
    /**
     * Id of case meta
     */
    id?: number;
    /**
     * case suspension info
     */
    opschorten?: string;
    /**
     * stalled since date
     */
    stalled_since_date?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCasePayment {
    /**
     * payment amount
     */
    amount?: number;
    /**
     * payment status
     */
    status?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCasePhase {
    /**
     * label of phase
     */
    label?: string;
    /**
     * milestone label of the phase
     */
    milestone_label?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseRelatedToCaseType {
    /**
     * Allocation of case_type
     */
    allocation?: {
      /**
       * Department assigned for phase
       */
      department: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the department
             */
            description?: string;
            /**
             * Name of the department
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role assigned for phase
       */
      role: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the role
             */
            description?: string;
            /**
             * Name of the role
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role set for phase
       */
      role_set?: number;
      [k: string]: any;
    };
    /**
     * Copy custom_fields from parent
     */
    copy_custom_fields_from_parent?: boolean;
    /**
     * Label case in pip
     */
    label_in_pip?: string;
    /**
     * Open case on create
     */
    open_case_on_create?: boolean;
    /**
     * UUID of related element
     */
    related_casetype_element: string;
    /**
     * Requestor of related case
     */
    requestor?: {
      /**
       * Related role for case
       */
      related_role?: string;
      /**
       * Type of requestor for case
       */
      requestor_type: string;
      [k: string]: any;
    };
    /**
     * Resolve before phase
     */
    resolve_before_phase?: number;
    /**
     * Show the case in pip
     */
    show_in_pip?: boolean;
    /**
     * Boolean to check if the case_type starts on transition
     */
    start_on_transition?: boolean;
    /**
     * Type of case_relation
     */
    type_of_relation: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseResult {
    /**
     * archival attributes
     */
    archival_attributes?: {
      /**
       * selection list
       */
      selection_list?: string;
      /**
       * archival state
       */
      state?: string;
      [k: string]: any;
    };
    /**
     * result of the case
     */
    result?: string;
    /**
     * result id
     */
    result_id?: number;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeAllocation {
    /**
     * Department assigned for phase
     */
    department: {
      /**
       * Pydantic based Entity object
       *
       * Entity object based on pydantic and the "new way" of creating entities in
       * our minty platform. It has the same functionality as the EntityBase object,
       * but does not depend on it. Migrationpaths are unsure.
       */
      data: {
        attributes: {
          /**
           * Longer description of the department
           */
          description?: string;
          /**
           * Name of the department
           */
          name: string;
          [k: string]: any;
        };
        id: string;
        links?: {
          self?: string;
          [k: string]: any;
        };
        meta?: {
          /**
           * Human readable summary of the content of the object
           */
          summary?: string;
          [k: string]: any;
        };
        relationships?: {
          parent?: {
            data: {
              id?: string;
              type?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        type: string;
        [k: string]: any;
      };
      links: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    /**
     * Role assigned for phase
     */
    role: {
      /**
       * Pydantic based Entity object
       *
       * Entity object based on pydantic and the "new way" of creating entities in
       * our minty platform. It has the same functionality as the EntityBase object,
       * but does not depend on it. Migrationpaths are unsure.
       */
      data: {
        attributes: {
          /**
           * Longer description of the role
           */
          description?: string;
          /**
           * Name of the role
           */
          name: string;
          [k: string]: any;
        };
        id: string;
        links?: {
          self?: string;
          [k: string]: any;
        };
        meta?: {
          /**
           * Human readable summary of the content of the object
           */
          summary?: string;
          [k: string]: any;
        };
        relationships?: {
          parent?: {
            data: {
              id?: string;
              type?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        type: string;
        [k: string]: any;
      };
      links: {
        self?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    /**
     * Role set for phase
     */
    role_set?: number;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeDefaultFolder {
    /**
     * Name of default folder
     */
    name: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeMetaData {
    /**
     * Source of the person
     */
    adjourn_period?: string;
    /**
     * Source of the person
     */
    archive_classification_code?: string;
    /**
     * Source of the person
     */
    bag?: string;
    /**
     * Source of the person
     */
    designation_of_confidentiality?: string;
    /**
     * Source of the person
     */
    e_webform?: string;
    /**
     * Source of the person
     */
    extension_period?: string;
    /**
     * Source of the person
     */
    legal_basis: string;
    /**
     * Source of the person
     */
    lex_silencio_positivo?: string;
    /**
     * Source of the person
     */
    local_basis?: string;
    /**
     * Source of the person
     */
    may_extend?: string;
    /**
     * Source of the person
     */
    may_postpone?: string;
    /**
     * Source of the person
     */
    motivation?: string;
    /**
     * Source of the person
     */
    penalty_law?: string;
    /**
     * Source of the person
     */
    possibility_for_objection_and_appeal?: string;
    /**
     * Process description of case_type
     */
    process_description: string;
    /**
     * Source of the person
     */
    publication?: string;
    /**
     * Source of the person
     */
    publication_text?: string;
    /**
     * Source of the person
     */
    purpose?: string;
    /**
     * Source of the person
     */
    responsible_relationship?: string;
    /**
     * Source of the person
     */
    responsible_subject?: string;
    /**
     * Source of the person
     */
    wkpb_applies?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypePayment {
    /**
     * Payment for assignee
     */
    assignee?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    /**
     * Payment for frontdesk
     */
    frontdesk?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    /**
     * Payment for mail
     */
    mail?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    /**
     * Payment for phone
     */
    phone?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    /**
     * Payment for post
     */
    post?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    /**
     * Payment for webform
     */
    webform?: {
      /**
       * Source of the person
       */
      amount?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypePaymentDict {
    /**
     * Source of the person
     */
    amount?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypePhase {
    /**
     * Allocation of case_type phase
     */
    allocation?: {
      /**
       * Department assigned for phase
       */
      department: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the department
             */
            description?: string;
            /**
             * Name of the department
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role assigned for phase
       */
      role: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the role
             */
            description?: string;
            /**
             * Name of the role
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role set for phase
       */
      role_set?: number;
      [k: string]: any;
    };
    /**
     * Cases releted to case_type
     */
    cases?: {
      /**
       * Allocation of case_type
       */
      allocation?: {
        /**
         * Department assigned for phase
         */
        department: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Role assigned for phase
         */
        role: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the role
               */
              description?: string;
              /**
               * Name of the role
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Role set for phase
         */
        role_set?: number;
        [k: string]: any;
      };
      /**
       * Copy custom_fields from parent
       */
      copy_custom_fields_from_parent?: boolean;
      /**
       * Label case in pip
       */
      label_in_pip?: string;
      /**
       * Open case on create
       */
      open_case_on_create?: boolean;
      /**
       * UUID of related element
       */
      related_casetype_element: string;
      /**
       * Requestor of related case
       */
      requestor?: {
        /**
         * Related role for case
         */
        related_role?: string;
        /**
         * Type of requestor for case
         */
        requestor_type: string;
        [k: string]: any;
      };
      /**
       * Resolve before phase
       */
      resolve_before_phase?: number;
      /**
       * Show the case in pip
       */
      show_in_pip?: boolean;
      /**
       * Boolean to check if the case_type starts on transition
       */
      start_on_transition?: boolean;
      /**
       * Type of case_relation
       */
      type_of_relation: string;
      [k: string]: any;
    }[];
    /**
     * Checklist items releted to case_type
     */
    checklist_items?: string[];
    /**
     * Custom_fields releted to case_type
     */
    custom_fields?: {
      /**
       * Options available for custom_field
       */
      date_field_limit?: {
        /**
         * Name of custom field publication
         */
        end?: {
          /**
           * Is the datelimit is active
           */
          active: boolean;
          /**
           * During time for datelimit
           */
          during: string;
          /**
           * Value of interval
           */
          interval: number;
          /**
           * Type of interval
           */
          interval_type: string;
          /**
           * Reference of datelimit
           */
          reference: string;
          [k: string]: any;
        };
        /**
         * Name of custom field publication
         */
        start?: {
          /**
           * Is the datelimit is active
           */
          active: boolean;
          /**
           * During time for datelimit
           */
          during: string;
          /**
           * Value of interval
           */
          interval: number;
          /**
           * Type of interval
           */
          interval_type: string;
          /**
           * Reference of datelimit
           */
          reference: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Default value for custom_field
       */
      default_value?: string;
      /**
       * Description of case_type
       */
      description?: string;
      /**
       * Edit authorizations for custom_field
       */
      edit_authorizations?: {
        /**
         * Department assigned for phase
         */
        department: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Role assigned for phase
         */
        role: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the role
               */
              description?: string;
              /**
               * Name of the role
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Role set for phase
         */
        role_set?: number;
        [k: string]: any;
      }[];
      /**
       * Is skip of queue enabled for case_type
       */
      enable_skip_of_queue?: boolean;
      /**
       * External description of case_type
       */
      external_description?: string;
      /**
       * Magic string of custom_field
       */
      field_magic_string: string;
      /**
       * Options available for custom_field
       */
      field_options?: string[];
      /**
       * Type of custom_field
       */
      field_type: string;
      /**
       * Is the custom_field hidden
       */
      is_hidden_field?: boolean;
      /**
       * Is the custom_field multiple choice
       */
      is_multiple?: boolean;
      /**
       * Is the custom_field required
       */
      is_required?: boolean;
      /**
       * Name of custom field
       */
      name: string;
      /**
       * Public name of custom_field
       */
      public_name?: string;
      /**
       * Places where the case_type is published
       */
      publish_on?: {
        /**
         * Name of custom field publication
         */
        name: string;
        [k: string]: any;
      }[];
      /**
       * Referential for custom_field
       */
      referential?: boolean;
      /**
       * Name of custom field relationship
       */
      relationship_name?: string;
      /**
       * Name of custom field relationship
       */
      relationship_subject_role?: string;
      /**
       * Type of relationship
       */
      relationship_type?: string;
      /**
       * Inertnal identifier of custom_field relationship
       */
      relationship_uuid?: string;
      /**
       * Can requestor chnage custom_field from pip
       */
      requestor_can_change_from_pip?: boolean;
      /**
       * Does the custom_field contains sensitive data
       */
      sensitive_data?: boolean;
      /**
       * Title of custom_object
       */
      title?: string;
      /**
       * Title multiple of custom_field
       */
      title_multiple?: string;
      /**
       * UUID of the catalog attribute
       */
      uuid: string;
      [k: string]: any;
    }[];
    /**
     * Documents releted to case_type
     */
    documents?: {
      /**
       * If the document is automatic
       */
      automatic?: boolean;
      /**
       * Copy custom_fields from parent
       */
      data: {
        /**
         * If the document is automatically generated
         */
        automatisch_genereren?: boolean;
        /**
         * Description
         */
        description?: string;
        /**
         * Filename of document
         */
        filename: string;
        /**
         * Target format for document
         */
        target_format: string;
        [k: string]: any;
      };
      /**
       * Label on document
       */
      label: string;
      [k: string]: any;
    }[];
    /**
     * Emails releted to case_type
     */
    emails?: {
      /**
       * If the email is automatic
       */
      automatic: boolean;
      /**
       * Email data
       */
      data: {
        /**
         * If the email is automatic
         */
        automatic_phase?: boolean;
        /**
         * BCC of email
         */
        bcc?: string;
        /**
         * Email receiver
         */
        behandelaar?: string;
        /**
         * Role of email receiver
         */
        betrokkene_role?: string;
        /**
         * Id of bibliotheek_notificatie
         */
        bibliotheek_notificaties_id: number;
        /**
         * Body of email
         */
        body: string;
        /**
         * CC of email
         */
        case_document_attachments?: {
          /**
           * Id of case document
           */
          case_document_ids: number;
          /**
           * Name of case_document attachment
           */
          name: string;
          /**
           * Selected
           */
          selected?: number;
          [k: string]: any;
        }[];
        /**
         * CC of email
         */
        cc?: string;
        /**
         * Description
         */
        description?: string;
        /**
         * Email template
         */
        email?: string;
        /**
         * Intern block of email
         */
        intern_block?: string;
        /**
         * Email rcpt
         */
        rcpt: string;
        /**
         * Name of sender
         */
        sender?: string;
        /**
         * Address of the sender
         */
        sender_address?: string;
        /**
         * Subject of email
         */
        subject: string;
        /**
         * Id of zaaktype_notificati
         */
        zaaktype_notificatie_id: number;
        [k: string]: any;
      };
      /**
       * Label on email
       */
      label: string;
      [k: string]: any;
    }[];
    /**
     * Milestone for case_type phase
     */
    milestone: number;
    /**
     * Name of case_type phase
     */
    name: string;
    /**
     * Phase number for case_type phase
     */
    phase: string;
    /**
     * Rules on case_type
     */
    rules?: {
      /**
       * Type of condition
       */
      condition_type: string;
      /**
       * List of rule conditions
       */
      conditions: {
        /**
         * Attribute releated to rule
         */
        kenmerk?:
          | (
              | 'contactchannel'
              | 'aanvrager'
              | 'payment_status'
              | 'confidentiality'
              | 'vertrouwelijkheidsaanduiding'
              | 'beroep_mogelijk'
              | 'publicatie'
              | 'bag'
              | 'lex_silencio_positivo'
              | 'opschorten_mogelijk'
              | 'verlenging_mogelijk'
              | 'wet_dwangsom'
              | 'wkpb'
              | 'preferred_contact_channel'
            )
          | string;
        /**
         * Value of rule condition
         */
        value?: {
          [k: string]: any;
        };
        /**
         * Value checkbox
         */
        value_checkbox?: {
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      /**
       * List of rule actions to execute when the condition matches
       */
      match_actions: {
        /**
         * Case type rule action
         */
        action?:
          | (
              | 'set_value'
              | 'set_value_formula'
              | 'set_value_magic_string'
              | 'show_attribute'
              | 'hide_attribute'
              | 'show_text_block'
              | 'hide_text_block'
              | 'pause_application'
              | 'hide_group'
              | 'show_group'
              | 'set_allocation'
              | 'change_confidentiality'
              | 'change_html_mail_template'
            )
          | string;
        /**
         * Value
         */
        value?: {
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      /**
       * Name of case_type_rule
       */
      name: string;
      /**
       * List of rule actions to execute when the condition doesnot match
       */
      nomatch_actions?: {
        /**
         * Case type rule action
         */
        action?:
          | (
              | 'set_value'
              | 'set_value_formula'
              | 'set_value_magic_string'
              | 'show_attribute'
              | 'hide_attribute'
              | 'show_text_block'
              | 'hide_text_block'
              | 'pause_application'
              | 'hide_group'
              | 'show_group'
              | 'set_allocation'
              | 'change_confidentiality'
              | 'change_html_mail_template'
            )
          | string;
        /**
         * Value
         */
        value?: {
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      [k: string]: any;
    }[];
    /**
     * Subjects releted to case_type
     */
    subjects?: {
      /**
       * If the email is automatic
       */
      automatic: boolean;
      /**
       * Subject data
       */
      data: {
        /**
         * Betrokkene Identifier
         */
        betrokkene_identifier?: string;
        /**
         * Type of subject
         */
        betrokkene_type: string;
        /**
         * Boolean indicating if the subject is authorized
         */
        gemachtigd?: boolean;
        /**
         * Magic string for subject
         */
        magic_string_prefix?: string;
        /**
         * Name of subject
         */
        naam: string;
        /**
         * Should the subject get notified
         */
        notify?: boolean;
        /**
         * Role of subject
         */
        rol?: string;
        /**
         * UUID of the subject
         */
        uuid: string;
        [k: string]: any;
      };
      /**
       * Label of subject relationship
       */
      label: string;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Price details for case type
   */
  export interface DefCaseTypePrice {
    /**
     * Price for counter
     */
    counter?: string;
    /**
     * Price for email
     */
    email?: string;
    /**
     * Price for employee
     */
    employee?: string;
    /**
     * Price for post
     */
    post?: string;
    /**
     * Price for telephone
     */
    telephone?: string;
    /**
     * Price for web
     */
    web?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeRequestor {
    /**
     * Source of the person
     */
    type_of_requestors?: string[];
    /**
     * Source of the person
     */
    use_for_correspondence?: boolean;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeSettings {
    /**
     * Boolean indicates allow reuse of case_data
     */
    allow_reuse_casedata?: boolean;
    /**
     * Boolean indicates if API can transition
     */
    api_can_transition?: boolean;
    /**
     * Boolean indicates if there is an ACL check on allocation
     */
    check_acl_on_allocation?: boolean;
    /**
     * Boolean indicates disable_captcha_for_predefined_requestor
     */
    disable_captcha_for_predefined_requestor?: boolean;
    /**
     * Boolean indicates disable pip for requestor
     */
    disable_pip_for_requestor?: boolean;
    /**
     * Boolean indicates enable allocation on requestor
     */
    enable_allocation_on_form?: boolean;
    /**
     * Boolean indicates enable manual payment
     */
    enable_manual_payment?: boolean;
    /**
     * Boolean indicates enable online payment
     */
    enable_online_payment?: boolean;
    /**
     * Boolean indicates if queue for changes from other subjects is enabled
     */
    enable_queue_for_changes_from_other_subjects?: boolean;
    /**
     * Boolean indicates enabling subject relations on form
     */
    enable_subject_relations_on_form?: boolean;
    /**
     * Boolean indicates enable of webform
     */
    enable_webform?: boolean;
    /**
     * Boolean indicates if case_type is public
     */
    is_public?: boolean;
    /**
     * List of case_type default folders
     */
    list_of_default_folders?: {
      /**
       * Name of default folder
       */
      name: string;
      [k: string]: any;
    }[];
    /**
     * Boolean indicates lock_registration_phase
     */
    lock_registration_phase?: boolean;
    /**
     * Boolean indicates open case on create
     */
    open_case_on_create?: boolean;
    /**
     * Case type payment
     */
    payment: {
      /**
       * Payment for assignee
       */
      assignee?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      /**
       * Payment for frontdesk
       */
      frontdesk?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      /**
       * Payment for mail
       */
      mail?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      /**
       * Payment for phone
       */
      phone?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      /**
       * Payment for post
       */
      post?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      /**
       * Payment for webform
       */
      webform?: {
        /**
         * Source of the person
         */
        amount?: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    /**
     * Boolean indicates require email on webform
     */
    require_email_on_webform?: boolean;
    /**
     * Boolean indicates require mobile_number on webform
     */
    require_mobilenumber_on_webform?: boolean;
    /**
     * Boolean indicates require phonenumber on webform
     */
    require_phonenumber_on_webform?: boolean;
    /**
     * Boolean indicates showing confidentiality
     */
    show_confidentiality?: boolean;
    /**
     * Boolean indicates showing contact info
     */
    show_contact_info?: boolean;
    /**
     * Text confirmation message title
     */
    text_confirmation_message_title?: string;
    /**
     * Text public confirmation message title
     */
    text_public_confirmation_message?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeTermObject {
    /**
     * Type of case_type term
     */
    type: string;
    /**
     * Value of case_type term
     */
    value: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCaseTypeTerms {
    /**
     * Lead time legal
     */
    lead_time_legal: {
      /**
       * Type of case_type term
       */
      type: string;
      /**
       * Value of case_type term
       */
      value: string;
      [k: string]: any;
    };
    /**
     * Lead time service
     */
    lead_time_service: {
      /**
       * Type of case_type term
       */
      type: string;
      /**
       * Value of case_type term
       */
      value: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCasetypeRule {
    /**
     * Type of condition
     */
    condition_type: string;
    /**
     * List of rule conditions
     */
    conditions: {
      /**
       * Attribute releated to rule
       */
      kenmerk?:
        | (
            | 'contactchannel'
            | 'aanvrager'
            | 'payment_status'
            | 'confidentiality'
            | 'vertrouwelijkheidsaanduiding'
            | 'beroep_mogelijk'
            | 'publicatie'
            | 'bag'
            | 'lex_silencio_positivo'
            | 'opschorten_mogelijk'
            | 'verlenging_mogelijk'
            | 'wet_dwangsom'
            | 'wkpb'
            | 'preferred_contact_channel'
          )
        | string;
      /**
       * Value of rule condition
       */
      value?: {
        [k: string]: any;
      };
      /**
       * Value checkbox
       */
      value_checkbox?: {
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    /**
     * List of rule actions to execute when the condition matches
     */
    match_actions: {
      /**
       * Case type rule action
       */
      action?:
        | (
            | 'set_value'
            | 'set_value_formula'
            | 'set_value_magic_string'
            | 'show_attribute'
            | 'hide_attribute'
            | 'show_text_block'
            | 'hide_text_block'
            | 'pause_application'
            | 'hide_group'
            | 'show_group'
            | 'set_allocation'
            | 'change_confidentiality'
            | 'change_html_mail_template'
          )
        | string;
      /**
       * Value
       */
      value?: {
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    /**
     * Name of case_type_rule
     */
    name: string;
    /**
     * List of rule actions to execute when the condition doesnot match
     */
    nomatch_actions?: {
      /**
       * Case type rule action
       */
      action?:
        | (
            | 'set_value'
            | 'set_value_formula'
            | 'set_value_magic_string'
            | 'show_attribute'
            | 'hide_attribute'
            | 'show_text_block'
            | 'hide_text_block'
            | 'pause_application'
            | 'hide_group'
            | 'show_group'
            | 'set_allocation'
            | 'change_confidentiality'
            | 'change_html_mail_template'
          )
        | string;
      /**
       * Value
       */
      value?: {
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefContactInformation {
    /**
     * Email address of the contact
     */
    email?: string;
    /**
     * Internal note
     */
    internal_note?: string;
    /**
     * Mobile phone number
     */
    mobile_number?: string;
    /**
     * Preferred phone number
     */
    phone_number?: string;
    /**
     * Preffered contact channel
     */
    preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefContactInformationPerson {
    /**
     * Email address of the contact
     */
    email?: string;
    /**
     * Internal note
     */
    internal_note?: string;
    /**
     * Is the contact person is anonymous
     */
    is_an_anonymous_contact_person?: boolean;
    /**
     * Mobile phone number
     */
    mobile_number?: string;
    /**
     * Preferred phone number
     */
    phone_number?: string;
    /**
     * Preffered contact channel
     */
    preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
    [k: string]: any;
  }

  /**
   * Definition of a object type related custom field
   */
  export interface DefCustomField {
    /**
     * UUID referencing the field in our attribute catalog
     */
    attribute_uuid?: string;
    /**
     * Specification around a certain custom field type
     */
    custom_field_specification?:
      | {
          /**
           * Name of the relationship
           */
          name?: string;
          /**
           * Type of this relationship
           */
          type: 'case' | 'custom_object' | 'document' | 'subject';
          /**
           * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
           */
          use_on_map?: boolean;
          /**
           * UUID of the relationship type
           */
          uuid?: string;
          [k: string]: any;
        }
      | {
          /**
           * Use the address_v2 or geolocation on the map of custom_object.
           */
          use_on_map?: boolean;
          [k: string]: any;
        };
    /**
     * Type of this custom field
     */
    custom_field_type?:
      | 'relationship'
      | 'text'
      | 'option'
      | 'select'
      | 'textarea'
      | 'richtext'
      | 'checkbox'
      | 'date'
      | 'geojson'
      | 'valuta'
      | 'email'
      | 'url'
      | 'bankaccount'
      | 'address_v2'
      | 'numeric';
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicates whether this field is a hidden field, or 'system field'
     */
    is_hidden_field?: boolean;
    /**
     * Indicates whether this field is a required field
     */
    is_required?: boolean;
    /**
     * Label of this field
     */
    label: string;
    /**
     * The magic string of this field
     */
    magic_string?: string;
    /**
     * Indicates if multiple values are allowed.
     */
    multiple_values?: boolean;
    /**
     * Name of this field in the catalog
     */
    name: string;
    /**
     * List of possible options for this custom field
     */
    options?: string[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldDateLimit {
    /**
     * Name of custom field publication
     */
    end?: {
      /**
       * Is the datelimit is active
       */
      active: boolean;
      /**
       * During time for datelimit
       */
      during: string;
      /**
       * Value of interval
       */
      interval: number;
      /**
       * Type of interval
       */
      interval_type: string;
      /**
       * Reference of datelimit
       */
      reference: string;
      [k: string]: any;
    };
    /**
     * Name of custom field publication
     */
    start?: {
      /**
       * Is the datelimit is active
       */
      active: boolean;
      /**
       * During time for datelimit
       */
      during: string;
      /**
       * Value of interval
       */
      interval: number;
      /**
       * Type of interval
       */
      interval_type: string;
      /**
       * Reference of datelimit
       */
      reference: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldDateLimitObject {
    /**
     * Is the datelimit is active
     */
    active: boolean;
    /**
     * During time for datelimit
     */
    during: string;
    /**
     * Value of interval
     */
    interval: number;
    /**
     * Type of interval
     */
    interval_type: string;
    /**
     * Reference of datelimit
     */
    reference: string;
    [k: string]: any;
  }

  /**
   * Custom field definition pointing to configured custom fields
   */
  export interface DefCustomFieldDefinition {
    /**
     * List of custom fields
     */
    custom_fields?: {
      /**
       * UUID referencing the field in our attribute catalog
       */
      attribute_uuid?: string;
      /**
       * Specification around a certain custom field type
       */
      custom_field_specification?:
        | {
            /**
             * Name of the relationship
             */
            name?: string;
            /**
             * Type of this relationship
             */
            type: 'case' | 'custom_object' | 'document' | 'subject';
            /**
             * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
             */
            use_on_map?: boolean;
            /**
             * UUID of the relationship type
             */
            uuid?: string;
            [k: string]: any;
          }
        | {
            /**
             * Use the address_v2 or geolocation on the map of custom_object.
             */
            use_on_map?: boolean;
            [k: string]: any;
          };
      /**
       * Type of this custom field
       */
      custom_field_type?:
        | 'relationship'
        | 'text'
        | 'option'
        | 'select'
        | 'textarea'
        | 'richtext'
        | 'checkbox'
        | 'date'
        | 'geojson'
        | 'valuta'
        | 'email'
        | 'url'
        | 'bankaccount'
        | 'address_v2'
        | 'numeric';
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicates whether this field is a hidden field, or 'system field'
       */
      is_hidden_field?: boolean;
      /**
       * Indicates whether this field is a required field
       */
      is_required?: boolean;
      /**
       * Label of this field
       */
      label: string;
      /**
       * The magic string of this field
       */
      magic_string?: string;
      /**
       * Indicates if multiple values are allowed.
       */
      multiple_values?: boolean;
      /**
       * Name of this field in the catalog
       */
      name: string;
      /**
       * List of possible options for this custom field
       */
      options?: string[];
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldMapSpecification {
    /**
     * Use the address_v2 or geolocation on the map of custom_object.
     */
    use_on_map?: boolean;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldPublication {
    /**
     * Name of custom field publication
     */
    name: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldRelatedToCaseType {
    /**
     * Options available for custom_field
     */
    date_field_limit?: {
      /**
       * Name of custom field publication
       */
      end?: {
        /**
         * Is the datelimit is active
         */
        active: boolean;
        /**
         * During time for datelimit
         */
        during: string;
        /**
         * Value of interval
         */
        interval: number;
        /**
         * Type of interval
         */
        interval_type: string;
        /**
         * Reference of datelimit
         */
        reference: string;
        [k: string]: any;
      };
      /**
       * Name of custom field publication
       */
      start?: {
        /**
         * Is the datelimit is active
         */
        active: boolean;
        /**
         * During time for datelimit
         */
        during: string;
        /**
         * Value of interval
         */
        interval: number;
        /**
         * Type of interval
         */
        interval_type: string;
        /**
         * Reference of datelimit
         */
        reference: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    /**
     * Default value for custom_field
     */
    default_value?: string;
    /**
     * Description of case_type
     */
    description?: string;
    /**
     * Edit authorizations for custom_field
     */
    edit_authorizations?: {
      /**
       * Department assigned for phase
       */
      department: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the department
             */
            description?: string;
            /**
             * Name of the department
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role assigned for phase
       */
      role: {
        /**
         * Pydantic based Entity object
         *
         * Entity object based on pydantic and the "new way" of creating entities in
         * our minty platform. It has the same functionality as the EntityBase object,
         * but does not depend on it. Migrationpaths are unsure.
         */
        data: {
          attributes: {
            /**
             * Longer description of the role
             */
            description?: string;
            /**
             * Name of the role
             */
            name: string;
            [k: string]: any;
          };
          id: string;
          links?: {
            self?: string;
            [k: string]: any;
          };
          meta?: {
            /**
             * Human readable summary of the content of the object
             */
            summary?: string;
            [k: string]: any;
          };
          relationships?: {
            parent?: {
              data: {
                id?: string;
                type?: string;
                [k: string]: any;
              };
              links?: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
          type: string;
          [k: string]: any;
        };
        links: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      /**
       * Role set for phase
       */
      role_set?: number;
      [k: string]: any;
    }[];
    /**
     * Is skip of queue enabled for case_type
     */
    enable_skip_of_queue?: boolean;
    /**
     * External description of case_type
     */
    external_description?: string;
    /**
     * Magic string of custom_field
     */
    field_magic_string: string;
    /**
     * Options available for custom_field
     */
    field_options?: string[];
    /**
     * Type of custom_field
     */
    field_type: string;
    /**
     * Is the custom_field hidden
     */
    is_hidden_field?: boolean;
    /**
     * Is the custom_field multiple choice
     */
    is_multiple?: boolean;
    /**
     * Is the custom_field required
     */
    is_required?: boolean;
    /**
     * Name of custom field
     */
    name: string;
    /**
     * Public name of custom_field
     */
    public_name?: string;
    /**
     * Places where the case_type is published
     */
    publish_on?: {
      /**
       * Name of custom field publication
       */
      name: string;
      [k: string]: any;
    }[];
    /**
     * Referential for custom_field
     */
    referential?: boolean;
    /**
     * Name of custom field relationship
     */
    relationship_name?: string;
    /**
     * Name of custom field relationship
     */
    relationship_subject_role?: string;
    /**
     * Type of relationship
     */
    relationship_type?: string;
    /**
     * Inertnal identifier of custom_field relationship
     */
    relationship_uuid?: string;
    /**
     * Can requestor chnage custom_field from pip
     */
    requestor_can_change_from_pip?: boolean;
    /**
     * Does the custom_field contains sensitive data
     */
    sensitive_data?: boolean;
    /**
     * Title of custom_object
     */
    title?: string;
    /**
     * Title multiple of custom_field
     */
    title_multiple?: string;
    /**
     * UUID of the catalog attribute
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomFieldRelationshipSpecification {
    /**
     * Name of the relationship
     */
    name?: string;
    /**
     * Type of this relationship
     */
    type: 'case' | 'custom_object' | 'document' | 'subject';
    /**
     * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
     */
    use_on_map?: boolean;
    /**
     * UUID of the relationship type
     */
    uuid?: string;
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefCustomFieldRelationshipTypes =
    | 'case'
    | 'custom_object'
    | 'document'
    | 'subject';

  /**
   * An enumeration.
   */
  export type DefCustomFieldTypes =
    | 'relationship'
    | 'text'
    | 'option'
    | 'select'
    | 'textarea'
    | 'richtext'
    | 'checkbox'
    | 'date'
    | 'geojson'
    | 'valuta'
    | 'email'
    | 'url'
    | 'bankaccount'
    | 'address_v2'
    | 'numeric';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefCustomObjectArchiveMetadata {
    /**
     * Archival ground for this custom object
     */
    ground?: string;
    /**
     * Archival retention for this custom object
     */
    retention?: number;
    /**
     * Archival status for this custom object
     */
    status?: 'archived' | 'to destroy' | 'to preserve';
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefCustomObjectAuthorizationLevels =
    | 'read'
    | 'readwrite'
    | 'admin';

  /**
   * Auditlog for this Custom Object Type
   */
  export interface DefCustomObjectTypeAuditLog {
    /**
     * Description of this change
     */
    description: string;
    /**
     * Components affected by this change
     */
    updated_components: (
      | 'attributes'
      | 'authorizations'
      | 'custom_fields'
      | 'relationships'
    )[];
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeAuthorization {
    /**
     * Set of permissions
     */
    authorization: 'read' | 'readwrite' | 'admin';
    /**
     * Group name
     */
    department: {
      /**
       * Name of the department
       */
      name: string;
      /**
       * Unique identifier for this department
       */
      uuid: string;
      [k: string]: any;
    };
    /**
     * Role name
     */
    role: {
      /**
       * Name of the role
       */
      name: string;
      /**
       * Unique identifier for this role
       */
      uuid: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeAuthorizationDefinition {
    /**
     * Optional list of authorizations for this object
     */
    authorizations?: {
      /**
       * Set of permissions
       */
      authorization: 'read' | 'readwrite' | 'admin';
      /**
       * Group name
       */
      department: {
        /**
         * Name of the department
         */
        name: string;
        /**
         * Unique identifier for this department
         */
        uuid: string;
        [k: string]: any;
      };
      /**
       * Role name
       */
      role: {
        /**
         * Name of the role
         */
        name: string;
        /**
         * Unique identifier for this role
         */
        uuid: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * A department within an organization
   */
  export interface DefCustomObjectTypeDepartment {
    /**
     * Name of the department
     */
    name: string;
    /**
     * Unique identifier for this department
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Authorization settings for this object type
   */
  export interface DefCustomObjectTypeRelationship {
    /**
     * UUID of related Case Type
     */
    case_type_uuid?: string;
    /**
     * UUID of related Custom Object Type
     */
    custom_object_type_uuid?: string;
    /**
     * Description of this field for internal purposes
     */
    description?: string;
    /**
     * Description of this field for public purposes
     */
    external_description?: string;
    /**
     * Indicate that this relationship is required on creation of this object
     */
    is_required?: boolean;
    /**
     * Name of this relationship
     */
    name: string;
    [k: string]: any;
  }

  /**
   * Relationship settings for this object type
   */
  export interface DefCustomObjectTypeRelationshipDefinition {
    /**
     * Optional list of relationships for this object
     */
    relationships?: {
      /**
       * UUID of related Case Type
       */
      case_type_uuid?: string;
      /**
       * UUID of related Custom Object Type
       */
      custom_object_type_uuid?: string;
      /**
       * Description of this field for internal purposes
       */
      description?: string;
      /**
       * Description of this field for public purposes
       */
      external_description?: string;
      /**
       * Indicate that this relationship is required on creation of this object
       */
      is_required?: boolean;
      /**
       * Name of this relationship
       */
      name: string;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  /**
   * A role in the organization
   */
  export interface DefCustomObjectTypeRole {
    /**
     * Name of the role
     */
    name: string;
    /**
     * Unique identifier for this role
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Days left for the case
   */
  export interface DefDaysLeft {
    /**
     * Number of days left
     */
    amount: number;
    /**
     * Unit of measure
     */
    unit: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefDocumentData {
    /**
     * If the document is automatically generated
     */
    automatisch_genereren?: boolean;
    /**
     * Description
     */
    description?: string;
    /**
     * Filename of document
     */
    filename: string;
    /**
     * Target format for document
     */
    target_format: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefDocumentRelatedToCaseType {
    /**
     * If the document is automatic
     */
    automatic?: boolean;
    /**
     * Copy custom_fields from parent
     */
    data: {
      /**
       * If the document is automatically generated
       */
      automatisch_genereren?: boolean;
      /**
       * Description
       */
      description?: string;
      /**
       * Filename of document
       */
      filename: string;
      /**
       * Target format for document
       */
      target_format: string;
      [k: string]: any;
    };
    /**
     * Label on document
     */
    label: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefDutchAddress {
    /**
     * City
     */
    city: string;
    /**
     * Country
     */
    country: string;
    /**
     * Indicates that this is an address outside the Netherlands
     */
    is_foreign?: false;
    /**
     * Street name
     */
    street: string;
    /**
     * House number
     */
    street_number: number;
    /**
     * House letter (part of house number)
     */
    street_number_letter?: string;
    /**
     * House number extension
     */
    street_number_suffix?: string;
    /**
     * Postal code (zipcode)
     */
    zipcode?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefEmailData {
    /**
     * If the email is automatic
     */
    automatic_phase?: boolean;
    /**
     * BCC of email
     */
    bcc?: string;
    /**
     * Email receiver
     */
    behandelaar?: string;
    /**
     * Role of email receiver
     */
    betrokkene_role?: string;
    /**
     * Id of bibliotheek_notificatie
     */
    bibliotheek_notificaties_id: number;
    /**
     * Body of email
     */
    body: string;
    /**
     * CC of email
     */
    case_document_attachments?: {
      /**
       * Id of case document
       */
      case_document_ids: number;
      /**
       * Name of case_document attachment
       */
      name: string;
      /**
       * Selected
       */
      selected?: number;
      [k: string]: any;
    }[];
    /**
     * CC of email
     */
    cc?: string;
    /**
     * Description
     */
    description?: string;
    /**
     * Email template
     */
    email?: string;
    /**
     * Intern block of email
     */
    intern_block?: string;
    /**
     * Email rcpt
     */
    rcpt: string;
    /**
     * Name of sender
     */
    sender?: string;
    /**
     * Address of the sender
     */
    sender_address?: string;
    /**
     * Subject of email
     */
    subject: string;
    /**
     * Id of zaaktype_notificati
     */
    zaaktype_notificatie_id: number;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefEmailRelatedToCaseType {
    /**
     * If the email is automatic
     */
    automatic: boolean;
    /**
     * Email data
     */
    data: {
      /**
       * If the email is automatic
       */
      automatic_phase?: boolean;
      /**
       * BCC of email
       */
      bcc?: string;
      /**
       * Email receiver
       */
      behandelaar?: string;
      /**
       * Role of email receiver
       */
      betrokkene_role?: string;
      /**
       * Id of bibliotheek_notificatie
       */
      bibliotheek_notificaties_id: number;
      /**
       * Body of email
       */
      body: string;
      /**
       * CC of email
       */
      case_document_attachments?: {
        /**
         * Id of case document
         */
        case_document_ids: number;
        /**
         * Name of case_document attachment
         */
        name: string;
        /**
         * Selected
         */
        selected?: number;
        [k: string]: any;
      }[];
      /**
       * CC of email
       */
      cc?: string;
      /**
       * Description
       */
      description?: string;
      /**
       * Email template
       */
      email?: string;
      /**
       * Intern block of email
       */
      intern_block?: string;
      /**
       * Email rcpt
       */
      rcpt: string;
      /**
       * Name of sender
       */
      sender?: string;
      /**
       * Address of the sender
       */
      sender_address?: string;
      /**
       * Subject of email
       */
      subject: string;
      /**
       * Id of zaaktype_notificati
       */
      zaaktype_notificatie_id: number;
      [k: string]: any;
    };
    /**
     * Label on email
     */
    label: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefEnqueuedEmailData {
    /**
     * Internal identifier of case
     */
    case_uuid?: string;
    /**
     * Counter value for loop protection
     */
    loop_protection_counter?: number;
    /**
     * Type of related subject
     */
    subject_type?: string;
    /**
     * Internal identifier of subject
     */
    subject_uuid?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefInternationalAddress {
    /**
     * First line of the address
     */
    address_line_1?: string;
    /**
     * Second line of the address
     */
    address_line_2?: string;
    /**
     * Third line of the address
     */
    address_line_3?: string;
    /**
     * Country
     */
    country: string;
    /**
     * Indicates that this is an address outside the Netherlands
     */
    is_foreign?: true;
    [k: string]: any;
  }

  /**
   * Notification settings for an employee
   */
  export interface DefNotificationSettings {
    /**
     * Send notification when the suspension period of case has expired
     */
    case_suspension_term_exceeded: boolean;
    /**
     * Send notification outside zaaksytem when the processing time of case has expired
     */
    case_term_exceeded: boolean;
    /**
     * Send notification outside zaaksytem when new case is assigned to the employee
     */
    new_assigned_case: boolean;
    /**
     * Send notification outside zaaksytem when attribute has changed
     */
    new_attribute_proposal: boolean;
    /**
     * Notify outside zaaksytem in case of new document events.
     */
    new_document: boolean;
    /**
     * Send notification outside zaaksyteem when employee receives new pip message
     */
    new_ext_pip_message: boolean;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefOrganizationActivity {
    /**
     * Activity code
     */
    code: string;
    /**
     * Description of the activity
     */
    description: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefOrganizationContactPerson {
    /**
     * Family name of the contact person
     */
    family_name?: string;
    /**
     * First name of the contact person
     */
    first_name?: string;
    /**
     * Initials in the name of contact person
     */
    initials?: string;
    /**
     * Insertions in the name of contact person
     */
    insertions?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefOutputUser {
    /**
     * Permssions of output user
     */
    permissions: {
      [k: string]: any;
    };
    /**
     * UUID of output user
     */
    user_uuid: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRelatedCase {
    /**
     * Internal identifier of related Case
     */
    id?: string;
    /**
     * Type is case
     */
    type?: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRelatedCaseRequestor {
    /**
     * Related role for case
     */
    related_role?: string;
    /**
     * Type of requestor for case
     */
    requestor_type: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRelatedSubject {
    /**
     * Internal identifier of related subject
     */
    id?: string;
    /**
     * Name of related subject
     */
    name?: string;
    /**
     * Type of related subject
     */
    type?: string;
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefRelatedSubjectTypes = 'person' | 'organization' | 'employee';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRequestor {
    /**
     * Name of the requestor of the case
     */
    name: string;
    /**
     * Type of the requestor of the case
     */
    type: string;
    /**
     * UUID of the requestor of the case
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRuleAction {
    /**
     * Case type rule action
     */
    action?:
      | (
          | 'set_value'
          | 'set_value_formula'
          | 'set_value_magic_string'
          | 'show_attribute'
          | 'hide_attribute'
          | 'show_text_block'
          | 'hide_text_block'
          | 'pause_application'
          | 'hide_group'
          | 'show_group'
          | 'set_allocation'
          | 'change_confidentiality'
          | 'change_html_mail_template'
        )
      | string;
    /**
     * Value
     */
    value?: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefRuleCondition {
    /**
     * Attribute releated to rule
     */
    kenmerk?:
      | (
          | 'contactchannel'
          | 'aanvrager'
          | 'payment_status'
          | 'confidentiality'
          | 'vertrouwelijkheidsaanduiding'
          | 'beroep_mogelijk'
          | 'publicatie'
          | 'bag'
          | 'lex_silencio_positivo'
          | 'opschorten_mogelijk'
          | 'verlenging_mogelijk'
          | 'wet_dwangsom'
          | 'wkpb'
          | 'preferred_contact_channel'
        )
      | string;
    /**
     * Value of rule condition
     */
    value?: {
      [k: string]: any;
    };
    /**
     * Value checkbox
     */
    value_checkbox?: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  /**
   * An enumeration.
   */
  export type DefStaticRuleAction =
    | 'set_value'
    | 'set_value_formula'
    | 'set_value_magic_string'
    | 'show_attribute'
    | 'hide_attribute'
    | 'show_text_block'
    | 'hide_text_block'
    | 'pause_application'
    | 'hide_group'
    | 'show_group'
    | 'set_allocation'
    | 'change_confidentiality'
    | 'change_html_mail_template';

  /**
   * An enumeration.
   */
  export type DefStaticRuleAttribute =
    | 'contactchannel'
    | 'aanvrager'
    | 'payment_status'
    | 'confidentiality'
    | 'vertrouwelijkheidsaanduiding'
    | 'beroep_mogelijk'
    | 'publicatie'
    | 'bag'
    | 'lex_silencio_positivo'
    | 'opschorten_mogelijk'
    | 'verlenging_mogelijk'
    | 'wet_dwangsom'
    | 'wkpb'
    | 'preferred_contact_channel';

  /**
   * Introspectable object based on pydantic
   */
  export interface DefSubjectData {
    /**
     * Betrokkene Identifier
     */
    betrokkene_identifier?: string;
    /**
     * Type of subject
     */
    betrokkene_type: string;
    /**
     * Boolean indicating if the subject is authorized
     */
    gemachtigd?: boolean;
    /**
     * Magic string for subject
     */
    magic_string_prefix?: string;
    /**
     * Name of subject
     */
    naam: string;
    /**
     * Should the subject get notified
     */
    notify?: boolean;
    /**
     * Role of subject
     */
    rol?: string;
    /**
     * UUID of the subject
     */
    uuid: string;
    [k: string]: any;
  }

  /**
   * Introspectable object based on pydantic
   */
  export interface DefSubjectRelatedToCaseType {
    /**
     * If the email is automatic
     */
    automatic: boolean;
    /**
     * Subject data
     */
    data: {
      /**
       * Betrokkene Identifier
       */
      betrokkene_identifier?: string;
      /**
       * Type of subject
       */
      betrokkene_type: string;
      /**
       * Boolean indicating if the subject is authorized
       */
      gemachtigd?: boolean;
      /**
       * Magic string for subject
       */
      magic_string_prefix?: string;
      /**
       * Name of subject
       */
      naam: string;
      /**
       * Should the subject get notified
       */
      notify?: boolean;
      /**
       * Role of subject
       */
      rol?: string;
      /**
       * UUID of the subject
       */
      uuid: string;
      [k: string]: any;
    };
    /**
     * Label of subject relationship
     */
    label: string;
    [k: string]: any;
  }

  /**
   * Valid archival state for the case
   */
  export type DefValidArchivalState = 'overdragen' | 'vernietigen';

  /**
   * An enumeration.
   */
  export type DefValidArchiveStatus = 'archived' | 'to destroy' | 'to preserve';

  /**
   * An enumeration.
   */
  export type DefValidAuditLogComponents =
    | 'attributes'
    | 'authorizations'
    | 'custom_fields'
    | 'relationships';

  /**
   * Valid status for the case
   */
  export type DefValidCaseStatus = 'new' | 'stalled' | 'resolved' | 'open';

  /**
   * An enumeration.
   */
  export type DefValidEmployeeStatus = 'active' | 'inactive';

  /**
   * An enumeration.
   */
  export type DefValidObjectStatus = 'active' | 'inactive' | 'draft';

  /**
   * An enumeration.
   */
  export type DefValidObjectTypeStatus = 'active' | 'inactive';

  /**
   * An enumeration.
   */
  export type DefValidPreferredContactChannel =
    | 'pip'
    | 'email'
    | 'phone'
    | 'mail';

  export interface EntityCase {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * alternative case number
         */
        alternative_case_number?: string;
        /**
         * archival state of the case
         */
        archival_state?: string;
        /**
         * email queue id of assignee
         */
        assignee_email_queue_id?: string;
        /**
         * actions for the case
         */
        case_actions?: {
          [k: string]: any;
        };
        /**
         * phase of case type
         */
        case_type_phase?: string;
        /**
         * uuid of case type
         */
        case_type_uuid?: string;
        /**
         * case type version uuid
         */
        case_type_version_uuid?: string;
        /**
         * List of uuids of child cases for given case
         */
        child_uuids?: string;
        /**
         * case completion
         */
        completion?: string;
        /**
         * case completion date
         */
        completion_date?: string;
        /**
         * confidentiality of the case
         */
        confidentiality?: string;
        /**
         * contact channel
         */
        contact_channel?: string;
        /**
         * contact information
         */
        contact_information?: {
          [k: string]: any;
        };
        /**
         * case created date
         */
        created_date?: string;
        /**
         * custom fields for the case
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * department of the case
         */
        department?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Status indicating in case is destructable
         */
        destructable?: number;
        /**
         * des date of the case
         */
        destruction_date?: string;
        /**
         * enqueued documents data
         */
        enqueued_documents_data?: string;
        /**
         * enqueued emails data
         */
        enqueued_emails_data?: string;
        /**
         * subcase data
         */
        enqueued_subcases_data?: string;
        /**
         * enqueued subjects data
         */
        enqueued_subjects_data?: string;
        /**
         * Id of Case
         */
        id?: number;
        /**
         * last modification date
         */
        last_modified_date?: string;
        /**
         * meta info for the case
         */
        meta?: {
          /**
           * case handling info
           */
          afhandeling?: string;
          /**
           * Id of case meta
           */
          id?: number;
          /**
           * case suspension info
           */
          opschorten?: string;
          /**
           * stalled since date
           */
          stalled_since_date?: string;
          [k: string]: any;
        };
        /**
         * milestone for the case
         */
        milestone?: number;
        /**
         * Id of Case
         */
        number?: number;
        /**
         * UUID of parent case
         */
        parent_uuid?: string;
        /**
         * case payment
         */
        payment?: {
          /**
           * payment amount
           */
          amount?: number;
          /**
           * payment status
           */
          status?: string;
          [k: string]: any;
        };
        /**
         * case payment amount
         */
        payment_amount?: string;
        /**
         * case phase
         */
        phase?: {
          /**
           * label of phase
           */
          label?: string;
          /**
           * milestone label of the phase
           */
          milestone_label?: string;
          [k: string]: any;
        };
        /**
         * preset client for the case
         */
        preset_client?: string;
        /**
         * Progress status for given case
         */
        progress_status?: number;
        /**
         * case summary
         */
        public_summary?: string;
        /**
         * queue id list
         */
        queue_ids?: string;
        /**
         * case registration date
         */
        registration_date?: string;
        /**
         * case related to
         */
        related_to?: {
          [k: string]: any;
        };
        /**
         * List of uuids of cases related to given case
         */
        related_uuids?: string;
        /**
         * request trigger
         */
        request_trigger?: string;
        /**
         * result for the case
         */
        result?: {
          /**
           * archival attributes
           */
          archival_attributes?: {
            /**
             * selection list
             */
            selection_list?: string;
            /**
             * archival state
             */
            state?: string;
            [k: string]: any;
          };
          /**
           * result of the case
           */
          result?: string;
          /**
           * result id
           */
          result_id?: number;
          [k: string]: any;
        };
        /**
         * reason for resuming
         */
        resume_reason?: string;
        /**
         * role
         */
        role?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the role
               */
              description?: string;
              /**
               * Name of the role
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Email info
         */
        send_email_to_assignee?: string;
        /**
         * stalled since date
         */
        stalled_since_date?: string;
        /**
         * stalled until date
         */
        stalled_until_date?: string;
        /**
         * case status
         */
        status?: string;
        /**
         * subject for the case
         */
        subject?: string;
        /**
         * extern subject
         */
        subject_extern?: string;
        /**
         * list of subjects in the case
         */
        subjects?: any[];
        /**
         * case summary
         */
        summary?: string;
        /**
         * suspension reason
         */
        suspension_reason?: string;
        /**
         * case target completion date
         */
        target_completion_date?: string;
        /**
         * Count of unaccepted updates
         */
        unaccepted_attribute_update_count?: number;
        /**
         * Count of unaccepted files
         */
        unaccepted_files_count?: number;
        /**
         * urgency of the case
         */
        urgency?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        coordinator?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        related_contacts?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        requestor?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseBasic {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * uuid of case type
         */
        case_type_uuid: string;
        /**
         * case type version uuid
         */
        case_type_version_uuid: string;
        /**
         * custom fields for the case
         */
        custom_fields: {
          [k: string]: any;
        };
        /**
         * Id of Case
         */
        number: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseBasicList {
    data: {
      attributes: {
        /**
         * uuid of case type
         */
        case_type_uuid: string;
        /**
         * case type version uuid
         */
        case_type_version_uuid: string;
        /**
         * custom fields for the case
         */
        custom_fields: {
          [k: string]: any;
        };
        /**
         * Id of Case
         */
        number: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContact {
    /**
     * Requestor Entity
     */
    data: {
      attributes: {
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Role of the contact
         */
        role?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactEmployee {
    /**
     * Contains details of employee related to case with roles such as assginee,requestor etc
     */
    data: {
      attributes: {
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * Name of the department for employee
         */
        department?: string;
        /**
         * Email address of employee
         */
        email?: string;
        /**
         * Family name of the employee
         */
        family_name?: string;
        /**
         * First names for the employee
         */
        first_names?: string;
        /**
         * Full name of the employee
         */
        full_name?: string;
        /**
         * House number for the employee
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the employee
         */
        initials?: string;
        /**
         * Last name for the employee
         */
        last_name?: string;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Name of the employee
         */
        name?: string;
        /**
         * contact phone number of employee
         */
        phone_number?: string;
        /**
         * properties of subject
         */
        properties?: {
          [k: string]: any;
        };
        /**
         * Residence house number for employee
         */
        residence_house_number?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Status of employee
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the employee
         */
        surname?: string;
        /**
         * Title for the employee
         */
        title?: string;
        /**
         * Type of the employee
         */
        type?: string;
        /**
         * Unique name for the employee
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactEmployeeList {
    data: {
      attributes: {
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * Name of the department for employee
         */
        department?: string;
        /**
         * Email address of employee
         */
        email?: string;
        /**
         * Family name of the employee
         */
        family_name?: string;
        /**
         * First names for the employee
         */
        first_names?: string;
        /**
         * Full name of the employee
         */
        full_name?: string;
        /**
         * House number for the employee
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the employee
         */
        initials?: string;
        /**
         * Last name for the employee
         */
        last_name?: string;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Name of the employee
         */
        name?: string;
        /**
         * contact phone number of employee
         */
        phone_number?: string;
        /**
         * properties of subject
         */
        properties?: {
          [k: string]: any;
        };
        /**
         * Residence house number for employee
         */
        residence_house_number?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Status of employee
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the employee
         */
        surname?: string;
        /**
         * Title for the employee
         */
        title?: string;
        /**
         * Type of the employee
         */
        type?: string;
        /**
         * Unique name for the employee
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactList {
    data: {
      attributes: {
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Role of the contact
         */
        role?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactOrganization {
    /**
     * Contains details of organization related to case with various roles
     */
    data: {
      attributes: {
        /**
         * Chamber of Commerce number of the organization
         */
        coc?: string;
        /**
         * correspondence house number
         */
        correspondence_house_number?: string;
        /**
         * correspondence place of residence
         */
        correspondence_place_of_residence?: string;
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * country of residence for organization
         */
        country_of_residence?: string;
        /**
         * Email address of organization
         */
        email?: string;
        /**
         * Establishment number of requestor organization
         */
        establishment_number?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Boolean indicating if organization has correspondence address
         */
        has_briefadres?: boolean;
        /**
         * House number for the organization
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * KVK number for the organization
         */
        login?: string;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Contact mobile number of organization
         */
        mobile_number?: string;
        /**
         * Name of the organization
         */
        name?: string;
        /**
         * contact phone number of organization
         */
        phone_number?: string;
        /**
         * place of residence for organization
         */
        place_of_residence?: string;
        /**
         * Residence house number for organization
         */
        residence_house_number?: string;
        /**
         * Place of residence
         */
        residence_place_of_residence?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Status of v
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the organization
         */
        subject_type?: string;
        /**
         * Trade name for requestor organization
         */
        trade_name?: string;
        /**
         * Type of the organization
         */
        type?: string;
        /**
         * Type of the business
         */
        type_of_business_entity?: string;
        /**
         * Unique name for the organization
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactOrganizationList {
    data: {
      attributes: {
        /**
         * Chamber of Commerce number of the organization
         */
        coc?: string;
        /**
         * correspondence house number
         */
        correspondence_house_number?: string;
        /**
         * correspondence place of residence
         */
        correspondence_place_of_residence?: string;
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * country of residence for organization
         */
        country_of_residence?: string;
        /**
         * Email address of organization
         */
        email?: string;
        /**
         * Establishment number of requestor organization
         */
        establishment_number?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Boolean indicating if organization has correspondence address
         */
        has_briefadres?: boolean;
        /**
         * House number for the organization
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * KVK number for the organization
         */
        login?: string;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Contact mobile number of organization
         */
        mobile_number?: string;
        /**
         * Name of the organization
         */
        name?: string;
        /**
         * contact phone number of organization
         */
        phone_number?: string;
        /**
         * place of residence for organization
         */
        place_of_residence?: string;
        /**
         * Residence house number for organization
         */
        residence_house_number?: string;
        /**
         * Place of residence
         */
        residence_place_of_residence?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Status of v
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the organization
         */
        subject_type?: string;
        /**
         * Trade name for requestor organization
         */
        trade_name?: string;
        /**
         * Type of the organization
         */
        type?: string;
        /**
         * Type of the business
         */
        type_of_business_entity?: string;
        /**
         * Unique name for the organization
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactPerson {
    /**
     * Contains details of person related to case with various roles
     */
    data: {
      attributes: {
        /**
         * Another identifier in the dutch systems, called a_nummer
         */
        a_number?: string;
        /**
         * BSN number for the person
         */
        bsn?: string;
        /**
         * person's country of birth
         */
        country_of_birth?: string;
        /**
         * country of residence for person
         */
        country_of_residence?: string;
        /**
         * person's date of birth
         */
        date_of_birth?: string;
        /**
         * Date of death for person
         */
        date_of_death?: string;
        /**
         * Date of divorce for person
         */
        date_of_divorce?: string;
        /**
         * Marriage date for person
         */
        date_of_marriage?: string;
        /**
         * Email address of person
         */
        email?: string;
        /**
         * Family name of the person
         */
        family_name?: string;
        /**
         * First names for the person
         */
        first_names?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Full name of the person
         */
        full_name?: string;
        /**
         * Gender of the person
         */
        gender?: string;
        /**
         * Boolean indicating if person has correspondence address
         */
        has_briefadres?: boolean;
        /**
         * House number for the person
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the person
         */
        initials?: string;
        /**
         * investigation value for person
         */
        investigation?: boolean;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Contact mobile number of person
         */
        mobile_number?: string;
        /**
         * Identifier of case type
         */
        naamgebruik?: string;
        /**
         * Name of the person
         */
        name?: string;
        /**
         * Title of the person
         */
        noble_title?: string;
        /**
         * contact phone number of person
         */
        phone_number?: string;
        /**
         * person's place of birth
         */
        place_of_birth?: string;
        /**
         * place of residence for person
         */
        place_of_residence?: string;
        /**
         * Residence house number for person
         */
        residence_house_number?: string;
        /**
         * Place of residence
         */
        residence_place_of_residence?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Salutation used for v
         */
        salutation?: string;
        /**
         * Another Salutation used for person
         */
        salutation1?: string;
        /**
         * Another Salutation used for person
         */
        salutation2?: string;
        /**
         * Status of person
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the person
         */
        surname?: string;
        /**
         * Surname prefix of the person
         */
        surname_prefix?: string;
        /**
         * Type is person
         */
        type?: string;
        /**
         * Unique name for the person
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseContactPersonList {
    data: {
      attributes: {
        /**
         * Another identifier in the dutch systems, called a_nummer
         */
        a_number?: string;
        /**
         * BSN number for the person
         */
        bsn?: string;
        /**
         * person's country of birth
         */
        country_of_birth?: string;
        /**
         * country of residence for person
         */
        country_of_residence?: string;
        /**
         * person's date of birth
         */
        date_of_birth?: string;
        /**
         * Date of death for person
         */
        date_of_death?: string;
        /**
         * Date of divorce for person
         */
        date_of_divorce?: string;
        /**
         * Marriage date for person
         */
        date_of_marriage?: string;
        /**
         * Email address of person
         */
        email?: string;
        /**
         * Family name of the person
         */
        family_name?: string;
        /**
         * First names for the person
         */
        first_names?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Full name of the person
         */
        full_name?: string;
        /**
         * Gender of the person
         */
        gender?: string;
        /**
         * Boolean indicating if person has correspondence address
         */
        has_briefadres?: boolean;
        /**
         * House number for the person
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the person
         */
        initials?: string;
        /**
         * investigation value for person
         */
        investigation?: boolean;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * Magic string prefix
         */
        magic_string_prefix?: string;
        /**
         * Contact mobile number of person
         */
        mobile_number?: string;
        /**
         * Identifier of case type
         */
        naamgebruik?: string;
        /**
         * Name of the person
         */
        name?: string;
        /**
         * Title of the person
         */
        noble_title?: string;
        /**
         * contact phone number of person
         */
        phone_number?: string;
        /**
         * person's place of birth
         */
        place_of_birth?: string;
        /**
         * place of residence for person
         */
        place_of_residence?: string;
        /**
         * Residence house number for person
         */
        residence_house_number?: string;
        /**
         * Place of residence
         */
        residence_place_of_residence?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Role of the contact
         */
        role?: string;
        /**
         * Salutation used for v
         */
        salutation?: string;
        /**
         * Another Salutation used for person
         */
        salutation1?: string;
        /**
         * Another Salutation used for person
         */
        salutation2?: string;
        /**
         * Status of person
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the person
         */
        surname?: string;
        /**
         * Surname prefix of the person
         */
        surname_prefix?: string;
        /**
         * Type is person
         */
        type?: string;
        /**
         * Unique name for the person
         */
        unique_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseList {
    data: {
      attributes: {
        /**
         * alternative case number
         */
        alternative_case_number?: string;
        /**
         * archival state of the case
         */
        archival_state?: string;
        /**
         * email queue id of assignee
         */
        assignee_email_queue_id?: string;
        /**
         * actions for the case
         */
        case_actions?: {
          [k: string]: any;
        };
        /**
         * phase of case type
         */
        case_type_phase?: string;
        /**
         * uuid of case type
         */
        case_type_uuid?: string;
        /**
         * case type version uuid
         */
        case_type_version_uuid?: string;
        /**
         * List of uuids of child cases for given case
         */
        child_uuids?: string;
        /**
         * case completion
         */
        completion?: string;
        /**
         * case completion date
         */
        completion_date?: string;
        /**
         * confidentiality of the case
         */
        confidentiality?: string;
        /**
         * contact channel
         */
        contact_channel?: string;
        /**
         * contact information
         */
        contact_information?: {
          [k: string]: any;
        };
        /**
         * case created date
         */
        created_date?: string;
        /**
         * custom fields for the case
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * department of the case
         */
        department?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Status indicating in case is destructable
         */
        destructable?: number;
        /**
         * des date of the case
         */
        destruction_date?: string;
        /**
         * enqueued documents data
         */
        enqueued_documents_data?: string;
        /**
         * enqueued emails data
         */
        enqueued_emails_data?: string;
        /**
         * subcase data
         */
        enqueued_subcases_data?: string;
        /**
         * enqueued subjects data
         */
        enqueued_subjects_data?: string;
        /**
         * Id of Case
         */
        id?: number;
        /**
         * last modification date
         */
        last_modified_date?: string;
        /**
         * meta info for the case
         */
        meta?: {
          /**
           * case handling info
           */
          afhandeling?: string;
          /**
           * Id of case meta
           */
          id?: number;
          /**
           * case suspension info
           */
          opschorten?: string;
          /**
           * stalled since date
           */
          stalled_since_date?: string;
          [k: string]: any;
        };
        /**
         * milestone for the case
         */
        milestone?: number;
        /**
         * Id of Case
         */
        number?: number;
        /**
         * UUID of parent case
         */
        parent_uuid?: string;
        /**
         * case payment
         */
        payment?: {
          /**
           * payment amount
           */
          amount?: number;
          /**
           * payment status
           */
          status?: string;
          [k: string]: any;
        };
        /**
         * case payment amount
         */
        payment_amount?: string;
        /**
         * case phase
         */
        phase?: {
          /**
           * label of phase
           */
          label?: string;
          /**
           * milestone label of the phase
           */
          milestone_label?: string;
          [k: string]: any;
        };
        /**
         * preset client for the case
         */
        preset_client?: string;
        /**
         * Progress status for given case
         */
        progress_status?: number;
        /**
         * case summary
         */
        public_summary?: string;
        /**
         * queue id list
         */
        queue_ids?: string;
        /**
         * case registration date
         */
        registration_date?: string;
        /**
         * case related to
         */
        related_to?: {
          [k: string]: any;
        };
        /**
         * List of uuids of cases related to given case
         */
        related_uuids?: string;
        /**
         * request trigger
         */
        request_trigger?: string;
        /**
         * result for the case
         */
        result?: {
          /**
           * archival attributes
           */
          archival_attributes?: {
            /**
             * selection list
             */
            selection_list?: string;
            /**
             * archival state
             */
            state?: string;
            [k: string]: any;
          };
          /**
           * result of the case
           */
          result?: string;
          /**
           * result id
           */
          result_id?: number;
          [k: string]: any;
        };
        /**
         * reason for resuming
         */
        resume_reason?: string;
        /**
         * role
         */
        role?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the role
               */
              description?: string;
              /**
               * Name of the role
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Email info
         */
        send_email_to_assignee?: string;
        /**
         * stalled since date
         */
        stalled_since_date?: string;
        /**
         * stalled until date
         */
        stalled_until_date?: string;
        /**
         * case status
         */
        status?: string;
        /**
         * subject for the case
         */
        subject?: string;
        /**
         * extern subject
         */
        subject_extern?: string;
        /**
         * list of subjects in the case
         */
        subjects?: any[];
        /**
         * case summary
         */
        summary?: string;
        /**
         * suspension reason
         */
        suspension_reason?: string;
        /**
         * case target completion date
         */
        target_completion_date?: string;
        /**
         * Count of unaccepted updates
         */
        unaccepted_attribute_update_count?: number;
        /**
         * Count of unaccepted files
         */
        unaccepted_files_count?: number;
        /**
         * urgency of the case
         */
        urgency?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        coordinator?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        related_contacts?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        requestor?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseRelation {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Boolean indicating blocks deletion
         */
        blocks_deletion?: boolean;
        /**
         * Id of first case
         */
        case_id_a?: number;
        /**
         * Id of second case
         */
        case_id_b?: number;
        /**
         * Identifier of current case
         */
        current_case_uuid?: string;
        /**
         * Indenitider for the first case
         */
        object1_uuid?: string;
        /**
         * Indenitider for the second case
         */
        object2_uuid?: string;
        /**
         * Sequesnce number of first case
         */
        order_seq_a?: number;
        /**
         * Sequesnce number of second case
         */
        order_seq_b?: number;
        /**
         * Identifier for the case relation owner
         */
        owner_uuid?: string;
        /**
         * Type of relation between cases
         */
        relation_type?: string;
        /**
         * Type of relationship wrt first case
         */
        relationship_type1?: string;
        /**
         * Type of relationship wrt second case
         */
        relationship_type2?: string;
        /**
         * Summary of first case
         */
        summary1?: string;
        /**
         * Summary of second case
         */
        summary2?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseRelationList {
    data: {
      attributes: {
        /**
         * Boolean indicating blocks deletion
         */
        blocks_deletion?: boolean;
        /**
         * Id of first case
         */
        case_id_a?: number;
        /**
         * Id of second case
         */
        case_id_b?: number;
        /**
         * Identifier of current case
         */
        current_case_uuid?: string;
        /**
         * Indenitider for the first case
         */
        object1_uuid?: string;
        /**
         * Indenitider for the second case
         */
        object2_uuid?: string;
        /**
         * Sequesnce number of first case
         */
        order_seq_a?: number;
        /**
         * Sequesnce number of second case
         */
        order_seq_b?: number;
        /**
         * Identifier for the case relation owner
         */
        owner_uuid?: string;
        /**
         * Type of relation between cases
         */
        relation_type?: string;
        /**
         * Type of relationship wrt first case
         */
        relationship_type1?: string;
        /**
         * Type of relationship wrt second case
         */
        relationship_type2?: string;
        /**
         * Summary of first case
         */
        summary1?: string;
        /**
         * Summary of second case
         */
        summary2?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseSearchResult {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Archival state of the case
         */
        archival_state?: string;
        /**
         * Assignee of the case
         */
        assignee: {
          /**
           * Name of the assignee of the case
           */
          name?: string;
          /**
           * UUID of the assignee of the case
           */
          uuid?: string;
          [k: string]: any;
        };
        /**
         * Title of the case_type
         */
        case_type_title?: string;
        /**
         * Destruction date of the case
         */
        destruction_date?: string;
        /**
         * Id of case
         */
        number: number;
        /**
         * Indicates with a percentage how much time is left to complete the case. The percentage will be above 100 if the remaining days are exeeded. When a case has the status stalled, then the value is null
         */
        percentage_days_left?: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Requestor of the case
         */
        requestor: {
          /**
           * Requestor Entity
           */
          data: {
            attributes: {
              /**
               * Another identifier in the dutch systems, called a_nummer
               */
              a_number?: string;
              /**
               * BSN number for the reqestor
               */
              burgerservicenummer?: string;
              /**
               * Chamber of Commerce number of the organization
               */
              coc?: string;
              /**
               * correspondence house number
               */
              correspondence_house_number?: string;
              /**
               * correspondence place of residence
               */
              correspondence_place_of_residence?: string;
              /**
               * correspondence address street value
               */
              correspondence_street?: string;
              /**
               * correspondence zipcode value
               */
              correspondence_zipcode?: string;
              /**
               * Requestor's country of birth
               */
              country_of_birth?: string;
              /**
               * country of residence for requestor
               */
              country_of_residence?: string;
              /**
               * Requestor's date of birth
               */
              date_of_birth?: string;
              /**
               * Date of divorce for requestor
               */
              date_of_divorce?: string;
              /**
               * Marriage date for requestor
               */
              date_of_marriage?: string;
              /**
               * Name of the department for requestor
               */
              department?: string;
              /**
               * Email address of requestor
               */
              email?: string;
              /**
               * Establishment number of requestor organization
               */
              establishment_number?: string;
              /**
               * Family name of the requestor
               */
              family_name?: string;
              /**
               * First names for the requestor
               */
              first_names?: string;
              /**
               * foreign residence address first line
               */
              foreign_residence_address_line1?: string;
              /**
               * foreign residence address second line
               */
              foreign_residence_address_line2?: string;
              /**
               * foreign residence address third line
               */
              foreign_residence_address_line3?: string;
              /**
               * Full name of the requestor
               */
              full_name?: string;
              /**
               * Gender of the requestor
               */
              gender?: string;
              /**
               * Indicate if requestor has correspondence address
               */
              has_correspondence_address?: boolean;
              /**
               * House number for the requestor
               */
              house_number?: string;
              /**
               * Id of the requestor
               */
              id?: string;
              /**
               * Initials for the requestor
               */
              initials?: string;
              /**
               * investigation value for reqestor
               */
              investigation?: string;
              /**
               * If its secret value
               */
              is_secret?: boolean;
              /**
               * login details for requestor
               */
              login?: string;
              /**
               * Contact mobile number of requestor
               */
              mobile_number?: string;
              /**
               * Name of the requestor
               */
              name?: string;
              /**
               * Title of the requestor
               */
              noble_title?: string;
              /**
               * Password details for requestor
               */
              password?: string;
              /**
               * contact phone number of requestor
               */
              phone_number?: string;
              /**
               * Requestor's place of birth
               */
              place_of_birth?: string;
              /**
               * place of residence for requestor
               */
              place_of_residence?: string;
              /**
               * Type of the requestor
               */
              requestor_type?: string;
              /**
               * Residence house number for requestor
               */
              residence_house_number?: string;
              /**
               * Residence address street name
               */
              residence_street?: string;
              /**
               * Residence address zipcode
               */
              residence_zipcode?: string;
              /**
               * Salutation used for requestor
               */
              salutation?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation1?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation2?: string;
              /**
               * Status of requestor
               */
              status?: string;
              /**
               * Address street name
               */
              street?: string;
              /**
               * type of the subject
               */
              subject_type?: string;
              /**
               * Surname of the requestor
               */
              surname?: string;
              /**
               * Surname prefix of the requestor
               */
              surname_prefix?: string;
              /**
               * Title for the requestor
               */
              title?: string;
              /**
               * Trade name for requestor organization
               */
              trade_name?: string;
              /**
               * Type of the business
               */
              type_of_business_entity?: string;
              /**
               * Unique name for the requestor
               */
              unique_name?: string;
              /**
               * Identifier of case type
               */
              used_name?: string;
              /**
               * Zipcode value
               */
              zipcode?: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Status of case
         */
        status: string;
        /**
         * Subject of te case
         */
        subject?: string;
        /**
         * Count of unaccepted updates
         */
        unaccepted_attribute_update_count: number;
        /**
         * Count of unaccepted files
         */
        unaccepted_files_count: number;
        /**
         * Number of unread messages
         */
        unread_message_count: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseSearchResultList {
    data: {
      attributes: {
        /**
         * Archival state of the case
         */
        archival_state?: string;
        /**
         * Assignee of the case
         */
        assignee: {
          /**
           * Name of the assignee of the case
           */
          name?: string;
          /**
           * UUID of the assignee of the case
           */
          uuid?: string;
          [k: string]: any;
        };
        /**
         * Title of the case_type
         */
        case_type_title?: string;
        /**
         * Destruction date of the case
         */
        destruction_date?: string;
        /**
         * Id of case
         */
        number: number;
        /**
         * Indicates with a percentage how much time is left to complete the case. The percentage will be above 100 if the remaining days are exeeded. When a case has the status stalled, then the value is null
         */
        percentage_days_left?: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Requestor of the case
         */
        requestor: {
          /**
           * Requestor Entity
           */
          data: {
            attributes: {
              /**
               * Another identifier in the dutch systems, called a_nummer
               */
              a_number?: string;
              /**
               * BSN number for the reqestor
               */
              burgerservicenummer?: string;
              /**
               * Chamber of Commerce number of the organization
               */
              coc?: string;
              /**
               * correspondence house number
               */
              correspondence_house_number?: string;
              /**
               * correspondence place of residence
               */
              correspondence_place_of_residence?: string;
              /**
               * correspondence address street value
               */
              correspondence_street?: string;
              /**
               * correspondence zipcode value
               */
              correspondence_zipcode?: string;
              /**
               * Requestor's country of birth
               */
              country_of_birth?: string;
              /**
               * country of residence for requestor
               */
              country_of_residence?: string;
              /**
               * Requestor's date of birth
               */
              date_of_birth?: string;
              /**
               * Date of divorce for requestor
               */
              date_of_divorce?: string;
              /**
               * Marriage date for requestor
               */
              date_of_marriage?: string;
              /**
               * Name of the department for requestor
               */
              department?: string;
              /**
               * Email address of requestor
               */
              email?: string;
              /**
               * Establishment number of requestor organization
               */
              establishment_number?: string;
              /**
               * Family name of the requestor
               */
              family_name?: string;
              /**
               * First names for the requestor
               */
              first_names?: string;
              /**
               * foreign residence address first line
               */
              foreign_residence_address_line1?: string;
              /**
               * foreign residence address second line
               */
              foreign_residence_address_line2?: string;
              /**
               * foreign residence address third line
               */
              foreign_residence_address_line3?: string;
              /**
               * Full name of the requestor
               */
              full_name?: string;
              /**
               * Gender of the requestor
               */
              gender?: string;
              /**
               * Indicate if requestor has correspondence address
               */
              has_correspondence_address?: boolean;
              /**
               * House number for the requestor
               */
              house_number?: string;
              /**
               * Id of the requestor
               */
              id?: string;
              /**
               * Initials for the requestor
               */
              initials?: string;
              /**
               * investigation value for reqestor
               */
              investigation?: string;
              /**
               * If its secret value
               */
              is_secret?: boolean;
              /**
               * login details for requestor
               */
              login?: string;
              /**
               * Contact mobile number of requestor
               */
              mobile_number?: string;
              /**
               * Name of the requestor
               */
              name?: string;
              /**
               * Title of the requestor
               */
              noble_title?: string;
              /**
               * Password details for requestor
               */
              password?: string;
              /**
               * contact phone number of requestor
               */
              phone_number?: string;
              /**
               * Requestor's place of birth
               */
              place_of_birth?: string;
              /**
               * place of residence for requestor
               */
              place_of_residence?: string;
              /**
               * Type of the requestor
               */
              requestor_type?: string;
              /**
               * Residence house number for requestor
               */
              residence_house_number?: string;
              /**
               * Residence address street name
               */
              residence_street?: string;
              /**
               * Residence address zipcode
               */
              residence_zipcode?: string;
              /**
               * Salutation used for requestor
               */
              salutation?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation1?: string;
              /**
               * Another Salutation used for requestor
               */
              salutation2?: string;
              /**
               * Status of requestor
               */
              status?: string;
              /**
               * Address street name
               */
              street?: string;
              /**
               * type of the subject
               */
              subject_type?: string;
              /**
               * Surname of the requestor
               */
              surname?: string;
              /**
               * Surname prefix of the requestor
               */
              surname_prefix?: string;
              /**
               * Title for the requestor
               */
              title?: string;
              /**
               * Trade name for requestor organization
               */
              trade_name?: string;
              /**
               * Type of the business
               */
              type_of_business_entity?: string;
              /**
               * Unique name for the requestor
               */
              unique_name?: string;
              /**
               * Identifier of case type
               */
              used_name?: string;
              /**
               * Zipcode value
               */
              zipcode?: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Status of case
         */
        status: string;
        /**
         * Subject of te case
         */
        subject?: string;
        /**
         * Count of unaccepted updates
         */
        unaccepted_attribute_update_count: number;
        /**
         * Count of unaccepted files
         */
        unaccepted_files_count: number;
        /**
         * Number of unread messages
         */
        unread_message_count: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseSummary {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Case assignee
         */
        assignee: {
          /**
           * Name of the assignee
           */
          name: string;
          /**
           * UUID of the assignee
           */
          uuid: string;
          [k: string]: any;
        };
        /**
         * Title of the case's case type
         */
        casetype_title: string;
        /**
         * Case number
         */
        number: number;
        /**
         *
         *         This value is expressed as a ratio of phases finished, with 0 meaning
         *         'not started' and 1 meaning 'completely done'.
         *
         */
        progress_status: number;
        /**
         * Short name of the result of the case
         */
        result: string;
        /**
         * Summary/subject of the case
         */
        summary: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseSummaryList {
    data: {
      attributes: {
        /**
         * Case assignee
         */
        assignee: {
          /**
           * Name of the assignee
           */
          name: string;
          /**
           * UUID of the assignee
           */
          uuid: string;
          [k: string]: any;
        };
        /**
         * Title of the case's case type
         */
        casetype_title: string;
        /**
         * Case number
         */
        number: number;
        /**
         *
         *         This value is expressed as a ratio of phases finished, with 0 meaning
         *         'not started' and 1 meaning 'completely done'.
         *
         */
        progress_status: number;
        /**
         * Short name of the result of the case
         */
        result: string;
        /**
         * Summary/subject of the case
         */
        summary: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseTypeForCase {
    /**
     * Details of case type related to given case
     */
    data: {
      attributes: {
        /**
         * Active status of case_type
         */
        active: boolean;
        /**
         * Adjourn period for the case type
         */
        adjourn_period?: string;
        /**
         * Archive classification code
         */
        archive_classification_code?: string;
        /**
         * Public summary of case_type
         */
        case_public_summary?: string;
        /**
         * Summary of case_type
         */
        case_summary?: string;
        /**
         * UUID of the case_type
         */
        case_type_uuid: string;
        /**
         * Created date of case_type
         */
        created: string;
        /**
         * Deleted date of case_type
         */
        deleted?: string;
        /**
         * Description for case type
         */
        description?: string;
        /**
         * Designation of confidentiality for case type
         */
        designation_of_confidentiality?: string;
        /**
         * Eform status for the case type
         */
        eform?: string;
        /**
         * Extension details for the case type
         */
        extension?: string;
        /**
         * Extension period for the case type
         */
        extension_period?: string;
        /**
         * Generic category for the case type
         */
        generic_category?: string;
        /**
         * Goal set for the case type
         */
        goal?: string;
        /**
         * Id of the case_type node
         */
        id?: string;
        /**
         * Identification for the case type
         */
        identification?: string;
        /**
         * Initiator source for the case type
         */
        initiator_source?: string;
        /**
         * Type of initiator for the case
         */
        initiator_type?: string;
        /**
         * Boolean to check if the case_type is eligible for case creation
         */
        is_eligible_for_case_creation: boolean;
        /**
         * Keywords defined for the case type
         */
        keywords?: string;
        /**
         * Last modified date for case_type
         */
        last_modified: string;
        /**
         * LSP license details
         */
        lex_silencio_positivo?: string;
        /**
         * Meta data of case_type
         */
        metadata: {
          /**
           * Source of the person
           */
          adjourn_period?: string;
          /**
           * Source of the person
           */
          archive_classification_code?: string;
          /**
           * Source of the person
           */
          bag?: string;
          /**
           * Source of the person
           */
          designation_of_confidentiality?: string;
          /**
           * Source of the person
           */
          e_webform?: string;
          /**
           * Source of the person
           */
          extension_period?: string;
          /**
           * Source of the person
           */
          legal_basis: string;
          /**
           * Source of the person
           */
          lex_silencio_positivo?: string;
          /**
           * Source of the person
           */
          local_basis?: string;
          /**
           * Source of the person
           */
          may_extend?: string;
          /**
           * Source of the person
           */
          may_postpone?: string;
          /**
           * Source of the person
           */
          motivation?: string;
          /**
           * Source of the person
           */
          penalty_law?: string;
          /**
           * Source of the person
           */
          possibility_for_objection_and_appeal?: string;
          /**
           * Process description of case_type
           */
          process_description: string;
          /**
           * Source of the person
           */
          publication?: string;
          /**
           * Source of the person
           */
          publication_text?: string;
          /**
           * Source of the person
           */
          purpose?: string;
          /**
           * Source of the person
           */
          responsible_relationship?: string;
          /**
           * Source of the person
           */
          responsible_subject?: string;
          /**
           * Source of the person
           */
          wkpb_applies?: string;
          [k: string]: any;
        };
        /**
         * Motivation details for case_type
         */
        motivation?: string;
        /**
         * Name of case_type
         */
        name?: string;
        /**
         * Objection appeal status
         */
        objection_and_appeal?: string;
        /**
         * IPenalty for the case type
         */
        penalty?: string;
        /**
         * Array of phases for case_type
         */
        phases: {
          /**
           * Allocation of case_type phase
           */
          allocation?: {
            /**
             * Department assigned for phase
             */
            department: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the department
                   */
                  description?: string;
                  /**
                   * Name of the department
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role assigned for phase
             */
            role: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the role
                   */
                  description?: string;
                  /**
                   * Name of the role
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role set for phase
             */
            role_set?: number;
            [k: string]: any;
          };
          /**
           * Cases releted to case_type
           */
          cases?: {
            /**
             * Allocation of case_type
             */
            allocation?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            };
            /**
             * Copy custom_fields from parent
             */
            copy_custom_fields_from_parent?: boolean;
            /**
             * Label case in pip
             */
            label_in_pip?: string;
            /**
             * Open case on create
             */
            open_case_on_create?: boolean;
            /**
             * UUID of related element
             */
            related_casetype_element: string;
            /**
             * Requestor of related case
             */
            requestor?: {
              /**
               * Related role for case
               */
              related_role?: string;
              /**
               * Type of requestor for case
               */
              requestor_type: string;
              [k: string]: any;
            };
            /**
             * Resolve before phase
             */
            resolve_before_phase?: number;
            /**
             * Show the case in pip
             */
            show_in_pip?: boolean;
            /**
             * Boolean to check if the case_type starts on transition
             */
            start_on_transition?: boolean;
            /**
             * Type of case_relation
             */
            type_of_relation: string;
            [k: string]: any;
          }[];
          /**
           * Checklist items releted to case_type
           */
          checklist_items?: string[];
          /**
           * Custom_fields releted to case_type
           */
          custom_fields?: {
            /**
             * Options available for custom_field
             */
            date_field_limit?: {
              /**
               * Name of custom field publication
               */
              end?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              /**
               * Name of custom field publication
               */
              start?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Default value for custom_field
             */
            default_value?: string;
            /**
             * Description of case_type
             */
            description?: string;
            /**
             * Edit authorizations for custom_field
             */
            edit_authorizations?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            }[];
            /**
             * Is skip of queue enabled for case_type
             */
            enable_skip_of_queue?: boolean;
            /**
             * External description of case_type
             */
            external_description?: string;
            /**
             * Magic string of custom_field
             */
            field_magic_string: string;
            /**
             * Options available for custom_field
             */
            field_options?: string[];
            /**
             * Type of custom_field
             */
            field_type: string;
            /**
             * Is the custom_field hidden
             */
            is_hidden_field?: boolean;
            /**
             * Is the custom_field multiple choice
             */
            is_multiple?: boolean;
            /**
             * Is the custom_field required
             */
            is_required?: boolean;
            /**
             * Name of custom field
             */
            name: string;
            /**
             * Public name of custom_field
             */
            public_name?: string;
            /**
             * Places where the case_type is published
             */
            publish_on?: {
              /**
               * Name of custom field publication
               */
              name: string;
              [k: string]: any;
            }[];
            /**
             * Referential for custom_field
             */
            referential?: boolean;
            /**
             * Name of custom field relationship
             */
            relationship_name?: string;
            /**
             * Name of custom field relationship
             */
            relationship_subject_role?: string;
            /**
             * Type of relationship
             */
            relationship_type?: string;
            /**
             * Inertnal identifier of custom_field relationship
             */
            relationship_uuid?: string;
            /**
             * Can requestor chnage custom_field from pip
             */
            requestor_can_change_from_pip?: boolean;
            /**
             * Does the custom_field contains sensitive data
             */
            sensitive_data?: boolean;
            /**
             * Title of custom_object
             */
            title?: string;
            /**
             * Title multiple of custom_field
             */
            title_multiple?: string;
            /**
             * UUID of the catalog attribute
             */
            uuid: string;
            [k: string]: any;
          }[];
          /**
           * Documents releted to case_type
           */
          documents?: {
            /**
             * If the document is automatic
             */
            automatic?: boolean;
            /**
             * Copy custom_fields from parent
             */
            data: {
              /**
               * If the document is automatically generated
               */
              automatisch_genereren?: boolean;
              /**
               * Description
               */
              description?: string;
              /**
               * Filename of document
               */
              filename: string;
              /**
               * Target format for document
               */
              target_format: string;
              [k: string]: any;
            };
            /**
             * Label on document
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Emails releted to case_type
           */
          emails?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Email data
             */
            data: {
              /**
               * If the email is automatic
               */
              automatic_phase?: boolean;
              /**
               * BCC of email
               */
              bcc?: string;
              /**
               * Email receiver
               */
              behandelaar?: string;
              /**
               * Role of email receiver
               */
              betrokkene_role?: string;
              /**
               * Id of bibliotheek_notificatie
               */
              bibliotheek_notificaties_id: number;
              /**
               * Body of email
               */
              body: string;
              /**
               * CC of email
               */
              case_document_attachments?: {
                /**
                 * Id of case document
                 */
                case_document_ids: number;
                /**
                 * Name of case_document attachment
                 */
                name: string;
                /**
                 * Selected
                 */
                selected?: number;
                [k: string]: any;
              }[];
              /**
               * CC of email
               */
              cc?: string;
              /**
               * Description
               */
              description?: string;
              /**
               * Email template
               */
              email?: string;
              /**
               * Intern block of email
               */
              intern_block?: string;
              /**
               * Email rcpt
               */
              rcpt: string;
              /**
               * Name of sender
               */
              sender?: string;
              /**
               * Address of the sender
               */
              sender_address?: string;
              /**
               * Subject of email
               */
              subject: string;
              /**
               * Id of zaaktype_notificati
               */
              zaaktype_notificatie_id: number;
              [k: string]: any;
            };
            /**
             * Label on email
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Milestone for case_type phase
           */
          milestone: number;
          /**
           * Name of case_type phase
           */
          name: string;
          /**
           * Phase number for case_type phase
           */
          phase: string;
          /**
           * Rules on case_type
           */
          rules?: {
            /**
             * Type of condition
             */
            condition_type: string;
            /**
             * List of rule conditions
             */
            conditions: {
              /**
               * Attribute releated to rule
               */
              kenmerk?:
                | (
                    | 'contactchannel'
                    | 'aanvrager'
                    | 'payment_status'
                    | 'confidentiality'
                    | 'vertrouwelijkheidsaanduiding'
                    | 'beroep_mogelijk'
                    | 'publicatie'
                    | 'bag'
                    | 'lex_silencio_positivo'
                    | 'opschorten_mogelijk'
                    | 'verlenging_mogelijk'
                    | 'wet_dwangsom'
                    | 'wkpb'
                    | 'preferred_contact_channel'
                  )
                | string;
              /**
               * Value of rule condition
               */
              value?: {
                [k: string]: any;
              };
              /**
               * Value checkbox
               */
              value_checkbox?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * List of rule actions to execute when the condition matches
             */
            match_actions: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * Name of case_type_rule
             */
            name: string;
            /**
             * List of rule actions to execute when the condition doesnot match
             */
            nomatch_actions?: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          }[];
          /**
           * Subjects releted to case_type
           */
          subjects?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Subject data
             */
            data: {
              /**
               * Betrokkene Identifier
               */
              betrokkene_identifier?: string;
              /**
               * Type of subject
               */
              betrokkene_type: string;
              /**
               * Boolean indicating if the subject is authorized
               */
              gemachtigd?: boolean;
              /**
               * Magic string for subject
               */
              magic_string_prefix?: string;
              /**
               * Name of subject
               */
              naam: string;
              /**
               * Should the subject get notified
               */
              notify?: boolean;
              /**
               * Role of subject
               */
              rol?: string;
              /**
               * UUID of the subject
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Label of subject relationship
             */
            label: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        }[];
        /**
         * Price for the case type
         */
        price?: {
          /**
           * Price for counter
           */
          counter?: string;
          /**
           * Price for email
           */
          email?: string;
          /**
           * Price for employee
           */
          employee?: string;
          /**
           * Price for post
           */
          post?: string;
          /**
           * Price for telephone
           */
          telephone?: string;
          /**
           * Price for web
           */
          web?: string;
          [k: string]: any;
        };
        /**
         * Principal local value for the case type
         */
        principle_local?: string;
        /**
         * Principle national value
         */
        principle_national?: string;
        /**
         * Publication info for case type
         */
        publication?: string;
        /**
         * Publicity info for case type
         */
        publicity?: string;
        /**
         * Registration bag id
         */
        registration_bag?: string;
        /**
         * Requestor of case_type
         */
        requestor: {
          /**
           * Source of the person
           */
          type_of_requestors?: string[];
          /**
           * Source of the person
           */
          use_for_correspondence?: boolean;
          [k: string]: any;
        };
        /**
         * Case type settings
         */
        settings: {
          /**
           * Boolean indicates allow reuse of case_data
           */
          allow_reuse_casedata?: boolean;
          /**
           * Boolean indicates if API can transition
           */
          api_can_transition?: boolean;
          /**
           * Boolean indicates if there is an ACL check on allocation
           */
          check_acl_on_allocation?: boolean;
          /**
           * Boolean indicates disable_captcha_for_predefined_requestor
           */
          disable_captcha_for_predefined_requestor?: boolean;
          /**
           * Boolean indicates disable pip for requestor
           */
          disable_pip_for_requestor?: boolean;
          /**
           * Boolean indicates enable allocation on requestor
           */
          enable_allocation_on_form?: boolean;
          /**
           * Boolean indicates enable manual payment
           */
          enable_manual_payment?: boolean;
          /**
           * Boolean indicates enable online payment
           */
          enable_online_payment?: boolean;
          /**
           * Boolean indicates if queue for changes from other subjects is enabled
           */
          enable_queue_for_changes_from_other_subjects?: boolean;
          /**
           * Boolean indicates enabling subject relations on form
           */
          enable_subject_relations_on_form?: boolean;
          /**
           * Boolean indicates enable of webform
           */
          enable_webform?: boolean;
          /**
           * Boolean indicates if case_type is public
           */
          is_public?: boolean;
          /**
           * List of case_type default folders
           */
          list_of_default_folders?: {
            /**
             * Name of default folder
             */
            name: string;
            [k: string]: any;
          }[];
          /**
           * Boolean indicates lock_registration_phase
           */
          lock_registration_phase?: boolean;
          /**
           * Boolean indicates open case on create
           */
          open_case_on_create?: boolean;
          /**
           * Case type payment
           */
          payment: {
            /**
             * Payment for assignee
             */
            assignee?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for frontdesk
             */
            frontdesk?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for mail
             */
            mail?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for phone
             */
            phone?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for post
             */
            post?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for webform
             */
            webform?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Boolean indicates require email on webform
           */
          require_email_on_webform?: boolean;
          /**
           * Boolean indicates require mobile_number on webform
           */
          require_mobilenumber_on_webform?: boolean;
          /**
           * Boolean indicates require phonenumber on webform
           */
          require_phonenumber_on_webform?: boolean;
          /**
           * Boolean indicates showing confidentiality
           */
          show_confidentiality?: boolean;
          /**
           * Boolean indicates showing contact info
           */
          show_contact_info?: boolean;
          /**
           * Text confirmation message title
           */
          text_confirmation_message_title?: string;
          /**
           * Text public confirmation message title
           */
          text_public_confirmation_message?: string;
          [k: string]: any;
        };
        /**
         * Supervisor for the case type
         */
        supervisor?: string;
        /**
         * Relation of supervisor for the case type
         */
        supervisor_relation?: string;
        /**
         * Suspension status for the case type
         */
        suspension?: string;
        /**
         * Tags for case_type
         */
        tags?: string;
        /**
         * Terms for a case_type
         */
        terms: {
          /**
           * Lead time legal
           */
          lead_time_legal: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          /**
           * Lead time service
           */
          lead_time_service: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Text used for publication
         */
        text_for_publication?: string;
        /**
         * Version number of case_type
         */
        version: number;
        /**
         * Version creation date for case type
         */
        version_date_of_creation?: string;
        /**
         * Date of expiration for particular case type version
         */
        version_date_of_expiration?: string;
        /**
         * WKPB value for the case type
         */
        wkpb?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        catalog_folder?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        preset_assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseTypeForCaseList {
    data: {
      attributes: {
        /**
         * Active status of case_type
         */
        active: boolean;
        /**
         * Adjourn period for the case type
         */
        adjourn_period?: string;
        /**
         * Archive classification code
         */
        archive_classification_code?: string;
        /**
         * Public summary of case_type
         */
        case_public_summary?: string;
        /**
         * Summary of case_type
         */
        case_summary?: string;
        /**
         * UUID of the case_type
         */
        case_type_uuid: string;
        /**
         * Created date of case_type
         */
        created: string;
        /**
         * Deleted date of case_type
         */
        deleted?: string;
        /**
         * Description for case type
         */
        description?: string;
        /**
         * Designation of confidentiality for case type
         */
        designation_of_confidentiality?: string;
        /**
         * Eform status for the case type
         */
        eform?: string;
        /**
         * Extension details for the case type
         */
        extension?: string;
        /**
         * Extension period for the case type
         */
        extension_period?: string;
        /**
         * Generic category for the case type
         */
        generic_category?: string;
        /**
         * Goal set for the case type
         */
        goal?: string;
        /**
         * Id of the case_type node
         */
        id?: string;
        /**
         * Identification for the case type
         */
        identification?: string;
        /**
         * Initiator source for the case type
         */
        initiator_source?: string;
        /**
         * Type of initiator for the case
         */
        initiator_type?: string;
        /**
         * Boolean to check if the case_type is eligible for case creation
         */
        is_eligible_for_case_creation: boolean;
        /**
         * Keywords defined for the case type
         */
        keywords?: string;
        /**
         * Last modified date for case_type
         */
        last_modified: string;
        /**
         * LSP license details
         */
        lex_silencio_positivo?: string;
        /**
         * Meta data of case_type
         */
        metadata: {
          /**
           * Source of the person
           */
          adjourn_period?: string;
          /**
           * Source of the person
           */
          archive_classification_code?: string;
          /**
           * Source of the person
           */
          bag?: string;
          /**
           * Source of the person
           */
          designation_of_confidentiality?: string;
          /**
           * Source of the person
           */
          e_webform?: string;
          /**
           * Source of the person
           */
          extension_period?: string;
          /**
           * Source of the person
           */
          legal_basis: string;
          /**
           * Source of the person
           */
          lex_silencio_positivo?: string;
          /**
           * Source of the person
           */
          local_basis?: string;
          /**
           * Source of the person
           */
          may_extend?: string;
          /**
           * Source of the person
           */
          may_postpone?: string;
          /**
           * Source of the person
           */
          motivation?: string;
          /**
           * Source of the person
           */
          penalty_law?: string;
          /**
           * Source of the person
           */
          possibility_for_objection_and_appeal?: string;
          /**
           * Process description of case_type
           */
          process_description: string;
          /**
           * Source of the person
           */
          publication?: string;
          /**
           * Source of the person
           */
          publication_text?: string;
          /**
           * Source of the person
           */
          purpose?: string;
          /**
           * Source of the person
           */
          responsible_relationship?: string;
          /**
           * Source of the person
           */
          responsible_subject?: string;
          /**
           * Source of the person
           */
          wkpb_applies?: string;
          [k: string]: any;
        };
        /**
         * Motivation details for case_type
         */
        motivation?: string;
        /**
         * Name of case_type
         */
        name?: string;
        /**
         * Objection appeal status
         */
        objection_and_appeal?: string;
        /**
         * IPenalty for the case type
         */
        penalty?: string;
        /**
         * Array of phases for case_type
         */
        phases: {
          /**
           * Allocation of case_type phase
           */
          allocation?: {
            /**
             * Department assigned for phase
             */
            department: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the department
                   */
                  description?: string;
                  /**
                   * Name of the department
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role assigned for phase
             */
            role: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the role
                   */
                  description?: string;
                  /**
                   * Name of the role
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role set for phase
             */
            role_set?: number;
            [k: string]: any;
          };
          /**
           * Cases releted to case_type
           */
          cases?: {
            /**
             * Allocation of case_type
             */
            allocation?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            };
            /**
             * Copy custom_fields from parent
             */
            copy_custom_fields_from_parent?: boolean;
            /**
             * Label case in pip
             */
            label_in_pip?: string;
            /**
             * Open case on create
             */
            open_case_on_create?: boolean;
            /**
             * UUID of related element
             */
            related_casetype_element: string;
            /**
             * Requestor of related case
             */
            requestor?: {
              /**
               * Related role for case
               */
              related_role?: string;
              /**
               * Type of requestor for case
               */
              requestor_type: string;
              [k: string]: any;
            };
            /**
             * Resolve before phase
             */
            resolve_before_phase?: number;
            /**
             * Show the case in pip
             */
            show_in_pip?: boolean;
            /**
             * Boolean to check if the case_type starts on transition
             */
            start_on_transition?: boolean;
            /**
             * Type of case_relation
             */
            type_of_relation: string;
            [k: string]: any;
          }[];
          /**
           * Checklist items releted to case_type
           */
          checklist_items?: string[];
          /**
           * Custom_fields releted to case_type
           */
          custom_fields?: {
            /**
             * Options available for custom_field
             */
            date_field_limit?: {
              /**
               * Name of custom field publication
               */
              end?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              /**
               * Name of custom field publication
               */
              start?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Default value for custom_field
             */
            default_value?: string;
            /**
             * Description of case_type
             */
            description?: string;
            /**
             * Edit authorizations for custom_field
             */
            edit_authorizations?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            }[];
            /**
             * Is skip of queue enabled for case_type
             */
            enable_skip_of_queue?: boolean;
            /**
             * External description of case_type
             */
            external_description?: string;
            /**
             * Magic string of custom_field
             */
            field_magic_string: string;
            /**
             * Options available for custom_field
             */
            field_options?: string[];
            /**
             * Type of custom_field
             */
            field_type: string;
            /**
             * Is the custom_field hidden
             */
            is_hidden_field?: boolean;
            /**
             * Is the custom_field multiple choice
             */
            is_multiple?: boolean;
            /**
             * Is the custom_field required
             */
            is_required?: boolean;
            /**
             * Name of custom field
             */
            name: string;
            /**
             * Public name of custom_field
             */
            public_name?: string;
            /**
             * Places where the case_type is published
             */
            publish_on?: {
              /**
               * Name of custom field publication
               */
              name: string;
              [k: string]: any;
            }[];
            /**
             * Referential for custom_field
             */
            referential?: boolean;
            /**
             * Name of custom field relationship
             */
            relationship_name?: string;
            /**
             * Name of custom field relationship
             */
            relationship_subject_role?: string;
            /**
             * Type of relationship
             */
            relationship_type?: string;
            /**
             * Inertnal identifier of custom_field relationship
             */
            relationship_uuid?: string;
            /**
             * Can requestor chnage custom_field from pip
             */
            requestor_can_change_from_pip?: boolean;
            /**
             * Does the custom_field contains sensitive data
             */
            sensitive_data?: boolean;
            /**
             * Title of custom_object
             */
            title?: string;
            /**
             * Title multiple of custom_field
             */
            title_multiple?: string;
            /**
             * UUID of the catalog attribute
             */
            uuid: string;
            [k: string]: any;
          }[];
          /**
           * Documents releted to case_type
           */
          documents?: {
            /**
             * If the document is automatic
             */
            automatic?: boolean;
            /**
             * Copy custom_fields from parent
             */
            data: {
              /**
               * If the document is automatically generated
               */
              automatisch_genereren?: boolean;
              /**
               * Description
               */
              description?: string;
              /**
               * Filename of document
               */
              filename: string;
              /**
               * Target format for document
               */
              target_format: string;
              [k: string]: any;
            };
            /**
             * Label on document
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Emails releted to case_type
           */
          emails?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Email data
             */
            data: {
              /**
               * If the email is automatic
               */
              automatic_phase?: boolean;
              /**
               * BCC of email
               */
              bcc?: string;
              /**
               * Email receiver
               */
              behandelaar?: string;
              /**
               * Role of email receiver
               */
              betrokkene_role?: string;
              /**
               * Id of bibliotheek_notificatie
               */
              bibliotheek_notificaties_id: number;
              /**
               * Body of email
               */
              body: string;
              /**
               * CC of email
               */
              case_document_attachments?: {
                /**
                 * Id of case document
                 */
                case_document_ids: number;
                /**
                 * Name of case_document attachment
                 */
                name: string;
                /**
                 * Selected
                 */
                selected?: number;
                [k: string]: any;
              }[];
              /**
               * CC of email
               */
              cc?: string;
              /**
               * Description
               */
              description?: string;
              /**
               * Email template
               */
              email?: string;
              /**
               * Intern block of email
               */
              intern_block?: string;
              /**
               * Email rcpt
               */
              rcpt: string;
              /**
               * Name of sender
               */
              sender?: string;
              /**
               * Address of the sender
               */
              sender_address?: string;
              /**
               * Subject of email
               */
              subject: string;
              /**
               * Id of zaaktype_notificati
               */
              zaaktype_notificatie_id: number;
              [k: string]: any;
            };
            /**
             * Label on email
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Milestone for case_type phase
           */
          milestone: number;
          /**
           * Name of case_type phase
           */
          name: string;
          /**
           * Phase number for case_type phase
           */
          phase: string;
          /**
           * Rules on case_type
           */
          rules?: {
            /**
             * Type of condition
             */
            condition_type: string;
            /**
             * List of rule conditions
             */
            conditions: {
              /**
               * Attribute releated to rule
               */
              kenmerk?:
                | (
                    | 'contactchannel'
                    | 'aanvrager'
                    | 'payment_status'
                    | 'confidentiality'
                    | 'vertrouwelijkheidsaanduiding'
                    | 'beroep_mogelijk'
                    | 'publicatie'
                    | 'bag'
                    | 'lex_silencio_positivo'
                    | 'opschorten_mogelijk'
                    | 'verlenging_mogelijk'
                    | 'wet_dwangsom'
                    | 'wkpb'
                    | 'preferred_contact_channel'
                  )
                | string;
              /**
               * Value of rule condition
               */
              value?: {
                [k: string]: any;
              };
              /**
               * Value checkbox
               */
              value_checkbox?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * List of rule actions to execute when the condition matches
             */
            match_actions: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * Name of case_type_rule
             */
            name: string;
            /**
             * List of rule actions to execute when the condition doesnot match
             */
            nomatch_actions?: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          }[];
          /**
           * Subjects releted to case_type
           */
          subjects?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Subject data
             */
            data: {
              /**
               * Betrokkene Identifier
               */
              betrokkene_identifier?: string;
              /**
               * Type of subject
               */
              betrokkene_type: string;
              /**
               * Boolean indicating if the subject is authorized
               */
              gemachtigd?: boolean;
              /**
               * Magic string for subject
               */
              magic_string_prefix?: string;
              /**
               * Name of subject
               */
              naam: string;
              /**
               * Should the subject get notified
               */
              notify?: boolean;
              /**
               * Role of subject
               */
              rol?: string;
              /**
               * UUID of the subject
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Label of subject relationship
             */
            label: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        }[];
        /**
         * Price for the case type
         */
        price?: {
          /**
           * Price for counter
           */
          counter?: string;
          /**
           * Price for email
           */
          email?: string;
          /**
           * Price for employee
           */
          employee?: string;
          /**
           * Price for post
           */
          post?: string;
          /**
           * Price for telephone
           */
          telephone?: string;
          /**
           * Price for web
           */
          web?: string;
          [k: string]: any;
        };
        /**
         * Principal local value for the case type
         */
        principle_local?: string;
        /**
         * Principle national value
         */
        principle_national?: string;
        /**
         * Publication info for case type
         */
        publication?: string;
        /**
         * Publicity info for case type
         */
        publicity?: string;
        /**
         * Registration bag id
         */
        registration_bag?: string;
        /**
         * Requestor of case_type
         */
        requestor: {
          /**
           * Source of the person
           */
          type_of_requestors?: string[];
          /**
           * Source of the person
           */
          use_for_correspondence?: boolean;
          [k: string]: any;
        };
        /**
         * Case type settings
         */
        settings: {
          /**
           * Boolean indicates allow reuse of case_data
           */
          allow_reuse_casedata?: boolean;
          /**
           * Boolean indicates if API can transition
           */
          api_can_transition?: boolean;
          /**
           * Boolean indicates if there is an ACL check on allocation
           */
          check_acl_on_allocation?: boolean;
          /**
           * Boolean indicates disable_captcha_for_predefined_requestor
           */
          disable_captcha_for_predefined_requestor?: boolean;
          /**
           * Boolean indicates disable pip for requestor
           */
          disable_pip_for_requestor?: boolean;
          /**
           * Boolean indicates enable allocation on requestor
           */
          enable_allocation_on_form?: boolean;
          /**
           * Boolean indicates enable manual payment
           */
          enable_manual_payment?: boolean;
          /**
           * Boolean indicates enable online payment
           */
          enable_online_payment?: boolean;
          /**
           * Boolean indicates if queue for changes from other subjects is enabled
           */
          enable_queue_for_changes_from_other_subjects?: boolean;
          /**
           * Boolean indicates enabling subject relations on form
           */
          enable_subject_relations_on_form?: boolean;
          /**
           * Boolean indicates enable of webform
           */
          enable_webform?: boolean;
          /**
           * Boolean indicates if case_type is public
           */
          is_public?: boolean;
          /**
           * List of case_type default folders
           */
          list_of_default_folders?: {
            /**
             * Name of default folder
             */
            name: string;
            [k: string]: any;
          }[];
          /**
           * Boolean indicates lock_registration_phase
           */
          lock_registration_phase?: boolean;
          /**
           * Boolean indicates open case on create
           */
          open_case_on_create?: boolean;
          /**
           * Case type payment
           */
          payment: {
            /**
             * Payment for assignee
             */
            assignee?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for frontdesk
             */
            frontdesk?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for mail
             */
            mail?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for phone
             */
            phone?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for post
             */
            post?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for webform
             */
            webform?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Boolean indicates require email on webform
           */
          require_email_on_webform?: boolean;
          /**
           * Boolean indicates require mobile_number on webform
           */
          require_mobilenumber_on_webform?: boolean;
          /**
           * Boolean indicates require phonenumber on webform
           */
          require_phonenumber_on_webform?: boolean;
          /**
           * Boolean indicates showing confidentiality
           */
          show_confidentiality?: boolean;
          /**
           * Boolean indicates showing contact info
           */
          show_contact_info?: boolean;
          /**
           * Text confirmation message title
           */
          text_confirmation_message_title?: string;
          /**
           * Text public confirmation message title
           */
          text_public_confirmation_message?: string;
          [k: string]: any;
        };
        /**
         * Supervisor for the case type
         */
        supervisor?: string;
        /**
         * Relation of supervisor for the case type
         */
        supervisor_relation?: string;
        /**
         * Suspension status for the case type
         */
        suspension?: string;
        /**
         * Tags for case_type
         */
        tags?: string;
        /**
         * Terms for a case_type
         */
        terms: {
          /**
           * Lead time legal
           */
          lead_time_legal: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          /**
           * Lead time service
           */
          lead_time_service: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Text used for publication
         */
        text_for_publication?: string;
        /**
         * Version number of case_type
         */
        version: number;
        /**
         * Version creation date for case type
         */
        version_date_of_creation?: string;
        /**
         * Date of expiration for particular case type version
         */
        version_date_of_expiration?: string;
        /**
         * WKPB value for the case type
         */
        wkpb?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        catalog_folder?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        preset_assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseTypeVersionEntity {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Active status of case_type
         */
        active: boolean;
        /**
         * Public summary of case_type
         */
        case_public_summary?: string;
        /**
         * Summary of case_type
         */
        case_summary?: string;
        /**
         * UUID of the case_type
         */
        case_type_uuid: string;
        /**
         * Created date of case_type
         */
        created: string;
        /**
         * Deleted date of case_type
         */
        deleted?: string;
        /**
         * Description of case_type
         */
        description?: string;
        /**
         * Identification for case_type
         */
        identification?: string;
        /**
         * Initiator source for case_type
         */
        initiator_source?: string;
        /**
         * Initiator type for case_type
         */
        initiator_type?: string;
        /**
         * Boolean to check if the case_type is eligible for case creation
         */
        is_eligible_for_case_creation: boolean;
        /**
         * Last modified date for case_type
         */
        last_modified: string;
        /**
         * Meta data of case_type
         */
        metadata: {
          /**
           * Source of the person
           */
          adjourn_period?: string;
          /**
           * Source of the person
           */
          archive_classification_code?: string;
          /**
           * Source of the person
           */
          bag?: string;
          /**
           * Source of the person
           */
          designation_of_confidentiality?: string;
          /**
           * Source of the person
           */
          e_webform?: string;
          /**
           * Source of the person
           */
          extension_period?: string;
          /**
           * Source of the person
           */
          legal_basis: string;
          /**
           * Source of the person
           */
          lex_silencio_positivo?: string;
          /**
           * Source of the person
           */
          local_basis?: string;
          /**
           * Source of the person
           */
          may_extend?: string;
          /**
           * Source of the person
           */
          may_postpone?: string;
          /**
           * Source of the person
           */
          motivation?: string;
          /**
           * Source of the person
           */
          penalty_law?: string;
          /**
           * Source of the person
           */
          possibility_for_objection_and_appeal?: string;
          /**
           * Process description of case_type
           */
          process_description: string;
          /**
           * Source of the person
           */
          publication?: string;
          /**
           * Source of the person
           */
          publication_text?: string;
          /**
           * Source of the person
           */
          purpose?: string;
          /**
           * Source of the person
           */
          responsible_relationship?: string;
          /**
           * Source of the person
           */
          responsible_subject?: string;
          /**
           * Source of the person
           */
          wkpb_applies?: string;
          [k: string]: any;
        };
        /**
         * Name of case_type
         */
        name: string;
        /**
         * Array of phases for case_type
         */
        phases: {
          /**
           * Allocation of case_type phase
           */
          allocation?: {
            /**
             * Department assigned for phase
             */
            department: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the department
                   */
                  description?: string;
                  /**
                   * Name of the department
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role assigned for phase
             */
            role: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the role
                   */
                  description?: string;
                  /**
                   * Name of the role
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role set for phase
             */
            role_set?: number;
            [k: string]: any;
          };
          /**
           * Cases releted to case_type
           */
          cases?: {
            /**
             * Allocation of case_type
             */
            allocation?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            };
            /**
             * Copy custom_fields from parent
             */
            copy_custom_fields_from_parent?: boolean;
            /**
             * Label case in pip
             */
            label_in_pip?: string;
            /**
             * Open case on create
             */
            open_case_on_create?: boolean;
            /**
             * UUID of related element
             */
            related_casetype_element: string;
            /**
             * Requestor of related case
             */
            requestor?: {
              /**
               * Related role for case
               */
              related_role?: string;
              /**
               * Type of requestor for case
               */
              requestor_type: string;
              [k: string]: any;
            };
            /**
             * Resolve before phase
             */
            resolve_before_phase?: number;
            /**
             * Show the case in pip
             */
            show_in_pip?: boolean;
            /**
             * Boolean to check if the case_type starts on transition
             */
            start_on_transition?: boolean;
            /**
             * Type of case_relation
             */
            type_of_relation: string;
            [k: string]: any;
          }[];
          /**
           * Checklist items releted to case_type
           */
          checklist_items?: string[];
          /**
           * Custom_fields releted to case_type
           */
          custom_fields?: {
            /**
             * Options available for custom_field
             */
            date_field_limit?: {
              /**
               * Name of custom field publication
               */
              end?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              /**
               * Name of custom field publication
               */
              start?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Default value for custom_field
             */
            default_value?: string;
            /**
             * Description of case_type
             */
            description?: string;
            /**
             * Edit authorizations for custom_field
             */
            edit_authorizations?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            }[];
            /**
             * Is skip of queue enabled for case_type
             */
            enable_skip_of_queue?: boolean;
            /**
             * External description of case_type
             */
            external_description?: string;
            /**
             * Magic string of custom_field
             */
            field_magic_string: string;
            /**
             * Options available for custom_field
             */
            field_options?: string[];
            /**
             * Type of custom_field
             */
            field_type: string;
            /**
             * Is the custom_field hidden
             */
            is_hidden_field?: boolean;
            /**
             * Is the custom_field multiple choice
             */
            is_multiple?: boolean;
            /**
             * Is the custom_field required
             */
            is_required?: boolean;
            /**
             * Name of custom field
             */
            name: string;
            /**
             * Public name of custom_field
             */
            public_name?: string;
            /**
             * Places where the case_type is published
             */
            publish_on?: {
              /**
               * Name of custom field publication
               */
              name: string;
              [k: string]: any;
            }[];
            /**
             * Referential for custom_field
             */
            referential?: boolean;
            /**
             * Name of custom field relationship
             */
            relationship_name?: string;
            /**
             * Name of custom field relationship
             */
            relationship_subject_role?: string;
            /**
             * Type of relationship
             */
            relationship_type?: string;
            /**
             * Inertnal identifier of custom_field relationship
             */
            relationship_uuid?: string;
            /**
             * Can requestor chnage custom_field from pip
             */
            requestor_can_change_from_pip?: boolean;
            /**
             * Does the custom_field contains sensitive data
             */
            sensitive_data?: boolean;
            /**
             * Title of custom_object
             */
            title?: string;
            /**
             * Title multiple of custom_field
             */
            title_multiple?: string;
            /**
             * UUID of the catalog attribute
             */
            uuid: string;
            [k: string]: any;
          }[];
          /**
           * Documents releted to case_type
           */
          documents?: {
            /**
             * If the document is automatic
             */
            automatic?: boolean;
            /**
             * Copy custom_fields from parent
             */
            data: {
              /**
               * If the document is automatically generated
               */
              automatisch_genereren?: boolean;
              /**
               * Description
               */
              description?: string;
              /**
               * Filename of document
               */
              filename: string;
              /**
               * Target format for document
               */
              target_format: string;
              [k: string]: any;
            };
            /**
             * Label on document
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Emails releted to case_type
           */
          emails?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Email data
             */
            data: {
              /**
               * If the email is automatic
               */
              automatic_phase?: boolean;
              /**
               * BCC of email
               */
              bcc?: string;
              /**
               * Email receiver
               */
              behandelaar?: string;
              /**
               * Role of email receiver
               */
              betrokkene_role?: string;
              /**
               * Id of bibliotheek_notificatie
               */
              bibliotheek_notificaties_id: number;
              /**
               * Body of email
               */
              body: string;
              /**
               * CC of email
               */
              case_document_attachments?: {
                /**
                 * Id of case document
                 */
                case_document_ids: number;
                /**
                 * Name of case_document attachment
                 */
                name: string;
                /**
                 * Selected
                 */
                selected?: number;
                [k: string]: any;
              }[];
              /**
               * CC of email
               */
              cc?: string;
              /**
               * Description
               */
              description?: string;
              /**
               * Email template
               */
              email?: string;
              /**
               * Intern block of email
               */
              intern_block?: string;
              /**
               * Email rcpt
               */
              rcpt: string;
              /**
               * Name of sender
               */
              sender?: string;
              /**
               * Address of the sender
               */
              sender_address?: string;
              /**
               * Subject of email
               */
              subject: string;
              /**
               * Id of zaaktype_notificati
               */
              zaaktype_notificatie_id: number;
              [k: string]: any;
            };
            /**
             * Label on email
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Milestone for case_type phase
           */
          milestone: number;
          /**
           * Name of case_type phase
           */
          name: string;
          /**
           * Phase number for case_type phase
           */
          phase: string;
          /**
           * Rules on case_type
           */
          rules?: {
            /**
             * Type of condition
             */
            condition_type: string;
            /**
             * List of rule conditions
             */
            conditions: {
              /**
               * Attribute releated to rule
               */
              kenmerk?:
                | (
                    | 'contactchannel'
                    | 'aanvrager'
                    | 'payment_status'
                    | 'confidentiality'
                    | 'vertrouwelijkheidsaanduiding'
                    | 'beroep_mogelijk'
                    | 'publicatie'
                    | 'bag'
                    | 'lex_silencio_positivo'
                    | 'opschorten_mogelijk'
                    | 'verlenging_mogelijk'
                    | 'wet_dwangsom'
                    | 'wkpb'
                    | 'preferred_contact_channel'
                  )
                | string;
              /**
               * Value of rule condition
               */
              value?: {
                [k: string]: any;
              };
              /**
               * Value checkbox
               */
              value_checkbox?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * List of rule actions to execute when the condition matches
             */
            match_actions: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * Name of case_type_rule
             */
            name: string;
            /**
             * List of rule actions to execute when the condition doesnot match
             */
            nomatch_actions?: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          }[];
          /**
           * Subjects releted to case_type
           */
          subjects?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Subject data
             */
            data: {
              /**
               * Betrokkene Identifier
               */
              betrokkene_identifier?: string;
              /**
               * Type of subject
               */
              betrokkene_type: string;
              /**
               * Boolean indicating if the subject is authorized
               */
              gemachtigd?: boolean;
              /**
               * Magic string for subject
               */
              magic_string_prefix?: string;
              /**
               * Name of subject
               */
              naam: string;
              /**
               * Should the subject get notified
               */
              notify?: boolean;
              /**
               * Role of subject
               */
              rol?: string;
              /**
               * UUID of the subject
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Label of subject relationship
             */
            label: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        }[];
        /**
         * Requestor of case_type
         */
        requestor: {
          /**
           * Source of the person
           */
          type_of_requestors?: string[];
          /**
           * Source of the person
           */
          use_for_correspondence?: boolean;
          [k: string]: any;
        };
        /**
         * Case type settings
         */
        settings: {
          /**
           * Boolean indicates allow reuse of case_data
           */
          allow_reuse_casedata?: boolean;
          /**
           * Boolean indicates if API can transition
           */
          api_can_transition?: boolean;
          /**
           * Boolean indicates if there is an ACL check on allocation
           */
          check_acl_on_allocation?: boolean;
          /**
           * Boolean indicates disable_captcha_for_predefined_requestor
           */
          disable_captcha_for_predefined_requestor?: boolean;
          /**
           * Boolean indicates disable pip for requestor
           */
          disable_pip_for_requestor?: boolean;
          /**
           * Boolean indicates enable allocation on requestor
           */
          enable_allocation_on_form?: boolean;
          /**
           * Boolean indicates enable manual payment
           */
          enable_manual_payment?: boolean;
          /**
           * Boolean indicates enable online payment
           */
          enable_online_payment?: boolean;
          /**
           * Boolean indicates if queue for changes from other subjects is enabled
           */
          enable_queue_for_changes_from_other_subjects?: boolean;
          /**
           * Boolean indicates enabling subject relations on form
           */
          enable_subject_relations_on_form?: boolean;
          /**
           * Boolean indicates enable of webform
           */
          enable_webform?: boolean;
          /**
           * Boolean indicates if case_type is public
           */
          is_public?: boolean;
          /**
           * List of case_type default folders
           */
          list_of_default_folders?: {
            /**
             * Name of default folder
             */
            name: string;
            [k: string]: any;
          }[];
          /**
           * Boolean indicates lock_registration_phase
           */
          lock_registration_phase?: boolean;
          /**
           * Boolean indicates open case on create
           */
          open_case_on_create?: boolean;
          /**
           * Case type payment
           */
          payment: {
            /**
             * Payment for assignee
             */
            assignee?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for frontdesk
             */
            frontdesk?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for mail
             */
            mail?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for phone
             */
            phone?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for post
             */
            post?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for webform
             */
            webform?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Boolean indicates require email on webform
           */
          require_email_on_webform?: boolean;
          /**
           * Boolean indicates require mobile_number on webform
           */
          require_mobilenumber_on_webform?: boolean;
          /**
           * Boolean indicates require phonenumber on webform
           */
          require_phonenumber_on_webform?: boolean;
          /**
           * Boolean indicates showing confidentiality
           */
          show_confidentiality?: boolean;
          /**
           * Boolean indicates showing contact info
           */
          show_contact_info?: boolean;
          /**
           * Text confirmation message title
           */
          text_confirmation_message_title?: string;
          /**
           * Text public confirmation message title
           */
          text_public_confirmation_message?: string;
          [k: string]: any;
        };
        /**
         * Tags for case_type
         */
        tags?: string;
        /**
         * Terms for a case_type
         */
        terms: {
          /**
           * Lead time legal
           */
          lead_time_legal: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          /**
           * Lead time service
           */
          lead_time_service: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Version number of case_type
         */
        version: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        catalog_folder?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        preset_assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCaseTypeVersionEntityList {
    data: {
      attributes: {
        /**
         * Active status of case_type
         */
        active: boolean;
        /**
         * Public summary of case_type
         */
        case_public_summary?: string;
        /**
         * Summary of case_type
         */
        case_summary?: string;
        /**
         * UUID of the case_type
         */
        case_type_uuid: string;
        /**
         * Created date of case_type
         */
        created: string;
        /**
         * Deleted date of case_type
         */
        deleted?: string;
        /**
         * Description of case_type
         */
        description?: string;
        /**
         * Identification for case_type
         */
        identification?: string;
        /**
         * Initiator source for case_type
         */
        initiator_source?: string;
        /**
         * Initiator type for case_type
         */
        initiator_type?: string;
        /**
         * Boolean to check if the case_type is eligible for case creation
         */
        is_eligible_for_case_creation: boolean;
        /**
         * Last modified date for case_type
         */
        last_modified: string;
        /**
         * Meta data of case_type
         */
        metadata: {
          /**
           * Source of the person
           */
          adjourn_period?: string;
          /**
           * Source of the person
           */
          archive_classification_code?: string;
          /**
           * Source of the person
           */
          bag?: string;
          /**
           * Source of the person
           */
          designation_of_confidentiality?: string;
          /**
           * Source of the person
           */
          e_webform?: string;
          /**
           * Source of the person
           */
          extension_period?: string;
          /**
           * Source of the person
           */
          legal_basis: string;
          /**
           * Source of the person
           */
          lex_silencio_positivo?: string;
          /**
           * Source of the person
           */
          local_basis?: string;
          /**
           * Source of the person
           */
          may_extend?: string;
          /**
           * Source of the person
           */
          may_postpone?: string;
          /**
           * Source of the person
           */
          motivation?: string;
          /**
           * Source of the person
           */
          penalty_law?: string;
          /**
           * Source of the person
           */
          possibility_for_objection_and_appeal?: string;
          /**
           * Process description of case_type
           */
          process_description: string;
          /**
           * Source of the person
           */
          publication?: string;
          /**
           * Source of the person
           */
          publication_text?: string;
          /**
           * Source of the person
           */
          purpose?: string;
          /**
           * Source of the person
           */
          responsible_relationship?: string;
          /**
           * Source of the person
           */
          responsible_subject?: string;
          /**
           * Source of the person
           */
          wkpb_applies?: string;
          [k: string]: any;
        };
        /**
         * Name of case_type
         */
        name: string;
        /**
         * Array of phases for case_type
         */
        phases: {
          /**
           * Allocation of case_type phase
           */
          allocation?: {
            /**
             * Department assigned for phase
             */
            department: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the department
                   */
                  description?: string;
                  /**
                   * Name of the department
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role assigned for phase
             */
            role: {
              /**
               * Pydantic based Entity object
               *
               * Entity object based on pydantic and the "new way" of creating entities in
               * our minty platform. It has the same functionality as the EntityBase object,
               * but does not depend on it. Migrationpaths are unsure.
               */
              data: {
                attributes: {
                  /**
                   * Longer description of the role
                   */
                  description?: string;
                  /**
                   * Name of the role
                   */
                  name: string;
                  [k: string]: any;
                };
                id: string;
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                meta?: {
                  /**
                   * Human readable summary of the content of the object
                   */
                  summary?: string;
                  [k: string]: any;
                };
                relationships?: {
                  parent?: {
                    data: {
                      id?: string;
                      type?: string;
                      [k: string]: any;
                    };
                    links?: {
                      self?: string;
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  [k: string]: any;
                };
                type: string;
                [k: string]: any;
              };
              links: {
                self?: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Role set for phase
             */
            role_set?: number;
            [k: string]: any;
          };
          /**
           * Cases releted to case_type
           */
          cases?: {
            /**
             * Allocation of case_type
             */
            allocation?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            };
            /**
             * Copy custom_fields from parent
             */
            copy_custom_fields_from_parent?: boolean;
            /**
             * Label case in pip
             */
            label_in_pip?: string;
            /**
             * Open case on create
             */
            open_case_on_create?: boolean;
            /**
             * UUID of related element
             */
            related_casetype_element: string;
            /**
             * Requestor of related case
             */
            requestor?: {
              /**
               * Related role for case
               */
              related_role?: string;
              /**
               * Type of requestor for case
               */
              requestor_type: string;
              [k: string]: any;
            };
            /**
             * Resolve before phase
             */
            resolve_before_phase?: number;
            /**
             * Show the case in pip
             */
            show_in_pip?: boolean;
            /**
             * Boolean to check if the case_type starts on transition
             */
            start_on_transition?: boolean;
            /**
             * Type of case_relation
             */
            type_of_relation: string;
            [k: string]: any;
          }[];
          /**
           * Checklist items releted to case_type
           */
          checklist_items?: string[];
          /**
           * Custom_fields releted to case_type
           */
          custom_fields?: {
            /**
             * Options available for custom_field
             */
            date_field_limit?: {
              /**
               * Name of custom field publication
               */
              end?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              /**
               * Name of custom field publication
               */
              start?: {
                /**
                 * Is the datelimit is active
                 */
                active: boolean;
                /**
                 * During time for datelimit
                 */
                during: string;
                /**
                 * Value of interval
                 */
                interval: number;
                /**
                 * Type of interval
                 */
                interval_type: string;
                /**
                 * Reference of datelimit
                 */
                reference: string;
                [k: string]: any;
              };
              [k: string]: any;
            };
            /**
             * Default value for custom_field
             */
            default_value?: string;
            /**
             * Description of case_type
             */
            description?: string;
            /**
             * Edit authorizations for custom_field
             */
            edit_authorizations?: {
              /**
               * Department assigned for phase
               */
              department: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the department
                     */
                    description?: string;
                    /**
                     * Name of the department
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role assigned for phase
               */
              role: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    /**
                     * Longer description of the role
                     */
                    description?: string;
                    /**
                     * Name of the role
                     */
                    name: string;
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    parent?: {
                      data: {
                        id?: string;
                        type?: string;
                        [k: string]: any;
                      };
                      links?: {
                        self?: string;
                        [k: string]: any;
                      };
                      [k: string]: any;
                    };
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              /**
               * Role set for phase
               */
              role_set?: number;
              [k: string]: any;
            }[];
            /**
             * Is skip of queue enabled for case_type
             */
            enable_skip_of_queue?: boolean;
            /**
             * External description of case_type
             */
            external_description?: string;
            /**
             * Magic string of custom_field
             */
            field_magic_string: string;
            /**
             * Options available for custom_field
             */
            field_options?: string[];
            /**
             * Type of custom_field
             */
            field_type: string;
            /**
             * Is the custom_field hidden
             */
            is_hidden_field?: boolean;
            /**
             * Is the custom_field multiple choice
             */
            is_multiple?: boolean;
            /**
             * Is the custom_field required
             */
            is_required?: boolean;
            /**
             * Name of custom field
             */
            name: string;
            /**
             * Public name of custom_field
             */
            public_name?: string;
            /**
             * Places where the case_type is published
             */
            publish_on?: {
              /**
               * Name of custom field publication
               */
              name: string;
              [k: string]: any;
            }[];
            /**
             * Referential for custom_field
             */
            referential?: boolean;
            /**
             * Name of custom field relationship
             */
            relationship_name?: string;
            /**
             * Name of custom field relationship
             */
            relationship_subject_role?: string;
            /**
             * Type of relationship
             */
            relationship_type?: string;
            /**
             * Inertnal identifier of custom_field relationship
             */
            relationship_uuid?: string;
            /**
             * Can requestor chnage custom_field from pip
             */
            requestor_can_change_from_pip?: boolean;
            /**
             * Does the custom_field contains sensitive data
             */
            sensitive_data?: boolean;
            /**
             * Title of custom_object
             */
            title?: string;
            /**
             * Title multiple of custom_field
             */
            title_multiple?: string;
            /**
             * UUID of the catalog attribute
             */
            uuid: string;
            [k: string]: any;
          }[];
          /**
           * Documents releted to case_type
           */
          documents?: {
            /**
             * If the document is automatic
             */
            automatic?: boolean;
            /**
             * Copy custom_fields from parent
             */
            data: {
              /**
               * If the document is automatically generated
               */
              automatisch_genereren?: boolean;
              /**
               * Description
               */
              description?: string;
              /**
               * Filename of document
               */
              filename: string;
              /**
               * Target format for document
               */
              target_format: string;
              [k: string]: any;
            };
            /**
             * Label on document
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Emails releted to case_type
           */
          emails?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Email data
             */
            data: {
              /**
               * If the email is automatic
               */
              automatic_phase?: boolean;
              /**
               * BCC of email
               */
              bcc?: string;
              /**
               * Email receiver
               */
              behandelaar?: string;
              /**
               * Role of email receiver
               */
              betrokkene_role?: string;
              /**
               * Id of bibliotheek_notificatie
               */
              bibliotheek_notificaties_id: number;
              /**
               * Body of email
               */
              body: string;
              /**
               * CC of email
               */
              case_document_attachments?: {
                /**
                 * Id of case document
                 */
                case_document_ids: number;
                /**
                 * Name of case_document attachment
                 */
                name: string;
                /**
                 * Selected
                 */
                selected?: number;
                [k: string]: any;
              }[];
              /**
               * CC of email
               */
              cc?: string;
              /**
               * Description
               */
              description?: string;
              /**
               * Email template
               */
              email?: string;
              /**
               * Intern block of email
               */
              intern_block?: string;
              /**
               * Email rcpt
               */
              rcpt: string;
              /**
               * Name of sender
               */
              sender?: string;
              /**
               * Address of the sender
               */
              sender_address?: string;
              /**
               * Subject of email
               */
              subject: string;
              /**
               * Id of zaaktype_notificati
               */
              zaaktype_notificatie_id: number;
              [k: string]: any;
            };
            /**
             * Label on email
             */
            label: string;
            [k: string]: any;
          }[];
          /**
           * Milestone for case_type phase
           */
          milestone: number;
          /**
           * Name of case_type phase
           */
          name: string;
          /**
           * Phase number for case_type phase
           */
          phase: string;
          /**
           * Rules on case_type
           */
          rules?: {
            /**
             * Type of condition
             */
            condition_type: string;
            /**
             * List of rule conditions
             */
            conditions: {
              /**
               * Attribute releated to rule
               */
              kenmerk?:
                | (
                    | 'contactchannel'
                    | 'aanvrager'
                    | 'payment_status'
                    | 'confidentiality'
                    | 'vertrouwelijkheidsaanduiding'
                    | 'beroep_mogelijk'
                    | 'publicatie'
                    | 'bag'
                    | 'lex_silencio_positivo'
                    | 'opschorten_mogelijk'
                    | 'verlenging_mogelijk'
                    | 'wet_dwangsom'
                    | 'wkpb'
                    | 'preferred_contact_channel'
                  )
                | string;
              /**
               * Value of rule condition
               */
              value?: {
                [k: string]: any;
              };
              /**
               * Value checkbox
               */
              value_checkbox?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * List of rule actions to execute when the condition matches
             */
            match_actions: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            /**
             * Name of case_type_rule
             */
            name: string;
            /**
             * List of rule actions to execute when the condition doesnot match
             */
            nomatch_actions?: {
              /**
               * Case type rule action
               */
              action?:
                | (
                    | 'set_value'
                    | 'set_value_formula'
                    | 'set_value_magic_string'
                    | 'show_attribute'
                    | 'hide_attribute'
                    | 'show_text_block'
                    | 'hide_text_block'
                    | 'pause_application'
                    | 'hide_group'
                    | 'show_group'
                    | 'set_allocation'
                    | 'change_confidentiality'
                    | 'change_html_mail_template'
                  )
                | string;
              /**
               * Value
               */
              value?: {
                [k: string]: any;
              };
              [k: string]: any;
            }[];
            [k: string]: any;
          }[];
          /**
           * Subjects releted to case_type
           */
          subjects?: {
            /**
             * If the email is automatic
             */
            automatic: boolean;
            /**
             * Subject data
             */
            data: {
              /**
               * Betrokkene Identifier
               */
              betrokkene_identifier?: string;
              /**
               * Type of subject
               */
              betrokkene_type: string;
              /**
               * Boolean indicating if the subject is authorized
               */
              gemachtigd?: boolean;
              /**
               * Magic string for subject
               */
              magic_string_prefix?: string;
              /**
               * Name of subject
               */
              naam: string;
              /**
               * Should the subject get notified
               */
              notify?: boolean;
              /**
               * Role of subject
               */
              rol?: string;
              /**
               * UUID of the subject
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Label of subject relationship
             */
            label: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        }[];
        /**
         * Requestor of case_type
         */
        requestor: {
          /**
           * Source of the person
           */
          type_of_requestors?: string[];
          /**
           * Source of the person
           */
          use_for_correspondence?: boolean;
          [k: string]: any;
        };
        /**
         * Case type settings
         */
        settings: {
          /**
           * Boolean indicates allow reuse of case_data
           */
          allow_reuse_casedata?: boolean;
          /**
           * Boolean indicates if API can transition
           */
          api_can_transition?: boolean;
          /**
           * Boolean indicates if there is an ACL check on allocation
           */
          check_acl_on_allocation?: boolean;
          /**
           * Boolean indicates disable_captcha_for_predefined_requestor
           */
          disable_captcha_for_predefined_requestor?: boolean;
          /**
           * Boolean indicates disable pip for requestor
           */
          disable_pip_for_requestor?: boolean;
          /**
           * Boolean indicates enable allocation on requestor
           */
          enable_allocation_on_form?: boolean;
          /**
           * Boolean indicates enable manual payment
           */
          enable_manual_payment?: boolean;
          /**
           * Boolean indicates enable online payment
           */
          enable_online_payment?: boolean;
          /**
           * Boolean indicates if queue for changes from other subjects is enabled
           */
          enable_queue_for_changes_from_other_subjects?: boolean;
          /**
           * Boolean indicates enabling subject relations on form
           */
          enable_subject_relations_on_form?: boolean;
          /**
           * Boolean indicates enable of webform
           */
          enable_webform?: boolean;
          /**
           * Boolean indicates if case_type is public
           */
          is_public?: boolean;
          /**
           * List of case_type default folders
           */
          list_of_default_folders?: {
            /**
             * Name of default folder
             */
            name: string;
            [k: string]: any;
          }[];
          /**
           * Boolean indicates lock_registration_phase
           */
          lock_registration_phase?: boolean;
          /**
           * Boolean indicates open case on create
           */
          open_case_on_create?: boolean;
          /**
           * Case type payment
           */
          payment: {
            /**
             * Payment for assignee
             */
            assignee?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for frontdesk
             */
            frontdesk?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for mail
             */
            mail?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for phone
             */
            phone?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for post
             */
            post?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            /**
             * Payment for webform
             */
            webform?: {
              /**
               * Source of the person
               */
              amount?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          /**
           * Boolean indicates require email on webform
           */
          require_email_on_webform?: boolean;
          /**
           * Boolean indicates require mobile_number on webform
           */
          require_mobilenumber_on_webform?: boolean;
          /**
           * Boolean indicates require phonenumber on webform
           */
          require_phonenumber_on_webform?: boolean;
          /**
           * Boolean indicates showing confidentiality
           */
          show_confidentiality?: boolean;
          /**
           * Boolean indicates showing contact info
           */
          show_contact_info?: boolean;
          /**
           * Text confirmation message title
           */
          text_confirmation_message_title?: string;
          /**
           * Text public confirmation message title
           */
          text_public_confirmation_message?: string;
          [k: string]: any;
        };
        /**
         * Tags for case_type
         */
        tags?: string;
        /**
         * Terms for a case_type
         */
        terms: {
          /**
           * Lead time legal
           */
          lead_time_legal: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          /**
           * Lead time service
           */
          lead_time_service: {
            /**
             * Type of case_type term
             */
            type: string;
            /**
             * Value of case_type term
             */
            value: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Version number of case_type
         */
        version: number;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        catalog_folder?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        preset_assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityContactRelatedCase {
    /**
     * Content of related cases for given contact
     */
    data: {
      attributes: {
        /**
         * Archival state of case
         */
        archival_state: 'overdragen' | 'vernietigen';
        /**
         * Days left for given case
         */
        days_left?: {
          /**
           * Number of days left
           */
          amount: number;
          /**
           * Unit of measure
           */
          unit: string;
          [k: string]: any;
        };
        /**
         * Destruction date of case
         */
        destruction_date: string;
        /**
         * Authorization status for given case
         */
        is_authorized?: boolean;
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Roles that contact has for the case (can be an empty list)
         */
        roles: string[];
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        /**
         * Summary of the case
         */
        summary?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityContactRelatedCaseList {
    data: {
      attributes: {
        /**
         * Archival state of case
         */
        archival_state: 'overdragen' | 'vernietigen';
        /**
         * Days left for given case
         */
        days_left?: {
          /**
           * Number of days left
           */
          amount: number;
          /**
           * Unit of measure
           */
          unit: string;
          [k: string]: any;
        };
        /**
         * Destruction date of case
         */
        destruction_date: string;
        /**
         * Authorization status for given case
         */
        is_authorized?: boolean;
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Roles that contact has for the case (can be an empty list)
         */
        roles: string[];
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        /**
         * Summary of the case
         */
        summary?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCountry {
    /**
     * Data structure for country data
     */
    data: {
      attributes: {
        /**
         * Dutch code of the country
         */
        code: number;
        /**
         * Name of the country
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCountryList {
    data?: {
      attributes: {
        /**
         * Dutch code of the country
         */
        code: number;
        /**
         * Name of the country
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface EntityCustomObject {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * Archival status for this custom object
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * The current status of this custom object
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for the custom object
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectList {
    data: {
      attributes: {
        /**
         * Archiving metadata for this object
         */
        archive_metadata: {
          /**
           * Archival ground for this custom object
           */
          ground?: string;
          /**
           * Archival retention for this custom object
           */
          retention?: number;
          /**
           * Archival status for this custom object
           */
          status?: 'archived' | 'to destroy' | 'to preserve';
          [k: string]: any;
        };
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * The current status of this custom object
         */
        status?: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The current version of this custom object
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for the custom object
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        cases?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_objects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        documents?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        subjects?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectSearchResult {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Archive status of the custom object
         */
        archive_status: 'archived' | 'to destroy' | 'to preserve';
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * Status of the custom object
         */
        status: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectSearchResultList {
    data: {
      attributes: {
        /**
         * Archive status of the custom object
         */
        archive_status: 'archived' | 'to destroy' | 'to preserve';
        /**
         * Key-value pair of custom fields
         */
        custom_fields?: {
          [k: string]: any;
        };
        /**
         * Date this custom object got created
         */
        date_created: string;
        /**
         * Deleted date for this custom object
         */
        date_deleted?: string;
        /**
         * External reference this specific object
         */
        external_reference?: string;
        /**
         * Last modified date for this custom object
         */
        last_modified: string;
        /**
         * Name of the related object type
         */
        name: string;
        /**
         * Status of the custom object
         */
        status: 'active' | 'inactive' | 'draft';
        /**
         * Subtitle of this specific object
         */
        subtitle: string;
        /**
         * Title of this specific object
         */
        title: string;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectType {
    /**
     * Definition of an object
     */
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * Set of permissions
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * Type of this relationship
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * Type of this custom field
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Indicates if multiple values are allowed.
             */
            multiple_values?: boolean;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * The current status of this object type
         */
        status?: 'active' | 'inactive';
        /**
         * Subtitle for the created object type
         */
        subtitle?: string;
        /**
         * Title for the created object type
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for objects of the custom object type
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityCustomObjectTypeList {
    data: {
      attributes: {
        /**
         * Auditlog for this Custom Object Type Version
         */
        audit_log: {
          /**
           * Description of this change
           */
          description: string;
          /**
           * Components affected by this change
           */
          updated_components: (
            | 'attributes'
            | 'authorizations'
            | 'custom_fields'
            | 'relationships'
          )[];
          [k: string]: any;
        };
        /**
         * Authorization settings for this custom object type
         */
        authorization_definition?: {
          /**
           * Optional list of authorizations for this object
           */
          authorizations?: {
            /**
             * Set of permissions
             */
            authorization: 'read' | 'readwrite' | 'admin';
            /**
             * Group name
             */
            department: {
              /**
               * Name of the department
               */
              name: string;
              /**
               * Unique identifier for this department
               */
              uuid: string;
              [k: string]: any;
            };
            /**
             * Role name
             */
            role: {
              /**
               * Name of the role
               */
              name: string;
              /**
               * Unique identifier for this role
               */
              uuid: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Integer of folder in our elements catalog
         */
        catalog_folder_id?: number;
        /**
         * Definition of the related custom fields
         */
        custom_field_definition?: {
          /**
           * List of custom fields
           */
          custom_fields?: {
            /**
             * UUID referencing the field in our attribute catalog
             */
            attribute_uuid?: string;
            /**
             * Specification around a certain custom field type
             */
            custom_field_specification?:
              | {
                  /**
                   * Name of the relationship
                   */
                  name?: string;
                  /**
                   * Type of this relationship
                   */
                  type: 'case' | 'custom_object' | 'document' | 'subject';
                  /**
                   * Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  /**
                   * UUID of the relationship type
                   */
                  uuid?: string;
                  [k: string]: any;
                }
              | {
                  /**
                   * Use the address_v2 or geolocation on the map of custom_object.
                   */
                  use_on_map?: boolean;
                  [k: string]: any;
                };
            /**
             * Type of this custom field
             */
            custom_field_type?:
              | 'relationship'
              | 'text'
              | 'option'
              | 'select'
              | 'textarea'
              | 'richtext'
              | 'checkbox'
              | 'date'
              | 'geojson'
              | 'valuta'
              | 'email'
              | 'url'
              | 'bankaccount'
              | 'address_v2'
              | 'numeric';
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicates whether this field is a hidden field, or 'system field'
             */
            is_hidden_field?: boolean;
            /**
             * Indicates whether this field is a required field
             */
            is_required?: boolean;
            /**
             * Label of this field
             */
            label: string;
            /**
             * The magic string of this field
             */
            magic_string?: string;
            /**
             * Indicates if multiple values are allowed.
             */
            multiple_values?: boolean;
            /**
             * Name of this field in the catalog
             */
            name: string;
            /**
             * List of possible options for this custom field
             */
            options?: string[];
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * Date this object got created
         */
        date_created: string;
        /**
         * Deleted date for this object
         */
        date_deleted?: string;
        /**
         * An external reference for the object
         */
        external_reference?: string;
        /**
         * Indicates whether this entity is the latest version
         */
        is_active_version: boolean;
        /**
         * Last modified date for this object
         */
        last_modified: string;
        /**
         * Name of this object type
         */
        name: string;
        /**
         * Relationship settings for this custom object type
         */
        relationship_definition?: {
          /**
           * Optional list of relationships for this object
           */
          relationships?: {
            /**
             * UUID of related Case Type
             */
            case_type_uuid?: string;
            /**
             * UUID of related Custom Object Type
             */
            custom_object_type_uuid?: string;
            /**
             * Description of this field for internal purposes
             */
            description?: string;
            /**
             * Description of this field for public purposes
             */
            external_description?: string;
            /**
             * Indicate that this relationship is required on creation of this object
             */
            is_required?: boolean;
            /**
             * Name of this relationship
             */
            name: string;
            [k: string]: any;
          }[];
          [k: string]: any;
        };
        /**
         * The current status of this object type
         */
        status?: 'active' | 'inactive';
        /**
         * Subtitle for the created object type
         */
        subtitle?: string;
        /**
         * Title for the created object type
         */
        title?: string;
        /**
         * The current version of this object type'
         */
        version: number;
        /**
         * The version independent UUID for this object
         */
        version_independent_uuid?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * The set of rights/authorizations the current user has for objects of the custom object type
         */
        authorizations?: ('read' | 'readwrite' | 'admin')[];
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        latest_version?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartment {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Longer description of the department
         */
        description?: string;
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentList {
    data: {
      attributes: {
        /**
         * Longer description of the department
         */
        description?: string;
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentSummary {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityDepartmentSummaryList {
    data: {
      attributes: {
        /**
         * Name of the department
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityEmployee {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Contact info of the person
         */
        contact_information?: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * First names of employee
         */
        first_name?: string;
        /**
         * True if the contact has a valid address. Always false for employees.
         */
        has_valid_address?: boolean;
        /**
         * User is active/inactive
         */
        is_active?: boolean;
        /**
         * Name of the person
         */
        name: string;
        /**
         * Source of the employee
         */
        source?: string;
        /**
         * Status of the employee.
         */
        status: 'active' | 'inactive';
        /**
         * Surname of employee
         */
        surname?: string;
        /**
         * Title of person
         */
        title?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        department?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        roles?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityEmployeeList {
    data: {
      attributes: {
        /**
         * Contact info of the person
         */
        contact_information?: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * First names of employee
         */
        first_name?: string;
        /**
         * True if the contact has a valid address. Always false for employees.
         */
        has_valid_address?: boolean;
        /**
         * User is active/inactive
         */
        is_active?: boolean;
        /**
         * Name of the person
         */
        name: string;
        /**
         * Source of the employee
         */
        source?: string;
        /**
         * Status of the employee.
         */
        status: 'active' | 'inactive';
        /**
         * Surname of employee
         */
        surname?: string;
        /**
         * Title of person
         */
        title?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        department?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        roles?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityEmployeeSettings {
    /**
     * Settings for an employee
     */
    data: {
      attributes: {
        /**
         * Notification settings of employee
         */
        notification_settings?: {
          /**
           * Send notification when the suspension period of case has expired
           */
          case_suspension_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when the processing time of case has expired
           */
          case_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when new case is assigned to the employee
           */
          new_assigned_case: boolean;
          /**
           * Send notification outside zaaksytem when attribute has changed
           */
          new_attribute_proposal: boolean;
          /**
           * Notify outside zaaksytem in case of new document events.
           */
          new_document: boolean;
          /**
           * Send notification outside zaaksyteem when employee receives new pip message
           */
          new_ext_pip_message: boolean;
          [k: string]: any;
        };
        /**
         * The phone extension is used by the phone/PBX integration to show a pop-up when there's a call on the specified extension.
         */
        phone_extension?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        employee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        signature?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityEmployeeSettingsList {
    data: {
      attributes: {
        /**
         * Notification settings of employee
         */
        notification_settings?: {
          /**
           * Send notification when the suspension period of case has expired
           */
          case_suspension_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when the processing time of case has expired
           */
          case_term_exceeded: boolean;
          /**
           * Send notification outside zaaksytem when new case is assigned to the employee
           */
          new_assigned_case: boolean;
          /**
           * Send notification outside zaaksytem when attribute has changed
           */
          new_attribute_proposal: boolean;
          /**
           * Notify outside zaaksytem in case of new document events.
           */
          new_document: boolean;
          /**
           * Send notification outside zaaksyteem when employee receives new pip message
           */
          new_ext_pip_message: boolean;
          [k: string]: any;
        };
        /**
         * The phone extension is used by the phone/PBX integration to show a pop-up when there's a call on the specified extension.
         */
        phone_extension?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        employee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        signature?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityExportFile {
    /**
     * Data structure for export file data
     */
    data: {
      attributes: {
        /**
         * Number of downloads
         */
        downloads?: number;
        /**
         * The date that export file expires
         */
        expires_at: string;
        /**
         * Name of the export_file
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityExportFileList {
    data: {
      attributes: {
        /**
         * Number of downloads
         */
        downloads?: number;
        /**
         * The date that export file expires
         */
        expires_at: string;
        /**
         * Name of the export_file
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityObjectRelation {
    /**
     * Entity represents object relation of attribute in the case
     */
    data: {
      attributes: {
        /**
         * Id of the the attribute
         */
        attribute_id: string;
        /**
         * UUID of the custom object
         */
        custom_object_uuid: string;
        /**
         * Magic string of the attribute
         */
        magic_string: string;
        /**
         * Id of the the attribute
         */
        source_custom_field_type_id: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityObjectRelationList {
    data: {
      attributes: {
        /**
         * Id of the the attribute
         */
        attribute_id: string;
        /**
         * UUID of the custom object
         */
        custom_object_uuid: string;
        /**
         * Magic string of the attribute
         */
        magic_string: string;
        /**
         * Id of the the attribute
         */
        source_custom_field_type_id: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityOrganization {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Branch identification number ('Vestigingsnummer')
         */
        coc_location_number?: number;
        /**
         * Chamber of Commerce (Kamer van Koophandel, KvK) number of the organization
         */
        coc_number: string;
        /**
         * Contact information
         */
        contact_information: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * Contact person for this organization
         */
        contact_person?: {
          /**
           * Family name of the contact person
           */
          family_name?: string;
          /**
           * First name of the contact person
           */
          first_name?: string;
          /**
           * Initials in the name of contact person
           */
          initials?: string;
          /**
           * Insertions in the name of contact person
           */
          insertions?: string;
          [k: string]: any;
        };
        /**
         * Correspondence address
         */
        correspondence_address:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Date the organization ceased operations
         */
        date_ceased: string;
        /**
         * Date the organization was founded
         */
        date_founded: string;
        /**
         * Date the organization was first registered
         */
        date_registered: string;
        /**
         * True if the contact has a valid address
         */
        has_valid_address: boolean;
        /**
         * Organization is active/inactive
         */
        is_active?: boolean;
        /**
         * Main address
         */
        location_address:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Code and descripton of the organization's main activity
         */
        main_activity: {
          /**
           * Activity code
           */
          code: string;
          /**
           * Description of the activity
           */
          description: string;
          [k: string]: any;
        };
        /**
         * Organization name
         */
        name: string;
        /**
         * OIN (Organization Identification Number)
         */
        oin: number;
        /**
         * Type of organization
         */
        organization_type: string;
        /**
         * Preferred channel of contact for this organization
         */
        preferred_contact_channel?: string;
        /**
         * RSIN of the organization
         */
        rsin: number;
        /**
         * Codes and descriptons of the organization's other activities
         */
        secondary_activities: {
          /**
           * Activity code
           */
          code: string;
          /**
           * Description of the activity
           */
          description: string;
          [k: string]: any;
        }[];
        /**
         *
         *         The source of the data in this entity. Will be 'Zaaksysteem' if it was
         *         created manually, or name of the integration if it was imported from
         *         elsewhere.
         *
         */
        source: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityOrganizationList {
    data: {
      attributes: {
        /**
         * Branch identification number ('Vestigingsnummer')
         */
        coc_location_number?: number;
        /**
         * Chamber of Commerce (Kamer van Koophandel, KvK) number of the organization
         */
        coc_number: string;
        /**
         * Contact information
         */
        contact_information: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * Contact person for this organization
         */
        contact_person?: {
          /**
           * Family name of the contact person
           */
          family_name?: string;
          /**
           * First name of the contact person
           */
          first_name?: string;
          /**
           * Initials in the name of contact person
           */
          initials?: string;
          /**
           * Insertions in the name of contact person
           */
          insertions?: string;
          [k: string]: any;
        };
        /**
         * Correspondence address
         */
        correspondence_address:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Date the organization ceased operations
         */
        date_ceased: string;
        /**
         * Date the organization was founded
         */
        date_founded: string;
        /**
         * Date the organization was first registered
         */
        date_registered: string;
        /**
         * True if the contact has a valid address
         */
        has_valid_address: boolean;
        /**
         * Organization is active/inactive
         */
        is_active?: boolean;
        /**
         * Main address
         */
        location_address:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Code and descripton of the organization's main activity
         */
        main_activity: {
          /**
           * Activity code
           */
          code: string;
          /**
           * Description of the activity
           */
          description: string;
          [k: string]: any;
        };
        /**
         * Organization name
         */
        name: string;
        /**
         * OIN (Organization Identification Number)
         */
        oin: number;
        /**
         * Type of organization
         */
        organization_type: string;
        /**
         * Preferred channel of contact for this organization
         */
        preferred_contact_channel?: string;
        /**
         * RSIN of the organization
         */
        rsin: number;
        /**
         * Codes and descriptons of the organization's other activities
         */
        secondary_activities: {
          /**
           * Activity code
           */
          code: string;
          /**
           * Description of the activity
           */
          description: string;
          [k: string]: any;
        }[];
        /**
         *
         *         The source of the data in this entity. Will be 'Zaaksysteem' if it was
         *         created manually, or name of the integration if it was imported from
         *         elsewhere.
         *
         */
        source: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityPerson {
    /**
     * Content of a user defined Custom Object
     */
    data: {
      attributes: {
        /**
         * Boolean indicates if the person is authenticated
         */
        authenticated: boolean;
        /**
         * BSN number for the person
         */
        bsn?: string;
        /**
         * Contact info of the person
         */
        contact_information?: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Is the contact person is anonymous
           */
          is_an_anonymous_contact_person?: boolean;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * Correspondence address of the person
         */
        correspondence_address?:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Country Code of the address
         */
        country_code?: string;
        /**
         * Date of birth of person
         */
        date_of_birth?: string;
        /**
         * Date of death of person
         */
        date_of_death?: string;
        /**
         * Family name of person
         */
        family_name?: string;
        /**
         * First names of person
         */
        first_names?: string;
        /**
         * Gender of the person
         */
        gender: string;
        /**
         * True if the contact has a valid address
         */
        has_valid_address: boolean;
        /**
         * Initials of Person
         */
        initials?: string;
        /**
         * Insertions in the name of the person
         */
        insertions?: string;
        /**
         * Boolean that indicates if the person lives within the municipality or not
         */
        inside_municipality?: boolean;
        /**
         * Mark user as active/inactive
         */
        is_active?: boolean;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * Name of the person
         */
        name: string;
        /**
         * Title of person
         */
        noble_title?: string;
        /**
         * Residence address of the person
         */
        residence_address?:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Source of the person
         */
        source?: string;
        /**
         * Surname of person
         */
        surname?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityPersonList {
    data: {
      attributes: {
        /**
         * Boolean indicates if the person is authenticated
         */
        authenticated: boolean;
        /**
         * BSN number for the person
         */
        bsn?: string;
        /**
         * Contact info of the person
         */
        contact_information?: {
          /**
           * Email address of the contact
           */
          email?: string;
          /**
           * Internal note
           */
          internal_note?: string;
          /**
           * Is the contact person is anonymous
           */
          is_an_anonymous_contact_person?: boolean;
          /**
           * Mobile phone number
           */
          mobile_number?: string;
          /**
           * Preferred phone number
           */
          phone_number?: string;
          /**
           * Preffered contact channel
           */
          preferred_contact_channel?: 'pip' | 'email' | 'phone' | 'mail';
          [k: string]: any;
        };
        /**
         * Correspondence address of the person
         */
        correspondence_address?:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Country Code of the address
         */
        country_code?: string;
        /**
         * Date of birth of person
         */
        date_of_birth?: string;
        /**
         * Date of death of person
         */
        date_of_death?: string;
        /**
         * Family name of person
         */
        family_name?: string;
        /**
         * First names of person
         */
        first_names?: string;
        /**
         * Gender of the person
         */
        gender: string;
        /**
         * True if the contact has a valid address
         */
        has_valid_address: boolean;
        /**
         * Initials of Person
         */
        initials?: string;
        /**
         * Insertions in the name of the person
         */
        insertions?: string;
        /**
         * Boolean that indicates if the person lives within the municipality or not
         */
        inside_municipality?: boolean;
        /**
         * Mark user as active/inactive
         */
        is_active?: boolean;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * Name of the person
         */
        name: string;
        /**
         * Title of person
         */
        noble_title?: string;
        /**
         * Residence address of the person
         */
        residence_address?:
          | {
              /**
               * City
               */
              city: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: false;
              /**
               * Street name
               */
              street: string;
              /**
               * House number
               */
              street_number: number;
              /**
               * House letter (part of house number)
               */
              street_number_letter?: string;
              /**
               * House number extension
               */
              street_number_suffix?: string;
              /**
               * Postal code (zipcode)
               */
              zipcode?: string;
              [k: string]: any;
            }
          | {
              /**
               * First line of the address
               */
              address_line_1?: string;
              /**
               * Second line of the address
               */
              address_line_2?: string;
              /**
               * Third line of the address
               */
              address_line_3?: string;
              /**
               * Country
               */
              country: string;
              /**
               * Indicates that this is an address outside the Netherlands
               */
              is_foreign?: true;
              [k: string]: any;
            };
        /**
         * Source of the person
         */
        source?: string;
        /**
         * Surname of person
         */
        surname?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        related_custom_object?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityPersonSensitiveData {
    /**
     * Contains sensitive information about person
     */
    data: {
      attributes: {
        /**
         * Burger service number of the person
         */
        personal_number?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityPersonSensitiveDataList {
    data: {
      attributes: {
        /**
         * Burger service number of the person
         */
        personal_number?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCase {
    /**
     * Content of related_case
     */
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCaseList {
    data: {
      attributes: {
        /**
         * Case ID
         */
        number: number;
        /**
         * Progress status of the case
         */
        progress: number;
        /**
         * Result of the case
         */
        result?: string;
        /**
         * Status of the case
         */
        status: 'new' | 'stalled' | 'resolved' | 'open';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        assignee?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCustomObjectType {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedCustomObjectTypeList {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedObject {
    /**
     * Content of related_custom_object
     */
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedObjectList {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubject {
    /**
     * Content of related_subject
     */
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * Type of the subject (person/employee/organization)
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectList {
    data: {
      attributes: {
        /**
         * Roles of the subject
         */
        roles?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * Type of the subject (person/employee/organization)
         */
        type: 'person' | 'organization' | 'employee';
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectRole {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRelatedSubjectRoleList {
    data: {
      attributes: {
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRequestor {
    /**
     * Requestor Entity
     */
    data: {
      attributes: {
        /**
         * Another identifier in the dutch systems, called a_nummer
         */
        a_number?: string;
        /**
         * BSN number for the reqestor
         */
        burgerservicenummer?: string;
        /**
         * Chamber of Commerce number of the organization
         */
        coc?: string;
        /**
         * correspondence house number
         */
        correspondence_house_number?: string;
        /**
         * correspondence place of residence
         */
        correspondence_place_of_residence?: string;
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * Requestor's country of birth
         */
        country_of_birth?: string;
        /**
         * country of residence for requestor
         */
        country_of_residence?: string;
        /**
         * Requestor's date of birth
         */
        date_of_birth?: string;
        /**
         * Date of divorce for requestor
         */
        date_of_divorce?: string;
        /**
         * Marriage date for requestor
         */
        date_of_marriage?: string;
        /**
         * Name of the department for requestor
         */
        department?: string;
        /**
         * Email address of requestor
         */
        email?: string;
        /**
         * Establishment number of requestor organization
         */
        establishment_number?: string;
        /**
         * Family name of the requestor
         */
        family_name?: string;
        /**
         * First names for the requestor
         */
        first_names?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Full name of the requestor
         */
        full_name?: string;
        /**
         * Gender of the requestor
         */
        gender?: string;
        /**
         * Indicate if requestor has correspondence address
         */
        has_correspondence_address?: boolean;
        /**
         * House number for the requestor
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the requestor
         */
        initials?: string;
        /**
         * investigation value for reqestor
         */
        investigation?: string;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * login details for requestor
         */
        login?: string;
        /**
         * Contact mobile number of requestor
         */
        mobile_number?: string;
        /**
         * Name of the requestor
         */
        name?: string;
        /**
         * Title of the requestor
         */
        noble_title?: string;
        /**
         * Password details for requestor
         */
        password?: string;
        /**
         * contact phone number of requestor
         */
        phone_number?: string;
        /**
         * Requestor's place of birth
         */
        place_of_birth?: string;
        /**
         * place of residence for requestor
         */
        place_of_residence?: string;
        /**
         * Type of the requestor
         */
        requestor_type?: string;
        /**
         * Residence house number for requestor
         */
        residence_house_number?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Salutation used for requestor
         */
        salutation?: string;
        /**
         * Another Salutation used for requestor
         */
        salutation1?: string;
        /**
         * Another Salutation used for requestor
         */
        salutation2?: string;
        /**
         * Status of requestor
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the requestor
         */
        surname?: string;
        /**
         * Surname prefix of the requestor
         */
        surname_prefix?: string;
        /**
         * Title for the requestor
         */
        title?: string;
        /**
         * Trade name for requestor organization
         */
        trade_name?: string;
        /**
         * Type of the business
         */
        type_of_business_entity?: string;
        /**
         * Unique name for the requestor
         */
        unique_name?: string;
        /**
         * Identifier of case type
         */
        used_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRequestorList {
    data: {
      attributes: {
        /**
         * Another identifier in the dutch systems, called a_nummer
         */
        a_number?: string;
        /**
         * BSN number for the reqestor
         */
        burgerservicenummer?: string;
        /**
         * Chamber of Commerce number of the organization
         */
        coc?: string;
        /**
         * correspondence house number
         */
        correspondence_house_number?: string;
        /**
         * correspondence place of residence
         */
        correspondence_place_of_residence?: string;
        /**
         * correspondence address street value
         */
        correspondence_street?: string;
        /**
         * correspondence zipcode value
         */
        correspondence_zipcode?: string;
        /**
         * Requestor's country of birth
         */
        country_of_birth?: string;
        /**
         * country of residence for requestor
         */
        country_of_residence?: string;
        /**
         * Requestor's date of birth
         */
        date_of_birth?: string;
        /**
         * Date of divorce for requestor
         */
        date_of_divorce?: string;
        /**
         * Marriage date for requestor
         */
        date_of_marriage?: string;
        /**
         * Name of the department for requestor
         */
        department?: string;
        /**
         * Email address of requestor
         */
        email?: string;
        /**
         * Establishment number of requestor organization
         */
        establishment_number?: string;
        /**
         * Family name of the requestor
         */
        family_name?: string;
        /**
         * First names for the requestor
         */
        first_names?: string;
        /**
         * foreign residence address first line
         */
        foreign_residence_address_line1?: string;
        /**
         * foreign residence address second line
         */
        foreign_residence_address_line2?: string;
        /**
         * foreign residence address third line
         */
        foreign_residence_address_line3?: string;
        /**
         * Full name of the requestor
         */
        full_name?: string;
        /**
         * Gender of the requestor
         */
        gender?: string;
        /**
         * Indicate if requestor has correspondence address
         */
        has_correspondence_address?: boolean;
        /**
         * House number for the requestor
         */
        house_number?: string;
        /**
         * Id of the requestor
         */
        id?: string;
        /**
         * Initials for the requestor
         */
        initials?: string;
        /**
         * investigation value for reqestor
         */
        investigation?: string;
        /**
         * If its secret value
         */
        is_secret?: boolean;
        /**
         * login details for requestor
         */
        login?: string;
        /**
         * Contact mobile number of requestor
         */
        mobile_number?: string;
        /**
         * Name of the requestor
         */
        name?: string;
        /**
         * Title of the requestor
         */
        noble_title?: string;
        /**
         * Password details for requestor
         */
        password?: string;
        /**
         * contact phone number of requestor
         */
        phone_number?: string;
        /**
         * Requestor's place of birth
         */
        place_of_birth?: string;
        /**
         * place of residence for requestor
         */
        place_of_residence?: string;
        /**
         * Type of the requestor
         */
        requestor_type?: string;
        /**
         * Residence house number for requestor
         */
        residence_house_number?: string;
        /**
         * Residence address street name
         */
        residence_street?: string;
        /**
         * Residence address zipcode
         */
        residence_zipcode?: string;
        /**
         * Salutation used for requestor
         */
        salutation?: string;
        /**
         * Another Salutation used for requestor
         */
        salutation1?: string;
        /**
         * Another Salutation used for requestor
         */
        salutation2?: string;
        /**
         * Status of requestor
         */
        status?: string;
        /**
         * Address street name
         */
        street?: string;
        /**
         * type of the subject
         */
        subject_type?: string;
        /**
         * Surname of the requestor
         */
        surname?: string;
        /**
         * Surname prefix of the requestor
         */
        surname_prefix?: string;
        /**
         * Title for the requestor
         */
        title?: string;
        /**
         * Trade name for requestor organization
         */
        trade_name?: string;
        /**
         * Type of the business
         */
        type_of_business_entity?: string;
        /**
         * Unique name for the requestor
         */
        unique_name?: string;
        /**
         * Identifier of case type
         */
        used_name?: string;
        /**
         * Zipcode value
         */
        zipcode?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRole {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Longer description of the role
         */
        description?: string;
        /**
         * Name of the role
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityRoleList {
    data: {
      attributes: {
        /**
         * Longer description of the role
         */
        description?: string;
        /**
         * Name of the role
         */
        name: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        parent?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySearchResult {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySearchResultList {
    data: {
      attributes: {
        /**
         * Description of the object represented by this search result
         */
        description?: string;
        /**
         * Type of the object represented by this search result
         */
        result_type: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        case_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        custom_object_type?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySubject {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * system of subject
         */
        department?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * group ids of subject
         */
        group_ids: number[];
        /**
         * Id of subject
         */
        id: number;
        /**
         * last modified date of subject
         */
        last_modified: string;
        /**
         * nobody of subject
         */
        nobody: boolean;
        /**
         * properties of subject
         */
        properties?: {
          [k: string]: any;
        };
        /**
         * system of subject
         */
        related_custom_object_uuid?: string;
        /**
         * role ids of subject
         */
        role_ids: number[];
        /**
         * settingsof subject
         */
        settings?: {
          [k: string]: any;
        };
        /**
         * type of subject
         */
        subject_type: string;
        /**
         * system of subject
         */
        system: boolean;
        /**
         * user of subject
         */
        username: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySubjectList {
    data: {
      attributes: {
        /**
         * system of subject
         */
        department?: {
          /**
           * Pydantic based Entity object
           *
           * Entity object based on pydantic and the "new way" of creating entities in
           * our minty platform. It has the same functionality as the EntityBase object,
           * but does not depend on it. Migrationpaths are unsure.
           */
          data: {
            attributes: {
              /**
               * Longer description of the department
               */
              description?: string;
              /**
               * Name of the department
               */
              name: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              parent?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * group ids of subject
         */
        group_ids: number[];
        /**
         * Id of subject
         */
        id: number;
        /**
         * last modified date of subject
         */
        last_modified: string;
        /**
         * nobody of subject
         */
        nobody: boolean;
        /**
         * properties of subject
         */
        properties?: {
          [k: string]: any;
        };
        /**
         * system of subject
         */
        related_custom_object_uuid?: string;
        /**
         * role ids of subject
         */
        role_ids: number[];
        /**
         * settingsof subject
         */
        settings?: {
          [k: string]: any;
        };
        /**
         * type of subject
         */
        subject_type: string;
        /**
         * system of subject
         */
        system: boolean;
        /**
         * user of subject
         */
        username: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySubjectRelation {
    /**
     * Pydantic based Entity object
     *
     * Entity object based on pydantic and the "new way" of creating entities in
     * our minty platform. It has the same functionality as the EntityBase object,
     * but does not depend on it. Migrationpaths are unsure.
     */
    data: {
      attributes: {
        /**
         * Authorization value for the given subject
         */
        authorized?: boolean;
        /**
         * Details of related case
         */
        case?: {
          /**
           * Content of related_case
           */
          data: {
            attributes: {
              /**
               * Case ID
               */
              number: number;
              /**
               * Progress status of the case
               */
              progress: number;
              /**
               * Result of the case
               */
              result?: string;
              /**
               * Status of the case
               */
              status: 'new' | 'stalled' | 'resolved' | 'open';
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              assignee?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              case_type?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Details for enqueued email
         */
        enqueued_email_data?: {
          /**
           * Internal identifier of case
           */
          case_uuid?: string;
          /**
           * Counter value for loop protection
           */
          loop_protection_counter?: number;
          /**
           * Type of related subject
           */
          subject_type?: string;
          /**
           * Internal identifier of subject
           */
          subject_uuid?: string;
          [k: string]: any;
        };
        /**
         * Boolean determining present client
         */
        is_preset_client?: boolean;
        /**
         * Magic string prefix for subject
         */
        magic_string_prefix?: string;
        /**
         * Permission for the subject
         */
        permission?: string;
        /**
         * Role of subject to be related
         */
        role?: string;
        /**
         * Boolean indicating about send email or not
         */
        send_confirmation_email?: boolean;
        /**
         * Id of source custom field type
         */
        source_custom_field_type_id?: string;
        /**
         * Details of related subject
         */
        subject?: {
          /**
           * Content of related_subject
           */
          data: {
            attributes: {
              /**
               * Roles of the subject
               */
              roles?: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              /**
               * Type of the subject (person/employee/organization)
               */
              type: 'person' | 'organization' | 'employee';
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Type is subject_relation
         */
        type?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntitySubjectRelationList {
    data: {
      attributes: {
        /**
         * Authorization value for the given subject
         */
        authorized?: boolean;
        /**
         * Details of related case
         */
        case?: {
          /**
           * Content of related_case
           */
          data: {
            attributes: {
              /**
               * Case ID
               */
              number: number;
              /**
               * Progress status of the case
               */
              progress: number;
              /**
               * Result of the case
               */
              result?: string;
              /**
               * Status of the case
               */
              status: 'new' | 'stalled' | 'resolved' | 'open';
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              assignee?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              case_type?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Details for enqueued email
         */
        enqueued_email_data?: {
          /**
           * Internal identifier of case
           */
          case_uuid?: string;
          /**
           * Counter value for loop protection
           */
          loop_protection_counter?: number;
          /**
           * Type of related subject
           */
          subject_type?: string;
          /**
           * Internal identifier of subject
           */
          subject_uuid?: string;
          [k: string]: any;
        };
        /**
         * Boolean determining present client
         */
        is_preset_client?: boolean;
        /**
         * Magic string prefix for subject
         */
        magic_string_prefix?: string;
        /**
         * Permission for the subject
         */
        permission?: string;
        /**
         * Role of subject to be related
         */
        role?: string;
        /**
         * Boolean indicating about send email or not
         */
        send_confirmation_email?: boolean;
        /**
         * Id of source custom field type
         */
        source_custom_field_type_id?: string;
        /**
         * Details of related subject
         */
        subject?: {
          /**
           * Content of related_subject
           */
          data: {
            attributes: {
              /**
               * Roles of the subject
               */
              roles?: {
                /**
                 * Pydantic based Entity object
                 *
                 * Entity object based on pydantic and the "new way" of creating entities in
                 * our minty platform. It has the same functionality as the EntityBase object,
                 * but does not depend on it. Migrationpaths are unsure.
                 */
                data: {
                  attributes: {
                    [k: string]: any;
                  };
                  id: string;
                  links?: {
                    self?: string;
                    [k: string]: any;
                  };
                  meta?: {
                    /**
                     * Human readable summary of the content of the object
                     */
                    summary?: string;
                    [k: string]: any;
                  };
                  relationships?: {
                    [k: string]: any;
                  };
                  type: string;
                  [k: string]: any;
                };
                links: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              /**
               * Type of the subject (person/employee/organization)
               */
              type: 'person' | 'organization' | 'employee';
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        /**
         * Type is subject_relation
         */
        type?: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineEntry {
    /**
     * Entity represents properties of timeline entry
     */
    data: {
      attributes: {
        /**
         * Date of the event
         */
        date: string;
        /**
         * Description of the event
         */
        description: string;
        /**
         * Type of the the event
         */
        type: string;
        /**
         * User of the event
         */
        user: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineEntryList {
    data: {
      attributes: {
        /**
         * Date of the event
         */
        date: string;
        /**
         * Description of the event
         */
        description: string;
        /**
         * Type of the the event
         */
        type: string;
        /**
         * User of the event
         */
        user: string;
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        case?: {
          data: {
            id?: string;
            type?: string;
            [k: string]: any;
          };
          links?: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineExport {
    /**
     * Entity represents timeline export
     */
    data: {
      attributes: {
        /**
         * List of timeline entries you want to import
         */
        entries?: {
          /**
           * Entity represents properties of timeline entry
           */
          data: {
            attributes: {
              /**
               * Date of the event
               */
              date: string;
              /**
               * Description of the event
               */
              description: string;
              /**
               * Type of the the event
               */
              type: string;
              /**
               * User of the event
               */
              user: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              case?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * UUID of stored file
         */
        filestore_uuid?: string;
        /**
         * User who is trying to export the timeline
         */
        output_user?: {
          /**
           * Permssions of output user
           */
          permissions: {
            [k: string]: any;
          };
          /**
           * UUID of output user
           */
          user_uuid: string;
          [k: string]: any;
        };
        /**
         * End time filter for the timeline
         */
        period_end?: string;
        /**
         * Start time filter for the timeline
         */
        period_start?: string;
        /**
         * Type of the the object which you want to export timeline
         */
        type?: string;
        /**
         * Dictionary containing details of upload result
         */
        upload_result?: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    };
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EntityTimelineExportList {
    data: {
      attributes: {
        /**
         * List of timeline entries you want to import
         */
        entries?: {
          /**
           * Entity represents properties of timeline entry
           */
          data: {
            attributes: {
              /**
               * Date of the event
               */
              date: string;
              /**
               * Description of the event
               */
              description: string;
              /**
               * Type of the the event
               */
              type: string;
              /**
               * User of the event
               */
              user: string;
              [k: string]: any;
            };
            id: string;
            links?: {
              self?: string;
              [k: string]: any;
            };
            meta?: {
              /**
               * Human readable summary of the content of the object
               */
              summary?: string;
              [k: string]: any;
            };
            relationships?: {
              case?: {
                data: {
                  id?: string;
                  type?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            type: string;
            [k: string]: any;
          };
          links: {
            self?: string;
            [k: string]: any;
          };
          [k: string]: any;
        }[];
        /**
         * UUID of stored file
         */
        filestore_uuid?: string;
        /**
         * User who is trying to export the timeline
         */
        output_user?: {
          /**
           * Permssions of output user
           */
          permissions: {
            [k: string]: any;
          };
          /**
           * UUID of output user
           */
          user_uuid: string;
          [k: string]: any;
        };
        /**
         * End time filter for the timeline
         */
        period_end?: string;
        /**
         * Start time filter for the timeline
         */
        period_start?: string;
        /**
         * Type of the the object which you want to export timeline
         */
        type?: string;
        /**
         * Dictionary containing details of upload result
         */
        upload_result?: {
          [k: string]: any;
        };
        [k: string]: any;
      };
      id: string;
      links?: {
        self?: string;
        [k: string]: any;
      };
      meta?: {
        /**
         * Human readable summary of the content of the object
         */
        summary?: string;
        [k: string]: any;
      };
      relationships?: {
        [k: string]: any;
      };
      type: string;
      [k: string]: any;
    }[];
    links: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface Entities {
    [k: string]: any;
  }
}
