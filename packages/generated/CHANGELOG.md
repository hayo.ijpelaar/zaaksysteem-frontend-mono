# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.30.0...@zaaksysteem/generated@0.31.0) (2022-01-26)


### Bug Fixes

* **audit:** Bump node-fetch ([5cf9a94](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cf9a94b88cf774eb778636e1095b9bd82b35e10))
* **ContactView:** MINTY-7377 - Display the contact instead of the logged in user ([c8579da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c8579da1570dd6101eda0d239a123eb20aa69ec9))
* **ContactView:** MINTY-7565 - Fix cases on address view visibility and text ([e129e1a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e129e1a9d2f0dbdb788965e1a84f7c2431712a89))


### Features

* **Case:** Add relation tab with the related cases tables ([c4b3925](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c4b39253759c0c3831c61b9d287c70fc3afd061b))
* **ContactView:** MINTY-7772 - Indicate whether contact is inactive ([4d3ccd3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4d3ccd32287766cdb5f8a7cf3a17fdd35073aba7))
* **Intake:** MINTY-6443 - add option to reject document in intake ([88f50eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/88f50eb53bf3d7a7b79eac8c450c0f7480fac966))
* **ObjectForm:** MINTY-7466 - Hide system attributes ([821bfdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/821bfdf78b9d6241778212d11fdedb547b152519))
* **Timeline:** add timeline page for cases ([ca7e98c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ca7e98c5bb3dff9376cb19a397d7dc3d683ddb9a))
* **Timeline:** MINTY-5337 - add Timeline component ([5ad8858](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ad885846dfa18a48da2245ee482cd49984fac15))





# [0.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.29.1...@zaaksysteem/generated@0.30.0) (2020-12-18)


### Features

* **ObjectForm:** MINTY-5537 - Prefill case data when creating a new object ([996ee0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/996ee0f7422a81e84b04c77140b3f148af1c8908))





## [0.29.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.29.0...@zaaksysteem/generated@0.29.1) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.28.0...@zaaksysteem/generated@0.29.0) (2020-11-20)


### Features

* **ObjectForm:** MINTY-5648 - Include all case relations when updating objects ([853dab3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/853dab3cc0cad370d6fb9c8962989898029eef88))





# [0.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.27.0...@zaaksysteem/generated@0.28.0) (2020-11-18)


### Features

* **Objectform:** MINTY-5650 - Support checkbox field ([c0aeb79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0aeb79))





# [0.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.26.0...@zaaksysteem/generated@0.27.0) (2020-10-15)


### Features

* **ObjectView:** MINTY-5260 - related subjects in object overview ([3fb33fc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3fb33fc))





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.25.1...@zaaksysteem/generated@0.26.0) (2020-10-08)


### Features

* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))





## [0.25.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.25.0...@zaaksysteem/generated@0.25.1) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.24.1...@zaaksysteem/generated@0.25.0) (2020-09-03)


### Features

* **ObjectView:** MINTY-4867, MINTY-4877 - add relationships overview to Object View ([57ef51d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57ef51d))





## [0.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.24.0...@zaaksysteem/generated@0.24.1) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.23.1...@zaaksysteem/generated@0.24.0) (2020-08-27)


### Features

* **Dashboard:** MINTY-4800 - add Tasks Dashboard widget merged ([df0dffc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/df0dffc))





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.23.0...@zaaksysteem/generated@0.23.1) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.20.0...@zaaksysteem/generated@0.23.0) (2020-08-17)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.20.0...@zaaksysteem/generated@0.22.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.20.0...@zaaksysteem/generated@0.21.0) (2020-08-06)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.19.1...@zaaksysteem/generated@0.20.0) (2020-07-23)


### Features

* **Catalog:** Add support for attribute type Relation ([546907a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/546907a))





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.19.0...@zaaksysteem/generated@0.19.1) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.18.0...@zaaksysteem/generated@0.19.0) (2020-06-24)


### Features

* **Object:** MINTY-4239 - Relate created objects to the case ([a73e78e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a73e78e))





# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.17.3...@zaaksysteem/generated@0.18.0) (2020-06-11)


### Features

* **Intake:** MINTY-4179 - add document number tooltip ([9cf6c1f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cf6c1f))





## [0.17.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.17.2...@zaaksysteem/generated@0.17.3) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.17.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.17.1...@zaaksysteem/generated@0.17.2) (2020-05-19)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.17.0...@zaaksysteem/generated@0.17.1) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.16.0...@zaaksysteem/generated@0.17.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.15.3...@zaaksysteem/generated@0.16.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.15.2...@zaaksysteem/generated@0.15.3) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.15.1...@zaaksysteem/generated@0.15.2) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.15.0...@zaaksysteem/generated@0.15.1) (2020-04-28)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.14.0...@zaaksysteem/generated@0.15.0) (2020-04-20)


### Features

* **CommunicationModule:** MINTY-3790 Send filename when creating external message ([b72104d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b72104d))





# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.13.1...@zaaksysteem/generated@0.14.0) (2020-04-16)


### Features

* **Communication:** MINTY-3645 - change endpoint Email to document ([27c1d6a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27c1d6a))





## [0.13.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.13.0...@zaaksysteem/generated@0.13.1) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.12.0...@zaaksysteem/generated@0.13.0) (2020-04-02)


### Features

* **Communication:** MINTY-3411 - save Emails to PDF in Documents ([08046ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/08046ce))
* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.11.0...@zaaksysteem/generated@0.12.0) (2020-04-01)


### Features

* **ObjectView:** Implement first iteration of Object View ([daa2da5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/daa2da5))





# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.2...@zaaksysteem/generated@0.11.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3519 Implement add object type action ([a1df778](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a1df778))





## [0.10.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.1...@zaaksysteem/generated@0.10.2) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.10.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.10.0...@zaaksysteem/generated@0.10.1) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.9.0...@zaaksysteem/generated@0.10.0) (2020-03-19)


### Features

* **Catalog:** MINTY-3344 - Add support for custom object types ([55ee940](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/55ee940))





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.8.1...@zaaksysteem/generated@0.9.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))





## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.8.0...@zaaksysteem/generated@0.8.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/generated





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.3...@zaaksysteem/generated@0.8.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2882 - enable download link ([09f2ce5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/09f2ce5))
* **CaseDocuments:** MINTY-2884 - add search functionality ([f91aea4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f91aea4))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))





## [0.7.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.2...@zaaksysteem/generated@0.7.3) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.1...@zaaksysteem/generated@0.7.2) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/generated





## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.7.0...@zaaksysteem/generated@0.7.1) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/generated

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.6.1...@zaaksysteem/generated@0.7.0) (2020-02-06)

### Features

- **Communication:** MINTY-2955 Expand unread messages in thread and mark unread messages as read ([f157214](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f157214))
- **Communication:** MINTY-2955 Show read/unread status of thread messages ([9e8ec85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9e8ec85))

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.6.0...@zaaksysteem/generated@0.6.1) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/generated

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.5.0...@zaaksysteem/generated@0.6.0) (2020-01-22)

### Features

- **MessageForm:** Replace upload ability with ability to select case documents in case context ([e1d1f92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1d1f92))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.4.0...@zaaksysteem/generated@0.5.0) (2020-01-10)

### Features

- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.3.0...@zaaksysteem/generated@0.4.0) (2020-01-09)

### Bug Fixes

- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **Tasks:** MINTY-2822 - fix displayname + typing issues ([6f5c722](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f5c722))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/generated@0.2.0...@zaaksysteem/generated@0.3.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **Thread:** Fix search case API to new version as filter-status ([85aff79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85aff79))

### Features

- **AddThread:** Filter out resolved cases ([7f31864](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f31864))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))

# 0.2.0 (2019-10-17)

### Bug Fixes

- add empty attachments to save, remove Document domain from generated ([8cbda28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8cbda28))

### Features

- **GeneratedTypes:** Generated types are now stored in git and manually regenerated ([e6a96b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e6a96b7))
- **GeneratedTypes:** Throw error when generation fails and disable Document schema since it's not being used and is causing conflicts ([80e5242](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/80e5242))
