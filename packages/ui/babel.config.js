// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-typescript',
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-syntax-object-rest-spread',
    'babel-plugin-syntax-dynamic-import',
    'transform-class-properties',
    '@babel/plugin-proposal-optional-chaining',
  ],
};
