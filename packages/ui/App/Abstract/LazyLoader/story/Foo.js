// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';

const Foo = ({ title }) => <h1>{title}</h1>;

export default Foo;
