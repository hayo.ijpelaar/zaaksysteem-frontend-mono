# 🔌 `Clone` component

> Use a *JSX* component instead of (yet) an(other) expression 
  to clone properties to a component’s first child.

## Example

### Expression 

    <div>
      {children.map(child =>
        cloneElement(child, {
          foo: 'foo',
          bar: 'bar',
        }))}
    </div>

### `Clone` component

    import { Clone } from '@mintlab/ui';

    <div>
      <Clone 
        foo="foo"
        bar="bar"
      >
        <SomeComponent/>
      </Clone>
    </div>

## See also

- [`Clone` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Clone)
- [`Clone` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-clone)
