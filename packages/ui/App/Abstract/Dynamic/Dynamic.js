// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createElement } from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';

/**
 * Pass a component dynamically with a property.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Dynamic
 * @see /npm-mintlab-ui/documentation/consumer/manual/Dynamic.html
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const Dynamic = props => {
  const { component } = props;
  const newProps = cloneWithout(props, 'component');

  return createElement(component, newProps);
};

export default Dynamic;
