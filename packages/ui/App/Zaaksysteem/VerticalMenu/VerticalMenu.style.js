// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the {@link VerticalMenu} component.
 * @return {JSS}
 */
export const VerticalMenuStylesheet = () => ({
  menu: {
    display: 'flex',
    'align-items': 'flex-start',
    'flex-direction': 'column',
    '& >div:not(:first-child)': {
      paddingTop: '22px',
    },
    overflow: 'auto',
  },
});
