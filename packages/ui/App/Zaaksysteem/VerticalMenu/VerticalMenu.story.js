// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories } from '../../story';
import VerticalMenu from '.';

stories(module, __dirname, {
  Default() {
    const data = [{ label: 'Foo' }, { label: 'Bar' }];

    return <VerticalMenu items={data} scope="story" />;
  },
  Icons() {
    const data = [
      {
        icon: 'menu',
        label: 'Foo',
        active: true,
      },
      {
        icon: 'star',
        label: 'Bar',
      },
      {
        icon: 'home',
        label: 'Baz',
      },
    ];

    return <VerticalMenu items={data} scope="story" />;
  },
});
