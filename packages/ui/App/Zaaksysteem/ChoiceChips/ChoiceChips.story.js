// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, boolean } from '../../story';
import Icon from '../../Material/Icon';
import ChoiceChips from '.';

const stores = {
  Default: {
    currentValue: [],
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, currentValue }) {
      const withIcons = boolean('Show icons', true);
      const choices = [
        {
          label: 'Folder',
          value: 'folder',
          icon: <Icon size="small">folder</Icon>,
        },
        {
          label: 'User',
          value: 'user',
          icon: <Icon size="small">access_time</Icon>,
        },
        {
          label: 'Account',
          value: 'account',
          icon: <Icon size="small">account_circle</Icon>,
        },
      ];

      return (
        <ChoiceChips
          choices={choices.map(({ label, value, icon }) =>
            withIcons ? { label, value, icon } : { label, value }
          )}
          value={currentValue}
          onChange={event =>
            store.set({
              currentValue: event.target.value,
            })
          }
          multiSelect={boolean('Multi select', false)}
        />
      );
    },
  },
  stores
);
