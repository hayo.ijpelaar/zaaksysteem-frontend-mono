# 🔌 `Select` component

> Facade for *React Select v2*.

Takes a Form Field definition as props and renders a React Select variant. 
If provided, the `choices` prop must be an array of objects:

    [{
      value: 1,
      label: 'Test item label 1',
    },
    {
      value: 2,
      label: 'Test item label 2',
    }]

or a single object.

## Variants

### Form

The `creatable` prop is `false` (default value).

If `config.getChoices` is provided, it will be called with 
the input box's value after the user has typed something
in the input field, to facilitate autocomplete behaviour.

### Generic

The `generic` prop is `true`.

Shares most of the behaviour and props of the Form variant, but has a more generic styling.
A `startAdornment` node may be provided.

### Creatable

The `creatable` props is `true`.

This behaves as a textfield where values can be added and removed.

#### Details

- Form and Generic share a base class (`SelectBase`)
- Form and Creatable share a stylesheet (Material UI look)

## See also

- [`Select` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Select)
- [`Select` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-select)

## External resources

- [React Select v2](https://react-select.com/home)
