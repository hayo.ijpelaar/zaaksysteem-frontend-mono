// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const wysiwygStyleSheet = ({
  palette: { error },
  typography: { fontFamily },
  mintlab: { radius },
}) => ({
  defaultClass: {
    backgroundColor: 'transparent',
    '& .rdw-option-wrapper': {
      backgroundColor: 'transparent',
    },
    borderRadius: radius.wysiwyg,
    fontFamily,
  },
  errorClass: {
    backgroundColor: error.light,
    '& .rdw-option-wrapper': {
      backgroundColor: error.light,
    },
  },
  editorClass: {
    padding: '10px',
  },
});
