// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable no-magic-numbers */
import { iterator } from './iterator';

/**
 * @test {iterator}
 */
test('iterator', () => {
  expect(iterator(5)).toEqual([1, 2, 3, 4, 5]);
});
