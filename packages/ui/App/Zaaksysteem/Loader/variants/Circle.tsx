// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { addScopeAttribute } from '../../../library/addScope';
import { iterator } from '../library/iterator';
import { LoaderVariantComponentType } from './LoaderVariantComponentType';
import { useCircleLoaderStyle, LENGTH } from './CircleLoader.style';

export const Circle: LoaderVariantComponentType = ({
  color,
  scope,
  className,
}) => {
  const classes = useCircleLoaderStyle();

  return (
    <div
      className={classnames(classes.circle, className)}
      {...addScopeAttribute(scope)}
    >
      {iterator(LENGTH).map(item => (
        <div key={item} className={classes.child}>
          <span
            className={classes.dot}
            style={{
              backgroundColor: color,
            }}
          />
        </div>
      ))}
    </div>
  );
};
