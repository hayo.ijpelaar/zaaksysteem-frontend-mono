// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const LENGTH = 12;

const childFrames = Array(LENGTH - 1)
  .fill(null)
  .reduce(
    (acc, __, index) => ({
      ...acc,
      [`&:nth-child(${index + 2})`]: {
        transform: `rotate(${(index + 1) * 30}deg)`,
        '&>span': { animationDelay: -1100 + index * 100 },
      },
    }),
    {}
  );

export const useCircleLoaderStyle = makeStyles((theme: Theme) => ({
  '@keyframes circleBounceDelay': {
    '0%, 80%, 100%': {
      transform: 'scale(0)',
    },
    '40%': {
      transform: 'scale(1)',
    },
  },
  circle: {
    margin: '40px auto',
    width: 40,
    height: 40,
    position: 'relative',
  },
  child: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
    ...childFrames,
  },
  dot: {
    constent: '',
    display: 'block',
    width: '15%',
    height: '15%',
    backgroundColor: '#333',
    borderRadius: '100%',
    animation: '$circleBounceDelay 1.2s infinite ease-in-out both',
  },
}));
