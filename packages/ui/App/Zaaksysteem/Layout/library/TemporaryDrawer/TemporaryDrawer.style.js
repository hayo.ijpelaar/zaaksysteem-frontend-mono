// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the TemporaryDrawer component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const temporaryDrawerStyleSheet = ({
  mintlab: { greyscale, shadows },
  palette: { primary },
  typography,
}) => ({
  paper: {
    minWidth: '16rem',
    'justify-content': 'space-between',
    boxShadow: shadows.medium,
  },
  titles: {
    padding: '14px 18px',
  },
  title: {
    marginBottom: '4px',
  },
  subtitle: {
    color: greyscale.evenDarker,
    fontFamily: typography.fontFamily,
    fontWeight: typography.fontWeightLight,
    fontSize: '10px',
  },
  primary: {
    flex: 'auto',
  },
  about: {
    'justify-content': 'flex-start',
    color: greyscale.evenDarker,
    margin: '16px 20px',
    '&:hover': {
      color: primary.main,
    },
  },
});
