// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Icon from '../../../../Material/Icon/Icon';
import { Body2 } from '../../../../Material/Typography';
import { addScopeAttribute } from '../../../../library/addScope';
import { menuGroupStylesheet } from './MenuGroup.style';

/**
 * @param {Array} navigation
 * @param {Object} classes
 * @param {number} active
 * @return {ReactElement}
 */
export const MenuGroup = ({ navigation, classes, active, scope }) => (
  <MenuList
    component="nav"
    classes={{
      root: classes.menuList,
    }}
    {...addScopeAttribute(scope, 'menu')}
  >
    {navigation.map(({ action, href, icon, label, target }, index) => (
      <MenuItem
        key={index}
        classes={{
          root: classNames(classes.root, {
            [classes.active]: active === index,
          }),
        }}
        component="a"
        href={href}
        target={target}
        onClick={
          action &&
          (event =>
            preventDefaultAndCall(() => {
              action();
            })(event))
        }
        {...addScopeAttribute(scope, 'menu', label, 'item')}
      >
        <ListItemIcon
          classes={{
            root: classes.inherit,
          }}
        >
          <Icon
            size="small"
            classes={{
              root: classes.inherit,
            }}
          >
            {icon}
          </Icon>
        </ListItemIcon>
        <ListItemText disableTypography={true}>
          <Body2
            classes={{
              root: classes.inherit,
            }}
          >
            {label}
          </Body2>
        </ListItemText>
      </MenuItem>
    ))}
  </MenuList>
);

export default withStyles(menuGroupStylesheet)(MenuGroup);
