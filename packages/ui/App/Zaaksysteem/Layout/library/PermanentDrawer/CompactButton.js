// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import ButtonBase from '@material-ui/core/ButtonBase';
import { withStyles } from '@material-ui/styles';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Icon from '../../../../Material/Icon/Icon';
import { Caption } from '../../../../Material/Typography';
import { addScopeAttribute } from '../../../../library/addScope';
import { compactButtonStyleSheet } from './CompactButton.style';

export const CompactButton = ({
  action,
  classes,
  icon,
  label,
  href,
  target,
  active = false,
  scope,
}) => (
  <ButtonBase
    classes={{
      root: classNames(classes.root, { [classes.active]: active }),
    }}
    onClick={
      action &&
      (event =>
        preventDefaultAndCall(() => {
          action();
        })(event))
    }
    component="a"
    href={href}
    target={target}
    {...addScopeAttribute(scope, 'button')}
  >
    <Icon size="small">{icon}</Icon>

    <Caption
      classes={{
        root: classes.label,
      }}
    >
      {label}
    </Caption>
  </ButtonBase>
);

export default withStyles(compactButtonStyleSheet)(CompactButton);
