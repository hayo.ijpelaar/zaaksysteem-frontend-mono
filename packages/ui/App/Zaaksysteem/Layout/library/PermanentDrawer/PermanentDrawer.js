// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/styles';
import { addScopeProp } from '../../../../library/addScope';
import { permanentDrawerStyleSheet } from './PermanentDrawer.style';
import CompactButton from './CompactButton';

const ACTIVE_DEFAULT = 0;

/**
 * @param {number} [active=0]
 * @param {Object} classes
 * @param {string} className
 * @param {Array} navigation
 * @return {ReactElement}
 */
export const PermanentDrawer = ({
  active = ACTIVE_DEFAULT,
  classes,
  className,
  navigation: { primary, secondary },
  scope,
}) => (
  <Drawer
    classes={{
      paper: classes.paper,
    }}
    className={className}
    variant="permanent"
  >
    <nav>
      <ul className={classes.list}>
        {primary.map(({ action, icon, label, href, target }, index) => (
          <li key={index}>
            <CompactButton
              action={() => action(label)}
              active={index === active}
              icon={icon}
              href={href}
              label={label}
              target={target}
              {...addScopeProp(scope, label)}
            />
          </li>
        ))}
      </ul>
    </nav>
    <nav>
      <ul className={classes.list}>
        {secondary.map(({ icon, label, href, target }, index) => (
          <li key={index}>
            <CompactButton
              href={href}
              icon={icon}
              label={label}
              target={target}
              {...addScopeProp(scope, label)}
            />
          </li>
        ))}
      </ul>
    </nav>
  </Drawer>
);

export default withStyles(permanentDrawerStyleSheet)(PermanentDrawer);
