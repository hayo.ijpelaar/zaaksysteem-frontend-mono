// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Note: do not set any colors here, this component must inherit its parents colors. *
 *
 * @return {JSS}
 */
export const closeIndicatorStylesheet = () => ({
  root: {
    padding: '6px',
  },
});
