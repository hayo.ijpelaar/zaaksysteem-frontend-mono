// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useBreadcrumbStyles = makeStyles(
  ({ palette: { common }, typography }: any) => {
    const item = {
      ...typography.h6,
      color: common.black,
      opacity: 0.4,
    };

    return {
      item,
      separator: {
        fill: common.black,
        opacity: 0.4,
      },
      link: {
        ...item,
        textDecoration: 'none',
        '&:hover': {
          opacity: 1,
        },
        cursor: 'pointer',
      },
      last: {
        ...typography.h6,
        color: common.black,
        textDecoration: 'none',
      },
    };
  }
);
