# 🔌 `Checkbox` component

> *Material Design* **Checkbox** selection control.

## See also

- [`Checkbox` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Checkbox)
- [`Checkbox` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-checkbox)

## External resources

- [*Material Guidelines*: Checkboxes](https://material.io/design/components/selection-controls.html#checkboxes)
- [*Material-UI* `Checkbox` API](https://material-ui.com/api/checkbox/)
