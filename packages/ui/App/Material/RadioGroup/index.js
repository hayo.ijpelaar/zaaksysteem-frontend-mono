// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as RadioGroup } from './RadioGroup';
export * from './RadioGroup';
export default RadioGroup;
