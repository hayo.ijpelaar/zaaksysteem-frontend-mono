// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as IconRounded } from './IconRounded';
export * from './IconRounded';
export default IconRounded;
