// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const switchStylesheet = ({
  palette: { common },
  mintlab: { greyscale },
}) => ({
  switchBase: {
    '&&$checked + span': {
      opacity: 1,
    },
  },
  checked: {
    transform: 'translateX(18px)',
  },
  bar: {
    top: '13px',
    left: '13px',
    borderRadius: '20px',
    width: '40px',
    height: '22px',
    margin: 0,
    backgroundColor: greyscale.darker,
    opacity: 1,
  },
  icon: {
    width: '16px',
    height: '16px',
  },
  iconChecked: {
    color: common.white,
  },
});
