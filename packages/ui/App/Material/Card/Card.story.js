// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text } from '../../story';
import Card from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Card
        title={text('Title', 'Card title')}
        description={text(
          'Description',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        )}
        scope="story"
      >
        {text(
          'Content',
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam et tortor felis. Nunc vel gravida turpis, mattis venenatis nibh. Praesent sapien arcu, laoreet in varius at, consequat eu elit. Morbi magna nunc, luctus eu tortor sed, mollis cursus urna. Vivamus placerat odio non bibendum molestie. Fusce molestie rutrum felis at sodales. Curabitur sollicitudin, nibh eu fermentum tempus, libero turpis condimentum sem, quis efficitur urna est ullamcorper mauris. Pellentesque a libero nec tortor rhoncus tristique. Fusce pellentesque venenatis erat. Quisque in rutrum nunc. Nunc bibendum est a viverra iaculis.'
        )}
      </Card>
    );
  },
  Multiple() {
    return (
      <div>
        <Card
          title={text('Title1', 'Card 1')}
          description={text('Description1', 'Pangram #1.')}
        >
          {text('Content1', 'The quick brown fox jumps over the lazy dog.')}
        </Card>
        <Card
          title={text('Title2', 'Card 2')}
          description={text('Description2', 'Pangram #2')}
        >
          {text('Content2', 'Pack my box with five dozen liquor jugs.')}
        </Card>
      </div>
    );
  },
});
