// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiSnackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/styles';
//@tg-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import IconButton from '@material-ui/core/IconButton';
import { addScopeAttribute } from '../../library/addScope';
import Icon from '../Icon/Icon';
import { snackbarStyleSheet } from './Snackbar.style';

const SnackbarCmp = props => {
  const { classes, handleClose, handleExited, closeLabel, scope, open } = props;

  return (
    <MuiSnackbar
      ContentProps={{
        classes: {
          root: classes.root,
          message: classes.message,
        },
      }}
      onClose={handleClose}
      onExited={handleExited}
      open={open}
      action={[
        <IconButton
          key="close"
          aria-label={closeLabel}
          color="inherit"
          className="close"
          onClick={handleClose}
          classes={{
            root: classes.iconButton,
          }}
        >
          <Icon size="small">close</Icon>
        </IconButton>,
      ]}
      {...cloneWithout(
        props,
        'onQueueEmpty',
        'onClose',
        'onExited',
        'handleClose',
        'handleExited',
        'open',
        'action',
        'classes',
        'scope'
      )}
      {...addScopeAttribute(scope, 'snackbar')}
    />
  );
};

export default withStyles(snackbarStyleSheet)(SnackbarCmp);
