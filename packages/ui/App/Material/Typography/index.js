// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { H1 } from './Scale/H1';
export { H2 } from './Scale/H2';
export { H3 } from './Scale/H3';
export { H4 } from './Scale/H4';
export { H5 } from './Scale/H5';
export { H6 } from './Scale/H6';
export { Subtitle1 } from './Scale/Subtitle1';
export { Subtitle2 } from './Scale/Subtitle2';
export { Body1 } from './Scale/Body1';
export { Body2 } from './Scale/Body2';
// This Button intentionally left blank
export { Caption } from './Scale/Caption';
export { Overline } from './Scale/Overline';
