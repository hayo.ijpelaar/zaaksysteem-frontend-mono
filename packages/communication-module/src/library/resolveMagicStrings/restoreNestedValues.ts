// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormValue,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FlatFormValues } from './resolveMagicStrings.types';
import { mapFieldNameToInternal } from './mapFieldNames';

/**
 * Reconstruct array value from flat string
 *
 * Example:
 * '[Foo|bar]' => ['Foo|bar']
 */
const restoreArrayValue = (value: string): FormValue[] | FormValue => {
  return value.charAt(0) === '['
    ? value.substr(1, value.length - 2).split(',')
    : value;
};

/**
 * Restore nested object values
 *
 * Example:
 * 'Foo|bar' => { label: 'Foo', value: 'bar' }
 */
const restoreObjectValue = (value: FormValue): FormValue => {
  const [nestedLabel, nestedValue] = value.toString().split('|');
  const restoredValue =
    typeof nestedValue !== 'undefined'
      ? {
          label: nestedLabel,
          value: nestedValue,
        }
      : value;

  return restoredValue;
};

const containsValue = (value: FormValue): boolean => {
  return typeof (value as NestedFormValue).value !== 'undefined'
    ? Boolean((value as NestedFormValue).value)
    : Boolean(value);
};

const restoreNestedValue = (value: string): FormValue[] | FormValue => {
  const arrayOrFlatValue = restoreArrayValue(value);

  return Array.isArray(arrayOrFlatValue)
    ? arrayOrFlatValue.map(restoreObjectValue).filter(containsValue)
    : restoreObjectValue(arrayOrFlatValue);
};

/**
 * Restore nested object structure.
 *
 * Note: All empty values are filtered
 */
export function restoreNestedValues<FormShape>(
  values: Partial<FlatFormValues<FormShape>>
): Partial<FormShape> {
  return Object.entries(values).reduce<Partial<FormShape>>((acc, entry) => {
    const [name, value] = entry as [string, string];
    const fieldName = mapFieldNameToInternal(name);
    return {
      ...acc,
      [fieldName]: restoreNestedValue(value),
    };
  }, {});
}
