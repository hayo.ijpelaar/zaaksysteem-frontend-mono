// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { communication } from './communication.reducer';
import { communicationMiddleware } from './communication.middleware';
import {
  setCommunicationContext,
  CommunicationSetContextActionPayload,
} from './context/communication.context.actions';

export function getCommunicationModule(
  context: CommunicationSetContextActionPayload
) {
  return {
    id: 'communication',
    reducerMap: {
      communication,
    },
    initialActions: [setCommunicationContext(context)],
    middlewares: [communicationMiddleware],
  };
}
