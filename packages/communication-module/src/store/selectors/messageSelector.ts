// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';

export const messageSelector =
  (messageUuid: string) => (state: CommunicationRootStateType) => {
    const {
      communication: {
        thread: { messages },
      },
    } = state;

    const message = (messages || []).find(
      message => message.id === messageUuid
    );

    return message;
  };
