// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICommunication } from '@zaaksysteem/generated';
import {
  SAVE_COMMUNICATION,
  SET_SAVE_COMMUNICATION_PENDING,
} from './communication.add.constants';
import { SaveContactMomentPayloadType } from './communication.add.actions';

export interface CommunicationAddState {
  state: AjaxState;
}

const initialState: CommunicationAddState = {
  state: AJAX_STATE_INIT,
};

export const add: Reducer<
  CommunicationAddState,
  AjaxAction<
    APICommunication.CreateContactMomentResponseBody,
    SaveContactMomentPayloadType
  >
> = (state = initialState, action) => {
  const { type } = action;

  const handleSaveAjaxStateChange = handleAjaxStateChange(SAVE_COMMUNICATION);

  switch (type) {
    case SAVE_COMMUNICATION.PENDING:
    case SAVE_COMMUNICATION.ERROR:
    case SAVE_COMMUNICATION.SUCCESS:
      return handleSaveAjaxStateChange(state, action);

    case SET_SAVE_COMMUNICATION_PENDING:
      return {
        ...state,
        state: 'pending',
      };
    default:
      return state;
  }
};

export default add;
