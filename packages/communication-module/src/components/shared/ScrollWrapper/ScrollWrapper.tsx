// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useScrollWrapperStyle } from './ScrollWrapper.style';

/* eslint-disable complexity */
const ScrollWrapper: React.FunctionComponent<{}> = ({ children }) => {
  const classes = useScrollWrapperStyle();
  return <div className={classes.root}>{children}</div>;
};

export default ScrollWrapper;
