// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import DropdownMenu from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/DropdownMenu';
//@ts-ignore
import { DropdownMenuList } from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/DropdownMenu';

type ActionConditionFunctionType = () => boolean;

export type ActionType = {
  action: () => void;
  label: string;
  condition?: boolean | ActionConditionFunctionType;
};

export type ActionGroupType = ActionType[];
export type ActionGroupsType = ActionGroupType[];

type ActionMenuPropsType = {
  actions: ActionGroupsType | ActionGroupType;
  scope: string;
};

const checkConditionOrTrue = (action: ActionType): boolean => {
  const { condition } = action;

  if (typeof condition === 'undefined') {
    return true;
  }

  return typeof condition === 'function' ? condition() : condition;
};

const isGroupedActions = (
  actions: ActionGroupType | ActionGroupsType
): actions is ActionGroupsType =>
  Boolean(actions.length) && Array.isArray(actions[0]);

const filterConditionalActions = (
  actions: ActionGroupType | ActionGroupsType
): ActionGroupsType | ActionGroupType => {
  return isGroupedActions(actions)
    ? actions.map(group => group.filter(checkConditionOrTrue))
    : actions.filter(checkConditionOrTrue);
};

const containsActions = (
  actions: ActionGroupType | ActionGroupsType
): boolean => {
  return isGroupedActions(actions)
    ? actions.some(group => group.length > 0)
    : actions.length > 0;
};

const ActionMenu: React.FunctionComponent<ActionMenuPropsType> = ({
  actions,
  scope,
}) => {
  const conditionalActions = filterConditionalActions(actions);

  return containsActions(conditionalActions) ? (
    <DropdownMenu
      trigger={
        <Button presets={['icon']} scope={`${scope}:actions`}>
          more_vert
        </Button>
      }
    >
      <DropdownMenuList items={conditionalActions} scope={scope} />
    </DropdownMenu>
  ) : null;
};

export default ActionMenu;
