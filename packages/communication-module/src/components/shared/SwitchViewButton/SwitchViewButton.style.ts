// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useSwitchViewButtonStyle = makeStyles(
  ({ mintlab: { shadows }, breakpoints }: any) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'flex-start',
      width: '100%',
      flex: '0 0 auto',
      padding: 10,
      paddingBottom: 9,
      boxShadow: shadows.flat,
      zIndex: 1,
      [breakpoints.up('sm')]: {
        padding: 20,
      },
    },
  })
);
