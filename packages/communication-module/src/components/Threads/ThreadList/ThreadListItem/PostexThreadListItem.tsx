// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem, { ThreadListItemPropsType } from './ThreadListItem';
import ThreadTypeIcon from './ThreadTypeIcon/ThreadTypeIcon';

type PostexThreadListItemPropsType = {
  createdByName: string;
  type: string;
  subject: string;
  messageCount: number;
  failureReason: string;
} & Omit<ThreadListItemPropsType, 'title' | 'tag' | 'icon' | 'subTitle'>;

const PostexThreadListItem: React.FunctionComponent<
  PostexThreadListItemPropsType
> = ({
  createdByName,
  type,
  subject,
  messageCount,
  failureReason,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.postex.title', {
    createdByName,
  });
  const tag = t(`thread.tags.${type}`);
  const iconType =
    messageCount > 1
      ? 'threadType.inverted.postexThread'
      : 'threadType.inverted.postex';

  return (
    <ThreadListItem
      {...rest}
      title={title}
      subTitle={subject}
      typeTag={tag}
      {...addScopeProp('thread', 'postex')}
      icon={
        <ThreadTypeIcon
          type={failureReason ? 'threadType.inverted.failed' : iconType}
        />
      }
      isFailed={Boolean(failureReason)}
    />
  );
};

export default PostexThreadListItem;
