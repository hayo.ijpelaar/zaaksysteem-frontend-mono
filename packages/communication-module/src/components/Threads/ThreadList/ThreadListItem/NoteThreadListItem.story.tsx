// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { boolean } from '@mintlab/ui/App/story';
import { stories } from '../../../../story';
import NoteThreadListItem from './NoteThreadListItem';

const Wrapper: React.ComponentType = ({ children }) => (
  <div style={{ maxWidth: 400 }}>{children}</div>
);

const getProps = () => ({
  createdByName: 'Foo bar',
  date: new Date(),
  id: '1',
  isUnread: boolean('Unread', false),
  selected: boolean('Selected', false),
  showLinkToCase: false,
  type: 'note',
  summary: 'Lorum ipsum dolar sit amet',
  style: {},
  rootPath: '',
  numberOfMessages: 0,
});

stories(module, `${__dirname}/NoteThreadListItem`, {
  Default() {
    return (
      <Wrapper>
        <NoteThreadListItem {...getProps()} />
      </Wrapper>
    );
  },

  Selected() {
    const selectedProps = {
      ...getProps(),
      selected: true,
    };
    return (
      <Wrapper>
        <NoteThreadListItem {...selectedProps} />
      </Wrapper>
    );
  },
});
