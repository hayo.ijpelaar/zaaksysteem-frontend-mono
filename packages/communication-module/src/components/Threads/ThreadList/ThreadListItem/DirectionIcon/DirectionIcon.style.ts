// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useDirectionIconStyleSheet = makeStyles({
  root: {
    marginRight: 6,
  },
  incoming: {
    transform: 'rotate(90deg)',
  },
  outgoing: {
    transform: 'rotate(-45deg)',
  },
});
