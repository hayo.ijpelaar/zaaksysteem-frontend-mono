// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { RoleAndSubjectType } from '@zaaksysteem/common/src/components/form/fields/CaseRoleFinder/CaseRoleFinder.types';

export const caseRoleValueResolve = ({ subject }: RoleAndSubjectType) => {
  /* Todo: This is a temporary work-arround because the API spec is not properly
   * implemented yet. Remove when MINTY-2950 is resolved */
  return subject &&
    subject.attributes &&
    subject.attributes.contact_information &&
    subject.attributes.contact_information.email
    ? subject.attributes.contact_information.email
    : '';
};
