// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { EmailRecipientFormValuesType } from './EmailRecipient.types';
import {
  getRecipientTypeField,
  otherField,
  colleagueField,
  getRoleField,
  getAuthorizedField,
  getRequestorField,
} from './library/fields';
import { EmailRecipientConfigType } from './EmailRecipient';

const getFormDefinition = ({
  caseUuid,
  selectedRecipientType,
}: EmailRecipientConfigType): FormDefinition<EmailRecipientFormValuesType> => [
  getRecipientTypeField(selectedRecipientType),
  otherField,
  colleagueField,
  ...(caseUuid
    ? [
        getRoleField(caseUuid),
        getAuthorizedField(caseUuid),
        getRequestorField(caseUuid),
      ]
    : []),
];

export default getFormDefinition;
