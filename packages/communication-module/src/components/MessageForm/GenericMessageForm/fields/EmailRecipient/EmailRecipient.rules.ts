// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  Rule,
  valueEquals,
  showFields,
  hideFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { EmailRecipientFormValuesType } from './EmailRecipient.types';

export default [
  new Rule<EmailRecipientFormValuesType>()
    .when('recipient_type', valueEquals('other'))
    .then(showFields(['other']))
    .else(hideFields(['other'])),

  new Rule<EmailRecipientFormValuesType>()
    .when('recipient_type', valueEquals('colleague'))
    .then(showFields(['colleague']))
    .else(hideFields(['colleague'])),

  new Rule<EmailRecipientFormValuesType>()
    .when('recipient_type', valueEquals('role'))
    .then(showFields(['role']))
    .else(hideFields(['role'])),

  new Rule<EmailRecipientFormValuesType>()
    .when('recipient_type', valueEquals('authorized'))
    .then(showFields(['authorized']))
    .else(hideFields(['authorized'])),

  new Rule<EmailRecipientFormValuesType>()
    .when('recipient_type', valueEquals('requestor'))
    .then(showFields(['requestor']))
    .else(hideFields(['requestor'])),
];
