// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  FormRendererFormField,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { resolveMagicStrings } from '../../../library/resolveMagicStrings';
import { EmailTemplateDataType } from '../../../types/EmailIntegration.types';
import { useGenericMessageFormStyle } from './GenericMessageForm.style';
import { renderPreviewField } from './library/renderPreviewField';
import { GenericMessageFormPropsType } from './GenericMessageForm.types';
import { parseHtmlEmailTemplate } from './library/parseHtmlEmailTemplate';

interface GenericMessageFormPreviewPropsType<Values = any>
  extends Pick<GenericMessageFormPropsType, 'mapPreviewValues' | 'formName'> {
  fields: FormRendererFormField<Values>[];
  values: FormValuesType<Values>;
  caseUuid: string;
  emailTemplateData?: EmailTemplateDataType;
}

export function GenericMessageFormPreview<Values = any>({
  formName,
  values,
  fields,
  caseUuid,
  mapPreviewValues,
  emailTemplateData,
}: React.PropsWithChildren<
  GenericMessageFormPreviewPropsType<Values>
>): React.ReactElement {
  const classes = useGenericMessageFormStyle();
  const [resolvedValues, setResolvedValues] =
    useState<FormValuesType<Values> | null>(null);

  useEffect(() => {
    async function getPreviewValues() {
      const resolvedValues = await resolveMagicStrings(values, caseUuid);
      const mappedValues = mapPreviewValues
        ? mapPreviewValues(resolvedValues)
        : resolvedValues;
      const parsedValues = emailTemplateData
        ? parseHtmlEmailTemplate(mappedValues, emailTemplateData)
        : mappedValues;
      setResolvedValues(parsedValues);
    }

    getPreviewValues();
  }, []);

  if (!resolvedValues) {
    return <Loader />;
  }

  const previewFields = fields.map(item => ({
    ...item,
    disabled: true,
    value: resolvedValues[item.name],
  }));

  return (
    <React.Fragment>
      {previewFields.map(
        renderPreviewField<Values>({ classes, formName, emailTemplateData })
      )}
    </React.Fragment>
  );
}

export default GenericMessageFormPreview;
