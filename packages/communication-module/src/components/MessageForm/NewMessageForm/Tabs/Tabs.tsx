// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import MUITabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
//@ts-ignore
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_EXTERNAL_MESSAGE,
} from '../../../../library/communicationTypes.constants';
import { useTabsStyle } from './Tabs.style';

export type TabsPropsType = {
  type: string;
  rootPath: string;
  canCreateContactMoment: boolean;
  canCreateNote: boolean;
  canCreateMessage: boolean;
};

const Tabs: React.FunctionComponent<TabsPropsType> = ({
  type,
  rootPath,
  canCreateContactMoment,
  canCreateNote,
  canCreateMessage,
}) => {
  const [t] = useTranslation('communication');
  const classes = useTabsStyle();
  const tabClasses = {
    root: classes.tabRoot,
    wrapper: classes.tabWrapper,
    selected: classes.selected,
  };

  type LinkComponentPropsType = {
    linktype: string;
  };

  const LinkComponent = React.forwardRef<
    HTMLAnchorElement,
    LinkComponentPropsType
  >((props, ref) => (
    <Link innerRef={ref} to={`${rootPath}/new/${props.linktype}`} {...props} />
  ));
  LinkComponent.displayName = 'TabLink';

  if (!canCreateContactMoment && !canCreateNote) return null;

  return (
    <React.Fragment>
      <MUITabs
        classes={{
          root: classes.tabsRoot,
          flexContainer: classes.tabsFlexContainer,
        }}
        TabIndicatorProps={{
          className: classes.tabIndicator,
        }}
        value={type}
        variant="fullWidth"
        indicatorColor="primary"
      >
        {canCreateContactMoment && (
          <Tab
            label={t('threadTypes.contact_moment')}
            value={TYPE_CONTACT_MOMENT}
            linktype={TYPE_CONTACT_MOMENT}
            classes={tabClasses}
            icon={<Icon size="small">{iconNames.chat_bubble}</Icon>}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', TYPE_CONTACT_MOMENT)}
          />
        )}
        {canCreateNote && (
          <Tab
            label={t('threadTypes.note')}
            value={TYPE_NOTE}
            linktype={TYPE_NOTE}
            icon={<Icon size="small">{iconNames.sort}</Icon>}
            classes={tabClasses}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', TYPE_NOTE)}
          />
        )}

        {canCreateMessage && (
          <Tab
            label={t('communication:threadTypes.message')}
            value={TYPE_EXTERNAL_MESSAGE}
            linktype={TYPE_EXTERNAL_MESSAGE}
            icon={<Icon size="small">{iconNames.email}</Icon>}
            classes={tabClasses}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', TYPE_EXTERNAL_MESSAGE)}
          />
        )}
      </MUITabs>
    </React.Fragment>
  );
};

export default Tabs;
