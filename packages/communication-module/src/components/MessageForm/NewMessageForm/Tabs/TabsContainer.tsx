// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { MessageTabsPropsType } from '../ExternalMessage/MessageTabs/MessageTabs';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import { TabsPropsType } from './Tabs';

type PropsFromStateType = Pick<
  TabsPropsType,
  'canCreateContactMoment' | 'canCreateNote' | 'canCreateMessage'
> &
  Pick<
    MessageTabsPropsType,
    'canCreatePipMessage' | 'canCreateEmail' | 'canCreateMijnOverheid'
  >;

const mapStateToProps = ({
  communication: {
    context: {
      capabilities: {
        canCreateContactMoment,
        canCreateNote,
        canCreatePipMessage,
        canCreateEmail,
        canCreateMijnOverheid,
      },
    },
  },
}: CommunicationRootStateType): PropsFromStateType => ({
  canCreateContactMoment,
  canCreateNote,
  canCreatePipMessage,
  canCreateEmail,
  canCreateMijnOverheid,
  canCreateMessage:
    canCreatePipMessage || canCreateEmail || canCreateMijnOverheid,
});

export default connect<PropsFromStateType, {}, {}, CommunicationRootStateType>(
  mapStateToProps
);
