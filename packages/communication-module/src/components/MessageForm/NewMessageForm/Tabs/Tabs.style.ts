// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useTabsStyle = makeStyles(
  ({ typography, palette: { primary }, mintlab: { greyscale } }: any) => ({
    tabsRoot: {
      marginBottom: '36px',
      minHeight: '60px',
    },

    tabsFlexContainer: {
      '&>:nth-child(2)': {
        margin: '0px 20px',
      },
    },
    selected: {},
    tabRoot: {
      flexBasis: 'auto',
    },
    tabWrapper: {
      flexDirection: 'row',
      display: 'inline-flex',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: '18px',
      padding: '3px 20px',
      backgroundColor: 'transparent',
      color: greyscale.darkest,
      '$selected &': {
        color: primary.main,
        backgroundColor: primary.lightest,
      },
      ...typography.subtitle1,
      textTransform: 'lowercase',
      flexBasis: 'auto',
      '&&& > span': {
        margin: 0,
        marginRight: '12px',
      },
    },
    tabIndicator: {
      display: 'none',
    },
  })
);
