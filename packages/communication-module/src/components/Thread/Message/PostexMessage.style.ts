// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const usePostexMessageStyle = makeStyles({
  panel: {
    padding: 0,
    boxShadow: 'none',
    '&:before': {
      height: 0,
    },
    marginBottom: 20,
  },
  summary: { padding: 0 },
  content: {
    margin: 0,
    display: 'inline',
  },
  expanded: {
    '&$expanded': {
      margin: 0,
      minHeight: 48,
    },
  },
  details: { padding: 0, display: 'inline' },
});
