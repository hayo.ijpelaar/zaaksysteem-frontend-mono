// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useConfirmDialog } from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import ActionMenu, {
  ActionGroupsType,
} from '../../../shared/ActionMenu/ActionMenu';
import {
  addSourceFileToCase,
  deleteMessage,
  markMessageAsUnread,
} from '../../../../store/thread/communication.thread.actions';
import { isExternalMessageSelector } from '../../../../store/selectors/isExternalMessageSelector';
import { isMessageUnreadSelector } from '../../../../store/selectors/isMessageUnreadSelector';
import { messageSubtypeSelector } from '../../../../store/selectors/messageSubtypeSelector';
import { canAddSourceFileToCaseSelector } from '../../../../store/selectors/canAddSourceFileToCaseSelector';
import { canDeleteMessageSelector } from '../../../../store/selectors/canDeleteMessageSelector';

/* eslint complexity: [2, 7]*/
export const MessageActions: React.ComponentType<{
  id: string;
  hasSourceFile?: boolean;
}> = ({ id, hasSourceFile }) => {
  const [t] = useTranslation('communication');
  const dispatch = useDispatch();
  const [confirmDeleteDialog, confirmDelete] = useConfirmDialog(
    t('thread.deleteMessage.title'),
    t('thread.deleteMessage.body'),
    () => dispatch(deleteMessage(id))
  );

  const isExternalMessageSelectorMemo = React.useMemo(
    () => isExternalMessageSelector(id),
    [id]
  );
  const isMessageUnreadSelectorMemo = React.useMemo(
    () => isMessageUnreadSelector(id),
    [id]
  );
  const messageSubtypeSelectorMemo = React.useMemo(
    () => messageSubtypeSelector(id),
    [id]
  );

  const isExternalMessage = useSelector(isExternalMessageSelectorMemo);
  const messageSubtype = useSelector(messageSubtypeSelectorMemo);
  const isUnread = useSelector(isMessageUnreadSelectorMemo);
  const canAddSourceFile = useSelector(canAddSourceFileToCaseSelector);
  const canDeleteMessage = useSelector(canDeleteMessageSelector);

  const mainActionGroup = [
    {
      label: t('thread.messageActions.delete'),
      action: confirmDelete,
      condition: canDeleteMessage && messageSubtype !== 'pip',
    },
    {
      label: t('thread.messageActions.addSourceFileToCaseOriginal'),
      action: () =>
        dispatch(addSourceFileToCase(id, messageSubtype, 'original')),
      condition: isExternalMessage && canAddSourceFile && hasSourceFile,
    },
    {
      label: t('thread.messageActions.addSourceFileToCasePDF'),
      action: () => dispatch(addSourceFileToCase(id, messageSubtype, 'pdf')),
      condition: isExternalMessage && canAddSourceFile,
    },
    {
      label: t('thread.messageActions.markAsUnread'),
      action: () => dispatch(markMessageAsUnread({ messageUuids: [id] })),
      condition: isExternalMessage && !isUnread,
    },
  ];
  const actions: ActionGroupsType = [mainActionGroup];

  return mainActionGroup.length > 0 ? (
    <React.Fragment>
      <ActionMenu actions={actions} scope={'thread:message:header'} />
      {confirmDeleteDialog}
    </React.Fragment>
  ) : null;
};
