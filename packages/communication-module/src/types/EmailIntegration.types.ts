// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type ImageType = {
  uuid: string;
};

export type EmailTemplateSettingsType = {
  label: string;
  template: string;
  image: ImageType[];
};

export type EmailIntegration = {
  instance: {
    interface_config: { rich_email_templates: EmailTemplateSettingsType[] };
  };
};

export type GetEmailIntegrationResponseBody = {
  result: {
    instance: { rows: EmailIntegration[] };
  };
};

export type EmailTemplateDataType = {
  label: string;
  template: string;
  imageUuid: string;
};
