// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';

export interface ThreadType {
  id: string;
  type: 'note' | 'contact_moment' | 'email' | 'pip_message' | 'postex';
  summary: string;
  date: Date;
  tag: string;
  numberOfMessages: number;
  caseNumber?: number;
  lastMessage: AnyLastMessageType;
  unreadPip: boolean;
  unreadEmployee: boolean;
  hasAttachment: boolean;
}

export interface LastMessageType {
  createdByName: string;
}
export interface LastMessageContactMomentType extends LastMessageType {
  type: 'contact_moment';
  direction: string;
  channel: string;
  withName: string;
}

export interface LastMessagePipMessageType extends LastMessageType {
  type: 'pip_message';
  subject: string;
}

export interface LastMessageEmailType extends LastMessageType {
  type: 'email';
  subject: string;
  failureReason?: string;
}

export interface LastMessageNoteType extends LastMessageType {
  type: 'note';
}

export interface LastMessagePostexType extends LastMessageType {
  type: 'postex';
  subject: string;
  failureReason?: string;
}

export type AddThreadToCaseValuesType = Pick<
  APICommunication.LinkThreadToCaseRequestBody,
  'case_uuid'
>;

export type AnyLastMessageType =
  | LastMessageContactMomentType
  | LastMessagePipMessageType
  | LastMessageEmailType
  | LastMessagePostexType
  | LastMessageNoteType;
