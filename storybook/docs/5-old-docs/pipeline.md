# Pipeline

> The pipeline is quite verbose and includes the following tasks

- test job
  - lint the source code
  - test the source code
- build job
  - install dependencies
  - build the package distribution
- publish job
  - publish the package distribution on the npm registry
- pages job
  - build the package distribution
    - this produces the webpack visualizer artefact
  - run tests with coverage
    - this produces the coverage artefacts
  - build the ESDoc documentation
  - build the static storybook distribution
  - generate the cover page
  - deploy the artefacts at
    https://zaaksysteem.gitlab.io/npm-mintlab-ui/

## The Pipeline ain't DRY

The performance of the pages job could be improved
by delegating artefact production for test coverage and
webpack visualizer to the test and build jobs and using
their pipeline artefacts, respectively.

That's not worth it, however, because then you'd have a
hard time
[producing the artefacts locally](./gitlab-pages.html)
without adding significanly more complexity.

Also beware a dry pipeline foreboding a dead business.

:-)
