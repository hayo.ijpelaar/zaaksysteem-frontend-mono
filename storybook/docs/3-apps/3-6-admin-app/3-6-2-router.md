# Admin: Routing

## 🔑

> Global routing is implemented as a simple component
> resolution mechanism based on state changes.

## State

The inital request URI reference is saved in the Redux store.
When the `invoke` action from `src/store/route/route.actions` is dispatched, the store's
`src/store/route/route.middleware` middleware

- calls `history.pushState` to add the payload URI reference on
  top of the history stack and update the browser's URL bar
- dispatches the `resolve` action to save that
  new value to the store

## Resolution

Component resolution is based on the second _segment_
of the _path component_ saved in the store.

The resolver uses the dictionary at
`/src/routes.js`

to resolve the route component with three possible outcomes:

1. If the dictionary does not have a property that
   matches the segment, the generic `ErrorNotFound`
   component is used.
2. If the value of that property is a string, the generic
   `InlineFrame`
   component is used.
3. Otherwise, the value must be a React component
   that implements the appropriate response handler.

### Example

Request **URI reference**: `https://example.org/foo/bar/quux`

Store **Path component**: `/foo/bar/quux`

1. The segment `foo` is configured by the web server as the
   SPA entry point so that all matching requests are resolved
   with `/foo/index.html`.
2. The segment `bar` is used by the client-side application
   loaded by `/foo/index.html` to select a component that
   handles the request.
3. The segment `quux` is passed as a parameter to the component
   that handles `bar`. It is entirely up to that component what
   to do with `quux`.
