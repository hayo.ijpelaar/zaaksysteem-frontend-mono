const cp = require('child_process');
const checker = require('license-checker');

cp.exec('git diff master -- yarn.lock', (err, stdout) => {
  console.log(stdout);
  if (stdout) {
    console.log('Dependencies change detected, checking licences...');
    checker.init({ start: process.cwd() }, function (err, packages) {
      if (err) {
        throw 'Cannot access dependency licenses';
      } else {
        const agpl = Object.values(packages).find(pckg =>
          pckg.licenses.includes('agpl')
        );

        if (agpl) {
          throw 'AGPL license detected';
        }
      }
    });
  }
});
